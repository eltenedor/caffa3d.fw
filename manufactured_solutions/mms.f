C THIS FILE CONTAINS ALL AVAILABLE MANUFACTURED SOLUTIONS
C THE MANUFACTURED SOLUTION IS CHOSEN AT COMPILE TIME BY COMBINING
C THE PREPROCESSOR FLAG -DUSE_ANALYTICAL WITH THE FOLLOWING COMBINATIONS
C OF PREPROCESSOR FLAGS
C     
C################################################################
C               VELOCITIES
C################################################################
      FUNCTION UMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 UMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_DEFAULT )
        UMMS=0.5D0
     *      *DSIN(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DCOS(LPI*Z) 
#elif defined( MMS_TAYLORGREEN )
        UMMS=-DCOS(2.D0*LPI*X)*DSIN(2.D0*LPI*Y)
#elif defined( MMS_DEFAULTBOUSS)
        UMMS=0.5D0
     *      *DSIN(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DCOS(LPI*Z) 
#elif defined( MMS_TEST)
        UMMS=0.5D0
     *      *DSIN(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DCOS(LPI*Z) 
#elif defined( MMS_OWN )
        UMMS=DCOS(X*Y*Z)*X*Z + DSIN(X*Y*Z)*X*Y
#elif defined( MMS_OWNBOUSS ) || defined( MMS_SIMPLERBOUSS )
        UMMS=0.2D1*DCOS(X**2+Y**2+Z**2)*Y+0.2D1*DSIN(X**2+Y**2+Z**2)*Z
#elif defined( MMS_BOUNDED ) || defined( MMS_BOUNDARY)
        UMMS=0.5D0
     *      *DSIN(X)
     *      *DCOS(Y)
     *      *DCOS(Z) 
#elif defined( MMS_VEDOVOTO )
        UMMS=DSIN(2.0D0*LPI*(X+Y+Z))**2
#elif defined( MMS_DEFAULTEULER )
        UMMS=0.5D0
     *      *DSIN(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DCOS(LPI*Z) 
#elif defined( MMS_DEFAULTVISC )
        UMMS=0.5D0
     *      *DSIN(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DCOS(LPI*Z) 
#else
        UMMS=0.0D0
#endif

      END FUNCTION UMMS

C################################################################
      FUNCTION VMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 VMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_DEFAULT )
        VMMS=0.5D0
     *      *DCOS(LPI*X)
     *      *DSIN(LPI*Y)
     *      *DCOS(LPI*Z)
#elif defined( MMS_TAYLORGREEN )
        VMMS=DSIN(2.D0*LPI*X)*COS(2.D0*LPI*Y)
#elif defined( MMS_DEFAULTBOUSS )
        VMMS=0.5D0
     *      *DCOS(LPI*X)
     *      *DSIN(LPI*Y)
     *      *DCOS(LPI*Z)
#elif defined( MMS_TEST)
        VMMS=0.5D0
     *      *DCOS(LPI*X)
     *      *DSIN(LPI*Y)
     *      *DCOS(LPI*Z)
#elif defined( MMS_OWN )
        VMMS=DCOS(X*Y*Z)*X*Y - DCOS(X*Y*Z)*Y*Z
#elif defined( MMS_OWNBOUSS ) || defined( MMS_SIMPLERBOUSS )
        VMMS=0.2D1*DCOS(X**2+Y**2+Z**2)*Z-0.2D1*DCOS(X**2+Y**2+Z**2)*X
#elif defined( MMS_BOUNDED ) || defined( MMS_BOUNDARY )
        VMMS=0.5D0
     *      *DCOS(X)
     *      *DSIN(Y)
     *      *DCOS(Z)
#elif defined( MMS_VEDOVOTO )
        VMMS=DCOS(2.0D0*LPI*(X+Y+Z))**2
#elif defined( MMS_DEFAULTEULER )
        VMMS=0.5D0
     *      *DCOS(LPI*X)
     *      *DSIN(LPI*Y)
     *      *DCOS(LPI*Z)
#elif defined( MMS_DEFAULTVISC )
        VMMS=0.5D0
     *      *DCOS(LPI*X)
     *      *DSIN(LPI*Y)
     *      *DCOS(LPI*Z)
#else
        VMMS=0.0D0
#endif

      END FUNCTION VMMS

C################################################################
      FUNCTION WMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 WMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_DEFAULT )
        WMMS=-1.0D0
     *      *DCOS(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DSIN(LPI*Z)
#elif defined( MMS_DEFAULTBOUSS )
        WMMS=-1.0D0
     *      *DCOS(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DSIN(LPI*Z)
#elif defined( MMS_TEST )
        WMMS=-1.0D0
     *      *DCOS(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DSIN(LPI*Z)
#elif defined( MMS_OWN )
        WMMS=-DSIN(X*Y*Z)*Y*Z - DCOS(X*Y*Z)*X*Z
#elif defined( MMS_OWNBOUSS )|| defined( MMS_SIMPLERBOUSS )
        WMMS=-0.2D1*DSIN(X**2+Y**2+Z**2)*X-0.2D1*DCOS(X**2+Y**2+Z**2)*Y
#elif defined( MMS_BOUNDED ) || defined( MMS_BOUNDARY )
        WMMS=-1.0D0
     *      *DCOS(X)
     *      *DCOS(Y)
     *      *DSIN(Z)
#elif defined( MMS_VEDOVOTO )
        WMMS=1.0D0
#elif defined( MMS_DEFAULTEULER )
        WMMS=-1.0D0
     *      *DCOS(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DSIN(LPI*Z)
#elif defined( MMS_DEFAULTVISC )
        WMMS=-1.0D0
     *      *DCOS(LPI*X)
     *      *DCOS(LPI*Y)
     *      *DSIN(LPI*Z)
#else
        WMMS=0.0D0
#endif

      END FUNCTION WMMS

C################################################################
      FUNCTION SUMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 SUMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_DEFAULT )
        SUMMS=
     *         +1.0D0/64.0D0*LPI*(
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%DIFFUSION TERM
     *                            +96.0D0*DSIN(LPI*X)*LPI*DCOS(LPI*Y)*DCOS(LPI*Z)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CONVECTION TERM
     *                            +32.0D0*DCOS(LPI*X)*DCOS(LPI*Y)**2*DSIN(LPI*X)
     *                            -16.0D0*DCOS(LPI*X)*DCOS(LPI*Z)**2*DSIN(LPI*X)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PRESSURE GRADIENT
     *                            -8.0D0*LPI*DCOS(X**2+Y**2+Z**2)**2*X
     *                            -LPI**2*DEXP(1.0D0/2.0D0*LPI*X)
     *                            +4.0D0*LPI*X
     *                           )
#elif defined( MMS_TAYLORGREEN )
       SUMMS=(-8.D0*LPI**2*DCOS(2.0D0*LPI*X)*DSIN(2.0D0*LPI*Y))
#elif defined( MMS_DEFAULTBOUSS )
       SUMMS=DCOS(LPI*X)*DCOS(LPI*Y)**2*DSIN(LPI*X)*LPI/0.2D1-DCOS(LPI*X)*DCOS(LPI*Z)**2*DSIN(LPI*X)*LPI/0.4D1+0.3D1/0.2D1*DSIN(LPI*X)*LPI**2*DCOS(LPI*Y)*DCOS(LPI*Z)-LPI**2*DCOS(X**2+Y**2+Z**2)**2*X/0.8D1+LPI**2*X/0.16D2-LPI**3*DEXP(LPI*X/0.2D1)/0.64D2+0.21D2*DCOS(LPI*Y)+0.14D2*DSIN(LPI*X**3)+0.35D2*DSIN(LPI*Z**5)
#elif defined( MMS_TEST)
       SUMMS=DCOS(LPI*X)*DCOS(LPI*Y)**2*DSIN(LPI*X)*LPI/0.2D1-DCOS(LPI*X)*DCOS(LPI*Z)**2*DSIN(LPI*X)*LPI/0.4D1+0.3D1/0.2D1*DSIN(LPI*X)*LPI**2*DCOS(LPI*Y)*DCOS(LPI*Z)+0.7D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)+0.4D1*DCOS(X**2+Y**2+Z**2)**2*X-0.2D1*X
#elif defined( MMS_OWN )
       SUMMS=DCOS(X*Y*Z)*X**3*Y**2*Z+DCOS(X*Y*Z)*X**3*Z**3
     * +DCOS(X*Y*Z)*Y**2*Z**3*X+DSIN(X*Y*Z)*X**3*Y**3
     * +DSIN(X*Y*Z)*X**3*Z**2*Y+DSIN(X*Y*Z)*Y**3*Z**2*X
     * -DCOS(X*Y*Z)**2*X**2*Z-DCOS(X*Y*Z)**2*X*Y**2
     * +DCOS(X*Y*Z)**2*X*Z**2+DCOS(X*Y*Z)*DSIN(X*Y*Z)*X**2*Y
     * -0.2D1*DCOS(X*Y*Z)*X**2*Z-0.2D1*DCOS(X*Y*Z)*Y**2*Z
     * +0.2D1*DSIN(X*Y*Z)*X**2*Y+0.2D1*DSIN(X*Y*Z)*Y*Z**2
     * +0.4D1*DCOS(X**2+Y**2+Z**2)**2*X+X*Y**2-0.2D1*X
#elif defined( MMS_OWNBOUSS )
       SUMMS=0.8D1*DCOS(X**2+Y**2+Z**2)*X**2*Y
     *      +0.8D1*DCOS(X**2+Y**2+Z**2)*Y**3
     *      +0.8D1*DCOS(X**2+Y**2+Z**2)*Z**2*Y
     *      +0.8D1*DSIN(X**2+Y**2+Z**2)*X**2*Z
     *      +0.8D1*DSIN(X**2+Y**2+Z**2)*Y**2*Z
     *      +0.8D1*DSIN(X**2+Y**2+Z**2)*Z**3
     *      +0.7D1*DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)
     *      +0.4D1*DCOS(X**2+Y**2+Z**2)**2*X
     *      +0.4D1*DCOS(X**2+Y**2+Z**2)**2*Z
     *      -0.4D1*DCOS(X**2+Y**2+Z**2)*DSIN(X**2+Y**2+Z**2)*Y
     *      -0.20D2*DCOS(X**2+Y**2+Z**2)*Z
     *      +0.20D2*DSIN(X**2+Y**2+Z**2)*Y
     *      -0.6D1*X
#elif defined( MMS_SIMPLERBOUSS )
       SUMMS=0.2D1*X*DCOS(X**2)*DCOS(Y**2)*DSIN(Z**2)+0.8D1*DCOS(X**2+Y**2+Z**2)*X**2*Y+0.8D1*DCOS(X**2+Y**2+Z**2)*Y**3+0.8D1*DCOS(X**2+Y**2+Z**2)*Z**2*Y+0.8D1*DSIN(X**2+Y**2+Z**2)*X**2*Z+0.8D1*DSIN(X**2+Y**2+Z**2)*Y**2*Z+0.8D1*DSIN(X**2+Y**2+Z**2)*Z**3+0.7D1*DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)+0.4D1*DCOS(X**2+Y**2+Z**2)**2*Z-0.4D1*DCOS(X**2+Y**2+Z**2)*DSIN(X**2+Y**2+Z**2)*Y-0.20D2*DCOS(X**2+Y**2+Z**2)*Z+0.20D2*DSIN(X**2+Y**2+Z**2)*Y-0.4D1*X
#elif defined( MMS_BOUNDED )
       SUMMS=DCOS(X)*DCOS(Y)**2*DSIN(X)/0.2D1-DCOS(X)*DCOS(Z)**2*DSIN(X)/0.4D1+0.3D1/0.2D1*DSIN(X)*DCOS(Y)*DCOS(Z)+0.2D1*DCOS(X+Y+Z)**2-0.1D1
#elif defined( MMS_BOUNDARY )
       SUMMS=DCOS(X)*DCOS(Y)**2*DSIN(X)/0.2D1-DCOS(X)*DCOS(Z)**2*DSIN(X)/0.4D1+0.3D1/0.2D1*DSIN(X)*DCOS(Y)*DCOS(Z)+DCOS(LPI*X/0.2D1+LPI*Y/0.2D1+LPI*Z/0.2D1)
#elif defined( MMS_VEDOVOTO )
       SUMMS=0.2D1*LPI*(-0.24D2*DCOS(0.2D1*LPI*(X+Y+Z))**2*LPI+0.4D1*DSIN(0.2D1*LPI*(X+Y+Z))*DCOS(0.2D1*LPI*(X+Y+Z))-DSIN(0.2D1*LPI*(X+Y+Z))+0.12D2*LPI)
#elif defined( MMS_DEFAULTEULER )
       SUMMS=LPI*(0.32D2*DCOS(LPI*X)*DCOS(LPI*Y)**2*DSIN(LPI*X)-0.16D2*DCOS(LPI*X)*DCOS(LPI*Z)**2*DSIN(LPI*X)-0.8D1*LPI*DCOS(X**2+Y**2+Z**2)**2*X-LPI**2*DEXP(LPI*X/0.2D1)+0.4D1*LPI*X)/0.64D2
#elif defined( MMS_DEFAULTVISC )
       SUMMS=LPI*(0.4000D4*DCOS(LPI*X)*DCOS(LPI*Y)**2*DSIN(LPI*X)+0.12D2*DSIN(LPI*X)*LPI*DCOS(LPI*Y)*DCOS(LPI*Z)-0.2000D4*DCOS(LPI*X)*DCOS(LPI*Z)**2*DSIN(LPI*X)-0.1000D4*LPI*DCOS(X**2+Y**2+Z**2)**2*X-0.125D3*LPI**2*DEXP(LPI*X/0.2D1)+0.500D3*LPI*X)/0.8000D4
#else
       SUMMS=0.0D0
#endif
C
      END FUNCTION SUMMS

C################################################################
      FUNCTION SVMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 SVMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_DEFAULT )
        SVMMS=
     *         +1.0D0/64.0D0*LPI*(
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%DIFFUSION TERM
     *                            +96.0D0*DCOS(LPI*X)*LPI*DSIN(LPI*Y)*DCOS(LPI*Z)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CONVECTION TERM
     *                            +32.0D0*DCOS(LPI*X)**2*DCOS(LPI*Y)*DSIN(LPI*Y)
     *                            -16.0D0*DCOS(LPI*Y)*DCOS(LPI*Z)**2*DSIN(LPI*Y)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PRESSURE GRADIENT
     *                            -8.0D0*LPI*DCOS(X**2+Y**2+Z**2)**2*Y
     *                            -LPI**2*DEXP(1.0D0/2.0D0*LPI*Y)
     *                            +4.0D0*LPI*Y
     *                           )
C
#elif defined( MMS_TAYLORGREEN )
        SVMMS=(8.d0*lpi**2*dsin(2*lpi*x)*dcos(2.0D0*lpi*y))
#elif defined( MMS_DEFAULTBOUSS )
        SVMMS=-DCOS(LPI*Y)*DCOS(LPI*Z)**2*DSIN(LPI*Y)*LPI/0.4D1+DCOS(LPI*X)**2*DCOS(LPI*Y)*DSIN(LPI*Y)*LPI/0.2D1+0.3D1/0.2D1*DCOS(LPI*X)*LPI**2*DSIN(LPI*Y)*DCOS(LPI*Z)-LPI**2*DCOS(X**2+Y**2+Z**2)**2*Y/0.8D1+LPI**2*Y/0.16D2-LPI**3*DEXP(LPI*Y/0.2D1)/0.64D2+0.39D2*DCOS(LPI*Y)+0.26D2*DSIN(LPI*X**3)+0.65D2*DSIN(LPI*Z**5)
#elif defined( MMS_TEST )
        SVMMS=-DCOS(LPI*Y)*DCOS(LPI*Z)**2*DSIN(LPI*Y)*LPI/0.4D1+DCOS(LPI*X)**2*DCOS(LPI*Y)*DSIN(LPI*Y)*LPI/0.2D1+0.3D1/0.2D1*DCOS(LPI*X)*LPI**2*DSIN(LPI*Y)*DCOS(LPI*Z)+0.13D2*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)+0.4D1*DCOS(X**2+Y**2+Z**2)**2*Y-0.2D1*Y
#elif defined( MMS_OWN )
        SVMMS=DCOS(X*Y*Z)*X**3*Y**3+DCOS(X*Y*Z)*X**3*Z**2*Y
     *  -DCOS(X*Y*Z)*X**2*Y**3*Z-DCOS(X*Y*Z)*X**2*Z**3*Y
     *  +DCOS(X*Y*Z)*Y**3*Z**2*X-DCOS(X*Y*Z)*Y**3*Z**3
     *  +DCOS(X*Y*Z)**2*X**2*Y+DCOS(X*Y*Z)**2*Y*Z**2
     *  +DCOS(X*Y*Z)*DSIN(X*Y*Z)*X*Y**2+DCOS(X*Y*Z)*DSIN(X*Y*Z)*Y**2*Z
     *  +0.2D1*DSIN(X*Y*Z)*X**2*Z-0.2D1*DSIN(X*Y*Z)*Y**2*X
     *  -0.2D1*DSIN(X*Y*Z)*Z**2*X+0.2D1*DSIN(X*Y*Z)*Y**2*Z
     *  +0.4D1*DCOS(X**2+Y**2+Z**2)**2*Y-0.2D1*Y
#elif defined( MMS_OWNBOUSS )
        SVMMS=-0.8D1*DCOS(X**2+Y**2+Z**2)*X**3
     *        +0.8D1*DCOS(X**2+Y**2+Z**2)*X**2*Z
     *        -0.8D1*DCOS(X**2+Y**2+Z**2)*Y**2*X
     *        -0.8D1*DCOS(X**2+Y**2+Z**2)*Z**2*X
     *        +0.8D1*DCOS(X**2+Y**2+Z**2)*Y**2*Z
     *        +0.8D1*DCOS(X**2+Y**2+Z**2)*Z**3
     *        +0.13D2*DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)
     *        -0.4D1*DCOS(X**2+Y**2+Z**2)**2*Y
     *        -0.4D1*DCOS(X**2+Y**2+Z**2)*DSIN(X**2+Y**2+Z**2)*X
     *        -0.4D1*DCOS(X**2+Y**2+Z**2)*DSIN(X**2+Y**2+Z**2)*Z
     *        -0.20D2*DSIN(X**2+Y**2+Z**2)*X
     *        +0.20D2*DSIN(X**2+Y**2+Z**2)*Z
     *        -0.2D1*Y
#elif defined( MMS_SIMPLERBOUSS )
        SVMMS=-0.2D1*DSIN(X**2)*DSIN(Y**2)*Y*DSIN(Z**2)-0.8D1*DCOS(X**2+Y**2+Z**2)*X**3+0.8D1*DCOS(X**2+Y**2+Z**2)*X**2*Z-0.8D1*DCOS(X**2+Y**2+Z**2)*Y**2*X-0.8D1*DCOS(X**2+Y**2+Z**2)*Z**2*X+0.8D1*DCOS(X**2+Y**2+Z**2)*Y**2*Z+0.8D1*DCOS(X**2+Y**2+Z**2)*Z**3+0.13D2*DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)-0.8D1*DCOS(X**2+Y**2+Z**2)**2*Y-0.4D1*DCOS(X**2+Y**2+Z**2)*DSIN(X**2+Y**2+Z**2)*X-0.4D1*DCOS(X**2+Y**2+Z**2)*DSIN(X**2+Y**2+Z**2)*Z-0.20D2*DSIN(X**2+Y**2+Z**2)*X+0.20D2*DSIN(X**2+Y**2+Z**2)*Z
#elif defined( MMS_BOUNDED )
        SVMMS=-DCOS(Y)*DCOS(Z)**2*DSIN(Y)/0.4D1+DCOS(X)**2*DCOS(Y)*DSIN(Y)/0.2D1+0.3D1/0.2D1*DCOS(X)*DSIN(Y)*DCOS(Z)+0.2D1*DCOS(X+Y+Z)**2-0.1D1
#elif defined( MMS_BOUNDARY )
        SVMMS=DCOS(X)**2*DCOS(Y)*DSIN(Y)/0.2D1-DCOS(Y)*DCOS(Z)**2*DSIN(Y)/0.4D1+0.3D1/0.2D1*DCOS(X)*DSIN(Y)*DCOS(Z)+DCOS(LPI*X/0.2D1+LPI*Y/0.2D1+LPI*Z/0.2D1)
#elif defined( MMS_VEDOVOTO )
        SVMMS=-0.2D1*LPI*(-0.24D2*DCOS(0.2D1*LPI*(X+Y+Z))**2*LPI+0.4D1*DSIN(0.2D1*LPI*(X+Y+Z))*DCOS(0.2D1*LPI*(X+Y+Z))+DSIN(0.2D1*LPI*(X+Y+Z))+0.12D2*LPI)
#elif defined( MMS_DEFAULTEULER )
        SVMMS=-LPI*(0.16D2*DCOS(LPI*Y)*DCOS(LPI*Z)**2*DSIN(LPI*Y)-0.32D2*DCOS(LPI*X)**2*DCOS(LPI*Y)*DSIN(LPI*Y)+0.8D1*LPI*DCOS(X**2+Y**2+Z**2)**2*Y+LPI**2*DEXP(LPI*Y/0.2D1)-0.4D1*LPI*Y)/0.64D2
#elif defined( MMS_DEFAULTVISC )
        SVMMS=-LPI*(0.2000D4*DCOS(LPI*Y)*DCOS(LPI*Z)**2*DSIN(LPI*Y)-0.4000D4*DCOS(LPI*X)**2*DCOS(LPI*Y)*DSIN(LPI*Y)-0.12D2*DCOS(LPI*X)*LPI*DSIN(LPI*Y)*DCOS(LPI*Z)+0.1000D4*LPI*DCOS(X**2+Y**2+Z**2)**2*Y+0.125D3*LPI**2*DEXP(LPI*Y/0.2D1)-0.500D3*LPI*Y)/0.8000D4
#else
        SVMMS=0.0D0
#endif

      END FUNCTION SVMMS

C################################################################
      FUNCTION SWMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 SWMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_DEFAULT )
        SWMMS=
     *         +1.0D0/64.0D0*LPI*(
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%DIFFUSION TERM
     *                            -192.0D0*DCOS(LPI*X)*LPI*DCOS(LPI*Y)*DSIN(LPI*Z)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CONVECTION TERM
     *                            +32.0D0*DCOS(LPI*Y)**2*DCOS(LPI*Z)*DSIN(LPI*Z)
     *                            +32.0D0*DCOS(LPI*X)**2*DCOS(LPI*Z)*DSIN(LPI*Z)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PRESSURE GRADIENT
     *                            -8.0D0*LPI*DCOS(X**2+Y**2+Z**2)**2*Z
     *                            -LPI**2*DEXP(1.0D0/2.0D0*LPI*Z)
     *                            +4.0D0*LPI*Z
     *                           )
#elif defined( MMS_DEFAULTBOUSS )
        SWMMS=-DCOS(LPI*Y)*DCOS(LPI*Z)**2*DSIN(LPI*Y)*LPI/0.4D1+DCOS(LPI*X)**2*DCOS(LPI*Y)*DSIN(LPI*Y)*LPI/0.2D1+0.3D1/0.2D1*DCOS(LPI*X)*LPI**2*DSIN(LPI*Y)*DCOS(LPI*Z)-LPI**2*DCOS(X**2+Y**2+Z**2)**2*Y/0.8D1+LPI**2*Y/0.16D2-LPI**3*DEXP(LPI*Y/0.2D1)/0.64D2+0.39D2*DCOS(LPI*Y)+0.26D2*DSIN(LPI*X**3)+0.65D2*DSIN(LPI*Z**5)
#elif defined( MMS_TEST )
        SWMMS=DCOS(LPI*Y)**2*DCOS(LPI*Z)*LPI*DSIN(LPI*Z)/0.2D1+DCOS(LPI*X)**2*DCOS(LPI*Z)*LPI*DSIN(LPI*Z)/0.2D1-0.3D1*DCOS(LPI*X)*LPI**2*DCOS(LPI*Y)*DSIN(LPI*Z)-0.19D2*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)+0.4D1*DCOS(X**2+Y**2+Z**2)**2*Z-0.2D1*Z
#elif defined( MMS_OWN )
        SWMMS=-DCOS(X*Y*Z)*X**3*Y**2*Z-DCOS(X*Y*Z)*X**3*Z**3
     *  -DCOS(X*Y*Z)*Y**2*Z**3*X-DSIN(X*Y*Z)*X**2*Y**3*Z
     *  -DSIN(X*Y*Z)*X**2*Z**3*Y-DSIN(X*Y*Z)*Y**3*Z**3
     *  +DCOS(X*Y*Z)**2*X**2*Z-DCOS(X*Y*Z)**2*X*Z**2
     *  -DCOS(X*Y*Z)**2*Y**2*Z+DCOS(X*Y*Z)*DSIN(X*Y*Z)*Y*Z**2
     *  +0.2D1*DCOS(X*Y*Z)*Y**2*X+0.2D1*DCOS(X*Y*Z)*X*Z**2
     *  -0.2D1*DSIN(X*Y*Z)*X**2*Y-0.2D1*DSIN(X*Y*Z)*Y*Z**2
     *  +0.4D1*DCOS(X**2+Y**2+Z**2)**2*Z+Y**2*Z-0.2D1*Z
#elif defined( MMS_OWNBOUSS )
        SWMMS=-0.8D1*DCOS(X**2+Y**2+Z**2)*X**2*Y
     *        -0.8D1*DCOS(X**2+Y**2+Z**2)*Y**3
     *        -0.8D1*DCOS(X**2+Y**2+Z**2)*Z**2*Y
     *        -0.8D1*DSIN(X**2+Y**2+Z**2)*X**3
     *        -0.8D1*DSIN(X**2+Y**2+Z**2)*Y**2*X
     *        -0.8D1*DSIN(X**2+Y**2+Z**2)*Z**2*X
     *        -0.19D2*DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)
     *        +0.4D1*DCOS(X**2+Y**2+Z**2)**2*X
     *        +0.4D1*DCOS(X**2+Y**2+Z**2)**2*Z
     *        -0.4D1*DCOS(X**2+Y**2+Z**2)*DSIN(X**2+Y**2+Z**2)*Y
     *        +0.20D2*DCOS(X**2+Y**2+Z**2)*X
     *        -0.20D2*DSIN(X**2+Y**2+Z**2)*Y-0.6D1*Z
#elif defined( MMS_SIMPLERBOUSS )
        SWMMS=0.2D1*Z*DSIN(X**2)*DCOS(Y**2)*DCOS(Z**2)-0.8D1*DCOS(X**2+Y**2+Z**2)*X**2*Y-0.8D1*DCOS(X**2+Y**2+Z**2)*Y**3-0.8D1*DCOS(X**2+Y**2+Z**2)*Z**2*Y-0.8D1*DSIN(X**2+Y**2+Z**2)*X**3-0.8D1*DSIN(X**2+Y**2+Z**2)*Y**2*X-0.8D1*DSIN(X**2+Y**2+Z**2)*Z**2*X-0.19D2*DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)+0.4D1*DCOS(X**2+Y**2+Z**2)**2*X-0.4D1*DCOS(X**2+Y**2+Z**2)*DSIN(X**2+Y**2+Z**2)*Y+0.20D2*DCOS(X**2+Y**2+Z**2)*X-0.20D2*DSIN(X**2+Y**2+Z**2)*Y-0.4D1*Z
#elif defined( MMS_BOUNDED )
        SWMMS=DCOS(Y)**2*DCOS(Z)*DSIN(Z)/0.2D1+DCOS(X)**2*DCOS(Z)*DSIN(Z)/0.2D1-0.3D1*DCOS(X)*DCOS(Y)*DSIN(Z)+0.2D1*DCOS(X+Y+Z)**2-0.1D1
#elif defined( MMS_BOUNDARY )
        SWMMS=DCOS(X)**2*DCOS(Z)*DSIN(Z)/0.2D1+DCOS(Y)**2*DCOS(Z)*DSIN(Z)/0.2D1-0.3D1*DCOS(X)*DCOS(Y)*DSIN(Z)+DCOS(LPI*X/0.2D1+LPI*Y/0.2D1+LPI*Z/0.2D1)
#elif defined( MMS_VEDOVOTO )
        SWMMS=-0.2D1*DSIN(0.2D1*LPI*(X+Y+Z))*LPI
#elif defined( MMS_DEFAULTEULER )
        SWMMS=-LPI*(-0.32D2*DCOS(LPI*Y)**2*DCOS(LPI*Z)*DSIN(LPI*Z)-0.32D2*DCOS(LPI*X)**2*DCOS(LPI*Z)*DSIN(LPI*Z)+0.8D1*LPI*DCOS(X**2+Y**2+Z**2)**2*Z+LPI**2*DEXP(LPI*Z/0.2D1)-0.4D1*LPI*Z)/0.64D2
#elif defined( MMS_DEFAULTVISC )
        SWMMS=LPI*(0.4000D4*DCOS(LPI*Y)**2*DCOS(LPI*Z)*DSIN(LPI*Z)-0.24D2*DCOS(LPI*X)*LPI*DCOS(LPI*Y)*DSIN(LPI*Z)+0.4000D4*DCOS(LPI*X)**2*DCOS(LPI*Z)*DSIN(LPI*Z)-0.1000D4*LPI*DCOS(X**2+Y**2+Z**2)**2*Z-0.125D3*LPI**2*DEXP(LPI*Z/0.2D1)+0.500D3*LPI*Z)/0.8000D4
#else
        SWMMS=0.0D0
#endif

      END FUNCTION SWMMS


C################################################################
C               PRESSURE
C################################################################
      FUNCTION PMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 PMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_DEFAULT )
        REAL*8 TCA
        TCA=LPI*0.25D0
        PMMS=(-(TCA**2)*0.5D0)*(DEXP(2.0D0*TCA*X)
     *       +DEXP(2.0D0*TCA*Y)+DEXP(2.0D0*TCA*Z)
     *       +DSIN(X**2+Y**2+Z**2)
     *       *DCOS(X**2+Y**2+Z**2))
#elif defined( MMS_TAYLORGREEN )
        PMMS=-1.D0/4.D0*(DCOS(4.D0*LPI*X)+DCOS(4.D0*LPI*Y))
#elif defined( MMS_TEST ) || defined( MMS_OWN ) || defined( MMS_OWNBOUSS )
        PMMS=DSIN(X**2+Y**2+Z**2)*DCOS(X**2+Y**2+Z**2)
#elif defined( MMS_SIMPLERBOUSS )
        PMMS=DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)
#elif defined( MMS_BOUNDED )
        PMMS=DSIN(X+Y+Z)*DCOS(X+Y+Z)
#elif defined( MMS_BOUNDARY )
        PMMS=2.0D0/LPI*DSIN(LPI/2.0D0*(X+Y+Z))
#elif defined( MMS_VEDOVOTO )
        PMMS=DCOS(2.0D0*LPI*(X+Y+Z))
#elif defined( MMS_DEFAULTEULER )
        REAL*8 TCA
        TCA=LPI*0.25D0
        PMMS=(-(TCA**2)*0.5D0)*(DEXP(2.0D0*TCA*X)
     *       +DEXP(2.0D0*TCA*Y)+DEXP(2.0D0*TCA*Z)
     *       +DSIN(X**2+Y**2+Z**2)
     *       *DCOS(X**2+Y**2+Z**2))
#elif defined( MMS_DEFAULTVISC )
        REAL*8 TCA
        TCA=LPI*0.25D0
        PMMS=(-(TCA**2)*0.5D0)*(DEXP(2.0D0*TCA*X)
     *       +DEXP(2.0D0*TCA*Y)+DEXP(2.0D0*TCA*Z)
     *       +DSIN(X**2+Y**2+Z**2)
     *       *DCOS(X**2+Y**2+Z**2))
#else
        PMMS=0.0D0
#endif

      END FUNCTION PMMS

C################################################################
      FUNCTION SPMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 SPMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

        SPMMS=0.0D0

      END FUNCTION SPMMS


C################################################################
C               TEMPERATURE
C################################################################
      FUNCTION TMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 TMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_TDEFAULT )
        TMMS=0.2D1*DSIN(LPI*X**3)+0.3D1*DCOS(LPI*Y)+0.5D1*DSIN(LPI*Z**5)
#elif defined( MMS_TEST ) || defined( MMS_OWN )
        TMMS=DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)
#elif defined( MMS_OWNBOUSS ) || defined( MMS_SIMPLERBOUSS )
        TMMS=DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)
#else
        TMMS=0.0D0
#endif

      END FUNCTION TMMS

C################################################################
      FUNCTION STMMS(X,Y,Z)
C################################################################

        IMPLICIT NONE
        REAL*8 STMMS,X,Y,Z
        REAL*8  LPI
        PARAMETER (LPI=3.141592653589793238462643383279D0)

#if defined( MMS_TDEFAULT )
        STMMS=LPI*(0.250D3*DSIN(LPI*Z**5)*LPI*Z**8-0.50D2*DCOS(LPI*Z**5)*Z**4*DCOS(LPI*X)*DCOS(LPI*Y)*DSIN(LPI*Z)+0.6D1*DCOS(LPI*X**3)*X**2*DSIN(LPI*X)*DCOS(LPI*Y)*DCOS(LPI*Z)+0.36D2*DSIN(LPI*X**3)*LPI*X**4+0.3D1*DCOS(LPI*Y)**2*DCOS(LPI*Z)*DCOS(LPI*X)-0.200D3*DCOS(LPI*Z**5)*Z**3+0.6D1*DCOS(LPI*Y)*LPI-0.3D1*DCOS(LPI*X)*DCOS(LPI*Z)-0.24D2*DCOS(LPI*X**3)*X)/0.2D1
#elif defined( MMS_TEST ) 
        STMMS=LPI*(-0.2D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DCOS(LPI*Y)*DCOS(LPI*X)*DSIN(LPI*Z)*DCOS(LPI*Z**2)*Z-DSIN(LPI*X**2)*DSIN(LPI*Z**2)*DCOS(LPI*Z)*DCOS(LPI*X)*DSIN(LPI*Y)*DSIN(LPI*Y**2)*Y+DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*DSIN(LPI*X)*DCOS(LPI*Y)*DCOS(LPI*Z)*DCOS(LPI*X**2)*X+0.4D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*LPI*X**2+0.4D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*LPI*Y**2+0.4D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*LPI*Z**2-0.2D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DCOS(LPI*Z**2)+0.2D1*DSIN(LPI*X**2)*DSIN(LPI*Z**2)*DSIN(LPI*Y**2)-0.2D1*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*DCOS(LPI*X**2))
#elif defined( MMS_OWN )
        STMMS=0.2D1*LPI*(-DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DCOS(LPI*Z**2)
     *  *DCOS(X*Y*Z)*X*Z**2
     *  -DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DCOS(LPI*Z**2)*DSIN(X*Y*Z)*Y*Z**2
     *  -DSIN(LPI*X**2)*DSIN(LPI*Z**2)*DSIN(LPI*Y**2)*DCOS(X*Y*Z)*X*Y**2
     *  +DSIN(LPI*X**2)*DSIN(LPI*Z**2)*DSIN(LPI*Y**2)*DCOS(X*Y*Z)*Y**2*Z
     *  +DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*DCOS(LPI*X**2)*DCOS(X*Y*Z)*X**2*Z
     *  +DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*DCOS(LPI*X**2)*DSIN(X*Y*Z)*X**2*Y
     *  +0.2D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*LPI*X**2
     *  +0.2D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*LPI*Y**2
     *  +0.2D1*DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*LPI*Z**2
     *  -DSIN(LPI*X**2)*DCOS(LPI*Y**2)*DCOS(LPI*Z**2)
     *  +DSIN(LPI*X**2)*DSIN(LPI*Z**2)*DSIN(LPI*Y**2)
     *  -DCOS(LPI*Y**2)*DSIN(LPI*Z**2)*DCOS(LPI*X**2))
#elif defined( MMS_OWNBOUSS ) || defined( MMS_SIMPLERBOUSS )
        STMMS=-0.4D1*DSIN(X**2)*DCOS(Y**2)
     *              *DCOS(Z**2)*DCOS(X**2+Y**2+Z**2)*Y*Z
     *        -0.4D1*DSIN(X**2)*DCOS(Y**2)
     *              *DCOS(Z**2)*DSIN(X**2+Y**2+Z**2)*X*Z
     *        +0.4D1*DSIN(X**2)*DSIN(Z**2)
     *              *DSIN(Y**2)*DCOS(X**2+Y**2+Z**2)*X*Y
     *        -0.4D1*DSIN(X**2)*DSIN(Z**2)
     *              *DSIN(Y**2)*DCOS(X**2+Y**2+Z**2)*Y*Z
     *        +0.4D1*DCOS(Y**2)*DSIN(Z**2)
     *              *DCOS(X**2)*DCOS(X**2+Y**2+Z**2)*X*Y
     *        +0.4D1*DCOS(Y**2)*DSIN(Z**2)
     *              *DCOS(X**2)*DSIN(X**2+Y**2+Z**2)*X*Z
     *        +0.4D1*X**2*DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)
     *        +0.4D1*DSIN(X**2)*DCOS(Y**2)*Y**2*DSIN(Z**2)
     *        +0.4D1*Z**2*DSIN(X**2)*DCOS(Y**2)*DSIN(Z**2)
     *        -0.2D1*DSIN(X**2)*DCOS(Y**2)*DCOS(Z**2)
     *        +0.2D1*DSIN(X**2)*DSIN(Z**2)*DSIN(Y**2)
     *        -0.2D1*DCOS(X**2)*DCOS(Y**2)*DSIN(Z**2)
#else
        STMMS=0.0D0
#endif

      END FUNCTION STMMS

