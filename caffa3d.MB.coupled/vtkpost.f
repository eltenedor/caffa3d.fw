C###################################################################
      SUBROUTINE POST(ICOUNT,RNK)
C###################################################################
C    DESCRIPTION
C
C==============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
#include "param3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "coef3d.inc"
#include "var3d.inc"
#include "logic3d.inc"

      CHARACTER(LEN=28) VTKFILE
      INTEGER M,I,J,K,NKMT,NIMT,NJMT,KSTT,ISTT,IJK
      INTEGER ICOUNT,RNK
      REAL*8  LPI,TCA,HPA,HPA2,TA,UA,VA,WA
      REAL*8  VM,VMG,PPOG
      PARAMETER (LPI=3.141592653589793238462643383279D0)
      PetscErrorCode IERR
      Vec UVWPL
#include "petsc.user.inc"
C=========================================================
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecGetSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef  USE_ENERGYCOUPLING 
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#endif
#endif
C
#ifdef USE_ANALYTICAL
C
      CALL VecGetArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecGetArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCVEC,ZCARR,ZCCI,IERR)
      CALL VecGetArray(VOLVEC,VOLARR,VOLLI,IERR)
C
      IF (NOUTBKAL.LE.0) THEN
#ifdef USE_INTERPOLATION
C.....CALCULATE REFERENCE LOCATION AND REFERENCE PRESSURE
      IF (RANK.EQ.RMON) THEN
        CALL SETIND(MMON)
        XMON=0.D0
        YMON=0.D0
        ZMON=0.D0
C
        IJK=NI*NI*KMON+NI*IMON+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*IMON+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*(IMON+1)+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*(IMON+1)+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*IMON+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*IMON+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
C......LINEAR INTERPOLATION (EQUIDISTANT GRID ASSUMED)
C
        XMON=0.125D0*XMON
        YMON=0.125D0*YMON
        ZMON=0.125D0*ZMON
        HPA2=PMMS(XMON,YMON,ZMON)
      END IF
#elif defined( USE_MEANPRESSURE )
      HPA2=0.0D0
      VM=0.0D0
      VMG=0.0D0
      PPOG=0.0D0
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        HPA2=HPA2+PMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
        VM=VM+VOL(IJK)
      END DO
      END DO
      END DO
C
      END DO
C
      CALL MPI_REDUCE(
     *    HPA2,PPOG,1,MPI_REAL8,MPI_SUM,RMON,PETSC_COMM_WORLD,IERR)
      CALL MPI_REDUCE(
     *    VM,VMG,1,MPI_REAL8,MPI_SUM,RMON,PETSC_COMM_WORLD,IERR)
      IF (RANK.EQ.RMON) HPA2=PPOG/VMG
#else
C
C......USE MONITORING LOCATION FROM control.cin
C
      IF (RANK.EQ.RMON) THEN
        CALL SETIND(MMON)
        XMON=XC(IJKMON)
        YMON=YC(IJKMON)
        ZMON=ZC(IJKMON)
        HPA2=PMMS(XMON,YMON,ZMON)
      END IF
#endif
      CALL MPI_BCAST(HPA2,1,MPI_REAL8,RMON,PETSC_COMM_WORLD,IERR)
      ELSE
        HPA2=0.0D0
      END IF
C
      CALL VecRestoreArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCVEC,ZCARR,ZCCI,IERR)
      CALL VecRestoreArray(VOLVEC,VOLARR,VOLLI,IERR)
C
#endif
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      DO M=1,NBLKS
C
        NKMT=NKBK(M)-1
        NIMT=NIBK(M)-1
        NJMT=NJBK(M)-1
        KSTT=KBK(M)
        ISTT=IBK(M)
C
        write(VTKFILE,'(A8,I4.4,A1,I4.4,A1,I6.6,A4)') 
     *               'res_out_',RANK,'_',M,'_',ICOUNT,'.vtk'
#ifdef USE_INFO
        write(*,*), ' *** GENERATING .VTK *** '
#endif
C
        open (UNIT=22,FILE=VTKFILE,POSITION='REWIND')
        write(22,'(A)') '# vtk DataFile Version 3.0'
        write(22,'(A)') 'grid'
        write(22,'(A)') 'ASCII'
        write(22,'(A)') 'DATASET STRUCTURED_GRID'
        write(22,'(A,I6,I6,I6)') 'DIMENSIONS', NIMT,NJMT,NKMT
        write(22,'(A6,I9,A6)') 'Points', NIMT*NJMT*NKMT, ' float'
C
        do K=1,NKMT
        do J=1,NJMT
        do I=1,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            write(22,'(E20.10,1X,E20.10,1X,E20.10)'), 
     *                     X(IJK),Y(IJK),Z(IJK)
        end do
        end do
        end do
C
        write(22,'(A10,1X,I9)') 'CELL_DATA ',(NIMT-1)*(NJMT-1)*(NKMT-1)
        write(22,'(A17)') 'VECTORS UVW float'
C
        do K=2,NKMT
        do J=2,NJMT
        do I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            write(22,'(3E20.10)') U(IJK),V(IJK),W(IJK)
        end do
        end do
        end do
C
        write(22,'(A15)') 'SCALARS P float'
        write(22,'(A20)') 'LOOKUP_TABLE default'
C
        do K=2,NKMT
        do J=2,NJMT
        do I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            write(22,'(E20.10)') P(IJK)
        end do
        end do
        end do
C
        IF (LCAL(IEN).OR.LCAL(ISC)) THEN
        write(22,'(A15)') 'SCALARS T float'
        write(22,'(A20)') 'LOOKUP_TABLE default'
C
        do K=2,NKMT
        do J=2,NJMT
        do I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            write(22,'(E20.10)') T(IJK)
        end do
        end do
        end do
        ENDIF
C
#ifdef USE_ANALYTICAL
        WRITE(22,'(A17)') 'SCALARS HPA float'
        WRITE(22,'(A20)') 'LOOKUP_TABLE default'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            HPA=PMMS(XC(IJK),YC(IJK),ZC(IJK))
            WRITE(22,'(E20.10)') HPA-HPA2
        END DO
        END DO
        END DO
C
        IF (LCAL(IEN).OR.LCAL(ISC)) THEN
        WRITE(22,'(A17)') 'SCALARS TA float'
        WRITE(22,'(A20)') 'LOOKUP_TABLE default'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            TA =TMMS(XC(IJK),YC(IJK),ZC(IJK))
            WRITE(22,'(E20.10)') TA
        END DO
        END DO
        END DO
        ENDIF
#endif
C
        close(UNIT=22)
      end do
C
C.....RESTORE ARRAYS FROM VECTORS
C
#if defined( USE_SUBVEC )
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecRestoreSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef USE_ENERGYCOUPLING 
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
      RETURN
      END 
C
#include "petsc.user.inc"
