C################################################################
C     ERROR CALCULATION
C################################################################
      SUBROUTINE CALCERR
C################################################################
C     CALCULATE L2 NORM OF ERROR
C================================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "charac3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
c
      INTEGER NCASE,IJK,I,ICONT,LS,M
      INTEGER K,J
      INTEGER RCOUNTG,RCOUNT
c
      REAL*8 SOURCE,ERRORV,ERRORP,UA,VA,UVA,UVW,HPA,HPA2,
     *              ERRORVG,ERRORPG,
     *              ERRORU,ERRORUG,
     *              ERRORW,ERRORWG,
     *              ERRORA,ERRORAG,
     *              ERRORT,ERRORTG,TA
      REAL*8  LPI,TCA,TCD,WA
      REAL*8 PPOG,VMG,VM
      PARAMETER (LPI=3.141592653589793238462643383279D0)
      INTEGER PID,OMP_GET_THREAD_NUM,NTHREADS,OMP_GET_NUM_THREADS
      INTEGER IJP,IJB,II
      PetscErrorCode IERR
      PetscLogDouble CALTIME,CALTIMS
      Vec UVWPL
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C==========================================================
C
      ERRORA=0.0D0
      ERRORU=0.0D0
      ERRORV=0.0D0
      ERRORW=0.0D0
      ERRORP=0.0D0
      ERRORT=0.0D0
      ERRORAG=0.0D0
      ERRORUG=0.0D0
      ERRORVG=0.0D0
      ERRORWG=0.0D0
      ERRORPG=0.0D0
      ERRORTG=0.0D0
      RCOUNT=0
      RCOUNTG=0
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecGetSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef  USE_ENERGYCOUPLING 
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
C
      CALL VecGetArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecGetArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCVEC,ZCARR,ZCCI,IERR)
C
C.....CALCULATE ANALYTICAL PRESSURE REFERENCE
C
        HPA2=0.0D0
#if defined( USE_DIRICHLETPRESSURE )
      IF (NOUTBKAL.LE.0) THEN
#endif
#ifdef USE_INTERPOLATION
C NEW.....CALCULATE REFERENCE LOCATION AND REFERENCE PRESSURE
      IF (RANK.EQ.RMON) THEN
        CALL SETIND(MMON)
        XMON=0.D0
        YMON=0.D0
        ZMON=0.D0
C
        IJK=NI*NI*KMON+NI*IMON+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*IMON+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*(IMON+1)+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*(IMON+1)+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*IMON+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*IMON+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
C......LINEAR INTERPOLATION (EQUIDISTANT GRID ASSUMED)
C
        XMON=0.125D0*XMON
        YMON=0.125D0*YMON
        ZMON=0.125D0*ZMON
        HPA2=PMMS(XMON,YMON,ZMON)
      END IF
#elif defined( USE_MEANPRESSURE )
        HPA2=0.0D0
        VM=0.0D0
        VMG=0.0D0
        PPOG=0.0D0
        DO M=1,NBLKS
C
        CALL SETIND(M)
C
        DO K=2,NKM
        DO I=2,NIM
        DO J=2,NJM
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          HPA2=HPA2+PMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
          VM=VM+VOL(IJK)
        END DO
        END DO
        END DO
C
        END DO
        CALL MPI_REDUCE(
     *      HPA2,PPOG,1,MPI_REAL8,MPI_SUM,RMON,PETSC_COMM_WORLD,IERR)
        CALL MPI_REDUCE(
     *      VM,VMG,1,MPI_REAL8,MPI_SUM,RMON,PETSC_COMM_WORLD,IERR)
        IF (RANK.EQ.RMON) HPA2=PPOG/VMG
#else
      IF (RANK.EQ.RMON) THEN
        CALL SETIND(MMON)
        IJKMON=NI*NI*KMON+NI*IMON+JMON
        XMON=XC(IJKMON)
        YMON=YC(IJKMON)
        ZMON=ZC(IJKMON)
        HPA2=PMMS(XMON,YMON,ZMON)
        WRITE(*,*) "PRESSURE REFERENCE", HPA2
      END IF
#endif
#if defined( USE_DIRICHLETPRESSURE )
      END IF
#endif
C
      CALL MPI_BCAST(HPA2,1,MPI_REAL8,RMON,PETSC_COMM_WORLD,IERR)
C       
      DO M=1,NBLKS
C
        CALL SETIND(M)
C      
        DO K=2,NKM
        DO I=2,NIM
        DO J=2,NJM
          RCOUNT=RCOUNT+1
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          UA=UMMS(XC(IJK),YC(IJK),ZC(IJK))
          VA=VMMS(XC(IJK),YC(IJK),ZC(IJK))
          WA=WMMS(XC(IJK),YC(IJK),ZC(IJK))
          UVA=DSQRT(UA**2+VA**2+WA**2)
          UVW=DSQRT(U(IJK)**2+V(IJK)**2+W(IJK)**2)
C
          HPA=PMMS(XC(IJK),YC(IJK),ZC(IJK))
          TA= TMMS(XC(IJK),YC(IJK),ZC(IJK))
C
          ERRORA=ERRORA+(UVA-UVW)**2
          ERRORU=ERRORU+(UA-U(IJK))**2
          ERRORV=ERRORV+(VA-V(IJK))**2
          ERRORW=ERRORW+(WA-W(IJK))**2
          ERRORP=ERRORP+((HPA-HPA2)-P(IJK))**2
          ERRORT=ERRORT+(TA-T(IJK))**2
        ENDDO
        ENDDO
        ENDDO
      ENDDO    
C
#if defined(USE_SUBVEC)
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecRestoreSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef USE_ENERGYCOUPLING 
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
C.....GLOBAL REDUCE OPERATION HERE!
C
       CALL MPI_REDUCE(
     *      ERRORU,ERRORUG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
       CALL MPI_REDUCE(
     *      ERRORV,ERRORVG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
       CALL MPI_REDUCE(
     *      ERRORW,ERRORWG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
       CALL MPI_REDUCE(
     *      ERRORA,ERRORAG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
       CALL MPI_REDUCE(
     *      ERRORP,ERRORPG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
       CALL MPI_REDUCE(
     *      ERRORT,ERRORTG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
       CALL MPI_REDUCE(
     *    RCOUNT,RCOUNTG,1,MPI_INTEGER,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
C
      IF (RANK.EQ.0) THEN
        ERRORA=ERRORAG
        ERRORU=ERRORUG
        ERRORV=ERRORVG
        ERRORW=ERRORWG
        ERRORP=ERRORPG
        ERRORT=ERRORTG
        RCOUNT=RCOUNTG
        ERRORA=DSQRT(ERRORA/RCOUNT)
        ERRORU=DSQRT(ERRORU/RCOUNT)
        ERRORV=DSQRT(ERRORV/RCOUNT)
        ERRORW=DSQRT(ERRORW/RCOUNT)
        ERRORP=DSQRT(ERRORP/RCOUNT)
        ERRORT=DSQRT(ERRORT/RCOUNT)
C
        WRITE(*,*) 'L2-NORM ERROR U    VELOCITY',ERRORU
        WRITE(*,*) 'L2-NORM ERROR V    VELOCITY',ERRORV
        WRITE(*,*) 'L2-NORM ERROR W    VELOCITY',ERRORW
        WRITE(*,*) 'L2-NORM ERROR ABS. VELOCITY',ERRORA
        WRITE(*,*) 'L2-NORM ERROR      PRESSURE',ERRORP      
C       WRITE(*,*) 'PESSURE REFERENCE',PREF,HPA2
        IF (LCAL(IEN).OR.LCAL(ISC)) THEN
        WRITE(*,*) 'L2-NORM ERROR   TEMPERATURE',ERRORT      
        ENDIF
        WRITE(23,*)'L2-NORM ERROR U    VELOCITY',ERRORU
        WRITE(23,*)'L2-NORM ERROR V    VELOCITY',ERRORV
        WRITE(23,*)'L2-NORM ERROR W    VELOCITY',ERRORW
        WRITE(23,*)'L2-NORM ERROR ABS. VELOCITY',ERRORA
        WRITE(23,*)'L2-NORM ERROR      PRESSURE',ERRORP      
C       WRITE(23,*)'PESSURE REFERENCE',PREF,HPA2
        IF (LCAL(IEN).OR.LCAL(ISC)) THEN
        WRITE(23,*)'L2-NORM ERROR   TEMPERATURE',ERRORT      
        ENDIF
      END IF
C
#include "petsc.user.inc"
C
      END SUBROUTINE CALCERR
