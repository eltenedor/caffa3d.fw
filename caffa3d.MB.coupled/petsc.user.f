C#############################################################
      SUBROUTINE DISTRIBUTELOAD
C#############################################################
C     This routine initializes the PETSc Matrix and Vector
C     Objects by allocating space for the calculated number of
C     nonzero elements and so distributing the load
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "gradold3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER DNNZ(NXYZA*NOEQ),IJRGL(NOCA+NFA)
      INTEGER ONNZT(NXYZA*NOEQ)
      INTEGER DNNZSC(NXYZA)
      INTEGER ONNZST(NXYZA)
      REAL*8 ONNZARR(1)
      REAL*8 ONNZSARR(1)
      INTEGER I,J,K,M,IJK,IJP,IJN,IJKP,C,IJKPSC
      REAL*8 VAL
C
      Vec ONNZVEC,ONNZL,ONNZGHOST
      Vec ONNZSVEC,ONNZSL,ONNZSGHOST
      PetscOffset ONNZZI
      PetscOffset ONNZSSI
      PetscInt NGH,NGHS
      PetscScalar PZERO
      PetscErrorCode IERR
C
      COMMON /MAPPING/ IJRGL
#include "petsc.user.inc"
C=============================================================
C
C......Initialize variables and arrays
C
      PZERO=0.0D0
      IJKPSC=0
      IJKP=(1-NOEQ)
      DNNZSC=1
#if defined( USE_BLOCKMATRIX )
      DNNZ=NOEQ
#else
      DNNZ=1
#endif
      NGH=1-NOEQ
      NGHS=0
      IJRGL=0
C
C.....FIND NUMBER OF VALUES THAT NEED TO BE GHOSTED AND CREATE VECTOR
C
      DO I=1,NOCBKAL
        IJN=IJRPBK(I)
        IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
          NGHS=NGHS+1
          IJGHS(NGHS)=IJN-1
C
          NGH=NGH+NOEQ
          IJN=(IJN-1)*NOEQ
          DO C=0,NOEQ-1
            IJGH(NGH+C)=IJN+C
          END DO
        END IF
      END DO
C
      DO I=1,NFSGBKAL
        IJN=IJFR(I)
        IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
          NGHS=NGHS+1
          IJGHS(NGHS)=IJN-1
C
          NGH=NGH+NOEQ
          IJN=(IJN-1)*NOEQ
          DO C=0,NOEQ-1
            IJGH(NGH+C)=IJN+C
          END DO
        END IF
      END DO
C FINAL NUMBER OF GHOST-VALUES
      IF (NGH.GT.0) THEN
        NGH=NGH+(NOEQ-1)
      ELSE
        NGH=0
      ENDIF
C
      CALL VecCreateGhost(PETSC_COMM_WORLD,
     *     NIJKBKAL*NOEQ,PETSC_DECIDE,NGH,IJGH,ONNZVEC,IERR)
      CALL VecCreateGhost(PETSC_COMM_WORLD,
     *     NIJKBKAL,PETSC_DECIDE,NGHS,IJGHS,ONNZSVEC,IERR)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....DETERMINE NUMBER OF NONZERO ELEMENTS (EXACT, NO REFINEMENT
C.....ASSUMED)
C
C.....NEW MAPPING FOR 4 (5) VARIABLES AT ONE NODE
C.....ASSEMBLED SYSTEM WILL CONSIST OF SUBMATRICES OF SHAPE 4x4 (5x5)
C.....        
C.....             X 0 0 X (X)   U
C.....             0 X 0 X (X)   V                       
C.....             0 0 X X (X)   W
C.....             X X X X (0)   P                    
C.....            (X X X X  X)  (T)
C.....           [(0 0 0 0  X)  (T)]
C.....
C.....LOOP THROUGH INNER CELLS, CORRECT NUMBER OF NONZEROS
C
#ifndef USE_BLOCKMATRIX
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJK=(IJK-1)*NOEQ+1
        DO C=0,2
          DNNZ(IJK+C)=2
        END DO
        C=3
        DNNZ(IJK+C)=4
#if defined( USE_ENERGYCOUPLING )
        DO C=0,2
          DNNZ(IJK+C)=DNNZ(IJK+C)+1
        END DO
        C=4
#if defined( USE_NEWTONRAPHSON )
        DNNZ(IJK+C)=5
#else
        DNNZ(IJK+C)=1
#endif
#endif
      END DO
      END DO
      END DO
#endif
C
C.....LOOP THROUGH INNER FACES. EAST FACES. CALCULATE NONZEROS FOR
C.....SCALAR TRANSPORT FIRST THEN MAP VALUES FOR FIELD INTERLACING
C
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        DNNZSC(IJK)=DNNZSC(IJK)+1
        DNNZSC(IJK+NJ)=DNNZSC(IJK+NJ)+1
C
C
        IJK=(IJK-1)*NOEQ+1
C
#if defined( USE_BLOCKMATRIX )
        DO C=0,NOEQ-1
          DNNZ(IJK+C)        =DNNZ(IJK+C)+NOEQ
          DNNZ(IJK+NJ*NOEQ+C)=DNNZ(IJK+NJ*NOEQ+C)+NOEQ
        END DO
#else
        DO C=0,2
          DNNZ(IJK+C)        =DNNZ(IJK+C)+2
          DNNZ(IJK+NJ*NOEQ+C)=DNNZ(IJK+NJ*NOEQ+C)+2
        END DO
        C=3
        DNNZ(IJK+C)        =DNNZ(IJK+C)+4
        DNNZ(IJK+NJ*NOEQ+C)=DNNZ(IJK+NJ*NOEQ+C)+4
C
#if defined( USE_ENERGYCOUPLING )
        C=4
#if defined( USE_NEWTONRAPHSON )
        DNNZ(IJK+C)        =DNNZ(IJK+C)+5
        DNNZ(IJK+NJ*NOEQ+C)=DNNZ(IJK+NJ*NOEQ+C)+5
#else
        DNNZ(IJK+C)        =DNNZ(IJK+C)+1
        DNNZ(IJK+NJ*NOEQ+C)=DNNZ(IJK+NJ*NOEQ+C)+1
#endif
#endif
C
#endif
C
      END DO
      END DO
      END DO
C
C.....LOOP THROUGH INNER FACES. NORTH FACES
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        DNNZSC(IJK)=DNNZSC(IJK)+1
        DNNZSC(IJK+1)=DNNZSC(IJK+1)+1
C
        IJK=(IJK-1)*NOEQ+1
C
#if defined( USE_BLOCKMATRIX )
        DO C=0,3
          DNNZ(IJK+C)     =DNNZ(IJK+C)+NOEQ
          DNNZ(IJK+NOEQ+C)=DNNZ(IJK+NOEQ+C)+NOEQ
        END DO
#else
        DO C=0,2
          DNNZ(IJK+C)     =DNNZ(IJK+C)+2
          DNNZ(IJK+NOEQ+C)=DNNZ(IJK+NOEQ+C)+2
        END DO
        C=3
        DNNZ(IJK+C)     =DNNZ(IJK+C)+4
        DNNZ(IJK+NOEQ+C)=DNNZ(IJK+NOEQ+C)+4
C
#if defined( USE_ENERGYCOUPLING )
        C=4
#if defined( USE_NEWTONRAPHSON )
        DNNZ(IJK+C)     =DNNZ(IJK+C)+5
        DNNZ(IJK+NOEQ+C)=DNNZ(IJK+NOEQ+C)+5
#else
        DNNZ(IJK+C)     =DNNZ(IJK+C)+1
        DNNZ(IJK+NOEQ+C)=DNNZ(IJK+NOEQ+C)+1
#endif
#endif
C
#endif
      END DO
      END DO
      END DO
C
C.....Loop through inner faces. Top
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        DNNZSC(IJK)=DNNZSC(IJK)+1
        DNNZSC(IJK+NIJ)=DNNZSC(IJK+NIJ)+1
C
        IJK=(IJK-1)*NOEQ+1
C
#if defined( USE_BLOCKMATRIX )
        DO C=0,3
          DNNZ(IJK+C)         =DNNZ(IJK+C)+NOEQ
          DNNZ(IJK+NIJ*NOEQ+C)=DNNZ(IJK+NIJ*NOEQ+C)+NOEQ
        END DO
#else
        DO C=0,2
          DNNZ(IJK+C)         =DNNZ(IJK+C)+2
          DNNZ(IJK+NIJ*NOEQ+C)=DNNZ(IJK+NIJ*NOEQ+C)+2
        END DO
        C=3
        DNNZ(IJK+C)         =DNNZ(IJK+C)+4
        DNNZ(IJK+NIJ*NOEQ+C)=DNNZ(IJK+NIJ*NOEQ+C)+4
C
#if defined( USE_ENERGYCOUPLING )
        C=4
#if defined( USE_NEWTONRAPHSON )
        DNNZ(IJK+C)         =DNNZ(IJK+C)+5
        DNNZ(IJK+NIJ*NOEQ+C)=DNNZ(IJK+NIJ*NOEQ+C)+5
#else
        DNNZ(IJK+C)         =DNNZ(IJK+C)+1
        DNNZ(IJK+NIJ*NOEQ+C)=DNNZ(IJK+NIJ*NOEQ+C)+1
#endif
#endif 
C
#endif
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. West/East bound. Once again scalar
C.....transport is first
C     
      DO K=1,NK
      DO I=1,NI,NI-1
      DO J=1,NJ
        IJKPSC=IJKPSC+1
        IJKP=IJKP+NOEQ
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
        ZEROSSC(IJKPSC)=IJK
C.....IJK is now a zero-based index thus the different mapping
        IJK=IJK*NOEQ
        DO C=0,NOEQ-1
          ZEROS(IJKP+C)=IJK+C
        END DO
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. South/North bound.
C     
      DO K=1,NK
      DO I=2,NI-1
      DO J=1,NJ,NJ-1
        IJKPSC=IJKPSC+1
        IJKP=IJKP+NOEQ
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
        ZEROSSC(IJKPSC)=IJK
        IJK=IJK*NOEQ
        DO C=0,NOEQ-1
          ZEROS(IJKP+C)=IJK+C
        END DO
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. Bottom/Top bound.
C     
      DO K=1,NK,NK-1
      DO I=2,NI-1
      DO J=2,NJ-1
        IJKPSC=IJKPSC+1
        IJKP=IJKP+NOEQ
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
        ZEROSSC(IJKPSC)=IJK
        IJK=IJK*NOEQ
        DO C=0,NOEQ-1
          ZEROS(IJKP+C)=IJK+C
        END DO
      END DO
      END DO
      END DO
C
      END DO
      NZEROSC=IJKPSC
      NZERO=IJKP+(NOEQ-1)
C
C.....Block Boundaries (OC and FSG)
C
      CALL VecGhostGetLocalForm(ONNZVEC,ONNZL,IERR)
      CALL VecGhostGetLocalForm(ONNZSVEC,ONNZSL,IERR)
      CALL VecSet(ONNZL,PZERO,IERR)
      CALL VecSet(ONNZSL,PZERO,IERR)
      CALL VecGetArray(ONNZL,ONNZARR,ONNZZI,IERR)
      CALL VecGetArray(ONNZSL,ONNZSARR,ONNZSSI,IERR)
      NGH=0
      NGHS=0
      DO I=1,NOCBKAL
C.....SUBTRACT THE PROCESSOR OFFSET; CV L IS ALWAYS LOCAL
        IJLPBK(I)=IJLPBK(I)-IJKPRC
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
C.....ELEMENT NOT ON PROCESSOR 
        IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN

          NGHS=NGHS+1
          ONNZS(IJP)=ONNZS(IJP)+1.0D0
C,,,,,CONSIDER OFFDIAGONAL ELEMENT ON NEIGHBOUR PROCESSOR
          ONNZS(NIJKBKAL+NGHS)=ONNZS(NIJKBKAL+NGHS)+1.0D0
C.....MAPPING FOR COUPLED INDEXING (FORTRAN INDEXING -> +1)
          IJP=(IJP-1)*NOEQ+1
          IJRPBK(I)=NIJKBKAL+NGHS
#if defined( USE_BLOCKMATRIX )
          DO C=0,NOEQ-1
            NGH=NGH+1
            ONNZ(IJP+C)=ONNZ(IJP+C)+NOEQ*1.0D0
            ONNZ((NIJKBKAL*NOEQ)+NGH)=ONNZ((NIJKBKAL*NOEQ)+NGH)+NOEQ*1.0D0
          END DO
#else
          DO C=0,2
            NGH=NGH+1
            ONNZ(IJP+C)=ONNZ(IJP+C)+2.0D0
            ONNZ((NIJKBKAL*NOEQ)+NGH)=ONNZ((NIJKBKAL*NOEQ)+NGH)+2.0D0
          END DO
          C=3
          NGH=NGH+1
          ONNZ(IJP+C)=ONNZ(IJP+C)+4.0D0
          ONNZ((NIJKBKAL*NOEQ)+NGH)=ONNZ((NIJKBKAL*NOEQ)+NGH)+4.0D0
#endif
        ELSE
C.....SUBTRACT THE PROCESSOR OFFSET
          IJRPBK(I)=IJRPBK(I)-IJKPRC
          DNNZSC(IJP)=DNNZSC(IJP)+1
C.....SUBTRACT THE PROCESSOR OFFSET
          DNNZSC(IJRPBK(I))=DNNZSC(IJRPBK(I))+1
#if defined( USE_BLOCKMATRIX )
          DO C=1,NOEQ
            DNNZ((IJP      -1)*NOEQ+C)=DNNZ((IJP      -1)*NOEQ+C)+NOEQ
            DNNZ((IJRPBK(I)-1)*NOEQ+C)=DNNZ((IJRPBK(I)-1)*NOEQ+C)+NOEQ
          END DO
#else
          DO C=1,3
            DNNZ((IJP      -1)*NOEQ+C)=DNNZ((IJP      -1)*NOEQ+C)+2
            DNNZ((IJRPBK(I)-1)*NOEQ+C)=DNNZ((IJRPBK(I)-1)*NOEQ+C)+2
          END DO
          C=4
          DNNZ((IJP      -1)*NOEQ+C)=DNNZ((IJP      -1)*NOEQ+C)+4
          DNNZ((IJRPBK(I)-1)*NOEQ+C)=DNNZ((IJRPBK(I)-1)*NOEQ+C)+4
#endif
        END IF
C.....SAVE THE GLOBAL INDEX (FOR ASSEMBLY ONLY)
        IJRGL(I)=IJN
      END DO
C
      DO I=1,NFSGBKAL
        IJFL(I)=IJFL(I)-IJKPRC
        IJP=IJFL(I)
        IJN=IJFR(I)
        IF (IJN.GE.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
          NGHS=NGHS+1
          ONNZS(IJP)=ONNZS(IJP)+1.0D0
          ONNZS(NIJKBKAL+NGHS)=ONNZS(NIJKBKAL+NGHS)+1.0D0
          IJP=(IJP-1)*NOEQ
          IJFR(I)=NIJKBKAL+NGHS
#if defined( USE_BLOCKMATRIX )
          DO C=1,NOEQ
            NGH=NGH+1
            ONNZ(IJP+C)=ONNZ(IJP+C)+NOEQ*1.0D0
            ONNZ((NIJKBKAL*NOEQ)+NGH)=ONNZ((NIJKBKAL*NOEQ)+NGH)+NOEQ*1.0D0
          END DO
#else
          DO C=1,3
            NGH=NGH+1
            ONNZ(IJP+C)=ONNZ(IJP+C)+2.0D0
            ONNZ((NIJKBKAL*NOEQ)+NGH)=ONNZ((NIJKBKAL*NOEQ)+NGH)+2.0D0
          END DO
          C=4
          NGH=NGH+1
          ONNZ(IJP+C)=ONNZ(IJP+C)+4.0D0
          ONNZ((NIJKBKAL*NOEQ)+NGH)=ONNZ((NIJKBKAL*NOEQ)+NGH)+4.0D0
C
#if defined( USE_ENERGYCOUPLING )
          C=5
          NGH=NGH+1
#if defined( USE_NEWTONRAPHSON )
          ONNZ(IJP+C)=ONNZ(IJP+C)+5.0D0
          ONNZ((NIJKBKAL*NOEQ)+NGH)=ONNZ((NIJKBKAL*NOEQ)+NGH)+5.0D0
#else
          ONNZ(IJP+C)=ONNZ(IJP+C)+1.0D0
          ONNZ((NIJKBKAL*NOEQ)+NGH)=ONNZ((NIJKBKAL*NOEQ)+NGH)+1.0D0
#endif
#endif
C
#endif
        ELSE
          IJFR(I)=IJFR(I)-IJKPRC
          DNNZSC(IJP)=DNNZSC(IJP)+1
          DNNZSC(IJFR(I))=DNNZSC(IJFR(I))+1
#if defined( USE_BLOCKMATRIX )
          DO C=1,NOEQ
            DNNZ((IJP    -1)*NOEQ+C)=DNNZ((IJP    -1)*NOEQ+C)+NOEQ
            DNNZ((IJFR(I)-1)*NOEQ+C)=DNNZ((IJFR(I)-1)*NOEQ+C)+NOEQ
          END DO
#else
          DO C=1,3
            DNNZ((IJP    -1)*NOEQ+C)=DNNZ((IJP    -1)*NOEQ+C)+2
            DNNZ((IJFR(I)-1)*NOEQ+C)=DNNZ((IJFR(I)-1)*NOEQ+C)+2
          END DO
          C=4
          DNNZ((IJP    -1)*NOEQ+C)=DNNZ((IJP    -1)*NOEQ+C)+4
          DNNZ((IJFR(I)-1)*NOEQ+C)=DNNZ((IJFR(I)-1)*NOEQ+C)+4
C
#if defined( USE_ENERGYCOUPLING )
          C=5
#if defined( USE_NEWTONRAPHSON )
          DNNZ((IJP    -1)*NOEQ+C)=DNNZ((IJP    -1)*NOEQ+C)+5
          DNNZ((IJFR(I)-1)*NOEQ+C)=DNNZ((IJFR(I)-1)*NOEQ+C)+5
#else
          DNNZ((IJP    -1)*NOEQ+C)=DNNZ((IJP    -1)*NOEQ+C)+1
          DNNZ((IJFR(I)-1)*NOEQ+C)=DNNZ((IJFR(I)-1)*NOEQ+C)+1
#endif
#endif
C
#endif
        END IF
        IJRGL(NOCBKAL+I)=IJN
      END DO
C
C.....SEND NONZEROS IN OFFDIAGONAL PART TO THE RESPECTIVE PROCESSOR
C
      CALL VecRestoreArray(ONNZL,ONNZARR,ONNZZI,IERR)
      CALL VecRestoreArray(ONNZSL,ONNZSARR,ONNZSSI,IERR)
      CALL VecGhostRestoreLocalForm(ONNZVEC,ONNZL,IERR)
      CALL VecGhostRestoreLocalForm(ONNZSVEC,ONNZSL,IERR)
C
      CALL VecGhostUpdateBegin(
     *     ONNZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(ONNZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
      CALL VecGhostUpdateBegin(
     *     ONNZSVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(ONNZSVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....CONVERT TO INTEGER
C
      CALL VecGetArray(ONNZVEC,ONNZARR,ONNZZI,IERR)
      DO I=1,NIJKBKAL*NOEQ
        ONNZT(I)=INT(ONNZ(I))
      END DO
      CALL VecRestoreArray(ONNZVEC,ONNZARR,ONNZZI,IERR)
      CALL VecDestroy(ONNZVEC,IERR)
C
      CALL VecGetArray(ONNZSVEC,ONNZSARR,ONNZSSI,IERR)
      DO I=1,NIJKBKAL
        ONNZST(I)=INT(ONNZS(I))
      END DO
      CALL VecRestoreArray(ONNZSVEC,ONNZSARR,ONNZSSI,IERR)
C
C.....Create Matrix for scalar
C
      CALL MatCreate(PETSC_COMM_WORLD,SCMAT,IERR)
      CALL MatSetSizes(SCMAT,NIJKBKAL,NIJKBKAL,
     *                 PETSC_DECIDE,PETSC_DECIDE,IERR)
C
      CALL MatSetFromOptions(SCMAT,IERR)
      CALL MatSeqAIJSetPreallocation(SCMAT,
     *                               PETSC_NULL_INTEGER,DNNZSC,IERR)
      CALL MatMPIAIJSetPreallocation(
     *   SCMAT,PETSC_NULL_INTEGER,DNNZSC,PETSC_NULL_INTEGER,ONNZST,IERR)
      CALL MatSetOption(SCMAT,MAT_NO_OFF_PROC_ZERO_ROWS,PETSC_TRUE,IERR)
#ifndef USE_INFO
      CALL MatSetOption(SCMAT,
     *                 MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE,IERR)
#endif
C
      CALL MatCreate(PETSC_COMM_WORLD,AMAT,IERR)
      CALL MatSetSizes(AMAT,NIJKBKAL*NOEQ,NIJKBKAL*NOEQ,
     *                 PETSC_DECIDE,PETSC_DECIDE,IERR)
      CALL MatSetFromOptions(AMAT,IERR)
C
#if defined( USE_BLOCKMATRIX )
C     CALL MatSeqBAIJSetPreallocation(
C    *     AMAT,4,PETSC_NULL_INTEGER,DNNZ,IERR)
      CALL MatSeqBAIJSetPreallocation(
     *     AMAT,NOEQ,7*NOEQ,PETSC_NULL_INTEGER,IERR)
C     CALL MatMPIBAIJSetPreallocation(
C    *     AMAT,4,PETSC_NULL_INTEGER,DNNZ,PETSC_NULL_INTEGER,ONNZT,IERR)
      CALL MatMPIBAIJSetPreallocation(
     *     AMAT,NOEQ,
     *     7*NOEQ,PETSC_NULL_INTEGER,
     *     PETSC_DECIDE,PETSC_NULL_INTEGER,IERR)
#else
      CALL MatSeqAIJSetPreallocation(AMAT,PETSC_NULL_INTEGER,DNNZ,IERR)
      CALL MatMPIAIJSetPreallocation(
     *     AMAT,PETSC_NULL_INTEGER,DNNZ,PETSC_NULL_INTEGER,ONNZT,IERR)
#endif
C
C......REDUCE PROCESSOR COMMUNICATION DURING MATZEROROWS, DISABLE
C......CHECKING FOR NEW NONZEROS
C
      CALL MatSetOption(AMAT,MAT_NO_OFF_PROC_ZERO_ROWS,PETSC_TRUE,IERR)
#ifndef USE_INFO
      CALL MatSetOption(AMAT,
     *                 MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE,IERR)
#endif
      CALL MatSetUp(AMAT,IERR)
      CALL MatSetUp(SCMAT,IERR)
C......Vereinheitlichung durch Verwendung dieser Routine
C     CALL MatXAIJSetPreallocation(
C    *     AMAT,4,,
C    *     PETSC_NULL_INTEGER,PETSC_NULL_INTEGER,IERR)
C......Not necessary because already using preallocation routine
C
C.....Create ghosted Solution Vector
C
      CALL VecCreateGhost(
     *     PETSC_COMM_WORLD,NIJKBKAL*NOEQ,PETSC_DECIDE,NGH,IJGH,
     *     UVWPVEC,IERR)
      CALL VecSetFromOptions(UVWPVEC,IERR)
C
C.....Duplicate ghosted Vector
C
      CALL VecDuplicate(UVWPVEC,SUVWPVEC,IERR)
C
C.....Create strided index sets
C
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGHS,0,NOEQ,ISU,IERR)
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGHS,1,NOEQ,ISV,IERR)
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGHS,2,NOEQ,ISW,IERR)
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGHS,3,NOEQ,ISP,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGHS,4,NOEQ,ISE,IERR)
#endif
C
C.....Create ghosted Vectors
C
      CALL VecCreateGhost(
     *     PETSC_COMM_WORLD,NIJKBKAL,PETSC_DECIDE,NGHS,IJGHS,
     *     DUXVEC,IERR)
      CALL VecSetFromOptions(DUXVEC,IERR)
C
C.....Duplicate ghosted Vector
C
      CALL VecDuplicate(DUXVEC,DUYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DUZVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DVXVEC,IERR)
      CALL VecDuplicate(DUXVEC,DVYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DVZVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DWXVEC,IERR)
      CALL VecDuplicate(DUXVEC,DWYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DWZVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DPXVEC,IERR)
      CALL VecDuplicate(DUXVEC,DPYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DPZVEC,IERR)
C
#ifndef USE_ENERGYCOUPLING
      CALL VecDuplicate(DUXVEC,TVEC,IERR)
      CALL VecDuplicate(DUXVEC,SSCVEC,IERR)
#endif
      CALL VecDuplicate(DUXVEC,DSCXVEC,IERR)
      CALL VecDuplicate(DUXVEC,DSCYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DSCZVEC,IERR)
#if defined( USE_NEWTONRAPHSON ) 
      CALL VecDuplicate(DUXVEC,APTVEC,IERR)
#endif
C
      CALL VecDuplicate(DUXVEC,DFXOVEC,IERR)
      CALL VecDuplicate(DUXVEC,DFYOVEC,IERR)
      CALL VecDuplicate(DUXVEC,DFZOVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DENVEC,IERR)
      CALL VecDuplicate(DUXVEC,VISVEC,IERR)
      CALL VecDuplicate(DUXVEC,VOLVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,XCVEC,IERR)
      CALL VecDuplicate(DUXVEC,YCVEC,IERR)
      CALL VecDuplicate(DUXVEC,ZCVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,APVEC,IERR)
      CALL VecDuplicate(DUXVEC,APRVEC,IERR)
      CALL VecDuplicate(DUXVEC,APUVEC,IERR)
      CALL VecDuplicate(DUXVEC,APVVEC,IERR)
      CALL VecDuplicate(DUXVEC,APWVEC,IERR)
C
#ifndef USE_ZEROS
#if defined( USE_DIRICHLETPRESSURE)
      IF (NOUTBKAL.LE.0) THEN
#endif
      CALL VecCreate(PETSC_COMM_WORLD,NSPANVEC,IERR)
      CALL VecSetType(NSPANVEC,VECMPI,IERR)
      CALL VecSetSizes(NSPANVEC,NIJKBKAL*NOEQ,PETSC_DECIDE,IERR)
      CALL VecSetFromOptions(NSPANVEC,IERR)
C
C.....INITIALIZE NULLSPACE VECTOR
C
      CALL VecSet(NSPANVEC,0.0D0,IERR)
      CALL VecGetArray(NSPANVEC,NSPANARR,NSSI,IERR)
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJK=(IJK-1)*NOEQ+4
        NSPAN(IJK)=1.0D0
      END DO
      END DO
      END DO
      END DO
C
      CALL VecRestoreArray(NSPANVEC,NSPANARR,NSSI,IERR)
      CALL VecNorm(NSPANVEC,NORM_2,VAL,IERR)
      CALL VecNormalize(NSPANVEC,VAL,IERR)
#if defined( USE_DIRICHLETPRESSURE )
      END IF
#endif
#endif
C
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE ASSEMBLESYSUVW(CMAT)
C#############################################################
C     This routine assembles the PETSc Matrix of the linear
C     subsystem corresponding to uvw-momentum balances
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,J,K,M,IJK,IJKP,IJP,IJN,C
      INTEGER IJRGL(NOCA+NFA)
C
      PetscInt       I1,I7,I14,ROW,COL(7*2),COL1
      PetscScalar    VAL(7*2),VAL1,PONE
      PetscScalar    VALL2(2),VALR2(2),VAL2(2)
      PetscInt       COL2(2),I2
      PetscScalar    VALR3(3),VALL3(3)
      PetscErrorCode IERR
      Mat CMAT
C
      COMMON /MAPPING/ IJRGL
C
#include "petsc.user.inc"
C=============================================================
C     
      I1=1; I2=2; I14=14
      PONE=1.0D0
      VAL=0.0D0
      CALL MatZeroEntries(CMAT,IERR)
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(APUVEC,APUARR,APUUI,IERR)
      CALL VecGetArray(APVVEC,APVARR,APVVI,IERR)
      CALL VecGetArray(APWVEC,APWARR,APWWI,IERR)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....INITIALIZE BOUNDARY ENTRIES SOUTH/NORH
C
      DO K=1,NK
      DO I=1,NI
      DO J=1,NJ,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        IJK=(IJK-1)*NOEQ+1
        DO C=0,NOEQ-1
          ROW=IJK-1+C
          COL1=IJK-1+C
          CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,IERR)
        END DO
      END DO
      END DO
      END DO
C
C.....INITIALIZE BOUNDARY ENTRIES WEST/EAST
C
      DO K=1,NK
      DO I=1,NI,NI-1
      DO J=2,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        IJK=(IJK-1)*NOEQ+1
        DO C=0,NOEQ-1
          ROW=IJK-1+C
          COL1=IJK-1+C
          CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,ierr)
        END DO
      END DO
      END DO
      END DO
C
C.....INITIALIZE BOUNDARY ENTRIES BOTTOM/TOP
C
      DO K=1,NK,NK-1
      DO I=2,NI-1
      DO J=2,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        IJK=(IJK-1)*NOEQ+1
        DO C=0,NOEQ-1
          ROW=IJK-1+C
          COL1=IJK-1+C
          CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,ierr)
        END DO
      END DO
      END DO
      END DO
C        
C.....ASSEMBLE MATRIX AND RHS-VECTOR
C
      DO K=3,NKM-1
      DO I=3,NIM-1
      DO J=3,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP
C       
        COL(1)=IJKP-NIJ*NOEQ; COL(2)=IJKP-NJ*NOEQ; COL(3)=IJKP-NOEQ
        COL(4)=IJKP
        COL(5)=IJKP+NOEQ; COL(6)=IJKP+NJ*NOEQ; COL(7)=IJKP+NIJ*NOEQ
C        
C.....WORK IN OFFSET TO CORRESPONDING PRESSURE VARIABLE POSITION (U,V,W,P)
C
        DO C=1,7
          COL(C+7)=COL(C)+3
        END DO
C       
        VAL(1)=AB(IJK); VAL(2)=AW(IJK); VAL(3)=AS(IJK)
        VAL(4)=AP(IJK)
        VAL(5)=AN(IJK); VAL(6)=AE(IJK); VAL(7)=AT(IJK)
C        
C.....COEFFICIENTS DUE TO IMPLICIT TREATMENT OF PRESSURE GRADIENT
C
        VAL(8)=ABU(IJK); VAL(9)=AWU(IJK); VAL(10)=ASU(IJK)
        VAL(11)=APU(IJK)
        VAL(12)=ANU(IJK); VAL(13)=AEU(IJK); VAL(14)=ATU(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,ierr)
C
C.....WORK IN OFFSET TO V-VARIABLE
C
        DO C=1,7
          COL(C)=COL(C)+1
        END DO
        ROW=ROW+1
C
        VAL(8)=ABV(IJK); VAL(9)=AWV(IJK); VAL(10)=ASV(IJK)
        VAL(11)=APV(IJK)
        VAL(12)=ANV(IJK); VAL(13)=AEV(IJK); VAL(14)=ATV(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,ierr)
C
C.....WORK IN OFFSET TO W-VARIABLE
C
        DO C=1,7
          COL(C)=COL(C)+1
        END DO
        ROW=ROW+1
C
        VAL(8)=ABW(IJK); VAL(9)=AWW(IJK); VAL(10)=ASW(IJK)
        VAL(11)=APW(IJK)
        VAL(12)=ANW(IJK); VAL(13)=AEW(IJK); VAL(14)=ATW(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C        
C.....WEST/EAST BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM,NIM-2
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=AP(IJK)
        VAL(11)=APU(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABU(IJK);  COL(8)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN
            VAL(2)=AW(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWU(IJK);  COL(9)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=AS(IJK); COL(3)=IJKP-NOEQ  
            VAL(10)=ASU(IJK);  COL(10)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=AN(IJK); COL(5)=IJKP+NOEQ  
            VAL(12)=ANU(IJK);  COL(12)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AE(IJK); COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEU(IJK);  COL(13)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATU(IJK);  COL(14)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO V-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APV(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABV(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWV(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASV(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANV(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEV(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATV(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO W-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APW(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABW(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWW(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASW(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANW(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEW(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATW(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....SOUTH/NORTH BOUNDARIES
C
      IF (NJM.EQ.2) NJM=3
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM,NJM-2
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=AP(IJK)
        VAL(11)=APU(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABU(IJK); COL(8)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AW(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWU(IJK); COL(9)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=AS(IJK); COL(3)=IJKP-NOEQ
            VAL(10)=ASU(IJK); COL(10)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=AN(IJK); COL(5)=IJKP+NOEQ  
            VAL(12)=ANU(IJK);  COL(12)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AE(IJK); COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEU(IJK);  COL(13)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATU(IJK);  COL(14)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO V-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APV(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABV(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWV(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASV(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANV(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEV(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATV(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO W-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APW(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABW(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWW(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASW(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANW(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEW(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATW(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....BOTTOM/TOP BOUNDARIES
C
#if defined( USE_2DCASE )
      DO K=2,NKM
#else
      DO K=2,NKM,NKM-2
#endif
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=AP(IJK)
        VAL(11)=APU(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABU(IJK);  COL(8)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN
            VAL(2)=AW(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWU(IJK);  COL(9)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=AS(IJK); COL(3)=IJKP-NOEQ  
            VAL(10)=ASU(IJK);  COL(10)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=AN(IJK); COL(5)=IJKP+NOEQ  
            VAL(12)=ANU(IJK);  COL(12)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AE(IJK); COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEU(IJK);  COL(13)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATU(IJK);  COL(14)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C
C.....WORK IN OFFSET TO V-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APV(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABV(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWV(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASV(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANV(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEV(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATV(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO W-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APW(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABW(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWW(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASW(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANW(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEW(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATW(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C
      END DO
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(APUVEC,APUARR,APUUI,IERR)
      CALL VecRestoreArray(APVVEC,APVARR,APVVI,IERR)
      CALL VecRestoreArray(APWVEC,APWARR,APWWI,IERR)
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRGL(I)
C
        VALR3=(/ARU(I),ARV(I),ARW(I)/)
        VALL3=(/ALU(I),ALV(I),ALW(I)/)
C
        VALR2(1)=AR(I)
        VALL2(1)=AL(I)
C
        DO C=0,2
          ROW=(IJKPRC+IJP-1)*NOEQ+C
          COL2(1)=(IJN-1)*NOEQ+C
          COL2(2)=COL2(1)+3-C
          VALR2(2)=VALR3(C+1)
C
          CALL MatSetValues(CMAT,I1,ROW,
     *                           I2,COL2,VALR2,INSERT_VALUES,IERR)
C
          ROW=(IJN-1)*NOEQ+C
          COL2(1)=(IJKPRC+IJP-1)*NOEQ+C
          COL2(2)=COL2(1)+3-C
          VALL2(2)=VALL3(C+1)
C
          CALL MatSetValues(CMAT,I1,ROW,
     *                           I2,COL2,VALL2,INSERT_VALUES,IERR)
C
        END DO
C
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE INTERNAL BOUNDARIES)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJRGL(NOCBKAL+I)
C
        VALR3=(/ARFU(I),ARFV(I),ARFW(I)/)
        VALL3=(/ALFU(I),ALFV(I),ALFW(I)/)
C
        VALR2(1)=AFR(I)
        VALL2(1)=AFL(I)
C
        DO C=0,2
          ROW=(IJKPRC+IJP-1)*NOEQ+C
          COL2(1)=(IJN-1)*NOEQ+C
          COL2(2)=COL2(1)+3-C
          VALR2(2)=VALR3(C+1)
C
          CALL MatSetValues(CMAT,I1,ROW,
     *                           I2,COL2,VALR2,INSERT_VALUES,IERR)
C
          ROW=(IJN-1)*NOEQ+C
          COL2(1)=(IJKPRC+IJP-1)*NOEQ+C
          COL2(2)=COL2(1)+3-C
          VALL2(2)=VALL3(C+1)
C
          CALL MatSetValues(CMAT,I1,ROW,
     *                           I2,COL2,VALL2,INSERT_VALUES,IERR)
C
        END DO
C
      END DO
C
C.....FLUSH MATRIX ASSEMBLY
C
      CALL MatAssemblyBegin(CMAT,MAT_FLUSH_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FLUSH_ASSEMBLY,IERR)
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE ASSEMBLESYSP(CMAT)
C#############################################################
C     This routine assembles the PETSc Matrix of the linear
C     subsystem corresponding to the pressure equation
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,J,K,M,IJK,IJKP,IJP,IJN,C
      INTEGER IJRGL(NOCA+NFA)
C
      PetscInt       I1,I28,ROW,COL(7*4),COL1
      PetscScalar    VAL(7*4),VALR1,VALL1,VAL1,PONE
      PetscScalar    VAL4(4)
      PetscInt       I4,COL4(4)
      PetscErrorCode IERR
      Mat CMAT
C
      COMMON /MAPPING/ IJRGL
C
#include "petsc.user.inc"
C=============================================================
C     
C
C.....FINAL MATRIX ASSEMBLY
C
      I1=1
      I4=4
      I28=28
      PONE=1.0D0
      VAL=0.0D0
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(APUVEC,APUARR,APUUI,IERR)
      CALL VecGetArray(APVVEC,APVARR,APVVI,IERR)
      CALL VecGetArray(APWVEC,APWARR,APWWI,IERR)
C        
C.....ASSEMBLE MATRIX
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=3,NKM-1
      DO I=3,NIM-1
      DO J=3,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP+3
C       
        COL(1)=IJKP-NIJ*NOEQ; COL(2)=IJKP-NJ*NOEQ; COL(3)=IJKP-NOEQ
        COL(4)=IJKP
        COL(5)=IJKP+NOEQ; COL(6)=IJKP+NJ*NOEQ; COL(7)=IJKP+NIJ*NOEQ
C        
C.....WORK IN OFFSET TO CORRESPONDING PRESSURE VARIABLE POSITION (U,V,W,P)
C
        DO C=1,7
          COL(C+7)=COL(C)+1
          COL(C+14)=COL(C)+2
          COL(C+21)=COL(C)+3
        END DO
C       
        VAL(1)=ABU(IJK); VAL(2)=AWU(IJK); VAL(3)=ASU(IJK)
        VAL(4)=APU(IJK)
        VAL(5)=ANU(IJK); VAL(6)=AEU(IJK); VAL(7)=ATU(IJK)
C
        VAL(8)=ABV(IJK); VAL(9)=AWV(IJK); VAL(10)=ASV(IJK)
        VAL(11)=APV(IJK)
        VAL(12)=ANV(IJK); VAL(13)=AEV(IJK); VAL(14)=ATV(IJK)
C
        VAL(15)=ABW(IJK); VAL(16)=AWW(IJK); VAL(17)=ASW(IJK)
        VAL(18)=APW(IJK)
        VAL(19)=ANW(IJK); VAL(20)=AEW(IJK); VAL(21)=ATW(IJK)
C
        VAL(22)=AB(IJK); VAL(23)=AW(IJK); VAL(24)=AS(IJK)
        VAL(25)=AP(IJK)
        VAL(26)=AN(IJK); VAL(27)=AE(IJK); VAL(28)=AT(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I28,COL,VAL,INSERT_VALUES,ierr)
C
      END DO
      END DO
      END DO
C        
C.....WEST/EAST BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM,NIM-2
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP+3
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=AB(IJK); COL(22)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AW(IJK); COL(23)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-NOEQ
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AS(IJK); COL(24)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+NOEQ  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=AN(IJK); COL(26)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AE(IJK); COL(27)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=AT(IJK); COL(28)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I28,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....SOUTH/NORTH BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM,NJM-2
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP+3
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=AB(IJK); COL(22)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AW(IJK); COL(23)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-NOEQ
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AS(IJK); COL(24)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+NOEQ  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=AN(IJK); COL(26)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AE(IJK); COL(27)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=AT(IJK); COL(28)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I28,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....BOTTOM/TOP BOUNDARIES
C
#if defined( USE_2DCASE )
      DO K=2,NKM
#else
      DO K=2,NKM,NKM-2
#endif
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP+3
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=AB(IJK); COL(22)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AW(IJK); COL(23)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-NOEQ
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AS(IJK); COL(24)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+NOEQ  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=AN(IJK); COL(26)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AE(IJK); COL(27)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=AT(IJK); COL(28)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I28,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C
      END DO
C
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(APUVEC,APUARR,APUUI,IERR)
      CALL VecRestoreArray(APVVEC,APVARR,APVVI,IERR)
      CALL VecRestoreArray(APWVEC,APWARR,APWWI,IERR)
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRGL(I)
C
        ROW=(IJKPRC+IJP-1)*NOEQ+3
        DO C=0,3
          COL4(C+1)=(IJN-1)*NOEQ+C
        ENDDO
        VAL4=(/ARU(I),ARV(I),ARW(I),AR(I)/)
        CALL MatSetValues(CMAT,I1,ROW,I4,COL4,VAL4,INSERT_VALUES,IERR)
C
        ROW=(IJN-1)*NOEQ+3
        DO C=0,3
          COL4(C+1)=(IJKPRC+IJP-1)*NOEQ+C
        ENDDO
        VAL4=(/ALU(I),ALV(I),ALW(I),AL(I)/)
        CALL MatSetValues(CMAT,I1,ROW,I4,COL4,VAL4,INSERT_VALUES,IERR)
C
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE INTERNAL BOUNDARIES)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJRGL(NOCBKAL+I)
C
        ROW=(IJKPRC+IJP-1)*NOEQ+3
        DO C=0,3
          COL4(C+1)=(IJN-1)*NOEQ+C
        ENDDO
        VAL4=(/ARFU(I),ARFV(I),ARFW(I),AFR(I)/)
        CALL MatSetValues(CMAT,I1,ROW,I4,COL4,VAL4,INSERT_VALUES,IERR)
C
        ROW=(IJN-1)*NOEQ+3
        DO C=0,3
          COL4(C+1)=(IJKPRC+IJP-1)*NOEQ+C
        ENDDO
        VAL4=(/ALFU(I),ALFV(I),ALFW(I),AFL(I)/)
        CALL MatSetValues(CMAT,I1,ROW,I4,COL4,VAL4,INSERT_VALUES,IERR)
C
      END DO
C
#if defined( USE_ENERGYCOUPLING )
C
C.....FLUSH MATRIX ASSEMBLY
C
      CALL MatAssemblyBegin(CMAT,MAT_FLUSH_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FLUSH_ASSEMBLY,IERR)
#else
C
C.....FINAL MATRIX ASSEMBLY
C
      CALL MatAssemblyBegin(CMAT,MAT_FINAL_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FINAL_ASSEMBLY,IERR)
C
#endif
C
      RETURN
      END
C
#include "petsc.user.inc"
C
#if defined( USE_NEWTONRAPHSON )
C#############################################################
      SUBROUTINE ASSEMBLESYSEN(CMAT)
C#############################################################
C     This routine assembles the PETSc Matrix of the linear
C     subsystem corresponding to the energy coupling and the 
C     energy balance. Note that this version uses a Newton-Raphson
C     linearization.
C=============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,J,K,M,IJK,IJKP,IJP,IJN,C
      INTEGER IJRGL(NOCA+NFA)
C
      PetscInt       I1,I35,ROW,COL(7*5),COL1
      PetscInt       I3,ROW3(3)
      PetscScalar    VAL(7*5),VALR1,VALL1,VAL1,PONE
      PetscScalar    VAL3(3)
      PetscScalar    VAL5(5)
      PetscInt       I5,COL5(5)
      PetscErrorCode IERR
      Mat CMAT
C
      COMMON /MAPPING/ IJRGL
C
#include "petsc.user.inc"
C=============================================================
C     
C
C.....FINAL MATRIX ASSEMBLY
C
      I1=1; I3=3; I5=5; I35=35
      PONE=1.0D0
      VAL=0.0D0
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(APTVEC,APTARR,APTTI,IERR)
      CALL VecGetArray(APUVEC,APUARR,APUUI,IERR)
      CALL VecGetArray(APVVEC,APVARR,APVVI,IERR)
      CALL VecGetArray(APWVEC,APWARR,APWWI,IERR)
C        
C.....ASSEMBLE MATRIX
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=3,NKM-1
      DO I=3,NIM-1
      DO J=3,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP+4
C       
        COL(1)=IJKP-NIJ*NOEQ; COL(2)=IJKP-NJ*NOEQ; COL(3)=IJKP-NOEQ
        COL(4)=IJKP
        COL(5)=IJKP+NOEQ; COL(6)=IJKP+NJ*NOEQ; COL(7)=IJKP+NIJ*NOEQ
C        
C.....WORK IN OFFSET TO CORRESPONDING PRESSURE VARIABLE POSITION (U,V,W,P)
C
        DO C=1,7
          COL(C+7)=COL(C)+1
          COL(C+14)=COL(C)+2
          COL(C+21)=COL(C)+3
          COL(C+28)=COL(C)+4
        END DO
C       
        VAL(1)=ABU(IJK); VAL(2)=AWU(IJK); VAL(3)=ASU(IJK)
        VAL(4)=APU(IJK)
        VAL(5)=ANU(IJK); VAL(6)=AEU(IJK); VAL(7)=ATU(IJK)
C
        VAL(8)=ABV(IJK); VAL(9)=AWV(IJK); VAL(10)=ASV(IJK)
        VAL(11)=APV(IJK)
        VAL(12)=ANV(IJK); VAL(13)=AEV(IJK); VAL(14)=ATV(IJK)
C
        VAL(15)=ABW(IJK); VAL(16)=AWW(IJK); VAL(17)=ASW(IJK)
        VAL(18)=APW(IJK)
        VAL(19)=ANW(IJK); VAL(20)=AEW(IJK); VAL(21)=ATW(IJK)
C
        VAL(22)=ABT(IJK); VAL(23)=AWT(IJK); VAL(24)=AST(IJK)
        VAL(25)=APT(IJK)
        VAL(26)=ANT(IJK); VAL(27)=AET(IJK); VAL(28)=ATT(IJK)
C
        VAL(29)=AB(IJK); VAL(30)=AW(IJK); VAL(31)=AS(IJK)
        VAL(32)=AP(IJK)
        VAL(33)=AN(IJK); VAL(34)=AE(IJK); VAL(35)=AT(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I35,COL,VAL,INSERT_VALUES,ierr)
C
      END DO
      END DO
      END DO
C        
C.....WEST/EAST BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM,NIM-2
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP+4
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1,  
     *        -1,-1,-1,IJKP+4,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=APT(IJK)
        VAL(32)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=ABT(IJK); COL(22)=COL(1)+3
            VAL(29)=AB(IJK); COL(29)=COL(1)+4
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AWT(IJK); COL(23)=COL(2)+3
            VAL(30)=AW(IJK); COL(30)=COL(2)+4
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-NOEQ
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AST(IJK); COL(24)=COL(3)+3
            VAL(31)=AS(IJK); COL(31)=COL(3)+4
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+NOEQ  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=ANT(IJK); COL(26)=COL(5)+3
            VAL(33)=AN(IJK); COL(33)=COL(5)+4
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AET(IJK); COL(27)=COL(6)+3
            VAL(34)=AE(IJK); COL(34)=COL(6)+4
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=ATT(IJK); COL(28)=COL(7)+3
            VAL(35)=AT(IJK); COL(35)=COL(7)+4
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I35,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....SOUTH/NORTH BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM,NJM-2
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP+4
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1,  
     *        -1,-1,-1,IJKP+4,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=APT(IJK)
        VAL(32)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=ABT(IJK); COL(22)=COL(1)+3
            VAL(29)=AB(IJK); COL(29)=COL(1)+4
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AWT(IJK); COL(23)=COL(2)+3
            VAL(30)=AW(IJK); COL(30)=COL(2)+4
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-NOEQ
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AST(IJK); COL(24)=COL(3)+3
            VAL(31)=AS(IJK); COL(31)=COL(3)+4
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+NOEQ  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=ANT(IJK); COL(26)=COL(5)+3
            VAL(33)=AN(IJK); COL(33)=COL(5)+4
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AET(IJK); COL(27)=COL(6)+3
            VAL(34)=AE(IJK); COL(34)=COL(6)+4
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=ATT(IJK); COL(28)=COL(7)+3
            VAL(35)=AT(IJK); COL(35)=COL(7)+4
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I35,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....BOTTOM/TOP BOUNDARIES
C
#if defined( USE_2DCASE )
      DO K=2,NKM
#else
      DO K=2,NKM,NKM-2
#endif
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
        ROW=IJKP+4
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1,  
     *        -1,-1,-1,IJKP+4,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=APT(IJK)
        VAL(32)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*NOEQ
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=ABT(IJK); COL(22)=COL(1)+3
            VAL(29)=AB(IJK); COL(29)=COL(1)+4
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*NOEQ
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AWT(IJK); COL(23)=COL(2)+3
            VAL(30)=AW(IJK); COL(30)=COL(2)+4
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-NOEQ
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AST(IJK); COL(24)=COL(3)+3
            VAL(31)=AS(IJK); COL(31)=COL(3)+4
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+NOEQ  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=ANT(IJK); COL(26)=COL(5)+3
            VAL(33)=AN(IJK); COL(33)=COL(5)+4
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*NOEQ
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AET(IJK); COL(27)=COL(6)+3
            VAL(34)=AE(IJK); COL(34)=COL(6)+4
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*NOEQ
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=ATT(IJK); COL(28)=COL(7)+3
            VAL(35)=AT(IJK); COL(35)=COL(7)+4
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I35,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....SET THE COEFFICIENTS FOR VELOCITY-TO-TEMPERATURE COUPLING
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
C
        VAL3(1)=APUT(IJK)
        VAL3(2)=APVT(IJK)
        VAL3(3)=APWT(IJK)
        ROW3=(/IJKP,IJKP+1,IJKP+2/)
        COL1=IJKP+4
C
        CALL MatSetValues(CMAT,I3,ROW3,I1,COL1,VAL3,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C
      END DO
C
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(APTVEC,APTARR,APTTI,IERR)
      CALL VecRestoreArray(APUVEC,APUARR,APUUI,IERR)
      CALL VecRestoreArray(APVVEC,APVARR,APVVI,IERR)
      CALL VecRestoreArray(APWVEC,APWARR,APWWI,IERR)
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRGL(I)
C
        ROW=(IJKPRC+IJP-1)*NOEQ+4
        DO C=0,4
          COL5(C+1)=(IJN-1)*NOEQ+C
        ENDDO
        VAL5=(/ARU(I),ARV(I),ARW(I),ART(I),AR(I)/)
        CALL MatSetValues(CMAT,I1,ROW,I5,COL5,VAL5,INSERT_VALUES,IERR)
C
        ROW=(IJN-1)*NOEQ+4
        DO C=0,4
          COL5(C+1)=(IJKPRC+IJP-1)*NOEQ+C
        ENDDO
        VAL5=(/ALU(I),ALV(I),ALW(I),ALT(I),AL(I)/)
        CALL MatSetValues(CMAT,I1,ROW,I5,COL5,VAL5,INSERT_VALUES,IERR)
C
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE INTERNAL BOUNDARIES)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJRGL(NOCBKAL+I)
C
        ROW=(IJKPRC+IJP-1)*NOEQ+4
        DO C=0,4
          COL5(C+1)=(IJN-1)*NOEQ+C
        ENDDO
        VAL5=(/ARFU(I),ARFV(I),ARFW(I),ARFT(I),AFR(I)/)
        CALL MatSetValues(CMAT,I1,ROW,I5,COL5,VAL5,INSERT_VALUES,IERR)
C
        ROW=(IJN-1)*NOEQ+4
        DO C=0,4
          COL5(C+1)=(IJKPRC+IJP-1)*NOEQ+C
        ENDDO
        VAL5=(/ALFU(I),ALFV(I),ALFW(I),ALFT(I),AFL(I)/)
        CALL MatSetValues(CMAT,I1,ROW,I5,COL5,VAL5,INSERT_VALUES,IERR)
C
      END DO
C
C
C.....FINAL MATRIX ASSEMBLY
C
      CALL MatAssemblyBegin(CMAT,MAT_FINAL_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FINAL_ASSEMBLY,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
#else
C#############################################################
      SUBROUTINE ASSEMBLESYSEN(CMAT)
C#############################################################
C     This routine assembles the PETSc Matrix of the linear
C     subsystem corresponding to the energy coupling and the 
C     energy balance.
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,J,K,M,IJK,IJKP,IJP,IJN
      INTEGER IJRGL(NOCA+NFA)
C
      PetscInt       I1,I3,I7,ROW,COL(7),COL1,ROW3(3)
      PetscScalar    VAL(7),VAL1,PONE,VAL3(3)
      PetscErrorCode IERR
      Mat CMAT
C
      COMMON /MAPPING/ IJRGL
C
#include "petsc.user.inc"
C=============================================================
C     
      I1=1
      I3=3
      I7=7
      PONE=1.0D0
C.....AVOID FLOATING POINT EXCEPTIONS IN SMALL CASES WITH ONLY BOUNDARY CELLS
      VAL=0.0D0
      VAL3=0.0D0
      COL=-1
      COL1=-1
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
C
      DO M=1,NBLKS
      CALL SETIND(M)
C        
C.....ASSEMBLE MATRIX
C
      DO K=3,NKM-1
      DO I=3,NIM-1
      DO J=3,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ+NOEQ-1
        ROW=IJKP
C       
        COL(1)=IJKP-NIJ*NOEQ; COL(2)=IJKP-NJ*NOEQ; COL(3)=IJKP-1*NOEQ
        COL(4)=IJKP
        COL(5)=IJKP+1*NOEQ; COL(6)=IJKP+NJ*NOEQ; COL(7)=IJKP+NIJ*NOEQ
C       
        VAL(1)=AB(IJK); VAL(2)=AW(IJK); VAL(3)=AS(IJK)
        VAL(4)=AP(IJK)
        VAL(5)=AN(IJK); VAL(6)=AE(IJK); VAL(7)=AT(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C        
C.....SOUTH/NORTH BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM,NJM-2
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ+NOEQ-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
      IF(AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*NOEQ; ENDIF
      IF(AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ *NOEQ; ENDIF
      IF(AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  *NOEQ; ENDIF
      IF(AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  *NOEQ; ENDIF
      IF(AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ *NOEQ; ENDIF
      IF(AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*NOEQ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....WEST/EAST BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM,NIM-2
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ+NOEQ-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
      IF(AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*NOEQ; ENDIF
      IF(AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ *NOEQ; ENDIF
      IF(AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  *NOEQ; ENDIF
      IF(AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  *NOEQ; ENDIF
      IF(AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ *NOEQ; ENDIF
      IF(AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*NOEQ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....BOTTOM/TOP BOUNDARIES
C
#if defined( USE_2DCASE )
      DO K=2,NKM
#else
      DO K=2,NKM,NKM-2
#endif
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ+NOEQ-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
      IF(AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*NOEQ; ENDIF
      IF(AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ *NOEQ; ENDIF
      IF(AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  *NOEQ; ENDIF
      IF(AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  *NOEQ; ENDIF
      IF(AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ *NOEQ; ENDIF
      IF(AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*NOEQ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....SET THE COEFFICIENTS FOR VELOCITY-TO-TEMPERATURE COUPLING
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*NOEQ
C
        VAL3(1)=APUT(IJK)
        VAL3(2)=APVT(IJK)
        VAL3(3)=APWT(IJK)
        ROW3=(/IJKP,IJKP+1,IJKP+2/)
        COL1=IJKP+4
C
        CALL MatSetValues(CMAT,I3,ROW3,I1,COL1,VAL3,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C
      END DO
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRGL(I)
C
        ROW=(IJKPRC+IJP-1)*NOEQ+NOEQ-1
        COL1=(IJN-1)*NOEQ+NOEQ-1
        VAL1=AR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=(IJN-1)*NOEQ+NOEQ-1
        COL1=(IJKPRC+IJP-1)*NOEQ+NOEQ-1
        VAL1=AL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE INTERNAL BOUNDARIES)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJRGL(NOCBKAL+I)
C
        ROW=(IJKPRC+IJP-1)*NOEQ+NOEQ-1
        COL1=(IJN-1)*NOEQ+NOEQ-1
        VAL1=AFR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=(IJN-1)*NOEQ+NOEQ-1
        COL1=(IJKPRC+IJP-1)*NOEQ+NOEQ-1
        VAL1=AFL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
      END DO
C
C.....FINAL MATRIX ASSEMBLY
C
      CALL MatAssemblyBegin(CMAT,MAT_FINAL_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FINAL_ASSEMBLY,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
#endif
C#############################################################
      SUBROUTINE ASSEMBLESYSSC(CMAT)
C#############################################################
C     This routine assembles the PETSc Matrix of the linear
C     system to be solved
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,J,K,M,IJK,IJKP,IJP,IJN
      INTEGER IJRGL(NOCA+NFA)
C
      PetscInt       I1,I7,ROW,COL(7),COL1
      PetscScalar    VAL(7),VAL1,PONE
      PetscErrorCode IERR
      Mat CMAT
C
      COMMON /MAPPING/ IJRGL
C
#include "petsc.user.inc"
C=============================================================
C     
      I1=1
      I7=7
      PONE=1.0D0
C.....AVOID FLOATING POINT EXCEPTIONS IN SMALL CASES WITH ONLY BOUNDARY CELLS
      VAL=0.0D0
      COL=-1
      COL1=-1
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
C
      DO M=1,NBLKS
      CALL SETIND(M)
C
C.....INITIALIZE BOUNDARY ENTRIES SOUTH/NORH
C
      DO K=1,NK
      DO I=1,NI
      DO J=1,NJ,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        ROW=IJK-1
        CALL MatSetValues(CMAT,I1,ROW,I1,ROW,PONE,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C
C.....INITIALIZE BOUNDARY ENTRIES WEST/EAST. Pass the entries that already have been considered
C
      DO K=1,NK
      DO I=1,NI,NI-1
      DO J=2,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        ROW=IJK-1
        CALL MatSetValues(CMAT,I1,ROW,I1,ROW,PONE,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C
C.....INITIALIZE BOUNDARY ENTRIES BOTTOM/TOP. Pass the entries that already have been considered
C
      DO K=1,NK,NK-1
      DO I=2,NI-1
      DO J=2,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        ROW=IJK-1
        CALL MatSetValues(CMAT,I1,ROW,I1,ROW,PONE,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C        
C.....ASSEMBLE MATRIX
C
      DO K=3,NKM-1
      DO I=3,NIM-1
      DO J=3,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        ROW=IJKP
C       
        COL(1)=IJKP-NIJ; COL(2)=IJKP-NJ; COL(3)=IJKP-1
        COL(4)=IJKP
        COL(5)=IJKP+1; COL(6)=IJKP+NJ; COL(7)=IJKP+NIJ
C       
        VAL(1)=AB(IJK); VAL(2)=AW(IJK); VAL(3)=AS(IJK)
        VAL(4)=AP(IJK)
        VAL(5)=AN(IJK); VAL(6)=AE(IJK); VAL(7)=AT(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C        
C.....SOUTH/NORTH BOUNDARIES
C
      IF (NJM.EQ.2) NJM=3
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM,NJM-2
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
        IF (AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ; ENDIF
        IF (AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ ; ENDIF
        IF (AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  ; ENDIF
        IF (AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  ; ENDIF
        IF (AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ ; ENDIF
        IF (AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....WEST/EAST BOUNDARIES
C
      IF (NIM.EQ.2) NIM=3
      DO K=2,NKM
      DO I=2,NIM,NIM-2
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
        IF (AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ; ENDIF
        IF (AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ ; ENDIF
        IF (AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  ; ENDIF
        IF (AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  ; ENDIF
        IF (AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ ; ENDIF
        IF (AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....BOTTOM/TOP BOUNDARIES
C
      IF (NKM.EQ.2) NKM=3
      DO K=2,NKM,NKM-2
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
        IF (AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ; ENDIF
        IF (AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ ; ENDIF
        IF (AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  ; ENDIF
        IF (AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  ; ENDIF
        IF (AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ ; ENDIF
        IF (AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C
      END DO
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRGL(I)
C
        ROW=IJKPRC+IJP-1
        COL1=IJN-1
        VAL1=AR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=IJN-1
        COL1=IJKPRC+IJP-1
        VAL1=AL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE INTERNAL BOUNDARIES)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJRGL(NOCBKAL+I)
C
        ROW=IJKPRC+IJP-1
        COL1=IJN-1
        VAL1=AFR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=IJN-1
        COL1=IJKPRC+IJP-1
        VAL1=AFL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
      END DO
C
C.....FINAL MATRIX ASSEMBLY
C
      CALL MatAssemblyBegin(CMAT,MAT_FINAL_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FINAL_ASSEMBLY,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE SOLVESYS(FIVEC,IFI,CMAT,RHSVEC)
C#############################################################
C     This routine solves the linear system for momentum and
C     pressure
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER IFI,LS,LC
C
      Vec FIVEC,VT1,VT2,RES,RHSVEC
      KSP KRYLOV,KRYLOVP,KRYLOVPIN
      PC PRECON,PRECONP
      PetscReal RTOL,RINIT
      MatNullSpace NULLSP
      PetscErrorCode IERR
      PetscOffset FIII
      PetscBool ISNULL
      Mat CMAT,DMAT
      INTEGER IJRGL(NOCA+NFA)
      INTEGER I,J,K,IJK,M
      COMMON /OUTER/ LS
      COMMON /CORRECTOR/ LC
      COMMON /PTEMP/ VT1,VT2,RES,
     *               KRYLOV,KRYLOVP,PRECON,PRECONP
      COMMON /MAPPING/ IJRGL
      PetscInt       I1,I7,ROW,COL(7),COL1
      PetscScalar    VAL(7),VAL1,PONE
      CHARACTER(LEN=100) PREFIX
      REAL*8 FIARR(1)
#if defined( USE_EXPORT )
      PetscViewer BINVIEW
#endif
#include "petsc.user.inc"
C     
C=============================================================
C
      PONE=1.0D0
C
      IF (INISOL) THEN
        INISOL=.FALSE.
        CALL VecDuplicate(RHSVEC,VT1,IERR)
        CALL VecDuplicate(RHSVEC,VT2,IERR)
C
        CALL KSPCreate(PETSC_COMM_WORLD,KRYLOV,IERR)
        WRITE(PREFIX,"(A13)") "coupledsolve_"
        CALL KSPSetOptionsPrefix(KRYLOV,PREFIX,IERR)
      END IF
C
      CALL MatZeroRows(CMAT,NZERO,ZEROS,PONE,FIVEC,RHSVEC,IERR)
      CALL KSPSetOperators(KRYLOV,CMAT,CMAT,IERR)
C
#if defined( USE_EXPORT )
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                        'coupledmat',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL MatView(CMAT,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                        'coupledrhs',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL VecView(RHSVEC,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                        'coupledfi',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL VecView(FIVEC,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
#endif
C
C......CALCULATE RESIDUAL
C
      CALL MatMult(CMAT,FIVEC,VT1,IERR)
      CALL VecCopy(RHSVEC,VT2,IERR)
      CALL VecAXPY(VT2,-1.0D0,VT1,IERR)
      CALL VecNorm(VT2,NORM_2,RINIT,IERR)
      IF (LS.EQ.1) THEN
        IF (.NOT.LREAD) THEN
          RESINI(IFI)=RINIT
          RESFIN(IFI)=RINIT*SORMAX
          RESOR(IFI)=RINIT/(RESINI(IFI)+SMALL)
        ELSE
          RINIT=RESINI(IFI)
          RESFIN(IFI)=RINIT*SORMAX
        END IF
        RTOL=SOR(IFI)
      ELSE                                                     
        IF (RINIT.GT.RESINI(IFI)) RESINI(IFI)=RINIT
        RESOR(IFI)=RINIT/(RESINI(IFI)+SMALL)
        RTOL=RESOR(IFI)*SOR(IFI)
C       IF (RTOL.GT.SOR(IFI)) RTOL=SOR(IFI)
      END IF
C     IF (RINIT/(RESINI(IFI)+SMALL).LT.SORMAX) RETURN
C
#if defined( USE_RESINFO ) 
      IF( RANK.EQ.0) WRITE(*,*) "RESIDUAL BEFORE SOLVE: ", RINIT
#endif
C
C.....SOLVE COUPLED SYSTEM
C
C
C.....CREATE NULLSPACE - PRESSURE MIGHT BE SINGULAR (DIM(KER(A))=1)
C
#if !defined( USE_ZEROS ) 
#if defined( USE_DIRICHLETPRESSURE )
      IF (NOUTBKAL.LE.0) THEN
#endif
        CALL MatNullSpaceCreate(
     *       PETSC_COMM_WORLD,PETSC_FALSE,1,NSPANVEC,NULLSP,IERR)
C       CALL KSPSetNullSpace(KRYLOV,NULLSP,IERR)
        CALL MatSetNullSpace(CMAT,NULLSP,IERR)
#if defined( USE_INFO )
        CALL MatNullSpaceTest(NULLSP,CMAT,ISNULL,IERR)
        IF (RANK.EQ.0) THEN
          WRITE(*,*) "ISNULL", ISNULL
          WRITE(*,*) "COUPLED SOLVE"
        END IF
#endif
#if defined( USE_DIRICHLETPRESSURE )
      END IF
#endif
#endif
      CALL KSPSetTolerances(KRYLOV,
     *                      1d-90, 
     *                      RINIT*SOR(IFI),
     *                      PETSC_DEFAULT_REAL,
     *                      PETSC_DEFAULT_INTEGER,
     *                      IERR)
      CALL KSPSetFromOptions(KRYLOV,IERR)
      CALL KSPSolve(KRYLOV,RHSVEC,FIVEC,IERR)
      IF (LS.EQ.1) CALL KSPView(KRYLOV,PETSC_VIEWER_STDOUT_WORLD,IERR)
      CALL KSPSetInitialGuessNonzero(KRYLOV,PETSC_TRUE,IERR)
C
#if defined( USE_EXPORT )
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                    'coupledfi.post',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL VecView(FIVEC,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
#endif
C
#if defined( USE_RESINFO )
      CALL MatMult(CMAT,FIVEC,VT1,IERR)
      CALL VecCopy(RHSVEC,VT2,IERR)
      CALL VecAXPY(VT2,-1.0D0,VT1,IERR)
      CALL VecNorm(VT2,NORM_2,RINIT,IERR)
      IF( RANK.EQ.0) WRITE(*,*) "RESIDUAL AFTER SOLVE: ", RINIT
#endif
C
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE SOLVESYSSC(FIVEC,IFI,CMAT,RHSVEC)
C#############################################################
C     This routine solves the linear systems for momentum or
C     pressure correction
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER IFI,LS,LC
      INTEGER I,J,K,IJK,M
      REAL*8 FIARR(1)
      CHARACTER(LEN=100) PREFIX
      LOGICAL ISANULL
C
      Vec FIVEC,RHSVEC,VT1SC,VT2SC,RESSC
      KSP KRYLOVSC
      PetscReal RTOL,RINIT
      PetscErrorCode IERR
      Mat CMAT
      PetscScalar    PONE
#if defined( USE_EXPORT )
      PetscViewer BINVIEW
#endif
C
      COMMON /OUTER/ LS
      COMMON /SCTEMP/ KRYLOVSC,VT1SC,VT2SC,RESSC
C
#include "petsc.user.inc"
C     
C=============================================================
C
#if defined( USE_EXPORT )
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                           'enmat',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL MatView(CMAT,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                           'enrhs',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL VecView(RHSVEC,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                           'enfi',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL VecView(FIVEC,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
#endif
      PONE=1.0D0
      IF (INISOLSC) THEN
        INISOLSC=.FALSE.
        CALL VecDuplicate(FIVEC,VT1SC,IERR)
        CALL VecDuplicate(FIVEC,VT2SC,IERR)
C
        CALL KSPCreate(PETSC_COMM_WORLD,KRYLOVSC,IERR)
        WRITE(PREFIX,"(A12)") "scalarsolve_"
        CALL KSPSetOptionsPrefix(KRYLOVSC,PREFIX,IERR)
C
      END IF
C
C.....SET OPERATORS, REUSE PRECONDITIONING MATRIX IN SUCCESSIVE MOMENTUM
C.....SOLVES OR PRESSURE CORRECTOR LOOPS WITHIN THE SAME OUTER ITERATION
C
C     IF (IFI.NE.1) THEN
C       CALL KSPSetReusePreconditioner(KRYLOV,PETSC_TRUE,IERR)
C     ELSE
C
C       IF (LS.NE.1) CALL MatDestroy(C2,IERR)
C       CALL MatConvert(CMAT,MATSAME,MAT_INITIAL_MATRIX,C2,IERR)
C       CALL KSPSetReusePreconditioner(KRYLOV,PETSC_FALSE,IERR)
C     END IF
C     CALL KSPSetOperators(KRYLOV,CMAT,C2,IERR)
      CALL MatZeroRows(CMAT,NZEROSC,ZEROSSC,PONE,FIVEC,RHSVEC,IERR)
      CALL KSPSetOperators(KRYLOVSC,CMAT,CMAT,IERR)
C
      CALL MatMult(CMAT,FIVEC,VT1SC,IERR)
      CALL VecCopy(RHSVEC,VT2SC,IERR)
      CALL VecAXPY(VT2SC,-1.0D0,VT1SC,IERR)
      CALL VecNorm(VT2SC,NORM_2,RINIT,IERR)
C     IF (LS.EQ.1.AND.LC.LE.1) THEN 
C NEED TO IMPLEMENT NON-ORTHOGONAL CORRECTOR LOOP
      IF (LS.EQ.1) THEN
        IF (.NOT.LREAD) THEN
          RESINI(IFI)=RINIT
          RESFIN(IFI)=RINIT*SORMAX
          RESOR(IFI)=RINIT/(RESINI(IFI)+SMALL)
        ELSE
          RINIT=RESINI(IFI)
          RESFIN(IFI)=RINIT*SORMAX
        END IF
      ELSE
        IF (RINIT.GT.RESINI(IFI)) RESINI(IFI)=RINIT
        RESOR(IFI)=RINIT/(RESINI(IFI)+SMALL)
      END IF
      RTOL=RESOR(IFI)*SOR(IFI)
C IST DIESE ZEILE WIRKLICH NÖTIG?
C     IF (RINIT.LT.SORMAX.AND.IFI.EQ.6) RETURN
#if defined( USE_RESINFO ) 
      IF (RANK.EQ.0) WRITE(*,*) "RESIDUAL BEFORE SOLVE: ", RINIT
#endif
C
C.....SOLVE MOMENTUM BALANCE
C
      CALL KSPSetTolerances(KRYLOVSC,
     *                      1D-90, 
C    *                      RESFIN(IFI)*1e-2,
     *                      RINIT*SOR(IFI),
     *                      PETSC_DEFAULT_REAL,
     *                      PETSC_DEFAULT_INTEGER,
     *                      IERR)
      CALL KSPSetFromOptions(KRYLOVSC,IERR)
#if defined( USE_INFO )
      IF (RANK.EQ.0) WRITE(*,*) "SCALAR SOLVE"
#endif
      CALL KSPSolve(KRYLOVSC,RHSVEC,FIVEC,IERR)
      IF (LS.EQ.1) CALL KSPView(KRYLOVSC,PETSC_VIEWER_STDOUT_WORLD,IERR)
      CALL KSPSetInitialGuessNonzero(KRYLOVSC,PETSC_TRUE,IERR)
C
#if defined( USE_RESINFO )
      CALL MatMult(CMAT,FIVEC,VT1SC,IERR)
      CALL VecCopy(RHSVEC,VT2SC,IERR)
      CALL VecAXPY(VT2SC,-1.0D0,VT1SC,IERR)
      CALL VecNorm(VT2SC,NORM_2,RINIT,IERR)
      IF( RANK.EQ.0) WRITE(*,*) "RESIDUAL AFTER SOLVE: ", RINIT
#endif
C
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE DESTROYPETSC
C#############################################################
C     This routine destroys the PETSc Matrix of the linear
C     system to be solved and objects related to SOLVESYS
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "gradold3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      Vec VT1,VT2,RES,VT1SC,VT2SC,RESSC
      KSP KRYLOV,KRYLOVP,KRYLOVSC
      PC PRECON,PRECONP
      PetscErrorCode IERR
      COMMON /PTEMP/ VT1,VT2,RES,
     *               KRYLOV,KRYLOVP,PRECON,PRECONP
      COMMON /SCTEMP/ KRYLOVSC,VT1SC,VT2SC,RESSC
C     
C=============================================================
C
C.....DESTROY VECTORS
C
      CALL VecDestroy(UVWPVEC,IERR)
C
      CALL VecDestroy(DUXVEC,IERR)
      CALL VecDestroy(DUYVEC,IERR)
      CALL VecDestroy(DUZVEC,IERR)
C
      CALL VecDestroy(DVXVEC,IERR)
      CALL VecDestroy(DVYVEC,IERR)
      CALL VecDestroy(DVZVEC,IERR)
C
      CALL VecDestroy(DWXVEC,IERR)
      CALL VecDestroy(DWYVEC,IERR)
      CALL VecDestroy(DWZVEC,IERR)
C
      CALL VecDestroy(DPXVEC,IERR)
      CALL VecDestroy(DPYVEC,IERR)
      CALL VecDestroy(DPZVEC,IERR)
C
#ifndef USE_ENERGYCOUPLING
      CALL VecDestroy(TVEC,IERR)
      CALL VecDestroy(SSCVEC,IERR)
#endif
      CALL VecDestroy(DSCXVEC,IERR)
      CALL VecDestroy(DSCYVEC,IERR)
      CALL VecDestroy(DSCZVEC,IERR)
#if defined( USE_NEWTONRAPHSON ) 
      CALL VecDestroy(APTVEC,IERR)
#endif
C
      CALL VecDestroy(DFXOVEC,IERR)
      CALL VecDestroy(DFYOVEC,IERR)
      CALL VecDestroy(DFZOVEC,IERR)
C
      CALL VecDestroy(DENVEC,IERR)
      CALL VecDestroy(VISVEC,IERR)
      CALL VecDestroy(VOLVEC,IERR)
C
      CALL VecDestroy(XCVEC,IERR)
      CALL VecDestroy(YCVEC,IERR)
      CALL VecDestroy(ZCVEC,IERR)
C
      CALL VecDestroy(SUVWPVEC,IERR)
      CALL VecDestroy(APVEC,IERR)
      CALL VecDestroy(APRVEC,IERR)
      CALL VecDestroy(APUVEC,IERR)
      CALL VecDestroy(APVVEC,IERR)
      CALL VecDestroy(APWVEC,IERR)
C
      CALL VecDestroy(VT1  ,IERR)
      CALL VecDestroy(VT2  ,IERR)
      CALL VecDestroy(RES  ,IERR)
      CALL VecDestroy(VT1SC,IERR)
      CALL VecDestroy(VT2SC,IERR)
      CALL VecDestroy(RESSC,IERR)
C
C.....DESTROY MATRICES
C
      CALL MatDestroy(AMAT,IERR)
      CALL MatDestroy(SCMAT,IERR)
C
C.....DESTROY KSP
C
      CALL KSPDestroy(KRYLOV,IERR) 
      CALL KSPDestroy(KRYLOVSC,IERR)
C
#ifndef USE_ZEROS
      IF (NOUTBKAL.LT.0) CALL VecDestroy(NSPANVEC,IERR)
#endif
C
      RETURN
      END
C
C
C#############################################################
      SUBROUTINE DEBUGME(RNK)
C#############################################################
C     This routine serves only for debugging purposes using
C     more than one mpi process
C=============================================================
C
C INCLUDE NECESSARY SYSTEM ROUTINES LIKE GETPIT AND HOSTNM
#if defined( USE_INTEL_COMPILER )
        USE IFPORT
#endif
        IMPLICIT NONE
#include "mpif.h"
        CHARACTER*20 HNAME
        INTEGER IDX,PID,HSTAT,RNK
C
        HSTAT = HOSTNM(HNAME)
        PID = GETPID()
        WRITE(*,"(A,I7,A,A,I7,A,A,A)")
     *              "RANK ", RNK, ": ",
     *              "PID ",PID,
     *              " ON HOST ",TRIM(HNAME),
     *              " IS READY FOR ATTACH"
        IDX = 0
500     IF (IDX == 0) THEN
          CALL SLEEP(5)
          GOTO 500
        END IF
C
        RETURN
        END SUBROUTINE DEBUGME
C
C
