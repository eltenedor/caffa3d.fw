C THIS FILE CONTAINS ALL NECESSARY DEFINITIONS TO RUN CAFFA3D.MB.F 
C WITH PETSC VECTORS PROVIDED THAT A PREPROCESSOR IS CALLED BEFORE
C COMPILATION
C
C BE SURE TO INCLUDE THIS FILE AT THE BEGINNING !!AND!! THE END OF EACH
C SUBROUTINE SO THE PREPROCESSOR CAN PERFORM THE NECESSARY CHANGES
C
C.....DEFINE DIRECTIVES
C
#ifndef STARTFUNC
C
#define STARTFUNC
C
#define AP(IJK)    APARR(APPI+IJK)
#define APR(IJK)   APRARR(APRRI+IJK)
#define APT(IJK)   APTARR(APTTI+IJK)
#define APU(IJK)   APUARR(APUUI+IJK)
#define APV(IJK)   APVARR(APVVI+IJK)
#define APW(IJK)   APWARR(APWWI+IJK)
#define DEN(IJK)   DENARR(DENNI+IJK)
#define DFX(IJK)   DFXARR(DFXXI+IJK)
#define DFXO(IJK)  DFXOARR(DFXOOI+IJK)
#define DFY(IJK)   DFYARR(DFYYI+IJK)
#define DFYO(IJK)  DFYOARR(DFYOOI+IJK)
#define DFZ(IJK)   DFZARR(DFZZI+IJK)
#define DFZO(IJK)  DFZOARR(DFZOOI+IJK)
#define DPX(IJK)   DPXARR(DPXXI+IJK)
#define DPY(IJK)   DPYARR(DPYYI+IJK)
#define DPZ(IJK)   DPZARR(DPZZI+IJK)
#define DSCX(IJK)  DSCXARR(DSCXXI+IJK)
#define DSCY(IJK)  DSCYARR(DSCYYI+IJK)
#define DSCZ(IJK)  DSCZARR(DSCZZI+IJK)
#define DUX(IJK)   DUXARR(DUXXI+IJK)
#define DUY(IJK)   DUYARR(DUYYI+IJK)
#define DUZ(IJK)   DUZARR(DUZZI+IJK)
#define DVX(IJK)   DVXARR(DVXXI+IJK)
#define DVY(IJK)   DVYARR(DVYYI+IJK)
#define DVZ(IJK)   DVZARR(DVZZI+IJK)
#define DWX(IJK)   DWXARR(DWXXI+IJK)
#define DWY(IJK)   DWYARR(DWYYI+IJK)
#define DWZ(IJK)   DWZARR(DWZZI+IJK)
#define FI(IJK)    FIARR(FIII+IJK)
#define NSPAN(IJK) NSPANARR(NSSI+IJK)
#define ONNZ(IJK)  ONNZARR(ONNZZI+IJK)
#define ONNZS(IJK) ONNZSARR(ONNZSSI+IJK)
#define PP(IJK)    PPARR(PPPI+IJK)
#define VIS(IJK)   VISARR(VISSI+IJK)
#define VOL(IJK)   VOLARR(VOLLI+IJK)
#define XC(IJK)    XCARR(XCCI+IJK)
#define YC(IJK)    YCARR(YCCI+IJK)
#define ZC(IJK)    ZCARR(ZCCI+IJK)
C
#if defined( USE_SUBVEC )
C
#define U(IJK)     UARR(UUI+IJK)
#define V(IJK)     VARR(VVI+IJK)
#define W(IJK)     WARR(WWI+IJK)
#define P(IJK)     PARR(PPI+IJK)
#define T(IJK)     TARR(TTI+IJK)
C
#define SU(IJK)    SUARR(SUUI+IJK)
#define SV(IJK)    SVARR(SVVI+IJK)
#define SW(IJK)    SWARR(SWWI+IJK)
#define SP(IJK)    SPARR(SPPI+IJK)
#define ST(IJK)    STARR(STTI+IJK)
#define SSC(IJK)   SSCARR(SSCCI+IJK)
C
#else
C
#define U(IJK)     UVWPARR(UVWPPI+(IJK-1)*NOEQ+1)
#define V(IJK)     UVWPARR(UVWPPI+(IJK-1)*NOEQ+2)
#define W(IJK)     UVWPARR(UVWPPI+(IJK-1)*NOEQ+3)
#define P(IJK)     UVWPARR(UVWPPI+(IJK-1)*NOEQ+4)
C
#if defined( USE_ENERGYCOUPLING )
#define T(IJK)     UVWPARR(UVWPPI+(IJK-1)*NOEQ+5)
#else
#define T(IJK)     TARR(TTI+IJK)
#endif
C
#define SU(IJK)    SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+1)
#define SV(IJK)    SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+2)
#define SW(IJK)    SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+3)
#define SP(IJK)    SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+4)
#if defined ( USE_ENERGYCOUPLING )
#define ST(IJK)    SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+5)
#define SSC(IJK)   SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+5)
#else
#define ST(IJK)    STARR(STTI+IJK)
#define SSC(IJK)   SSCARR(SSCCI+IJK)
#endif
C
#endif
C
C.....UNDEFINE DIRECTIVES
C
#else
C
#undef STARTFUNC
C
#undef AP
#undef APR
#undef APT
#undef APU
#undef APV
#undef APW
#undef DEN
#undef DFX
#undef DFXO
#undef DFY
#undef DFYO
#undef DFZ
#undef DFZO
#undef DPX
#undef DPY
#undef DPZ
#undef DUX
#undef DUY
#undef DUZ
#undef DVX
#undef DVY
#undef DVZ
#undef DWX
#undef DWY
#undef DWZ
#undef FI
#undef NSPAN
#undef ONNZ
#undef ONNZS
#undef P
#undef PP
#undef SP
#undef SSC
#undef ST
#undef SU
#undef SV
#undef SW
#undef T
#undef U
#undef V
#undef VIS
#undef VOL
#undef W
#undef XC
#undef YC
#undef ZC
C
#endif
