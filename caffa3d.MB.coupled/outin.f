C###########################################################
      SUBROUTINE OUTIN
C###########################################################
C     This routine prints title and parameters used in
C     computation.
C===========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "logic3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
C
      REAL*8 OMGT
C NEW
#include "petsc.user.inc"
C END NEW
C===========================================================
C
C.....PRINT TITLE, DENSITY, VISCOSITY, CONV. CRITERION, SIP-PARAMETER
C    
      WRITE(2,'(A50)') TITLE
      WRITE(2,*) '====================================================='
      WRITE(2,*) '  '
      WRITE(2,*) '     FLUID DENSITY     : ',DENS
      WRITE(2,*) '     DYNAMIC VISCOSITY : ',VISC
      WRITE(2,*) '     CONVERGENCE CRIT. : ',SORMAX
      WRITE(2,*) '     SIP-PARAMETER     : ',ALFA
      WRITE(2,*) '    '
      WRITE(2,*) '    '
C
C
C
C.....PRINT PARAMETERS FOR ENERGY EQUATION
C
      IF(LCAL(IEN)) THEN
        WRITE(2,*) '     PRANDTL NUMBER        : ',PRANL
        WRITE(2,*) '     HOT WALL TEMPERATURE  : ',TH
        WRITE(2,*) '     COLD WALL TEMPERATURE : ',TC
        WRITE(2,*) '     REFERENCE TEMPERATURE : ',TREF
        WRITE(2,*) '     '
      ENDIF
C
C.....PRINT UNDER-RELAXATION FACTORS
C
      IF(LCAL(IU))  WRITE(2,*) '     UNDER-RELAXATION FOR U: ',URF(IU)
      IF(LCAL(IV))  WRITE(2,*) '     UNDER-RELAXATION FOR V: ',URF(IV)
      IF(LCAL(IWW)) WRITE(2,*) '     UNDER-RELAXATION FOR W: ',URF(IWW)
      IF(LCAL(IP))  WRITE(2,*) '     UNDER-RELAXATION FOR P: ',URF(IP)
      IF(LCAL(IEN)) WRITE(2,*) '     UNDER-RELAXATION FOR T: ',URF(IEN)
      IF(LCAL(ITE)) WRITE(2,*) '     UNDER-RELAXATION FOR TE:',URF(ITE)
      IF(LCAL(IED)) WRITE(2,*) '     UNDER-RELAXATION FOR ED:',URF(IED)
      WRITE(2,*) '     '
C
C.....PRINT BLENDING FACTORS FOR CONVECTIVE FLUXES (CDS-PART)
C
      WRITE(2,*) '     DIFFUSIVE FLUXES DISCRETIZED USING CDS '
      WRITE(2,*) '     CONTRIBUTION OF CDS VS. UDS IN CONV. FLUXES: '
      IF(LCAL(IU))  WRITE(2,*) '     BLENDING FACTOR FOR U: ',GDS(IU)
      IF(LCAL(IV))  WRITE(2,*) '     BLENDING FACTOR FOR V: ',GDS(IV)
      IF(LCAL(IWW)) WRITE(2,*) '     BLENDING FACTOR FOR W: ',GDS(IWW)
      IF(LCAL(IEN)) WRITE(2,*) '     BLENDING FACTOR FOR T: ',GDS(IEN)
      IF(LCAL(ITE)) WRITE(2,*) '     BLENDING FACTOR FOR TE:',GDS(ITE)
      IF(LCAL(IED)) WRITE(2,*) '     BLENDING FACTOR FOR ED:',GDS(IED)
      WRITE(2,*) '     '
C
C.....PRINT CONVERGENCE CRITERION FOR INNER ITERATIONS
C
      IF(LCAL(IU))  WRITE(2,*) '     CONV. CRIT. FOR U: ',SOR(IU)
      IF(LCAL(IV))  WRITE(2,*) '     CONV. CRIT. FOR V: ',SOR(IV)
      IF(LCAL(IWW)) WRITE(2,*) '     CONV. CRIT. FOR W: ',SOR(IWW)
      IF(LCAL(IP))  WRITE(2,*) '     CONV. CRIT. FOR P: ',SOR(IP)
      IF(LCAL(IEN)) WRITE(2,*) '     CONV. CRIT. FOR T: ',SOR(IEN)
      IF(LCAL(ITE)) WRITE(2,*) '     CONV. CRIT. FOR TE:',SOR(ITE)
      IF(LCAL(IED)) WRITE(2,*) '     CONV. CRIT. FOR ED:',SOR(IED)
      WRITE(2,*) '     '
C
C.....PRINT MAXIMUM NUMBER OF INNER ITERATIONS
C
      IF(LCAL(IU))  WRITE(2,*) '     MAX. INNER ITER. FOR U: ',NSW(IU)
      IF(LCAL(IV))  WRITE(2,*) '     MAX. INNER ITER. FOR V: ',NSW(IV)
      IF(LCAL(IWW)) WRITE(2,*) '     MAX. INNER ITER. FOR W: ',NSW(IWW)
      IF(LCAL(IP))  WRITE(2,*) '     MAX. INNER ITER. FOR P: ',NSW(IP)
      IF(LCAL(IEN)) WRITE(2,*) '     MAX. INNER ITER. FOR T: ',NSW(IEN)
      IF(LCAL(ITE)) WRITE(2,*) '     MAX. INNER ITER. FOR TE:',NSW(ITE)
      IF(LCAL(IED)) WRITE(2,*) '     MAX. INNER ITER. FOR ED:',NSW(IED)
      WRITE(2,*) '     '
C
C.....PRINT TIME STEP SIZE, OUTPUT CONTROL, BLENDING FACTOR
C
      IF(LTIME) THEN
        OMGT=1.-GAMT
        WRITE(2,*) '     TIME STEP SIZE:                  ',DT
        WRITE(2,*) '     NUMBER OF TIME STEPS TO PERFORM: ',ITSTEP
        WRITE(2,*) '     SAVE RESULTS EVERY   ',NOTT,' TIME STEPS'
        WRITE(2,*) '     BLENDING ',GAMT,' OF THREE TIME LEVEL SCHEME'
        WRITE(2,*) '         WITH ',OMGT,' OF IMPLICIT EULER SCHEME '
        WRITE(2,*) '     '
      ENDIF
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C###########################################################
      SUBROUTINE OUTRES
C###########################################################
C     This routine prints out the variable fields.
C
C
C===========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "model3d.inc"
C
      INTEGER M
C
#include "petsc.user.inc"
C
C===========================================================
C
C.....PRINT VELOCITIES, PRESSURE AND TEMPERATURE
C
      DO M=1,NBLKS
C     IF(LCAL(IU))  CALL PRINT(M,U,'U VEL.')
C     IF(LCAL(IV))  CALL PRINT(M,V,'V VEL.')
C     IF(LCAL(IWW)) CALL PRINT(M,W,'W VEL.')
C     IF(LCAL(IP))  CALL PRINT(M,P,'PRESS.')
C     IF(LCAL(IEN)) CALL PRINT(M,T,'TEMPER')
      IF(LCAL(IU))  CALL PRINT(M,UVEC,'U VEL.')
      IF(LCAL(IV))  CALL PRINT(M,VVEC,'V VEL.')
      IF(LCAL(IWW)) CALL PRINT(M,WVEC,'W VEL.')
      IF(LCAL(IP))  CALL PRINT(M,PVEC,'PRESS.')
      IF(LCAL(IEN)) CALL PRINT(M,TVEC,'TEMPER')
C     IF(LCAL(ITE)) CALL PRINT(M,TE,'TKENE.')
C     IF(LCAL(IED)) CALL PRINT(M,ED,'EDISI.')
      END DO
C
C.....PRINT MASS FLUXES IF TESTING
C
C     IF(LTEST.AND.LCAL(IU)) CALL PRINT(F1,'MASS_E')
C     IF(LTEST.AND.LCAL(IU)) CALL PRINT(F2,'MASS_N')
C
      RETURN
C
      END
C
#include "petsc.user.inc"
C
C##########################################################
      SUBROUTINE PRINT(M,FIVEC,HEDFI)
C##########################################################
C     This routine prints the field variables in an easy to
C     read form.
C==========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "indexc3d.inc"
C
      INTEGER M,NL,LKK,IEND,L,IBEG,I,J,K
C
      REAL*8 FIARR( 1)
      Vec FIVEC
      PetscOffset FIII
      PetscErrorCode IERR
C
      CHARACTER(LEN=6) HEDFI
C
#include "petsc.user.inc"
C
C===========================================================
C
      CALL VecGetArray(FIVEC,FIARR,FIII,IERR)
C
C.....SET INDICES, PRINT HEADER, FIND HOW MANY 12-COLUMN BLOCKS
C
      CALL SETIND(M)
      WRITE(2,20) HEDFI
C
      NL=NI/12+1
      IF(MOD(NI,12).EQ.0) NL=NL-1
C
C.....PRINT THE ARRAY FI
C
      DO K=1,NK
        LKK=LKBK(K+KST)
        WRITE(2,21) K
        IEND=0
C
        DO L=1,NL
          IBEG=IEND+1
          IEND=MIN(IBEG+11,NI)
          WRITE(2,'(3X,4HI = ,I3,11I10)') (I,I=IBEG,IEND)
          WRITE(2,*) '  J'
          DO J=NJ,1,-1
            WRITE(2,'(1X,I3,1P12E10.2)')
     *            J,(FI(LKK+LIBK(I+IST)+J),I=IBEG,IEND)
          END DO
        END DO
C
      END DO
C
      CALL VecRestoreArray(FIVEC,FIARR,FIII,IERR)
C
   20 FORMAT(2X,26('*-'),6X,A7,6X,26('-*'))
   21 FORMAT(2X,26('*-'),6X,'K=',I4,6X,26('-*'))
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
