C##########################################################
      PROGRAM CAFFA
C##########################################################
C     This version of CAFFA has been extended to 3D.
C     Please see below
CC 
C     This code incorporates the Finite Volume Method using
C     SIMPLE algorithm on colocated body-fitted grids. For
C     a description of the solution method, see the book by
C     Ferziger and Peric (1996) or paper by Demirdzic,
C     Muzaferija and Peric (1996). Description of code 
C     features is provided in the acompanying README-file.
C     This version is based on the laminar code including 
C     the multiple pressure corrections and the effects of 
C     non-smoothness of grid lines when computing gradients
C     (see code caffac.f in directory 2dgl) and includes the 
C     k-eps turbulence model, which was implemented by 
C     Martin Schmid, PhD student at the Institute of
C     Shipbuilding in Hamburg.
C
C     This is Version 1.3 of the code, August 1997.
C
C     The user may modify the code and give it to third
C     parties, provided that an acknowledgement to the
C     source of the original version is retained.
C
C                M. Peric, Hamburg, 1996
C                peric@schiffbau.uni-hamburg.de
C                M. Schmid, Hamburg, 1997
C                schmid@schiffbau.uni-hamburg.de
C     
C     This version has been extended to 3D problems in
C     block-structured grids. The K-e model has been
C     retained and a simple Smagorinsky LES model
C     has been introduced as well.
C
C     However, the functionality of multiple grid
C     levels has been left out. A future
C     extension to multigrid is planed.
C
C     The 'K' index now refers to the third ('Z') direction,
C     and not to the grid level as before.
C
C     Also a new index 'M' has been introduced for the 
C     grid-block numbering. General interfaces, between
C     and within, grid blocks are treated now, in addition
C     to the original OC cuts. The domain is now block
C     structured then.
C
C     Otherwise, the original notation was kept wherever
C     possible, and the book by Ferziger & Peric has been
C     followed as closely as possible for the 3D extension,
C     as well as the multi-block treatment.
C
C     Also an option for the linear solver has been introduced.
C     The code can be compiled to run Stone's SIP Solver
C     or CGSTAB solver. Please see the readme file
C
C     Optional compiler directives for OpenMP parallelization
C     at grid block level have been introduced. Please
C     see the readme file.
C
C     The code has been extended to handle arbitrary block
C     boundaries.
C
C     The code has been parallelized using PETSc.
C
C     25.10.2014
C     Fabian Gabel gabel@mathematik.tu-darmstadt.de
C       
C
C===========================================================
C NB: CAFFA stands for "Computer Aided Fluid Flow Analysis". 
C===========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "charac3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
c
      INTEGER NCASE,IJK,I,ICONT,LS,M
      INTEGER NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT,K,J
C
      INTEGER RCOUNTG,RCOUNT
C
c
      REAL*8 SOURCE,ERRORV,ERRORP,UA,VA,UVA,UVW,HPA,HPA2,
     *              ERRORVG,ERRORPG,
     *              ERRORU,ERRORUG,
     *              ERRORW,ERRORWG,
     *              ERRORA,ERRORAG,
     *              ERRORT,ERRORTG,TA
      REAL*8  LPI,TCA,TCD,WA
      REAL*8 PPOG,VMG,VM
      PARAMETER (LPI=3.141592653589793238462643383279D0)
      INTEGER PID,OMP_GET_THREAD_NUM,NTHREADS,OMP_GET_NUM_THREADS
      INTEGER IJP,IJB,II
      PetscErrorCode IERR
      PetscLogDouble CALTIME,CALTIMS
      Vec UVWPL
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C
C=========================================================
C
C.....SET SOME CONSTANTS
C
      CALL PetscInitialize(PETSC_NULL_CHARACTER,IERR)
      CALL MPI_COMM_RANK(PETSC_COMM_WORLD,RANK,IERR)
      CALL SETDAT
C      CALL MODDAT
C
      F1(1:NXYZA)=0.0D0; F2(1:NXYZA)=0.0D0; F3(1:NXYZA)=0.0D0;
C
C.....READ PROBLEM NAME AND OPEN FILES
C
      IF (RANK.EQ.0) THEN
      WRITE(*,*), ' ENTER PROBLEM NAME (SIX CHARACTERS):  '
      WRITE(*,*) '****************************************************'
      END IF
      NAME='control'
C
      IF (RANK.EQ.0) THEN
      WRITE(*,*)'NAME OF PROBLEM SOLVED ',NAME
      WRITE(*,*) ''
      WRITE(*,*) '****************************************************'
      END IF
C     
      NCASE=INDEX(NAME,' ')
      
      IF(NCASE.NE.ZERO) STOP
      WRITE( FILIN,'(A7,4H.cin)') NAME
      WRITE(FILOUT,'(A7,4H.out)') NAME
      WRITE(FILERR,'(A7,4H.err)') NAME
      WRITE(FILBCK,'(A7,I4.4,4H.bck)') NAME, RANK
      WRITE(FILPRC,'(A7,I4.4,4H.prc)') NAME, RANK
C
      OPEN (UNIT=5,FILE=FILIN,POSITION='REWIND')
      OPEN (UNIT=2,FILE=FILOUT,POSITION='REWIND')
      OPEN (UNIT=4,FILE=FILBCK,FORM='UNFORMATTED',POSITION='REWIND')
      IF (RANK.EQ.0) OPEN (UNIT=23,FILE=FILERR)
      OPEN (UNIT=9,FILE=FILPRC,POSITION='REWIND')
C
C.....INPUT DATA AND INITIALIZATION
C
      CALL INIT
C
C.....INITIAL OUTPUT
C
      CALL OUTIN
      ITIM=0
      TIME=0.0D0
C
C======================================================
C.....READ RESULTS OF PREVIOUS RUN IF REQUIRED
C======================================================
C
      IF(LREAD) THEN
#if defined( USE_INFO )
        IF (RANK.EQ.0) WRITE(*,*) "READING DATA FROM PREVIOUS RUN"
#endif
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGetSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PVEC,IERR)
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
          WRITE(FILRES,'(A7,I4.4,4H.res)') NAME, RANK
          OPEN (UNIT=3,FILE=FILRES,FORM='UNFORMATTED',POSITION='REWIND')
C
          READ(3)ITIM,TIME,(F1(IJK),IJK=1,NIJKBKAL),
     *        (F2(IJK),IJK=1,NIJKBKAL),(F3(IJK),IJK=1,NIJKBKAL),
     *        (U(IJK), IJK=1,NIJKBKAL),(V(IJK), IJK=1,NIJKBKAL),
     *        (W(IJK), IJK=1,NIJKBKAL),(P(IJK), IJK=1,NIJKBKAL),
     *        (T(IJK), IJK=1,NIJKBKAL),(TE(IJK),IJK=1,NIJKBKAL),
     *        (ED(IJK),IJK=1,NIJKBKAL),(FMOC(I),I=1,NOCBKAL),
C
     *        (FMF(I),I=1,NFSGBKAL),(RESINI(I),I=1,4),
     *        (RESOR(I),I=1,4)
C
C
          IF(LTIME) READ(3) (UO(IJK),IJK=1,NIJKBKAL),
     *        (VO(IJK),IJK=1,NIJKBKAL),(WO(IJK),IJK=1,NIJKBKAL),
     *        (TO(IJK),IJK=1,NIJKBKAL),(TEO(IJK),IJK=1,NIJKBKAL),
     *        (EDO(IJK),IJK=1,NIJKBKAL)
          CLOSE(UNIT=3)
C
        ITIM=ITIM-1
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PVEC,IERR)
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
      ELSE
C
C======================================================
C.....START TIME LOOP
C======================================================
C
c
C======================================================
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecGetSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef  USE_ENERGYCOUPLING 
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
C
C Initialize FIELD VALUES
C.....OpenMP : Here starts parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*            NKMT,NIMT,NJMT,KSTT,ISTT)
       DO M=1,NBLKS
         NKMT=NKBK(M)-1
         NIMT=NIBK(M)-1
         NJMT=NJBK(M)-1
         KSTT=KBK(M)
         ISTT=IBK(M)
         NJT=NJMT+1
         NIJT=(NIMT+1)*NJT
      
         DO K=1,NKMT+1 
         DO I=1,NIMT+1
         DO J=1,NJMT+1
           IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
C#if defined( USE_ANALYTICAL )
C           U(IJK)=UMMS(XC(IJK),YC(IJK),ZC(IJK))
C           V(IJK)=VMMS(XC(IJK),YC(IJK),ZC(IJK))
C           W(IJK)=WMMS(XC(IJK),YC(IJK),ZC(IJK))
C           P(IJK)=PMMS(XC(IJK),YC(IJK),ZC(IJK))
C           T(IJK)=TMMS(XC(IJK),YC(IJK),ZC(IJK))
C#else
           U(IJK)=0.0D0
           V(IJK)=0.0D0
           W(IJK)=0.0D0  
           P(IJK)=0.0D0
           T(IJK)=0.0D0
C#endif
         ENDDO
         ENDDO
         ENDDO
      ENDDO
      END IF
C.....OpenMP : Here ends this parallel loop section
C
C.....RESTORE ARRAYS FROM VECTORS
C
#if defined( USE_SUBVEC )
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecRestoreSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef USE_ENERGYCOUPLING 
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
C......UPDATE VELOCITY, MATERIAL PROPERTIES AND TEMPERATURE
C
      CALL VecGhostUpdateBegin(
     *     UVWPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(
     *     UVWPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(
     *     DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(
     *     VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      IF (LCAL(ISC)) THEN
      CALL VecGhostUpdateBegin(TVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(TVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      ENDIF
C
      INIBC=.TRUE.
      INISOL=.TRUE.
      INISOLSC=.TRUE.
      ICONT=0
      ITIMS=ITIM+1
      ITIME=ITIM+ITSTEP
C
      CALL PetscBarrier(UVWPVEC,IERR)
      CALL PetscTime(CALTIMS,IERR)
      DO 400 ITIM=ITIMS,ITIME
      TIME=TIME+DT
C
C.....SHIFT SOLUTIONS IN TIME (OOLD = OLD, OLD = CURRENT)
C
      IF(LTIME) THEN
        UOO=UO; VOO=VO; WOO=WO
C
C.....EXTRACT ARRAYS FROM VECTORS
C
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecGetSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef USE_ENERGYCOUPLING 
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
      DO IJK=1,NIJKBKAL
        UO(IJK)=U(IJK)
        VO(IJK)=V(IJK)
        WO(IJK)=W(IJK)
        TO(IJK)=T(IJK)
      END DO
#if defined(USE_SUBVEC)
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecRestoreSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef USE_ENERGYCOUPLING 
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
       CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
       TOO=TO
       TEOO=TEO; TEO=TE;
       EDOO=EDO; EDO=ED;
C
        WRITE(2,*) '  '
        WRITE(2,*) '  TIME = ',TIME
        WRITE(2,*) '  *****************************'
      ENDIF
C
C.....SET INLET BOUNDARY CONDITIONS
C
      IF(INIBC) CALL BCIN
C
C.....PRINT INITAL FIELDS
C
C      IF(LOUTS.AND.(ITIM.EQ.ITIMS)) CALL OUTRES
C
C.....PRINT INDEX TO MONITORING LOCATION 
C
C     WRITE(2,600) MMON,IMON,JMON,KMON
C
C======================================================
C.....START COUPLED ALGORITHM (OUTER ITERATIONS)
C======================================================
C
      IF (RANK.EQ.0) THEN
        WRITE(*,*) '***************************************************'
        WRITE(*,*) "START COUPLED ALGORITHM"
        WRITE(*,*) '***************************************************'
      END IF
C
      CALL MASSFLUX
      DO LS=1,LSG
#if defined( USE_ANALYTICAL ) && defined( USE_CONTOUTPUT )
       CALL CALCERR
#endif
        IF(LCAL(IU))    CALL CALCUVW
        IF(LCAL(IP))    CALL CALCP
#if defined( USE_ENERGYCOUPLING )
        IF(LCAL(IEN))   CALL CALCEN
#endif
                        CALL PREFERENCE
                        CALL MASSFLUX
        IF(LCAL(ISC))   CALL CALCSC(ISC,TVEC,TO,TOO)
C
C.....Call to LES Smagorinsky routine.
C
C        IF(LCAL(ISMG))  CALL SMAGOR
C
C.....Call to K-e routines
C
C        IF(LCAL(ITE))   CALL CALCSC(ITE,TE,TEO,TEOO)
C        IF(LCAL(IED))   CALL CALCSC(IED,ED,EDO,EDOO)
C        IF(LCAL(IVIS))  CALL MODVIS
C
C.....NORMALIZE RESIDUALS, PRINT RES. LEVELS AND MONITORING VALUES
C
       SOURCE=MAX(RESOR(IU),RESOR(IEN),RESOR(ISC))
        IF (RANK.EQ.0) THEN
          WRITE(*,"(I8.7,4E12.4)"),
C    *          LS,RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP)
#if defined( USE_ENERGYCOUPLING )
     *          LS,RESOR(IU),RESOR(IEN)
#else
     *          LS,RESOR(IU),RESOR(ISC)
#endif
          WRITE(23,"(I8.7,4E12.4)"),
C    *          LS,RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP)
     *          LS,RESOR(IU)
        END IF
C
        IF (.NOT.LTIME.AND.LWRITE
     *                .AND.MOD(LS,NOTT).EQ.0) 
     *    CALL SRES(RANK)
C
       IF (LS.GT.1) THEN
        IF(SOURCE.GT.SLARGE) GO TO 510
        IF(SOURCE.LT.SORMAX) GO TO 250
      ENDIF
      END DO
C
  250 CONTINUE
C
C==========================================================
C.....SAVE SOLUTIONS FOR RE-START OR POSTPROCESSING; OUTPUT
C==========================================================
C
C.....UNSTEADY FLOW - INTERMEDIATE SOLUTIONS:
C
C NEW - OUTPUT STILL NOT IMPLEMENTED COMPLETELY
C END NEW
        IF(LTIME) THEN
C         WRITE(*,606) LS,(RESOR(I),I=1,5),(RESOR(I),I=7,8),
C    *            U(IJKMON),V(IJKMON),W(IJKMON),P(IJKMON),
C    *            T(IJKMON),RXVISC
C
          IF(MOD(ITIM,NOTT).EQ.0.AND.LPOST) THEN
            ICONT=ICONT+1
            CALL POST(ICONT,RANK)
          ENDIF
        ENDIF
C
  400 CONTINUE
      IF (RANK.EQ.0) THEN
        CALL PetscTime(CALTIME,IERR)
        WRITE(*,"(A,E12.4)"), "TIME FOR CALCULATION:", CALTIME-CALTIMS
        WRITE(23,"(A,E12.4)") ,"TIME FOR CALCULATION:",CALTIME-CALTIMS
      END IF
C
C.....STEADY FLOW, OR UNSTEADY FLOW - LAST TIME STEP: PRINT AND
C     SAVE RESULTS 
C
       ICONT=ICONT+1
       IF(LOUTE) CALL OUTRES
       IF (LWRITE) CALL SRES(RANK)
       IF (LPOST) CALL POST(ICONT,RANK)
       ITIM=0.0D0
       TIME=0.0D0
C
#if defined( USE_ANALYTICAL )
       CALL CALCERR
#endif
C
C==========================================================
C.....CLOSE FILES, FORMATS
C==========================================================
C
      CLOSE(UNIT=8)
      CLOSE(UNIT=3)
      CLOSE(UNIT=4)
      CLOSE(UNIT=2)
      CLOSE(UNIT=5)
      IF (RANK.EQ.0) CLOSE(UNIT=23)
C
      IF(RANK.EQ.0)
     *  WRITE(*,*),'     *** CALCULATION FINISHED - SEE RESULTS ***'
C
      CALL DESTROYPETSC
      CALL PetscFinalize(IERR)
C
      STOP
C
C.....MESSAGE FOR DIVERGENCE 
C
  510 IF (RANK.EQ.0) 
     * WRITE(*,*),'     *** TERMINATED - OUTER ITERATIONS DIVERGING ***'
C
      CALL DESTROYPETSC
      CALL PetscFinalize(IERR)
C
#include "petsc.user.inc"
C
C.....FORMAT SPECIFICATIONS
C
  600 FORMAT('IT',1X,
     * 'I-------------ABSOLUTE RESIDUAL SOURCE SUMS------------I',
     * 2X,
     * 'I--VALUES AT MONITOR POINT(',1X,I2,',',I3,',',I3,',',I3,
     * ')--I',/,
     * 'Nº',1X,2X,'UMOM',4X,'VMOM',4X,'WMOM',4X,'MASS',4X,
     *            'ENER',4X,'TKEN',4X,'TDIS',2X,
     * 2X,3X,'U' ,7X,'V',7X,'W',7X,'P',7X,'T',5X,'RXVIS')
  606 FORMAT(I6.6,1X,1P,7E8.1,2X,1P,6E8.1)
C
C
      END
C
C
C#########################################################
      SUBROUTINE CALCUVW
C#########################################################
C     This routine discretizes and solves the linearized
C     equations for X, Y and Z momentum componentS (U, V 
C     and W Cartesian velocity components).
C
C
C=========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER M
      INTEGER K,J,I,IJK,II,IJB,IJP,IO,IW,ISY,IJN,MIJ
      INTEGER LS
C
      REAL*8 GU,SB,APT,CP,CB,VISS,COEF,ARE,UPB,VPB,WPB
C 
      REAL*8 CPU,CBU,CPV,CBV,CPW,CBW
C 
      REAL*8 VNP,XTP,YTP,ZTP,VISOL,YNP,ZNP,XNP,FDE,DPB
      REAL*8  LPI
      PARAMETER (LPI=3.141592653589793238462643383279D0) 
C
      Vec SUL,SVL,SWL,SPL,APL,
     *    UL,VL,WL,PL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL
C
      Vec UVWPL,SUVWPL
      Vec APUL,APVL,APWL
C
      PetscScalar PZERO,PONE
      PetscInt psize
      PetscErrorCode IERR
      REAL*8 FMOUT
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C
C=========================================================
C
      PZERO=0.0D0
      PONE=1.0D0
      FMOUT=0.0D0
C
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
C.....CALCULATE GRADIENT VECTOR COMPONENTS AT CV-CENTER FOR U, V W, & P
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGhostGetLocalForm(SUVWPVEC,SUVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UL,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VL,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WL,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PL,IERR)
C
      CALL VecGetSubVector(SUVWPL,ISU,SUL,IERR)
      CALL VecGetSubVector(SUVWPL,ISV,SVL,IERR)
      CALL VecGetSubVector(SUVWPL,ISW,SWL,IERR)
C
      CALL VecSet(SUL,PZERO,IERR)
      CALL VecSet(SVL,PZERO,IERR)
      CALL VecSet(SWL,PZERO,IERR)
C
      CALL VecGetArray(UL,UARR,UUI,IERR)
      CALL VecGetArray(VL,VARR,VVI,IERR)
      CALL VecGetArray(WL,WARR,WWI,IERR)
      CALL VecGetArray(PL,PARR,PPI,IERR)
C
      CALL VecGetArray(SUL,SUARR,SUUI,IERR)
      CALL VecGetArray(SVL,SVARR,SVVI,IERR)
      CALL VecGetArray(SWL,SWARR,SWWI,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
      CALL VecSet(SUVWPL,PZERO,IERR)
      CALL VecGetArray(SUVWPL,SUVWPARR,SUVWPPI,IERR)
#endif
      IF(LCAL(ISC)) CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
C.....GET ARRAY OF VECTOR VALUES: VELOCITY, GRADIENT COMPONENTS, DENS, VISC
C
      CALL VecGhostGetLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostGetLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostGetLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostGetLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostGetLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostGetLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostGetLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostGetLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostGetLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecGetArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecGetArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecGetArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecGetArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecGetArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecGetArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecGetArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecGetArray(DWZL,DWZARR,DWZZI,IERR)
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)
C
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
      CALL VecGhostGetLocalForm(APUVEC,APUL,IERR)
      CALL VecGhostGetLocalForm(APVVEC,APVL,IERR)
      CALL VecGhostGetLocalForm(APWVEC,APWL,IERR)
C
      CALL VecSet(APL,PZERO,IERR)
      CALL VecSet(APUL,PZERO,IERR)
      CALL VecSet(APVL,PZERO,IERR)
      CALL VecSet(APWL,PZERO,IERR)
C
      CALL VecGetArray(APL,APARR,APPI,IERR)
      CALL VecGetArray(APUL,APUARR,APUUI,IERR)
      CALL VecGetArray(APVL,APVARR,APVVI,IERR)
      CALL VecGetArray(APWL,APWARR,APWWI,IERR)
C
      AE(1:NIJKBKAL)=0.0D0;  AW(1:NIJKBKAL)=0.0D0
      AN(1:NIJKBKAL)=0.0D0;  AS(1:NIJKBKAL)=0.0D0
      AT(1:NIJKBKAL)=0.0D0;  AB(1:NIJKBKAL)=0.0D0 
      AL(1:NOCBKAL) =0.0D0;  AR(1:NOCBKAL) =0.0D0
      ALU(1:NOCBKAL) =0.0D0; ARU(1:NOCBKAL) =0.0D0
      ALV(1:NOCBKAL) =0.0D0; ARV(1:NOCBKAL) =0.0D0
      ALW(1:NOCBKAL) =0.0D0; ARW(1:NOCBKAL) =0.0D0
      GU=GDS(1)
C
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
      ALFU(1:NFSGBKAL)=0.0D0; ARFU(1:NFSGBKAL)=0.0D0
      ALFV(1:NFSGBKAL)=0.0D0; ARFV(1:NFSGBKAL)=0.0D0
      ALFW(1:NFSGBKAL)=0.0D0; ARFW(1:NFSGBKAL)=0.0D0
C
C     APU(1:NIJKBKAL)=0.0D0; 
      AWU(1:NIJKBKAL)=0.0D0; AEU(1:NIJKBKAL)=0.0D0
      ANU(1:NIJKBKAL)=0.0D0; ASU(1:NIJKBKAL)=0.0D0
      ATU(1:NIJKBKAL)=0.0D0; ABU(1:NIJKBKAL)=0.0D0
C     APV(1:NIJKBKAL)=0.0D0; 
      AWV(1:NIJKBKAL)=0.0D0; AEV(1:NIJKBKAL)=0.0D0
      ANV(1:NIJKBKAL)=0.0D0; ASV(1:NIJKBKAL)=0.0D0
      ATV(1:NIJKBKAL)=0.0D0; ABV(1:NIJKBKAL)=0.0D0
C     APW(1:NIJKBKAL)=0.0D0; 
      AWW(1:NIJKBKAL)=0.0D0; AEW(1:NIJKBKAL)=0.0D0
      ANW(1:NIJKBKAL)=0.0D0; ASW(1:NIJKBKAL)=0.0D0
      ATW(1:NIJKBKAL)=0.0D0; ABW(1:NIJKBKAL)=0.0D0
C
      APUT(1:NIJKBKAL)=0.0D0
      APVT(1:NIJKBKAL)=0.0D0
      APWT(1:NIJKBKAL)=0.0D0
C
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT,
C$OMP*                    SB,APT)
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: EAST
C
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXUVW(IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                           XER(IJK),YER(IJK),ZER(IJK),
     *               F1(IJK),AW(IJK+NJ), AE(IJK),FX(IJK),GU,M,
     *                       AWU(IJK+NJ),AEU(IJK),
     *                       AWV(IJK+NJ),AEV(IJK),
     *                       AWW(IJK+NJ),AEW(IJK))
C
        APU(IJK)    =APU(IJK)  -AWU(IJK+NJ)
        APU(IJK+NJ)=APU(IJK+NJ)-AEU(IJK)
        APV(IJK)   =APV(IJK)   -AWV(IJK+NJ)
        APV(IJK+NJ)=APV(IJK+NJ)-AEV(IJK)
        APW(IJK)   =APW(IJK)   -AWW(IJK+NJ)
        APW(IJK+NJ)=APW(IJK+NJ)-AEW(IJK)
C
      END DO
      END DO
      END DO
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: NORTH
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXUVW(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                         XNR(IJK),YNR(IJK),ZNR(IJK),
     *               F2(IJK),AS(IJK+1), AN(IJK),FY(IJK),GU,M,
     *                       ASU(IJK+1),ANU(IJK),
     *                       ASV(IJK+1),ANV(IJK),
     *                       ASW(IJK+1),ANW(IJK))
C
        APU(IJK)  =APU(IJK)  -ASU(IJK+1)
        APU(IJK+1)=APU(IJK+1)-ANU(IJK)
        APV(IJK)  =APV(IJK)  -ASV(IJK+1)
        APV(IJK+1)=APV(IJK+1)-ANV(IJK)
        APW(IJK)  =APW(IJK)  -ASW(IJK+1)
        APW(IJK+1)=APW(IJK+1)-ANW(IJK)
C
      END DO
      END DO
      END DO
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: TOP
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXUVW(IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                            XTR(IJK),YTR(IJK),ZTR(IJK),
     *               F3(IJK),AB(IJK+NIJ), AT(IJK),FZ(IJK),GU,M,
     *                       ABU(IJK+NIJ),ATU(IJK),
     *                       ABV(IJK+NIJ),ATV(IJK),
     *                       ABW(IJK+NIJ),ATW(IJK))
C
        APU(IJK)     =APU(IJK)     -ABU(IJK+NIJ)
        APU(IJK+NIJ)=APU(IJK+NIJ)-ATU(IJK)
        APV(IJK)    =APV(IJK)    -ABV(IJK+NIJ)
        APV(IJK+NIJ)=APV(IJK+NIJ)-ATV(IJK)
        APW(IJK)    =APW(IJK)    -ABW(IJK+NIJ)
        APW(IJK+NIJ)=APW(IJK+NIJ)-ATW(IJK)
      END DO
      END DO
      END DO
C
C.....BUOYANCY SOURCE CONTRIBUTION
C
#if defined( USE_ENERGYCOUPLING )
      IF(LCAL(IEN)) THEN
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        SB=BETA*DEN(IJK)*VOL(IJK)
C
        APUT(IJK)=SB*GRAVX
        APVT(IJK)=SB*GRAVY
        APWT(IJK)=SB*GRAVZ
C
        SU(IJK)=SU(IJK)+SB*GRAVX*TREF
        SV(IJK)=SV(IJK)+SB*GRAVY*TREF
        SW(IJK)=SW(IJK)+SB*GRAVZ*TREF
      END DO
      END DO
      END DO
      ENDIF
#else
      IF(LCAL(ISC)) THEN
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        SB=-BETA*DEN(IJK)*VOL(IJK)*(T(IJK)-TREF)
        SU(IJK)=SU(IJK)+GRAVX*SB
        SV(IJK)=SV(IJK)+GRAVY*SB
        SW(IJK)=SW(IJK)+GRAVZ*SB
      END DO
      END DO
      END DO
      ENDIF
#endif
C
C.....ADDITIONAL SOURCE CONTRIBUTION DUE TO MANUFACTURED SOLUTION
C
#if defined( USE_ANALYTICAL )
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        SU(IJK)=SU(IJK)+SUMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
        SV(IJK)=SV(IJK)+SVMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
        SW(IJK)=SW(IJK)+SWMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
C
      END DO
      END DO
      END DO
#endif
C
#if defined( USE_DECOUPLING )
C
C.....PRESSURE SOURCE CONTRIBUTION
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        SU(IJK)=SU(IJK)-DPX(IJK)*VOL(IJK)
        SV(IJK)=SV(IJK)-DPY(IJK)*VOL(IJK)
        SW(IJK)=SW(IJK)-DPZ(IJK)*VOL(IJK)
      END DO
      END DO
      END DO
C
#endif
C
C.....UNSTEADY TERM CONTRIBUTION (GAMT = 0 -> IMPLICIT EULER;
C.....GAMT = 1 -> THREE TIME LEVELS; BLENDING POSSIBLE)
C
      IF(LTIME) THEN
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        APT=DEN(IJK)*VOL(IJK)*DTR
        SU(IJK)=SU(IJK)+APT*((1.0D0+GAMT)*UO(IJK)-0.5D0*GAMT*UOO(IJK))
        SV(IJK)=SV(IJK)+APT*((1.0D0+GAMT)*VO(IJK)-0.5D0*GAMT*VOO(IJK))
        SW(IJK)=SW(IJK)+APT*((1.0D0+GAMT)*WO(IJK)-0.5D0*GAMT*WOO(IJK))
        AP(IJK)=AP(IJK)+APT*(1.0D0+0.5D0*GAMT)
      END DO
      END DO
      END DO
      ENDIF
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
C.....VELOCITY INLET BOUNDARIES (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C
      DO II=1,NINLBKAL
        IJP=IJPI(II)
        IJB=IJI(II)
C
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C.......EXTRAPOLATE PRESSURE TO BOUNDARY (NICHT WIRKLICH NOTWENDIG)
C       P(IJB)=P(IJP)
        CALL FLUXUVW(IJP,IJB,XIC(II),YIC(II),ZIC(II),
     *                       XIR(II),YIR(II),ZIR(II),
     *               FMI(II),CP,CB,ONE,ZERO,1,
     *                       CPU,CBU,
     *                       CPV,CBV,
     *                       CPW,CBW)
        AP(IJP)=AP(IJP)-CB
#if !defined( USE_DECOUPLING )
        APU(IJP)=APU(IJP)+CBU
        APV(IJP)=APV(IJP)+CBV
        APW(IJP)=APW(IJP)+CBW
#endif
        SU(IJP)=SU(IJP)-CB*U(IJB)
        SV(IJP)=SV(IJP)-CB*V(IJB)
        SW(IJP)=SW(IJP)-CB*W(IJB)
      END DO
C
C.....PRESSURE INLET BOUNDARIES / OUTLET (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C
#if defined( USE_DIRICHLETPRESSURE )
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
C
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C
        CALL FLUXUVW(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                       XUR(IO),YOR(IO),ZOR(IO),
     *               FMO(IO),CP,CB,ONE,ZERO,1,
     *                       CPU,CBU,
     *                       CPV,CBV,
     *                       CPW,CBW)
C
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-CB*U(IJB)-P(IJB)*XUR(IO)
        SV(IJP)=SV(IJP)-CB*V(IJB)-P(IJB)*YOR(IO)
        SW(IJP)=SW(IJP)-CB*W(IJB)-P(IJB)*ZOR(IO)
      END DO
#else
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
C
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C
        CALL FLUXUVW(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                       XUR(IO),YOR(IO),ZOR(IO),
     *               FMO(IO),CP,CB,ONE,ZERO,1,
     *                       CPU,CBU,
     *                       CPV,CBV,
     *                       CPW,CBW)
C
        AP(IJP)=AP(IJP)-CB
        APU(IJP)=APU(IJP)+CBU
        APV(IJP)=APV(IJP)+CBV
        APW(IJP)=APW(IJP)+CBW
        SU(IJP)=SU(IJP)-CB*U(IJB)
        SV(IJP)=SV(IJP)-CB*V(IJB)
        SW(IJP)=SW(IJP)-CB*W(IJB)
C       SU(IJP)=SU(IJP)-CB*U(IJB)-P(IJB)*XUR(IO)
C       SV(IJP)=SV(IJP)-CB*V(IJB)-P(IJB)*YOR(IO)
C       SW(IJP)=SW(IJP)-CB*W(IJB)-P(IJB)*ZOR(IO)
      END DO
#endif
C
C.....Zu Wand- und Symmetrierandbedingungen siehe Peric S.305 (8.90).
C
C.....WALL BOUNDARIES
C
      DO IW=1,NWALBKAL
        IJP=IJPW(IW)
        IJB=IJW(IW)
C       VISS=MAX(VISC,VISW(IW))
        VISS=VISC
        COEF=VISS*SRDW(IW)
        ARE=SQRT(XNW(IW)**2+YNW(IW)**2+ZNW(IW)**2)
C
        UPB=U(IJP)-U(IJB)
        VPB=V(IJP)-V(IJB)
        WPB=W(IJP)-W(IJB)
C
        VNP=UPB*XNW(IW)+VPB*YNW(IW)+WPB*ZNW(IW)
C
        XTP=UPB-VNP*XNW(IW)/(ARE**2+SMALL)
        YTP=VPB-VNP*YNW(IW)/(ARE**2+SMALL)
        ZTP=WPB-VNP*ZNW(IW)/(ARE**2+SMALL)
C
        DPB=DSQRT((XC(IJP)-XC(IJB))**2
     *           +(YC(IJP)-YC(IJB))**2
     *           +(ZC(IJP)-ZC(IJB))**2)
        VISOL=VISS*ARE/DPB
C
        AP(IJP)=AP(IJP)+VISOL
        APU(IJP)=APU(IJP)+XNW(IW)
        APV(IJP)=APV(IJP)+YNW(IW)
        APW(IJP)=APW(IJP)+ZNW(IW)
C.......USE DEFERRED CORRECTION, DO NOT USE U(IJB)!
        SU(IJP)=SU(IJP)+VISOL*U(IJP)-COEF*XTP
        SV(IJP)=SV(IJP)+VISOL*V(IJP)-COEF*YTP
        SW(IJP)=SW(IJP)+VISOL*W(IJP)-COEF*ZTP
      END DO
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES!)
C.....AR ist der Koeffizient, der im CV P für den Nachbarn R gesetzt
C.....wird
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        MIJ=IBLKOCBK(I)        
C
        CALL FLUXUVW(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                       XOCR(I),YOCR(I),ZOCR(I),
     *              FMOC(I),AL(I),AR(I),FOCBK(I),GU,MIJ,
     *                       ALU(I),ARU(I),
     *                       ALV(I),ARV(I),
     *                       ALW(I),ARW(I))
C
        APU(IJP)=APU(IJP)-ALU(I)
        APU(IJN)=APU(IJN)-ARU(I)
        APV(IJP)=APV(IJP)-ALV(I)
        APV(IJN)=APV(IJN)-ARV(I)
        APW(IJP)=APW(IJP)-ALW(I)
        APW(IJN)=APW(IJN)-ARW(I)
        AP(IJP) =AP(IJP)-AR(I)
        AP(IJN) =AP(IJN)-AL(I)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE NO INTERNAL BOUNDARIES!)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXUVW(IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                       XFR(I),YFR(I),ZFR(I),
     *               FMF(I),AFL(I),AFR(I),FFSGBK(I),GU,MIJ,
     *                       ALFU(I),ARFU(I),
     *                       ALFV(I),ARFV(I),
     *                       ALFW(I),ARFW(I))
        APU(IJP)=APU(IJP)-ALFU(I)
        APU(IJN)=APU(IJN)-ARFU(I)
        APV(IJP)=APV(IJP)-ALFV(I)
        APV(IJN)=APV(IJN)-ARFV(I)
        APW(IJP)=APW(IJP)-ALFW(I)
        APW(IJN)=APW(IJN)-ARFW(I)
        AP(IJP) =AP(IJP)-AFR(I)
        AP(IJN) =AP(IJN)-AFL(I)
      END DO
C
C.....RESTORE ARRAYS THAT ARE NOT NEEDED ANYMORE
C
      CALL VecRestoreArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecRestoreArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecRestoreArray(DUZL,DUZARR,DUZZI,IERR)
C
      CALL VecRestoreArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecRestoreArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecRestoreArray(DVZL,DVZARR,DVZZI,IERR)
C
      CALL VecRestoreArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecRestoreArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecRestoreArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      IF(LCAL(ISC)) CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecGhostRestoreLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostRestoreLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostRestoreLocalForm(DUZVEC,DUZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostRestoreLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostRestoreLocalForm(DVZVEC,DVZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostRestoreLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostRestoreLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
      CALL VecRestoreArray(APUL,APUARR,APUUI,IERR)
      CALL VecRestoreArray(APVL,APVARR,APVVI,IERR)
      CALL VecRestoreArray(APWL,APWARR,APWWI,IERR)
C
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
      CALL VecGhostRestoreLocalForm(APUVEC,APUL,IERR)
      CALL VecGhostRestoreLocalForm(APVVEC,APVL,IERR)
      CALL VecGhostRestoreLocalForm(APWVEC,APWL,IERR)
C
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APVVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APVVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APWVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APWVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....GET LOCAL PART OF AP,APR
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(APRVEC,APRARR,APRRI,IERR)
C
C.....FINAL COEFFICIENT AND SOURCES MATRIX FOR U-EQUATION
C
C.....OpenMP : Here starts parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*            NKMT,NIMT,NJMT,KSTT,ISTT)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM 
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C NEWCOUPLED - NO UNDERRELAXATION NEEDED
C       AP(IJK)=(AP(IJK)-AE(IJK)-AW(IJK)
C                       -AN(IJK)-AS(IJK)
C    *                  -AT(IJK)-AB(IJK))*URFU
        AP(IJK)=AP(IJK)-AE(IJK)-AW(IJK)
     *                 -AN(IJK)-AS(IJK)
     *                 -AT(IJK)-AB(IJK)
COUTPUT
C       SU(IJK)=SU(IJK)+(1.0D0-URF(IU))*AP(IJK)*U(IJK)
C       SV(IJK)=SV(IJK)+(1.0D0-URF(IV))*AP(IJK)*V(IJK)
C       SW(IJK)=SW(IJK)+(1.0D0-URF(IWW))*AP(IJK)*W(IJK)
        SU(IJK)=SU(IJK)
        SV(IJK)=SV(IJK)
        SW(IJK)=SW(IJK)
        APR(IJK)=1.0D0/(AP(IJK)+SMALL)
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(APRVEC,APRARR,APRRI,IERR)
C
#if defined ( USE_SUBVEC )
      CALL VecRestoreArray(SUL,SUARR,SUUI,IERR)
      CALL VecRestoreArray(SVL,SVARR,SVVI,IERR)
      CALL VecRestoreArray(SWL,SWARR,SWWI,IERR)
C
C NEWCOUPLED - WAIT UNTIL PRESSURE SOURCE TERM HAS BEEN CALCULATED
      CALL VecRestoreSubVector(SUVWPL,ISU,SUL,IERR)
      CALL VecRestoreSubVector(SUVWPL,ISV,SVL,IERR)
      CALL VecRestoreSubVector(SUVWPL,ISW,SWL,IERR)
C
      CALL VecRestoreArray(UL,UARR,UUI,IERR)
      CALL VecRestoreArray(VL,VARR,VVI,IERR)
      CALL VecRestoreArray(WL,WARR,WWI,IERR)
      CALL VecRestoreArray(PL,PARR,PPI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PL,IERR)
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
      CALL VecRestoreArray(SUVWPL,SUVWPARR,SUVWPPI,IERR)
#endif
C
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGhostRestoreLocalForm(SUVWPVEC,SUVWPL,IERR)
C
      CALL ASSEMBLESYSUVW(AMAT)
C
      RETURN
      END 
C
#include "petsc.user.inc"
C
C
C################################################################
      SUBROUTINE FLUXUVW(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                   FM,CAP,CAN,FAC,G,MB,
     *                      CAPU,CANU,
     *                      CAPV,CANV,
     *                      CAPW,CANW)
C################################################################
C     This routine calculates momentum fluxes (convective and
C     diffusive) through the cell face between nodes IJP and IJN. 
C     IJ1 and IJ2 are the indices of CV corners defining the cell 
C     face. FM is the mass flux through the face, and FAC is the 
C     interpolation factor (distance from node IJP to cell face 
C     center over the sum of this distance and the distance from 
C     cell face center to node IJN). CAP and CAN are the 
C     contributions to matrix coefficients in the momentum
C     equations at nodes IJP and IJN. Diffusive fluxes are
C     discretized using central differences; for convective
C     fluxes, linear interpolation can be blended with upwind
C     approximation; see Sect. 8.6 for details. Note: cell
C     face surface vector is directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C     
C     CAP{U,V,W} and CAN{U,V,W} are the coefficients due to the
C     implicit consideration of the pressure gradient. They are
C     calculated according to darwish 2009.
C
C==============================================================
      IMPLICIT NONE
C
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP,MB,M
      INTEGER LS
C
      REAL*8 FM,FACP,FAC,VISI,DENI,ZZC,YYC,XXC
      REAL*8 YCR,ZCR,XCR,FMI,FMX,DUXI,DVXI,DWXI
      REAL*8 DUYI,DVYI,DWYI,DUZI,DVZI,DWZI,XI,YI,ZI
      REAL*8 UI,VI,WI,XPN,ZPN,YPN,VSOL,FCUE
      REAL*8 FCWE,FDUE,FDVE,FDWE,FCUI,FCVI,FCWI,FDWI
      REAL*8 CAN,CAP,FUC,G,FVC,SUM,FCVE,FDUI,FDVI,FWC 
      REAL*8 CAPU,CANU,CAPV,CANV,CAPW,CANW
C
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C=========================================================
C
C.....INTERPOLATE ALONG LINE P-N
C
      FACP=1.0D0-FAC
      VISI=VIS(IJN)*FAC+VIS(IJP)*FACP
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....COMPUTE FM 
C
      FMI=MIN(FM,ZERO)
      FMX=MAX(FM,ZERO)
C
C.....INTERPOLATE ALONG LINE P-N
C
      DUXI=DUX(IJN)*FAC+DUX(IJP)*FACP
      DVXI=DVX(IJN)*FAC+DVX(IJP)*FACP
      DWXI=DWX(IJN)*FAC+DWX(IJP)*FACP
C
      DUYI=DUY(IJN)*FAC+DUY(IJP)*FACP
      DVYI=DVY(IJN)*FAC+DVY(IJP)*FACP
      DWYI=DWY(IJN)*FAC+DWY(IJP)*FACP
C
      DUZI=DUZ(IJN)*FAC+DUZ(IJP)*FACP
      DVZI=DVZ(IJN)*FAC+DVZ(IJP)*FACP
      DWZI=DWZ(IJN)*FAC+DWZ(IJP)*FACP
C
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
C
C.....CALCULATE CELL-FACE VELOCITIES AND VISCOSITY
C
      UI=U(IJN)*FAC+U(IJP)*FACP
     *                         +DUXI*(XXC-XI)
     *                         +DUYI*(YYC-YI)
     *                         +DUZI*(ZZC-ZI)
      VI=V(IJN)*FAC+V(IJP)*FACP
     *                         +DVXI*(XXC-XI)
     *                         +DVYI*(YYC-YI)
     *                         +DVZI*(ZZC-ZI)
      WI=W(IJN)*FAC+W(IJP)*FACP
     *                         +DWXI*(XXC-XI)
     *                         +DWYI*(YYC-YI)
     *                         +DWZI*(ZZC-ZI)
C
C.....DISTANCE VECTOR COMPONENTS, DIFFUSION COEFFICIENT
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      VSOL=VISI*DSQRT((XCR**2+YCR**2+ZCR**2)/
     *                (XPN**2+YPN**2+ZPN**2))
C
C.....EXPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCUE=FM*UI
      FCVE=FM*VI
      FCWE=FM*WI
#ifdef USE_FULLNAVIERSTOKES
C.....projektion auf den normalenvektor n (FULL NAVIER STOKES)
      FDUE=VISI*((DUXI+DUXI)*XCR+(DUYI+DVXI)*YCR+(DUZI+DWXI)*ZCR)
      FDVE=VISI*((DVXI+DUYI)*XCR+(DVYI+DVYI)*YCR+(DVZI+DWYI)*ZCR)
      FDWE=VISI*((DWXI+DUZI)*XCR+(DWYI+DVZI)*YCR+(DWZI+DWZI)*ZCR)
#else
C......NO VARIABLE VISCOSITY VERSION NAVIER-STOKES-EQUATION (CONSISTENT
C......TREATMENT - VANISHES ON ORTHOGONAL GRID, VALID IN CASE OF
C......CONSTANT DENSITY AND VISCOSITY)
      FDUE=VISI*((DUXI)*XCR+(DUYI)*YCR+(DUZI)*ZCR)
      FDVE=VISI*((DVXI)*XCR+(DVYI)*YCR+(DVZI)*ZCR)
      FDWE=VISI*((DWXI)*XCR+(DWYI)*YCR+(DWZI)*ZCR)
#endif
C
C.....IMPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCUI=FMI*U(IJN)+FMX*U(IJP)
      FCVI=FMI*V(IJN)+FMX*V(IJP)
      FCWI=FMI*W(IJN)+FMX*W(IJP)
C.....projektion auf den verbindungsvektor xi
      FDUI=VSOL*(DUXI*XPN+DUYI*YPN+DUZI*ZPN)
      FDVI=VSOL*(DVXI*XPN+DVYI*YPN+DVZI*ZPN)
      FDWI=VSOL*(DWXI*XPN+DWYI*YPN+DWZI*ZPN)
C
C.....COEFFICIENTS, DEFERRED CORRECTION, SOURCE TERMS
C
      CAN=-VSOL+FMI
      CAP=-VSOL-FMX
C
#ifndef USE_DECOUPLING
C
C.....IMPLICIT TREATMENT OF PRESSURE GRADIENT
C
      CAPU=-FACP*XCR
      CANU=FAC*XCR
      CAPV=-FACP*YCR
      CANV=FAC*YCR
      CAPW=-FACP*ZCR
      CANW=FAC*ZCR
C
#endif
C
      FUC=G*(FCUE-FCUI)
      FVC=G*(FCVE-FCVI)
      FWC=G*(FCWE-FCWI)
C
      SU(IJP)=SU(IJP)-FUC+FDUE-FDUI
      SU(IJN)=SU(IJN)+FUC-FDUE+FDUI
C
      SV(IJP)=SV(IJP)-FVC+FDVE-FDVI
      SV(IJN)=SV(IJN)+FVC-FDVE+FDVI
C
      SW(IJP)=SW(IJP)-FWC+FDWE-FDWI
      SW(IJN)=SW(IJN)+FWC-FDWE+FDWI
C
      RETURN
C
C
      END
C
#include "petsc.user.inc"
C
C############################################################## 
      SUBROUTINE CALCP
C############################################################## 
C     This routine assembles and solves the pressure-correction
C     equation using colocated grid. SIMPLE algorithm with one
C     corrector step (non-orthogonality effects neglected) is
C     applied.
C
C==============================================================
      IMPLICIT NONE
C
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER M
      INTEGER I,J,K,IJK,IJP,IJN,II,IO,LC,ISY
      INTEGER LS
      INTEGER IJB,IJK1,IJK2,IJK3,IJK4
C
      REAL*8 SUM,FLOWO,PPO,UN,FMCOR,DX12,DY12,DZ12,DX13,DY13,DZ13
      REAL*8 DX14,DY14,DZ14,S23,S34,FLOMON,XR23,YR23,ZR23
      REAL*8 XR34,YR34,ZR34,XNV,YNV,ZNV,FAC
      REAL*8 VM,PREFG,VMG
      REAL*8 DENI
      REAL*8 CP,CB
      Vec SUL,APL,APRL,PL,PPL,TL,
     *    UL,VL,WL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL,
C
     *    UVWPL,SUVWPL,SPL,SSCL,
     *    APUL,APVL,APWL
C
      PetscScalar PZERO,PONE,ZEROSC
      PetscErrorCode IERR
      PetscInt ZEROPP
      COMMON /CORRECTOR/ LC
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C
C==============================================================
C
C.....INITIALIZE SUM AND COEFs
C
      SUM=0.0D0
      PZERO=0.0D0
      PONE=1.0D0
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGhostGetLocalForm(SUVWPVEC,SUVWPL,IERR)
C
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UL,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VL,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WL,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PL,IERR)
      CALL VecGetSubVector(SUVWPL,ISP,SPL,IERR)
C
      CALL VecSet(SPL,PZERO,IERR)
C
      CALL VecGetArray(UL,UARR,UUI,IERR)
      CALL VecGetArray(VL,VARR,VVI,IERR)
      CALL VecGetArray(WL,WARR,WWI,IERR)
      CALL VecGetArray(PL,PARR,PPI,IERR)
      CALL VecGetArray(SPL,SPARR,SPPI,IERR)
#if defined( USE_NEWTONRAPHSON )
      CALL VecGetSubVector(UVWPL,ISE,TL,IERR)
      CALL VecGetSubVector(SUVWPL,ISE,SSCL,IERR)
      CALL VecSet(SSCL,PZERO,IERR)
      CALL VecGetArray(TL,TARR,TTI,IERR)
      CALL VecGetArray(SSCL,SSCARR,SSCCI,IERR)
#endif
C
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
      CALL VecGetArray(SUVWPL,SUVWPARR,SUVWPPI,IERR)
#endif
C
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
      CALL VecGhostGetLocalForm(APUVEC,APUL,IERR)
      CALL VecGhostGetLocalForm(APVVEC,APVL,IERR)
      CALL VecGhostGetLocalForm(APWVEC,APWL,IERR)
C
      CALL VecSet(APL,PZERO,IERR)
      CALL VecSet(APUL,PZERO,IERR)
      CALL VecSet(APVL,PZERO,IERR)
      CALL VecSet(APWL,PZERO,IERR)
C
      CALL VecGetArray(APL,APARR,APPI,IERR)
      CALL VecGetArray(APUL,APUARR,APUUI,IERR)
      CALL VecGetArray(APVL,APVARR,APVVI,IERR)
      CALL VecGetArray(APWL,APWARR,APWWI,IERR)
C
      AE(1:NIJKBKAL)=0.0D0;  AW(1:NIJKBKAL)=0.0D0 
      AN(1:NIJKBKAL)=0.0D0;  AS(1:NIJKBKAL)=0.0D0 
      AT(1:NIJKBKAL)=0.0D0;  AB(1:NIJKBKAL)=0.0D0 
      AL(1:NOCBKAL )=0.0D0;  AR(1:NOCBKAL )=0.0D0 
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
C
C.....GET ARRAY OF VECTOR VALUES (without update)
C
      CALL VecGhostGetLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostGetLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostGetLocalForm(DUZVEC,DUZL,IERR)
C
      CALL VecGhostGetLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostGetLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostGetLocalForm(DVZVEC,DVZL,IERR)
C
      CALL VecGhostGetLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostGetLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostGetLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecGetArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecGetArray(DUZL,DUZARR,DUZZI,IERR)
C
      CALL VecGetArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecGetArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecGetArray(DVZL,DVZARR,DVZZI,IERR)
C
      CALL VecGetArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecGetArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecGetArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
C.....GET ARRAY OF VECTOR VALUES
C
      CALL VecGhostUpdateBegin(
     *     APRVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(APRVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
      CALL VecGhostGetLocalForm(APRVEC,APRL,IERR)
C
      CALL VecGetArray(APRL,APRARR,APRRI,IERR)
C
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT)
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
      DO K=2,NKM 
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        CALL FLUXP(IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                         XER(IJK),YER(IJK),ZER(IJK),
     *                        AW(IJK+NJ),AE(IJK),FX(IJK))
C
        DENI=DEN(IJK+NJ)*FX(IJK)+DEN(IJK)*(1.0D0-FX(IJK))
C
        AEU(IJK   )=AEU(IJK   )*DENI
        AWU(IJK+NJ)=AWU(IJK+NJ)*DENI
        APU(IJK   )=APU(IJK   )-AWU(IJK+NJ)
        APU(IJK+NJ)=APU(IJK+NJ)-AEU(IJK)
C
        AEV(IJK   )=AEV(IJK   )*DENI
        AWV(IJK+NJ)=AWV(IJK+NJ)*DENI
        APV(IJK   )=APV(IJK   )-AWV(IJK+NJ)
        APV(IJK+NJ)=APV(IJK+NJ)-AEV(IJK)
C
        AEW(IJK   )=AEW(IJK   )*DENI
        AWW(IJK+NJ)=AWW(IJK+NJ)*DENI
        APW(IJK   )=APW(IJK   )-AWW(IJK+NJ)
        APW(IJK+NJ)=APW(IJK+NJ)-AEW(IJK)
      END DO
      END DO
      END DO
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXP(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                       XNR(IJK),YNR(IJK),ZNR(IJK),
     *                     AS(IJK+1),AN(IJK),FY(IJK))
C
        DENI=DEN(IJK+1)*FY(IJK)+DEN(IJK)*(1.0D0-FY(IJK))
C
        ANU(IJK  )=ANU(IJK  )*DENI
        ASU(IJK+1)=ASU(IJK+1)*DENI
        APU(IJK  )=APU(IJK  )-ASU(IJK+1)
        APU(IJK+1)=APU(IJK+1)-ANU(IJK  )
C
        ANV(IJK  )=ANV(IJK  )*DENI
        ASV(IJK+1)=ASV(IJK+1)*DENI
        APV(IJK  )=APV(IJK  )-ASV(IJK+1)
        APV(IJK+1)=APV(IJK+1)-ANV(IJK  )
C        
        ANW(IJK  )=ANW(IJK  )*DENI
        ASW(IJK+1)=ASW(IJK+1)*DENI
        APW(IJK  )=APW(IJK  )-ASW(IJK+1)
        APW(IJK+1)=APW(IJK+1)-ANW(IJK  )
      END DO
      END DO
      END DO
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXP(IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                          XTR(IJK),YTR(IJK),ZTR(IJK),
     *                     AB(IJK+NIJ),AT(IJK),FZ(IJK))
C
        DENI=DEN(IJK+NIJ)*FZ(IJK)+DEN(IJK)*(1.0D0-FZ(IJK))
C 
        ATU(IJK    )=ATU(IJK    )*DENI        
        ABU(IJK+NIJ)=ABU(IJK+NIJ)*DENI
        APU(IJK    )=APU(IJK    )-ABU(IJK+NIJ)
        APU(IJK+NIJ)=APU(IJK+NIJ)-ATU(IJK    )
 
        ATV(IJK    )=ATV(IJK    )*DENI
        ABV(IJK+NIJ)=ABV(IJK+NIJ)*DENI
        APV(IJK    )=APV(IJK    )-ABV(IJK+NIJ)
        APV(IJK+NIJ)=APV(IJK+NIJ)-ATV(IJK    )
 
        ATW(IJK    )=ATW(IJK    )*DENI
        ABW(IJK+NIJ)=ABW(IJK+NIJ)*DENI
        APW(IJK    )=APW(IJK    )-ABW(IJK+NIJ)
        APW(IJK+NIJ)=APW(IJK+NIJ)-ATW(IJK    )
      END DO
      END DO
      END DO
C
      END DO
c.....OpenMP : Here ends this parallel loop section
C
C.....O- AND C-GRID CUTS
C    
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        CALL FLUXP(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                     XOCR(I),YOCR(I),ZOCR(I),
     *                     AL(I),AR(I),FOCBK(I))
C
        AP(IJP)=AP(IJP)-AR(I)
        AP(IJN)=AP(IJN)-AL(I)
C
        DENI=DEN(IJN)*FOCBK(I)+DEN(IJP)*(1.0D0-FOCBK(I))
C
        ALU(I  )=ALU(I  )*DENI
        ARU(I  )=ARU(I  )*DENI
        APU(IJP)=APU(IJP)-ALU(I)
        APU(IJN)=APU(IJN)-ARU(I)
C
        ALV(I  )=ALV(I  )*DENI
        ARV(I  )=ARV(I  )*DENI
        APV(IJP)=APV(IJP)-ALV(I)
        APV(IJN)=APV(IJN)-ARV(I)
C
        ALW(I  )=ALW(I  )*DENI
        ARW(I  )=ARW(I  )*DENI
        APW(IJP)=APW(IJP)-ALW(I)
        APW(IJN)=APW(IJN)-ARW(I)
      END DO
C
C.....FACE SEGMENTS
C    
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXP(IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                     XFR(I),YFR(I),ZFR(I),
     *                     AFL(I),AFR(I),FFSGBK(I))
C
        AP(IJP)=AP(IJP)-AFR(I)
        AP(IJN)=AP(IJN)-AFL(I)
C
        DENI=DEN(IJN)*FFSGBK(I)+DEN(IJP)*(1.0D0-FFSGBK(I))
C
        ARFU(I )=ARFU(I )*DENI
        ALFU(I )=ALFU(I )*DENI
        APU(IJP)=APU(IJP)-ALFU(I)
        APU(IJN)=APU(IJN)-ARFU(I)
C
        ARFV(I )=ARFV(I )*DENI
        ALFV(I )=ALFV(I )*DENI
        APV(IJP)=APV(IJP)-ALFV(I)
        APV(IJN)=APV(IJN)-ARFV(I)
C
        ARFW(I )=ARFW(I )*DENI
        ALFW(I )=ALFW(I )*DENI
        APW(IJP)=APW(IJP)-ALFW(I)
        APW(IJN)=APW(IJN)-ARFW(I)
      END DO
C
C.....INLET BOUNDARIES
C    
      DO II=1,NINLBKAL
        SP(IJPI(II))=SP(IJPI(II))-FMI(II)
      END DO
C
C.....PRESSURE INLET BOUNDARIES / OUTLET (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C
#if defined( USE_DIRICHLETPRESSURE )
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C
        DPX(IJB)=DPX(IJP)
        DPY(IJB)=DPY(IJP)
        DPZ(IJB)=DPZ(IJP)
C
        VOL(IJB)=VOL(IJP)
        APR(IJB)=APR(IJP)
C
        CALL FLUXP(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                     XUR(IO),YOR(IO),ZOR(IO),
     *                     CP,CB,ONE)
        AP(IJP)=AP(IJP)-CB
        APU(IJP)=APU(IJP)+XUR(IO)*DEN(IJB)
        APV(IJP)=APV(IJP)+YOR(IO)*DEN(IJB)
        APW(IJP)=APW(IJP)+ZOR(IO)*DEN(IJB)
        SP(IJP) =SP(IJP)-CB*P(IJB)
      END DO
#else
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C
        DPX(IJB)=DPX(IJP)
        DPY(IJB)=DPY(IJP)
        DPZ(IJB)=DPZ(IJP)
C
        VOL(IJB)=VOL(IJP)
        APR(IJB)=APR(IJP)
C
        CALL FLUXP(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                     XUR(IO),YOR(IO),ZOR(IO),
     *                     CP,CB,ONE)
C       AP(IJP)=AP(IJP)-CB
C       APU(IJP)=APU(IJP)+XUR(IO)*DEN(IJB)
C       APV(IJP)=APV(IJP)+YOR(IO)*DEN(IJB)
C       APW(IJP)=APW(IJP)+ZOR(IO)*DEN(IJB)
        SP(IJPO(IO))=SP(IJPO(IO))-FMO(IO)
      END DO
#endif
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT),
C$OMP*            REDUCTION(+:SUM)
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....SOURCE TERM AND CENTRAL COEFFICIENT 
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        AP(IJK)=(AP(IJK)-(AE(IJK)+AW(IJK)
     *                   +AN(IJK)+AS(IJK)
     *                   +AT(IJK)+AB(IJK)))
C    *                   +AT(IJK)+AB(IJK)))*URFP
C       SP(IJK)=SP(IJK)+(1.0D0-URF(IP))*AP(IJK)*P(IJK)
        SP(IJK)=SP(IJK)
#if defined( USE_DECOUPLING )
        SU(IJK)=SU(IJK)+F1(IJK-NJ)-F1(IJK)
     *                 +F2(IJK-1)-F2(IJK)
     *                 +F3(IJK-NIJ)-F3(IJK)
#endif
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
#if defined( USE_SUBVEC )
      CALL VecRestoreArray(UL,UARR,UUI,IERR)
      CALL VecRestoreArray(VL,VARR,VVI,IERR)
      CALL VecRestoreArray(WL,WARR,WWI,IERR)
      CALL VecRestoreArray(PL,PARR,PPI,IERR)
      CALL VecRestoreArray(SPL,SPARR,SPPI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PL,IERR)
      CALL VecRestoreSubVector(SUVWPL,ISP,SPL,IERR)
#if defined( USE_NEWTONRAPHSON )
      CALL VecRestoreArray(TL,TARR,TTI,IERR)
      CALL VecRestoreArray(SSCL,SSCARR,SSCCI,IERR)
      CALL VecRestoreSubVector(UVWPL,ISE,TL,IERR)
      CALL VecRestoreSubVector(SUVWPL,ISE,SSCL,IERR)
#endif
C
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
      CALL VecRestoreArray(SUVWPL,SUVWPARR,SUVWPPI,IERR)
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGhostRestoreLocalForm(SUVWPVEC,SUVWPL,IERR)
C
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
      CALL VecRestoreArray(APUL,APUARR,APUUI,IERR)
      CALL VecRestoreArray(APVL,APVARR,APVVI,IERR)
      CALL VecRestoreArray(APWL,APWARR,APWWI,IERR)
C
C.....ASSEMBLE P-SUBMATRIX
C
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
      CALL VecGhostRestoreLocalForm(APUVEC,APUL,IERR)
      CALL VecGhostRestoreLocalForm(APVVEC,APVL,IERR)
      CALL VecGhostRestoreLocalForm(APWVEC,APWL,IERR)
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APVVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APVVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APWVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APWVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL ASSEMBLESYSP(AMAT)
C
C.....RESTORE ARRAYS
C
      CALL VecRestoreArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecRestoreArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecRestoreArray(DUZL,DUZARR,DUZZI,IERR)
C
      CALL VecRestoreArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecRestoreArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecRestoreArray(DVZL,DVZARR,DVZZI,IERR)
C
      CALL VecRestoreArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecRestoreArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecRestoreArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGhostRestoreLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostRestoreLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostRestoreLocalForm(DUZVEC,DUZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostRestoreLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostRestoreLocalForm(DVZVEC,DVZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostRestoreLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostRestoreLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(APRL,APRARR,APRRI,IERR)
C
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(APRVEC,APRL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
#ifndef USE_ENERGYCOUPLING
      CALL VecGhostUpdateBegin(SUVWPVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  SUVWPVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
#endif
C
#if defined( USE_ZEROS )
      IF (NOUTBLKAL.LE.0) THEN
        IJK=(NI*NI*KMON+NI*IMON+JMON-1)*NOEQ+3
        CALL VecSetValue(UVWPVEC,IJK,
     *                   PMMS(XC(IJK),YC(IJK),ZC(IJK)),IERR)
        CALL MatZeroRowsColumns(
     *       AMAT,1,IJK,PONE,UVWPVEC,SUVWPVEC,IERR)
      END IF
#endif
C
#ifndef USE_ENERGYCOUPLING
C
C.....SOLVING COUPLED SYSTEM
C
      CALL MatZeroRowsColumns(
     *     AMAT,NZERO,ZEROS,PONE,UVWPVEC,SUVWPVEC,IERR)
      CALL PetscLogStagePush(STAGE1,IERR)
      CALL SOLVESYS(UVWPVEC,IU,AMAT,SUVWPVEC)
      CALL PetscLogStagePop(IERR)
C
      CALL BCIN
#endif
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C############################################################## 
      SUBROUTINE PREFERENCE
C############################################################## 
C
C     Apply rule for pressure reference (interpolation, mean
C     value, etc.). Update variable values on boundaries.
C
C==============================================================
      IMPLICIT NONE
C
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER M,NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT
      INTEGER I,J,K,IJK,IJP,IJN,II,IO,LC,ISY,IW
      INTEGER LS
      INTEGER IJB,IJK1,IJK2,IJK3,IJK4
C
      REAL*8 SUM,FLOWO,PPO,UN,FMCOR,DX12,DY12,DZ12,DX13,DY13,DZ13
      REAL*8 DX14,DY14,DZ14,S23,S34,FLOMON,XR23,YR23,ZR23
      REAL*8 XR34,YR34,ZR34,XNV,YNV,ZNV,FAC
      REAL*8 VM,PREFG,VMG
      REAL*8 UE,VE,WE
C
      REAL*8 DENI
      REAL*8 CP,CB
      REAL*8 CAPP,DPCOR,XPN,YPN,ZPN
      Vec SUL,APL,APRL,PL,PPL,
     *    UL,VL,WL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL,
C
     *    UVWPL,SUVWPL,SPL,
     *    APUL,APVL,APWL
C
      PetscScalar PZERO,PONE,ZEROSC
      PetscErrorCode IERR
      PetscInt ZEROPP
      COMMON /CORRECTOR/ LC
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C
C==============================================================
C
      CALL VecGetArray(VOLVEC,VOLARR,VOLLI,IERR)
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PVEC,IERR)
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#endif
C
      CALL VecGetArray(APRVEC,APRARR,APRRI,IERR)
      CALL VecGetArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecGetArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCVEC,ZCARR,ZCCI,IERR)
C
#if defined( USE_DIRICHLETPRESSURE )
      IF (NOUTBKAL.LE.0) THEN
#endif
C
C.......MITTLERER DRUCK
        PREF=0.0D0
C.......REFERENZDRUCKKORREKTUR
#if defined( USE_INTERPOLATION )
C.....CALCULATE REFERENCE LOCATION AND REFERENCE PRESSURE
        IF (RANK.EQ.RMON) THEN
          CALL SETIND(MMON)
          PREF=0.D0
C
          IJK=NI*NI*KMON+NI*IMON+JMON
          PREF=PREF+P(IJK)
C  
          IJK=NI*NI*KMON+NI*IMON+JMON+1
          PREF=PREF+P(IJK)
C  
          IJK=NI*NI*KMON+NI*(IMON+1)+JMON
          PREF=PREF+P(IJK)
C  
          IJK=NI*NI*KMON+NI*(IMON+1)+JMON+1
          PREF=PREF+P(IJK)
C  
          IJK=NI*NI*(KMON+1)+NI*IMON+JMON
          PREF=PREF+P(IJK)
C  
          IJK=NI*NI*(KMON+1)+NI*IMON+JMON+1
          PREF=PREF+P(IJK)
C
          IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON
          PREF=PREF+P(IJK)
C
          IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON+1
          PREF=PREF+P(IJK)
C
C......LINEAR INTERPOLATION (EQUIDISTANT GRID ASSUMED)
C
          PREF=PREF*0.125D0
        END IF
#elif defined( USE_MEANPRESSURE )
        PREF=0.0D0
        PREFG=0.0D0
        VM=0.0D0
        VMG=0.0D0
        DO M=1,NBLKS
C
        CALL SETIND(M)
C
        DO K=2,NKM 
        DO I=2,NIM 
        DO J=2,NJM 
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          PREF=PREF+P(IJK)*VOL(IJK)
          VM=VM+VOL(IJK)
        END DO
        END DO
        END DO
C
        END DO
        CALL MPI_REDUCE(
     *      PREF,PREFG,1,MPI_REAL8,MPI_SUM,RMON,PETSC_COMM_WORLD,IERR)
        CALL MPI_REDUCE(
     *      VM,VMG,1,MPI_REAL8,MPI_SUM,RMON,PETSC_COMM_WORLD,IERR)
        IF (RANK.EQ.RMON) PREF=PREFG/VMG
#else
        IF (RANK.EQ.RMON) THEN
          CALL SETIND(MMON)
          IJKMON=NI*NI*KMON+NI*IMON+JMON
          PREF=P(IJKMON)
        END IF
#endif
       CALL MPI_BCAST(PREF,1,MPI_REAL8,RMON,PETSC_COMM_WORLD,IERR)
C
       DO M=1,NBLKS
C
       CALL SETIND(M)
C
       DO K=2,NKM
       DO I=2,NIM
       DO J=2,NJM
         IJK=LKBK(K+KST)+LIBK(I+IST)+J
         P(IJK)=P(IJK)-PREF
       END DO
       END DO
       END DO
C
       END DO
C
C.......EXTRAPOLATE BOUNDARY VALUES AT VELOCITY INLETS
C
#if defined( USE_LINEAREXTRAPOLATION )
      DO M=1,NBLKS 
      CALL SETIND(M)
C
C.....EXTRAPOLATE TO SOUTH AN NORTH BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM
        IJK=LKBK(K+KST)+LIBK(I+IST)+1
        P(IJK)=P(IJK+1)+(P(IJK+1)-P(IJK+2))*FY(IJK+1)
        IJK=LKBK(K+KST)+LIBK(I+IST)+NJ
        P(IJK)=P(IJK-1)+(P(IJK-1)-P(IJK-2))*(1.0D0-FY(IJK-2))
      END DO
      END DO
C
C.....EXTRAPOLATE TO WEST AND EAST BOUNDARIES
C
      DO K=2,NKM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(1+IST)+J
        P(IJK)=P(IJK+NJ)+(P(IJK+NJ)-P(IJK+NJ+NJ))*FX(IJK+NJ)
        IJK=LKBK(K+KST)+LIBK(NI+IST)+J
        P(IJK)=P(IJK-NJ)+(P(IJK-NJ)-P(IJK-NJ-NJ))*
     *          (1.0D0-FX(IJK-NJ-NJ))
      END DO
      END DO
#ifndef USE_2DCASE
C
C.....EXTRAPOLATE TO BOTTOM AND TOP BOUNDARIES
C
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(1+KST)+LIBK(I+IST)+J
        P(IJK)=P(IJK+NIJ)+(P(IJK+NIJ)-P(IJK+NIJ+NIJ))*FZ(IJK+NIJ)
        IJK=LKBK(NK+KST)+LIBK(I+IST)+J
        P(IJK)=P(IJK-NIJ)+(P(IJK-NIJ)-P(IJK-NIJ-NIJ))*
     *         (1.0D0-FZ(IJK-NIJ-NIJ))
      END DO
      END DO
#endif
C
      END DO
#else
      DO II=1,NINLBKAL
        P(IJI(II))=P(IJPI(II))
      END DO
C
C.......EXTRAPOLATE BOUNDARY VALUES AT WALL BOUNDARIES
C
      DO IW=1,NWALBKAL
        P(IJW(IW))=P(IJPW(IW))
      END DO
C
C.......EXTRAPOLATE BOUNDARY VALUES AT OUTLET BOUNDARIES
C
      DO IO=1,NOUTBKAL
        P(IJO(IO))=P(IJPO(IO))
        U(IJO(IO))=U(IJPO(IO))
        V(IJO(IO))=V(IJPO(IO))
        W(IJO(IO))=W(IJPO(IO))
      END DO
#endif
C
C.....THERE ARE PRESSURE OUTLET CONDITIONS PRESENT
C
#if defined( USE_DIRICHLETPRESSURE )
      ENDIF
#endif
C
      CALL VecRestoreArray(DPXVEC,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYVEC,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZVEC,DPZARR,DPZZI,IERR)
C
      CALL VecRestoreArray(VOLVEC,VOLARR,VOLLI,IERR)
C
      CALL VecRestoreArray(APRVEC,APRARR,APRRI,IERR)
      CALL VecRestoreArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCVEC,ZCARR,ZCCI,IERR)
C
#if defined( USE_SUBVEC )
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PVEC,IERR)
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#endif
C
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
      CALL VecGhostUpdateBegin(
     *     UVWPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(
     *     UVWPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
C NECESSARY IF NOT USING PC REDISTRIBUTE; KSP CHANGES BOUNDARY VALUES
C     CALL BCIN
C
      RETURN
      END
C
#include "petsc.user.inc"
C#########################################################
      SUBROUTINE MASSFLUX
C#########################################################
C     This routine discretizes and solves the linearized
C     equations for X, Y and Z momentum componentS (U, V 
C     and W Cartesian velocity components).
C
C
C=========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER M,NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT
      INTEGER K,J,I,IJK,II,IJB,IJP,IO,IW,ISY,IJN,MIJ
      INTEGER LS
C
      REAL*8 GU,SB,APT,CP,CB,VISS,COEF,ARE,UPB,VPB,WPB
      REAL*8 CPU,CBU,CPV,CBV,CPW,CBW
      REAL*8 VNP,XTP,YTP,ZTP,VISOL,YNP,ZNP,XNP,FDE,DPB
      REAL*8 LPI,VISLL
      REAL*8 FAC,FLOWO,TEMP,FLOWOG
      REAL*8 DEN,SMDPN
      REAL*8 CAPP,DPCOR,XPN,YPN,ZPN
      PARAMETER (LPI=3.141592653589793238462643383279D0,VISLL=1.D0) 
C
      Vec APL,
     *    UL,VL,WL,PL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL,APRL
      Vec UVWPL,SUVWPL,SPL
      PetscScalar PZERO,PONE
      PetscInt PSIZE
      PetscErrorCode IERR
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C
C==============================================================
C
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UL,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VL,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WL,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PL,IERR)
C
C......UPDATE GRADIENTS
C
      CALL GRADFI(UL,DUXVEC,DUYVEC,DUZVEC)
      CALL GRADFI(VL,DVXVEC,DVYVEC,DVZVEC)
      CALL GRADFI(WL,DWXVEC,DWYVEC,DWZVEC)
      CALL GRADFI(PL,DPXVEC,DPYVEC,DPZVEC)
C
      CALL VecGetArray(UL,UARR,UUI,IERR)
      CALL VecGetArray(VL,VARR,VVI,IERR)
      CALL VecGetArray(WL,WARR,WWI,IERR)
      CALL VecGetArray(PL,PARR,PPI,IERR)
#else
      CALL GRADFI(UVWPL,DUXVEC,DUYVEC,DUZVEC,NOEQ,1)
      CALL GRADFI(UVWPL,DVXVEC,DVYVEC,DVZVEC,NOEQ,2)
      CALL GRADFI(UVWPL,DWXVEC,DWYVEC,DWZVEC,NOEQ,3)
      CALL GRADFI(UVWPL,DPXVEC,DPYVEC,DPZVEC,NOEQ,4)
C
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#endif
C
C.....GET ARRAY OF VECTOR VALUES: VELOCITY, GRADIENT COMPONENTS, DENS, VISC
C
      CALL VecGhostGetLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostGetLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostGetLocalForm(DUZVEC,DUZL,IERR)
C
      CALL VecGhostGetLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostGetLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostGetLocalForm(DVZVEC,DVZL,IERR)
C
      CALL VecGhostGetLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostGetLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostGetLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecGetArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecGetArray(DUZL,DUZARR,DUZZI,IERR)
C
      CALL VecGetArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecGetArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecGetArray(DVZL,DVZARR,DVZZI,IERR)
C
      CALL VecGetArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecGetArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecGetArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)
C
      CALL VecGhostGetLocalForm(APRVEC,APRL,IERR)
      CALL VecGetArray(APRL,APRARR,APRRI,IERR)
C
C......CACLULATE MASS FLUXES
C
C
C.....PRESSURE INLET BOUNDARIES / OUTLET (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C.....NOTE THAT VALUES HAVE BEEN EXTRAPOLATED IN PREFERENCE
C
      FLOWO=0.0D0
#if defined( USE_DIRICHLETPRESSURE )
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
C
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C
        DPX(IJB)=DPX(IJP)
        DPY(IJB)=DPY(IJP)
        DPZ(IJB)=DPZ(IJP)
C......EXTRAPOLATION OF THE VELOCITIES (ZERO ORDER PROFILE)
C
C       CALL FLUXM(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
C    *                     XUR(IO),YOR(IO),ZOR(IO),
C    *                     FMO(IO),ONE)
C
        XPN=XC(IJB)-XC(IJP)
        YPN=YC(IJB)-YC(IJP)
        ZPN=ZC(IJB)-ZC(IJP)
C
        IF (XUR(IO).NE.0.0D0) U(IJB)=U(IJP)
        IF (YOR(IO).NE.0.0D0) V(IJB)=V(IJP)
        IF (ZOR(IO).NE.0.0D0) W(IJB)=W(IJP)
C       U(IJB)=U(IJP)
C    *        +DUX(IJP)*XPN
C    *        +DUY(IJP)*YPN
C    *        +DUZ(IJP)*ZPN
C       V(IJB)=V(IJP)
C    *        +DVX(IJP)*XPN
C    *        +DVY(IJP)*YPN
C    *        +DVZ(IJP)*ZPN
C       W(IJB)=W(IJP)
C    *        +DWX(IJP)*XPN
C    *        +DWY(IJP)*YPN
C    *        +DWZ(IJP)*ZPN
C
        CAPP=VOL(IJP)*APR(IJP)
     *                        /DSQRT((XUR(IO)**2+YOR(IO)**2+ZOR(IO)**2)
     *                         *(XPN**2+YPN**2+ZPN**2))
C
        DPCOR=P(IJB)-P(IJP)-(DPX(IJP)*XPN
     *                      +DPY(IJP)*YPN
     *                      +DPZ(IJP)*ZPN)
C
        U(IJB)=U(IJB)-XUR(IO)*CAPP*DPCOR
        V(IJB)=V(IJB)-YOR(IO)*CAPP*DPCOR
        W(IJB)=W(IJB)-ZOR(IO)*CAPP*DPCOR
C
        FMO(IO)=U(IJB)*XUR(IO)
     *         +V(IJB)*YOR(IO)
     *         +W(IJB)*ZOR(IO)
C
        FLOWO=FLOWO+FMO(IO)
      END DO
#else
      CALL MPI_ALLREDUCE(
     *     FLOWO,FLOWOG,1,MPI_REAL8,MPI_SUM,PETSC_COMM_WORLD,IERR)
      FLOWO=FLOWOG
      FAC=FLOMAS/(FLOWO+SMALL)
C
       DO IO=1,NOUTBKAL
         IJP=IJPO(IO)
         IJB=IJO(IO)
C
         U(IJB)=U(IJP)
         V(IJB)=V(IJP)
         W(IJB)=W(IJP)
         FMO(IO)=U(IJB)*XUR(IO)
     *          +V(IJB)*YOR(IO)
     *          +W(IJB)*ZOR(IO)
         FMO(IO)=FMO(IO)*FAC
C        
C.......NOT SURE IF THIS IS CORRECT
         TEMP=DSQRT(XUR(IO)**2+YOR(IO)**2+ZOR(IO)**2)
         U(IJB)=U(IJB)*FAC*XUR(IO)/TEMP 
         V(IJB)=V(IJB)*FAC*YOR(IO)/TEMP
         W(IJB)=W(IJB)*FAC*ZOR(IO)/TEMP
C        U(IJB)=U(IJB)*FAC 
C        V(IJB)=V(IJB)*FAC
C        W(IJB)=W(IJB)*FAC
      END DO
#endif
#if defined( USE_INFO )
C.......CHECK CORRECTION
      TEMP=0.0D0
      DO IO=1,NOUTBKAL
        TEMP=TEMP+FMO(IO)
      END DO
      WRITE(*,*) "COMPARE INFLOW/OUTFLOW",FLOMAS,TEMP
#endif
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
      DO K=2,NKM 
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXM(IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                         XER(IJK),YER(IJK),ZER(IJK),
     *             F1(IJK),FX(IJK))
      END DO
      END DO
      END DO
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXM(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                       XNR(IJK),YNR(IJK),ZNR(IJK),
     *             F2(IJK),FY(IJK))
      END DO
      END DO
      END DO
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXM(IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                          XTR(IJK),YTR(IJK),ZTR(IJK),
     *             F3(IJK),FZ(IJK))
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
C.....O- AND C-GRID CUTS
C    
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        CALL FLUXM(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                     XOCR(I),YOCR(I),ZOCR(I),
     *             FMOC(I),FOCBK(I))
      END DO
C
C.....FACE SEGMENTS
C    
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXM(IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                     XFR(I),YFR(I),ZFR(I),
     *             FMF(I),FFSGBK(I))
      END DO
C
C......RESTORE ARRAYS
C
#if defined( USE_SUBVEC )
      CALL VecRestoreArray(UL,UARR,UUI,IERR)
      CALL VecRestoreArray(VL,VARR,VVI,IERR)
      CALL VecRestoreArray(WL,WARR,WWI,IERR)
      CALL VecRestoreArray(PL,PARR,PPI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PL,IERR)
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
      CALL VecRestoreArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecRestoreArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecRestoreArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecRestoreArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecRestoreArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecRestoreArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecRestoreArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecRestoreArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecRestoreArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
C
      CALL VecGhostRestoreLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostRestoreLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostRestoreLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostRestoreLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostRestoreLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostRestoreLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostRestoreLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostRestoreLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostRestoreLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
C
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecRestoreArray(APRL,APRARR,APRRI,IERR)
      CALL VecGhostRestoreLocalForm(APRVEC,APRL,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C##############################################################
      SUBROUTINE FLUXM(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                 FM,FAC)
C##############################################################
C     This routine calculates mass flux through the cell face 
C     between nodes IJP and IJN. IJ1 and IJ2 are the indices of 
C     CV corners defining the cell face. FM is the mass flux 
C     through the face, and FAC is the interpolation
C     factor (distance from node IJP to cell face center over
C     the sum of this distance and the distance from cell face 
C     center to node IJN). CAP and CAN are the contributions to
C     matrix coefficients in the pressure-correction equation
C     at nodes IJP and IJN. Surface vector directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI,DUXI,DVXI,DWXI
      REAL*8 DUYI,DVYI,DWYI,DUZI,DVZI,DWZI
      REAL*8 YYC,ZZC,UI,XXC,VI,WI,DENI,XPN,YPN
      REAL*8 ZPN,SMDPN,XCR,YCR,ZCR,CAP
      REAL*8 DPXI,DPYI,DPZI,FM
      REAL*8 VAP,VAN,SAP
C
#include "petsc.user.inc"
C
C==============================================================
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
C
      DUXI=DUX(IJN)*FAC+DUX(IJP)*FACP
      DVXI=DVX(IJN)*FAC+DVX(IJP)*FACP
      DWXI=DWX(IJN)*FAC+DWX(IJP)*FACP
C
      DUYI=DUY(IJN)*FAC+DUY(IJP)*FACP
      DVYI=DVY(IJN)*FAC+DVY(IJP)*FACP
      DWYI=DWY(IJN)*FAC+DWY(IJP)*FACP
C
      DUZI=DUZ(IJN)*FAC+DUZ(IJP)*FACP
      DVZI=DVZ(IJN)*FAC+DVZ(IJP)*FACP
      DWZI=DWZ(IJN)*FAC+DWZ(IJP)*FACP
C
C.....CALCULATE CELL-FACE VALUES (VELOCITIES AND DENSITY)
C
      UI=U(IJN)*FAC+U(IJP)*FACP
     *                         +DUXI*(XXC-XI)
     *                         +DUYI*(YYC-YI)
     *                         +DUZI*(ZZC-ZI)
      VI=V(IJN)*FAC+V(IJP)*FACP
     *                         +DVXI*(XXC-XI)
     *                         +DVYI*(YYC-YI)
     *                         +DVZI*(ZZC-ZI)
      WI=W(IJN)*FAC+W(IJP)*FACP
     *                         +DWXI*(XXC-XI)
     *                         +DWYI*(YYC-YI)
     *                         +DWZI*(ZZC-ZI)
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....SURFACE AND DISTANCE VECTOR COMPONENTS
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      SMDPN=(XCR**2+YCR**2+ZCR**2)/
     *      (XCR*XPN+YCR*YPN+ZCR*ZPN+SMALL)
C
C.....MASS FLUX
C
      CAP=-(FACP*VOL(IJP)*APR(IJP)+FAC*VOL(IJN)*APR(IJN))*DENI*SMDPN
      DPXI=0.5D0*(DPX(IJN)+DPX(IJP))*XPN
      DPYI=0.5D0*(DPY(IJN)+DPY(IJP))*YPN
      DPZI=0.5D0*(DPZ(IJN)+DPZ(IJP))*ZPN

      FM=DENI*(UI*XCR+VI*YCR+WI*ZCR)
     *   +CAP*(P(IJN)-P(IJP)-DPXI-DPYI-DPZI)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C##############################################################
      SUBROUTINE FLUXP(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                    CAP,CAN,FAC)
C##############################################################
C     This routine calculates mass flux through the cell face 
C     between nodes IJP and IJN. IJ1 and IJ2 are the indices of 
C     CV corners defining the cell face. FM is the mass flux 
C     through the face, and FAC is the interpolation
C     factor (distance from node IJP to cell face center over
C     the sum of this distance and the distance from cell face 
C     center to node IJN). CAP and CAN are the contributions to
C     matrix coefficients in the pressure-correction equation
C     at nodes IJP and IJN. Surface vector directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      IMPLICIT NONE
C
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI,DUXI,DVXI,DWXI
      REAL*8 DUYI,DVYI,DWYI,DUZI,DVZI,DWZI
      REAL*8 YYC,ZZC,UI,XXC,VI,WI,DENI,XPN,YPN,FII
      REAL*8 ZPN,SMDPN,XCR,YCR,ZCR,CAP,CAN
      REAL*8 DPXI,DPYI,DPZI,FM
C
      REAL*8 VAP,VAN,SAP,APRI,VOLI
C
#include "petsc.user.inc"
C
C==============================================================
C
      FACP=1.0D0-FAC
C
C.....CALCULATE CELL-FACE VALUES (DENSITY)
C
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
      VOLI=0.5D0*(VOL(IJN)+VOL(IJP))
      APRI=0.5D0*(APR(IJN)+APR(IJP))
C
C.....SURFACE AND DISTANCE VECTOR COMPONENTS
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
C----OVERRELAXED
      SMDPN=(XCR**2+YCR**2+ZCR**2)/
     *      (XCR*XPN+YCR*YPN+ZCR*ZPN+SMALL)
C
C.....COEFFICIENTS FOR THE P-EQUATION
C
C     CAP=-0.5D0*(VOL(IJP)*APR(IJP)+VOL(IJN)*APR(IJN))*DENI*SMDPN
      CAP=-(FACP*VOL(IJP)*APR(IJP)+FAC*VOL(IJN)*APR(IJN))*DENI*SMDPN
      CAN=CAP
C
C.....SOURCE TERM DUE TO RHIE-CHOW-INTERPOLATION (NO NON-ORTH-CORR.)
C
      VAP=VOL(IJP)*APR(IJP)
      VAN=VOL(IJN)*APR(IJN)
C
      DPXI=0.5D0*(DPX(IJN)+DPX(IJP))*XPN
      DPYI=0.5D0*(DPY(IJN)+DPY(IJP))*YPN
      DPZI=0.5D0*(DPZ(IJN)+DPZ(IJP))*ZPN
C
C     SAP=-((FAC *APR(IJN)*(DPX(IJN)*XCR
C    *                     +DPY(IJN)*YCR
C    *                     +DPZ(IJN)*ZCR)*VOL(IJN))
C    *     +(FACP*APR(IJP)*(DPX(IJP)*XCR
C    *                     +DPY(IJP)*YCR
C    *                     +DPZ(IJP)*ZCR)*VOL(IJP)))
      SAP=CAP*(DPXI+DPYI+DPZI)
C
      SP(IJP)=SP(IJP)+SAP
      SP(IJN)=SP(IJN)-SAP
C
#if defined( USE_NEWTONRAPHSON )
      FII=FAC*T(IJN)+FACP*T(IJP)
      SSC(IJP)=SSC(IJP)+SAP*FII*DENI
      SSC(IJN)=SSC(IJN)-SAP*FII*DENI
#endif
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C
C##############################################################
      SUBROUTINE FLUXMC(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                 FM,FAC)
C##############################################################
C     This routine calculates mass flux through the cell face 
C     between nodes IJP and IJN. IJ1 and IJ2 are the indices of 
C     CV corners defining the cell face. FM is the mass flux 
C     through the face, and FAC is the interpolation
C     factor (distance from node IJP to cell face center over
C     the sum of this distance and the distance from cell face 
C     center to node IJN). CAP and CAN are the contributions to
C     matrix coefficients in the pressure-correction equation
C     at nodes IJP and IJN. Surface vector directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI
      REAL*8 YYC,ZZC,XXC,DENI,XPN,YPN
      REAL*8 ZPN,SMDPN,XCR,YCR,ZCR
      REAL*8 DPXI,DPYI,DPZI,FM,RAPR,DN
C
#include "petsc.user.inc"
C==============================================================
C
C.....INTERPOLATE ALONG LINE P-N COORDINATES
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP    
C
C.....CALCULATE CELL-FACE VALUES (DENSITY)
C
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....SURFACE AND DISTANCE VECTOR COMPONENTS
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      SMDPN=XCR**2+YCR**2+ZCR**2
      DN=XPN*XCR+YPN*YCR+ZPN*ZCR
C
C.....MASS FLUX CORRECTION
C
      RAPR=-0.5D0*(VOL(IJP)*APR(IJP)+VOL(IJN)*APR(IJN))*DENI
      DPXI=0.5D0*(DPX(IJN)+DPX(IJP))
      DPYI=0.5D0*(DPY(IJN)+DPY(IJP))
      DPZI=0.5D0*(DPZ(IJN)+DPZ(IJP))
      FM=RAPR*((DN*XCR-XPN*SMDPN)*DPXI+(DN*YCR-YPN*SMDPN)*DPYI+
     &     (DN*ZCR-ZPN*SMDPN)*DPZI)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C###############################################################
#if defined( USE_SUBVEC )
      SUBROUTINE GRADFI(FIL,DFXVEC,DFYVEC,DFZVEC)
#else
      SUBROUTINE GRADFI(FIL,DFXVEC,DFYVEC,DFZVEC,MULTT,OFFSS)
#endif
C###############################################################
C     This routine calculates the components of the gradient
C     vector of a scalar FI at the CV center, using conservative
C     scheme based on the Gauss theorem; see Sect. 8.6 for 
C     details. FIE are values at east side, FIN at north side.
C     Contributions from boundary faces are calculated in a
C     separate loops...
C
C     Takes as argument vector FIL which is already in local form
C
C
C===============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "gradold3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER LC,M
      INTEGER I,II,IO,ISY,IW,K,LKK,LKI,JP,JPL,J
#ifndef USE_SUBVEC
      INTEGER OFFS,OFFSS,MULT,MULTT
#endif
C
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C
      Vec    DFXVEC,DFYVEC,DFZVEC,
     *       DFXL  ,DFYL  ,DFZL,
     *       DFXOL ,DFYOL ,DFZOL,
     *       FIVEC,FIL
      PetscOffset FIII,FIIIN,DFXXI ,DFYYI ,DFZZI,
     *                       DFXOOI,DFYOOI,DFZOOI
      PetscScalar PZERO
      PetscErrorCode IERR
      INTEGER LS,IJK
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
#ifndef USE_SUBVEC
     *                      OFFS,MULT,
#endif
     *                      DFXOOI,DFYOOI,DFZOOI
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C
C==============================================================
C
#if !defined( USE_SUBVEC )
      OFFS=OFFSS
      MULT=MULTT
#endif
      PZERO=0.0D0
C NEWCOUPLED - NOT NEEDED ANYMORE, UPDATE BEFORE CALLING THIS SUBROUTINE
C     CALL VecGhostUpdateBegin(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostUpdateEnd(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostGetLocalForm(FIVEC,FIL,IERR)
C
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C
C.....INITIALIZE OLD GRADIENT
C
      CALL VecSet(DFXOVEC,PZERO,IERR)
      CALL VecSet(DFYOVEC,PZERO,IERR)
      CALL VecSet(DFZOVEC,PZERO,IERR)
C
C.....START ITERATIVE CALCULATION OF GRADIENTS
C
      DO LC=1,NIGRAD
C
C.....GET ARRAY OF VECTOR VALUES
C
        IF (LC.NE.1) THEN
          CALL VecGhostUpdateBegin(
     *         DFXOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFXOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateBegin(
     *         DFYOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFYOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateBegin(
     *         DFZOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFZOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
        END IF
C
        CALL VecGhostGetLocalForm(DFXVEC,DFXL,IERR)
        CALL VecGhostGetLocalForm(DFYVEC,DFYL,IERR)
        CALL VecGhostGetLocalForm(DFZVEC,DFZL,IERR)
C
        CALL VecGhostGetLocalForm(DFXOVEC,DFXOL,IERR)
        CALL VecGhostGetLocalForm(DFYOVEC,DFYOL,IERR)
        CALL VecGhostGetLocalForm(DFZOVEC,DFZOL,IERR)
C
C.......INITIALIZE NEW GRADIENT
C
        CALL VecSet(DFXL,PZERO,IERR)
        CALL VecSet(DFYL,PZERO,IERR)
        CALL VecSet(DFZL,PZERO,IERR)
C
        CALL VecGetArray(DFXL,DFXARR,DFXXI,IERR)
        CALL VecGetArray(DFYL,DFYARR,DFYYI,IERR)
        CALL VecGetArray(DFZL,DFZARR,DFZZI,IERR)
C
        CALL VecGetArray(DFXOL,DFXOARR,DFXOOI,IERR)
        CALL VecGetArray(DFYOL,DFYOARR,DFYOOI,IERR)
        CALL VecGetArray(DFZOL,DFZOARR,DFZOOI,IERR)
C
c.....OpenMP : Start parallel loop
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT)
        DO M=1,NBLKS
C
        CALL SETIND(M)
C        
C.......CONTRIBUTION FROM INNER EAST SIDES
C
        CALL GRADCOLOOP(KST,IST,NKM,NIM-1,NJM,NJ,
     *       FIARR,DFXARR,DFYARR,DFZARR,FX,XEC,YEC,ZEC,XER,YER,ZER)
C
C.......CONTRIBUTION FROM INNER NORTH SIDES
C
        CALL GRADCOLOOP(KST,IST,NKM,NIM,NJM-1,1,
     *       FIARR,DFXARR,DFYARR,DFZARR,FY,XNC,YNC,ZNC,XNR,YNR,ZNR)
C
C.......CONTRIBUTION FROM INNER TOP SIDES
C
        CALL GRADCOLOOP(KST,IST,NKM-1,NIM,NJM,NIJ,
     *       FIARR,DFXARR,DFYARR,DFZARR,FZ,XTC,YTC,ZTC,XTR,YTR,ZTR)
C
        END DO
C.....OpenMP : Here ends this parrallel do
c
C
C.......CONTRIBUTION FROM O- AND C-GRID CUTS
C
        DO I=1,NOCBKAL
          CALL GRADCO(FIARR,DFXARR,DFYARR,DFZARR,
     *                FOCBK(I),IJLPBK(I),IJRPBK(I),
     *                XOCC(I),YOCC(I),ZOCC(I),
     *                XOCR(I),YOCR(I),ZOCR(I))
        END DO
C
C.......CONTRIBUTION FROM FACE SEGMENT BOUNDARIES
C
        DO I=1,NFSGBKAL
          CALL GRADCO(FIARR,DFXARR,DFYARR,DFZARR,
     *                FFSGBK(I),IJFL(I),IJFR(I),
     *                XFC(I),YFC(I),ZFC(I),
     *                XFR(I),YFR(I),ZFR(I))
        END DO
C
C.......CONTRIBUTION FROM INLET BOUNDARIES
C
        DO II=1,NINLBKAL
          CALL GRADBC(IJPI(II),IJI(II),XIR(II),YIR(II),ZIR(II),
     *                DFXARR,DFYARR,DFZARR,FIARR)
        END DO
C
C.......CONTRIBUTION FROM OUTLET BOUNDARIES
C
        DO IO=1,NOUTBKAL
          CALL GRADBC(IJPO(IO),IJO(IO),XUR(IO),YOR(IO),ZOR(IO),
     *                DFXARR,DFYARR,DFZARR,FIARR)
        END DO
C
C.......CONTRIBUTION FROM SYMMETRY BOUNDARIES
C
        DO ISY=1,NSYMBKAL
          CALL GRADBC(IJPS(ISY),IJS(ISY),XNS(ISY),YNS(ISY),ZNS(ISY),
     *                DFXARR,DFYARR,DFZARR,FIARR)
        END DO
C
C.......CONTRIBUTION FROM WALL BOUNDARIES
C
        DO IW=1,NWALBKAL
          CALL GRADBC(IJPW(IW),IJW(IW),XNW(IW),YNW(IW),ZNW(IW),
     *                DFXARR,DFYARR,DFZARR,FIARR)
        END DO
C
C.......CALCULATE GRADIENT COMPONENTS AT CV-CENTERS
C
C
C.....UPDATE GRADIENT COMPONENTS ON ALL PROCESSORS
C
        CALL VecRestoreArray(DFXL,DFXARR,DFXXI,IERR)
        CALL VecRestoreArray(DFYL,DFYARR,DFYYI,IERR)
        CALL VecRestoreArray(DFZL,DFZARR,DFZZI,IERR)
C
        CALL VecRestoreArray(DFXOL,DFXOARR,DFXOOI,IERR)
        CALL VecRestoreArray(DFYOL,DFYOARR,DFYOOI,IERR)
        CALL VecRestoreArray(DFZOL,DFZOARR,DFZOOI,IERR)
C
        CALL VecGhostRestoreLocalForm(DFXVEC,DFXL,IERR)
        CALL VecGhostRestoreLocalForm(DFYVEC,DFYL,IERR)
        CALL VecGhostRestoreLocalForm(DFZVEC,DFZL,IERR)
C
        CALL VecGhostRestoreLocalForm(DFXOVEC,DFXOL,IERR)
        CALL VecGhostRestoreLocalForm(DFYOVEC,DFYOL,IERR)
        CALL VecGhostRestoreLocalForm(DFZOVEC,DFZOL,IERR)
C
        CALL VecGhostUpdateBegin(DFXVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFXVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateBegin(DFYVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFYVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateBegin(DFZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
        CALL VecGetArray(DFXVEC,DFXARR,DFXXI,IERR)
        CALL VecGetArray(DFYVEC,DFYARR,DFYYI,IERR)
        CALL VecGetArray(DFZVEC,DFZARR,DFZZI,IERR)
C
C.....OpenMP : Here starts a parallel do
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,K,I,J,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,
C$OMP*                    LKK,LKI,JP,JPL)
        DO M=1,NBLKS
C
        CALL SETIND(M)
C        
        DO K=2,NKM
        DO I=2,NIM
        DO J=2,NJM
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          DFX(IJK)=DFX(IJK)/VOL(IJK)
          DFY(IJK)=DFY(IJK)/VOL(IJK)
          DFZ(IJK)=DFZ(IJK)/VOL(IJK)
        END DO
        END DO
        END DO
C
        END DO
C.....OpenMP : Here ends this parallel loop
C
C
        CALL VecRestoreArray(DFXVEC,DFXARR,DFXXI,IERR)
        CALL VecRestoreArray(DFYVEC,DFYARR,DFYYI,IERR)
        CALL VecRestoreArray(DFZVEC,DFZARR,DFZZI,IERR)
C
C.......SET OLD GRADIENT = NEW GRADIENT FOR THE NEXT ITERATION
C
        IF(LC.NE.NIGRAD) THEN
          CALL VecCopy(DFXVEC,DFXOVEC,IERR)
          CALL VecCopy(DFYVEC,DFYOVEC,IERR)
          CALL VecCopy(DFZVEC,DFZOVEC,IERR)
        ENDIF
C
      END DO
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
C
      CALL PetscBarrier(DFXVEC,IERR)
      CALL VecGhostUpdateBegin(
     *     DFXVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFXVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DFYVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFYVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DFZVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFZVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      RETURN
      END
C
#include "petsc.user.inc"
C
C###############################################################
      SUBROUTINE GRADCO(FIARR,DFXARR,DFYARR,DFZARR,FAC,IJP,IJN,
     *                  XXC,YYC,ZZC,XCR,YCR,ZCR)
C###############################################################
C     This routine calculates contribution to the gradient
C     vector of a scalar FI at the CV center, arising from
C     an inner cell face (cell-face value of FI times the 
C     corresponding component of the surface vector).
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C     For inner cells we use now gradcoloop. Gradco is used
C     only for interfaces now.
C
C===============================================================
      IMPLICIT NONE
C
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "gradold3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP
#ifndef USE_SUBVEC
      INTEGER OFFS,OFFSS,MULT,MULTT
#endif
C
      REAL*8 FACP,FAC,XI,YI,ZI,DFXI,DFYI,DFZI,XCC,YCC,ZCC
      REAL*8 DFXE,XCR,YCR,ZCR,DFYE,DFZE,FIE,XXC,YYC,ZZC
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
#ifndef USE_SUBVEC
     *                      OFFS,MULT,
#endif
     *                      DFXOOI,DFYOOI,DFZOOI
C
#include "petsc.user.inc"
C
C==============================================================
C
C.....COORDINATES OF POINT ON THE LINE CONNECTING CENTER AND NEIGHBOR,
C     OLD GRADIENT VECTOR COMPONENTS INTERPOLATED FOR THIS LOCATION
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
      DFXI=DFXO(IJN)*FAC+DFXO(IJP)*FACP
      DFYI=DFYO(IJN)*FAC+DFYO(IJP)*FACP
      DFZI=DFZO(IJN)*FAC+DFZO(IJP)*FACP
C
C.....VARIABLE VALUE AT THE CELL-FACE CENTER
C
#if defined( USE_SUBVEC )
      FIE=FI(IJN)*FAC+FI(IJP)*FACP
#else
      FIE=FI((IJN-1)*MULT+OFFS)*FAC
     *   +FI((IJP-1)*MULT+OFFS)*FACP
#endif
     *                            +DFXI*(XXC-XI)
     *                            +DFYI*(YYC-YI)
     *                            +DFZI*(ZZC-ZI)
C
C.....GRADIENT CONTRIBUTION FROM CELL FACE
C
      DFXE=FIE*XCR
      DFYE=FIE*YCR
      DFZE=FIE*ZCR  
C
C.....ACCUMULATE CONTRIBUTION AT CELL CENTER AND NEIGHBOR
C
      DFX(IJP)=DFX(IJP)+DFXE
      DFY(IJP)=DFY(IJP)+DFYE
      DFZ(IJP)=DFZ(IJP)+DFZE
      DFX(IJN)=DFX(IJN)-DFXE
      DFY(IJN)=DFY(IJN)-DFYE
      DFZ(IJN)=DFZ(IJN)-DFZE
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C###############################################################
      SUBROUTINE GRADCOLOOP(KSTTT,ISTTT,MKM,MIM,MJM,NPN,
     *           FIARR,DFXARR,DFYARR,DFZARR,
     *                          FACV,XXC,YYC,ZZC,XCR,YCR,ZCR)
C###############################################################
C     This routine calculates contribution to the gradient
C     vector of a scalar FI at the CV center, arising from
C     inner cell faces (cell-face value of FI times the 
C     corresponding component of the surface vector).
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C     This 'gradcoloop' routine substitutes gradco for the inner
C     cells. It incorporates de I,J,K loops, rather than being
C     called once for each node, so that it reduces the overhead
C     with significant savings ( gradient computations are 40%
C     faster this way ).      
C
C===============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "gradold3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER K,I,J,MKM,MIM,MJM,IJP,KSTTT,ISTTT,IJN,NPN
#ifndef USE_SUBVEC
      INTEGER OFFS,OFFSS,MULT,MULTT
#endif
C
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C
      REAL*8 XXC(NXYZA),YYC(NXYZA),ZZC(NXYZA)
      REAL*8 XCR(NXYZA),YCR(NXYZA),ZCR(NXYZA)
      REAL*8 FACV(NXYZA)
      REAL*8 FAC,FACP,XI,YI,ZI,DFXI,DFYI,DFZI,FIE
      REAL*8 DFXE,DFYE,DFZE
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
#ifndef USE_SUBVEC
     *                      OFFS,MULT,
#endif
     *                      DFXOOI,DFYOOI,DFZOOI
C
#include "petsc.user.inc"
C==============================================================
C
C.....LOOP THROUGH NODES
C
      DO K=2,MKM
      DO I=2,MIM
      DO J=2,MJM
        IJP=LKBK(K+KSTTT)+LIBK(I+ISTTT)+J
        IJN=IJP+NPN
C
C.....COORDINATES OF POINT ON THE LINE CONNECTING CENTER AND NEIGHBOR,
C.....OLD GRADIENT VECTOR COMPONENTS INTERPOLATED FOR THIS LOCATION
C
        FAC=FACV(IJP)
        FACP=1.0D0-FAC
        XI=XC(IJN)*FAC+XC(IJP)*FACP
        YI=YC(IJN)*FAC+YC(IJP)*FACP
        ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
        DFXI=DFXO(IJN)*FAC+DFXO(IJP)*FACP
        DFYI=DFYO(IJN)*FAC+DFYO(IJP)*FACP
        DFZI=DFZO(IJN)*FAC+DFZO(IJP)*FACP
C
C.....VARIABLE VALUE AT THE CELL-FACE CENTER
C
#if defined( USE_SUBVEC )
        FIE=FI(IJN)*FAC+FI(IJP)*FACP
#else
        FIE=FI((IJN-1)*MULT+OFFS)*FAC
     *     +FI((IJP-1)*MULT+OFFS)*FACP
#endif
     *                              +DFXI*(XXC(IJP)-XI)
     *                              +DFYI*(YYC(IJP)-YI)
     *                              +DFZI*(ZZC(IJP)-ZI)
C
C.....GRADIENT CONTRIBUTION FROM CELL FACE
C
        DFXE=FIE*XCR(IJP)
        DFYE=FIE*YCR(IJP)
        DFZE=FIE*ZCR(IJP)
C
C.....ACCUMULATE CONTRIBUTION AT CELL CENTER AND NEIGHBOR
C
        DFX(IJP)=DFX(IJP)+DFXE
        DFY(IJP)=DFY(IJP)+DFYE
        DFZ(IJP)=DFZ(IJP)+DFZE
        DFX(IJN)=DFX(IJN)-DFXE
        DFY(IJN)=DFY(IJN)-DFYE
        DFZ(IJN)=DFZ(IJN)-DFZE
C
      END DO
      END DO
      END DO
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C########################################################
      SUBROUTINE GRADBC(IJP,IJB,XCR,YCR,ZCR,
     *                  DFXARR,DFYARR,DFZARR,FIARR)
C########################################################
C     This routine calculates the contribution of a 
C     boundary cell face to the gradient at CV-center.
C
C=======================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "geo3d.inc"
#include "rcont3d.inc"
C
      INTEGER IJP,IJB
#ifndef USE_SUBVEC
      INTEGER OFFS,OFFSS,MULT,MULTT
#endif
C
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
      REAL*8 XCR,YCR,ZCR
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
#ifndef USE_SUBVEC
     *                      OFFS,MULT,
#endif
     *                      DFXOOI,DFYOOI,DFZOOI
C
#include "petsc.user.inc"
C
C=======================================================
C
#if defined( USE_SUBVEC )
      DFX(IJP)=DFX(IJP)+FI(IJB)*XCR
      DFY(IJP)=DFY(IJP)+FI(IJB)*YCR
      DFZ(IJP)=DFZ(IJP)+FI(IJB)*ZCR
#else
      DFX(IJP)=DFX(IJP)+FI((IJB-1)*MULT+OFFS)*XCR
      DFY(IJP)=DFY(IJP)+FI((IJB-1)*MULT+OFFS)*YCR
      DFZ(IJP)=DFZ(IJP)+FI((IJB-1)*MULT+OFFS)*ZCR
#endif
C
      RETURN
      END
C 
#include "petsc.user.inc"
C
C
#if defined( USE_ENERGYCOUPLING )
C#############################################################
      SUBROUTINE CALCEN
C#############################################################
C     This routine discretizes and solves the scalar transport
C     equation for temperature (implicit velocity-temperature 
C     coupling due to boussinesq approximation)
C=============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C     
      INTEGER IFI,M,I,J,K,IJK,IJP,IJN,MIJ,II,IJB
      INTEGER IO,ISY,IW
C
      REAL*8 GFI,URFFI,APT,CP,CB,FII,COEF
      REAL*8 FIARR(1)
      REAL*8  LPI
      PARAMETER (LPI=3.141592653589793238462643383279D0) 
C
      PetscScalar PONE,PZERO
      Vec STL,APL,TL,FIL,SSCL,
     *    DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL,UVWPL,SUVWPL
#if defined( USE_NEWTONRAPHSON )
      Vec APTL,
     *    APUL,APVL,APWL
#endif
C
      PetscErrorCode IERR
C
      PetscOffset FIII,STTI
      COMMON /SCALARFI/ FIII
C
#include "petsc.user.inc"
#ifndef USE_SUBVEC  
#undef FI
#undef SSC
#define FI(IJK)  UVWPARR(UVWPPI+(IJK-1)*NOEQ+5)
#define SSC(IJK) SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+5)
#endif
C============================================================
      PZERO=0.0D0
      PONE=1.0D0
C
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGhostGetLocalForm(SUVWPVEC,SUVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISE,FIL,IERR)
      CALL VecGetSubVector(SUVWPL,ISE,SSCL,IERR)
      CALL VecSet(SSCL,PZERO,IERR)
      CALL VecGetArray(SSCL,SSCARR,SSCCI,IERR)
C
C.....CALCULATE GRADIENTS OF T
C
      CALL GRADFI(FIL,DPXVEC,DPYVEC,DPZVEC)
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
#else
      CALL GRADFI(UVWPL,DPXVEC,DPYVEC,DPZVEC,NOEQ,5)
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
      CALL VecGetArray(SUVWPL,SUVWPARR,SUVWPPI,IERR)
#endif
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(DPXL,DSCXARR,DSCXXI,IERR)
      CALL VecGetArray(DPYL,DSCYARR,DSCYYI,IERR)
      CALL VecGetArray(DPZL,DSCZARR,DSCZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)
C
C
C.....INITIALIZE ARRAYS, SET BLENDING AND UNDER_RELAXATION COEFF.
C
C.....Zero the local Representation (including the ghost elements)
C
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
C
#if defined( USE_NEWTONRAPHSON )
      CALL VecGhostGetLocalForm(APTVEC,APTL,IERR)
      CALL VecGhostGetLocalForm(APUVEC,APUL,IERR)
      CALL VecGhostGetLocalForm(APVVEC,APVL,IERR)
      CALL VecGhostGetLocalForm(APWVEC,APWL,IERR)
C
      CALL VecSet(APTL,PZERO,IERR)
      CALL VecSet(APUL,PZERO,IERR)
      CALL VecSet(APVL,PZERO,IERR)
      CALL VecSet(APWL,PZERO,IERR)
C
      CALL VecGetArray(APTL,APTARR,APTTI,IERR)
      CALL VecGetArray(APUL,APUARR,APUUI,IERR)
      CALL VecGetArray(APVL,APVARR,APVVI,IERR)
      CALL VecGetArray(APWL,APWARR,APWWI,IERR)
C
      AET(1:NIJKBKAL)=0.0D0;  AWT(1:NIJKBKAL)=0.0D0
      ANT(1:NIJKBKAL)=0.0D0;  AST(1:NIJKBKAL)=0.0D0
      ATT(1:NIJKBKAL)=0.0D0;  ABT(1:NIJKBKAL)=0.0D0 
      ALT(1:NOCBKAL) =0.0D0;  ART(1:NOCBKAL )=0.0D0
      ALFT(1:NFSGBKAL)=0.0D0; ARFT(1:NFSGBKAL)=0.0D0
#else
C
      AE(1:NIJKBKAL)=0.0D0;  AW(1:NIJKBKAL)=0.0D0
      AN(1:NIJKBKAL)=0.0D0;  AS(1:NIJKBKAL)=0.0D0
      AT(1:NIJKBKAL)=0.0D0;  AB(1:NIJKBKAL)=0.0D0 
      AL(1:NOCBKAL) =0.0D0;  AR(1:NOCBKAL )=0.0D0
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
#endif
      CALL VecSet(APL,PZERO,IERR)
      CALL VecGetArray(APL,APARR,APPI,IERR)
C 
      GFI=GDS(IEN)
      URFFI=1.0D0/URF(IEN)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
CC.....OpenMP : Start parallel loop section
CC$OMP PARALLEL DO DEFAULT(SHARED), PRIVATE(M,K,I,J,
CC$OMP*  IJK,NKMT,NIMT,NJMT,NJT,NIJT,KSTT,ISTT,SB,APT)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
#if defined( USE_NEWTONRAPHSON )
C......COPY VALUES FROM PRESSURE EQUATION AND MULTIPLY WITH TEMPERATURE
        FII=FI(IJK+NJ)*FX(IJK)+FI(IJK)*(1.0D0-FX(IJK))
        AET(IJK   )=AE(IJK   )*FII
        AWT(IJK+NJ)=AW(IJK+NJ)*FII
        APT(IJK   )=APT(IJK   )-AWT(IJK+NJ)
        APT(IJK+NJ)=APT(IJK+NJ)-AET(IJK   )
#endif
C
        CALL FLUXSC(IEN,IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                             XER(IJK),YER(IJK),ZER(IJK),
     *               F1(IJK),AW(IJK+NJ),AE(IJK),FX(IJK),1.0D0,M,FIARR)
C
#if defined( USE_NEWTONRAPHSON )
        AEU(IJK   )=AEU(IJK   )*FII
        AWU(IJK+NJ)=AWU(IJK+NJ)*FII
        APU(IJK   )=APU(IJK   )-AWU(IJK+NJ)
        APU(IJK+NJ)=APU(IJK+NJ)-AEU(IJK   )
C
        AEV(IJK   )=AEV(IJK   )*FII
        AWV(IJK+NJ)=AWV(IJK+NJ)*FII
        APV(IJK   )=APV(IJK   )-AWV(IJK+NJ)
        APV(IJK+NJ)=APV(IJK+NJ)-AEV(IJK   )
C
        AEW(IJK   )=AEW(IJK   )*FII
        AWW(IJK+NJ)=AWW(IJK+NJ)*FII
        APW(IJK   )=APW(IJK   )-AWW(IJK+NJ)
        APW(IJK+NJ)=APW(IJK+NJ)-AEW(IJK   )
C
        SSC(IJK   )=SSC(IJK   )+F1(IJK)*FII
        SSC(IJK+NJ)=SSC(IJK+NJ)-F1(IJK)*FII
#endif
      END DO
      END DO
      END DO
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
#if defined( USE_NEWTONRAPHSON )
        FII=FI(IJK+1)*FY(IJK)+FI(IJK)*(1.0D0-FY(IJK))
        ANT(IJK  )=AN(IJK  )*FII
        AST(IJK+1)=AS(IJK+1)*FII
        APT(IJK  )=APT(IJK  )-AST(IJK+1)
        APT(IJK+1)=APT(IJK+1)-ANT(IJK  )
#endif
C
        CALL FLUXSC(IEN,IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                            XNR(IJK),YNR(IJK),ZNR(IJK),
     *               F2(IJK),AS(IJK+1),AN(IJK),FY(IJK),1.0D0,M,FIARR)
C
#if defined( USE_NEWTONRAPHSON )
        ANU(IJK  )=ANU(IJK  )*FII
        ASU(IJK+1)=ASU(IJK+1)*FII
        APU(IJK  )=APU(IJK  )-ASU(IJK+1)
        APU(IJK+1)=APU(IJK+1)-ANU(IJK  )
C
        ANV(IJK  )=ANV(IJK  )*FII
        ASV(IJK+1)=ASV(IJK+1)*FII
        APV(IJK  )=APV(IJK  )-ASV(IJK+1)
        APV(IJK+1)=APV(IJK+1)-ANV(IJK  )
C        
        ANW(IJK  )=ANW(IJK  )*FII
        ASW(IJK+1)=ASW(IJK+1)*FII
        APW(IJK  )=APW(IJK  )-ASW(IJK+1)
        APW(IJK+1)=APW(IJK+1)-ANW(IJK  )
C
        SSC(IJK  )=SSC(IJK  )+F2(IJK)*FII
        SSC(IJK+1)=SSC(IJK+1)-F2(IJK)*FII
#endif
      END DO
      END DO
      END DO
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
#if defined( USE_NEWTONRAPHSON )
        FII=FI(IJK+NIJ)*FZ(IJK)+FI(IJK)*(1.0D0-FZ(IJK))
        ATT(IJK    )=AT(IJK    )*FII
        ABT(IJK+NIJ)=AB(IJK+NIJ)*FII
        APT(IJK    )=APT(IJK    )-ABT(IJK+NIJ)
        APT(IJK+NIJ)=APT(IJK+NIJ)-ATT(IJK    )
#endif
C
        CALL FLUXSC(IEN,IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                              XTR(IJK),YTR(IJK),ZTR(IJK),
     *               F3(IJK),AB(IJK+NIJ),AT(IJK),FZ(IJK),1.0D0,M,FIARR)
C
#if defined( USE_NEWTONRAPHSON )
        ATU(IJK    )=ATU(IJK    )*FII        
        ABU(IJK+NIJ)=ABU(IJK+NIJ)*FII
        APU(IJK    )=APU(IJK    )-ABU(IJK+NIJ)
        APU(IJK+NIJ)=APU(IJK+NIJ)-ATU(IJK    )
C 
        ATV(IJK    )=ATV(IJK    )*FII
        ABV(IJK+NIJ)=ABV(IJK+NIJ)*FII
        APV(IJK    )=APV(IJK    )-ABV(IJK+NIJ)
        APV(IJK+NIJ)=APV(IJK+NIJ)-ATV(IJK    )
C 
        ATW(IJK    )=ATW(IJK    )*FII
        ABW(IJK+NIJ)=ABW(IJK+NIJ)*FII
        APW(IJK    )=APW(IJK    )-ABW(IJK+NIJ)
        APW(IJK+NIJ)=APW(IJK+NIJ)-ATW(IJK    )
C
        SSC(IJK    )=SSC(IJK    )+F3(IJK)*FII
        SSC(IJK+NIJ)=SSC(IJK+NIJ)-F3(IJK)*FII
#endif
      END DO
      END DO
      END DO
C
C......ADDITIONAL CONTRIBUTION DUE TO MANUFACTURED SOLUTION
C
#if defined( USE_ANALYTICAL )
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        SSC(IJK)=SSC(IJK)+STMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
C
      END DO
      END DO
      END DO
#endif
      END DO
C
C.....CONTRIBUTION FROM O- AND C-GRID CUTS
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
#if defined( USE_NEWTONRAPHSON )
        FII=FI(IJN)*FOCBK(I)+FI(IJP)*(1.0D0-FOCBK(I))
        ALT(I)=AL(I)*FII
        ART(I)=AR(I)*FII
        APT(IJP)=APT(IJP)-ART(I)
        APT(IJN)=APT(IJN)-ALT(I)
#endif
        MIJ=IBLKOCBK(I)
        CALL FLUXSC(IEN,IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                          XOCR(I),YOCR(I),ZOCR(I),
     *              FMOC(I),AL(I),AR(I),FOCBK(I),1.0D0,MIJ,FIARR)
        AP(IJP)=AP(IJP)-AR(I)
        AP(IJN)=AP(IJN)-AL(I)
C
#if defined( USE_NEWTONRAPHSON )
        ALU(I  )=ALU(I  )*FII
        ARU(I  )=ARU(I  )*FII
        APU(IJP)=APU(IJP)-ALU(I)
        APU(IJN)=APU(IJN)-ARU(I)
C
        ALV(I  )=ALV(I  )*FII
        ARV(I  )=ARV(I  )*FII
        APV(IJP)=APV(IJP)-ALV(I)
        APV(IJN)=APV(IJN)-ARV(I)
C
        ALW(I  )=ALW(I  )*FII
        ARW(I  )=ARW(I  )*FII
        APW(IJP)=APW(IJP)-ALW(I)
        APW(IJN)=APW(IJN)-ARW(I)
C
        SSC(IJP)=SSC(IJP)+FMOC(I)*FII
        SSC(IJN)=SSC(IJN)-FMOC(I)*FII
#endif
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE NO INTERNAL BOUNDARIES!)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
#if defined( USE_NEWTONRAPHSON )
        FII=FI(IJN)*FFSGBK(I)+FI(IJP)*(1.0D0-FFSGBK(I))
        ALFT(I)=AFL(I)*FII
        ARFT(I)=AFR(I)*FII
        APT(IJP)=APT(IJP)-ARFT(I)
        APT(IJN)=APT(IJN)-ALFT(I)
#endif
        CALL FLUXSC(IEN,IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                          XFR(I),YFR(I),ZFR(I),
     *              FMF(I),AFL(I),AFR(I),FFSGBK(I),1.0D0,MIJ,FIARR)
C
        AP(IJP) =AP(IJP)-AFR(I)
        AP(IJN) =AP(IJN)-AFL(I)
#if defined(USE_NEWTONRAPHSON)
C
        ARFU(I )=ARFU(I )*FII
        ALFU(I )=ALFU(I )*FII
        APU(IJP)=APU(IJP)-ALFU(I)
        APU(IJN)=APU(IJN)-ARFU(I)
C
        ARFV(I )=ARFV(I )*FII
        ALFV(I )=ALFV(I )*FII
        APV(IJP)=APV(IJP)-ALFV(I)
        APV(IJN)=APV(IJN)-ARFV(I)
C
        ARFW(I )=ARFW(I )*FII
        ALFW(I )=ALFW(I )*FII
        APW(IJP)=APW(IJP)-ALFW(I)
        APW(IJN)=APW(IJN)-ARFW(I)
C
        SSC(IJP)=SSC(IJP)+FMF(I)*FII
        SSC(IJN)=SSC(IJN)-FMF(I)*FII
C
#endif
      END DO
C
C.....UNSTEADY TERM CONTRIBUTION
C
      IF(LTIME) THEN
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        APT=DEN(IJK)*VOL(IJK)*DTR
        SSC(IJK)=SSC(IJK)+APT*((1.0D0+GAMT)*TO(IJK)-0.5D0*GAMT*TOO(IJK))
        AP(IJK)=AP(IJK)+APT*(1.0D0+0.5D0*GAMT)
      END DO
      END DO
      END DO
      END DO
      ENDIF
C
C.....INLET BOUNDARIES
C
      DO II=1,NINLBKAL
        IJP=IJPI(II)
        IJB=IJI(II)
        DSCX(IJB)=DSCX(IJP)
        DSCY(IJB)=DSCY(IJP)
        DSCZ(IJB)=DSCZ(IJP)
        CALL FLUXSC(IEN,IJP,IJB,XIC(II),YIC(II),ZIC(II),
     *                          XIR(II),YIR(II),ZIR(II),
     *               FMI(II),CP,CB,ONE,ZERO,1,FIARR)
        AP(IJP)=AP(IJP)-CB
        SSC(IJP)=SSC(IJP)-CB*FI(IJB)
      END DO
C
C.....OUTLET BOUNDARIES
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DSCX(IJB)=DSCX(IJP)
        DSCY(IJB)=DSCY(IJP)
        DSCZ(IJB)=DSCZ(IJP)
        CALL FLUXSC(IEN,IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                          XUR(IO),YOR(IO),ZOR(IO),
     *              FMO(IO),CP,CB,ONE,ZERO,1,FIARR)
        AP(IJP)=AP(IJP)-CB
        SSC(IJP)=SSC(IJP)-CB*FI(IJB)
      END DO
C
C.....ISOTHERMAL WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWST+1,IWST+NWALI
        IJP=IJPW(IW)
        IJB=IJW(IW)
        COEF=DCOEF*SRDW(IW)
        AP(IJP)=AP(IJP)+COEF
        SSC(IJP)=SSC(IJP)+COEF*FI(IJB)
      END DO
      END DO
C
C.....ADIABATIC WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWAT+1,IWAT+NWALA
        FI(IJW(IW))=FI(IJPW(IW))
      END DO
      END DO
C
C.....RESTORE ARRAYS THAT ARE NOT NEEDED ANYMORE
C
      CALL VecRestoreArray(DPXL,DSCXARR,DSCXXI,IERR)
      CALL VecRestoreArray(DPYL,DSCYARR,DSCYYI,IERR)
      CALL VecRestoreArray(DPZL,DSCZARR,DSCZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
C.....FINAL COEFFICIENT AND SOURCE MATRIX FOR FI-EQUATION
C
C.....OpenMP : Here starts parallel loop section
CC$OMP PARALLEL DO DEFAULT(SHARED), PRIVATE(M,
CC$OMP*  K,I,J,IJK,NKMT,NIMT,NJMT)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
C
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C       AP(IJK)=(AP(IJK)-AE(IJK)-AW(IJK)
C    *                  -AN(IJK)-AS(IJK)
C    *                  -AB(IJK)-AT(IJK))*URFFI
        AP(IJK)=AP(IJK)-AE(IJK)-AW(IJK)
     *                 -AN(IJK)-AS(IJK)
     *                 -AB(IJK)-AT(IJK)
C       SSC(IJK)=SSC(IJK)+(1.0D0-URF(IEN))*AP(IJK)*FI(IJK)
        SSC(IJK)=SSC(IJK)
C
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
C
C.....UPDATE SOURCE AND DIAGONAL VECTOR ON ALL PROCESSORS, ASSEMBLE MATRIX
C
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
#if defined( USE_NEWTONRAPHSON )
      CALL VecRestoreArray(APTL,APTARR,APTTI,IERR)
      CALL VecRestoreArray(APUL,APUARR,APUUI,IERR)
      CALL VecRestoreArray(APVL,APVARR,APVVI,IERR)
      CALL VecRestoreArray(APWL,APWARR,APWWI,IERR)
#endif
C
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
#if defined( USE_NEWTONRAPHSON )
      CALL VecGhostRestoreLocalForm(APTVEC,APTL,IERR)
      CALL VecGhostRestoreLocalForm(APUVEC,APUL,IERR)
      CALL VecGhostRestoreLocalForm(APVVEC,APVL,IERR)
      CALL VecGhostRestoreLocalForm(APWVEC,APWL,IERR)
#endif
C
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
#if defined( USE_NEWTONRAPHSON )
      CALL VecGhostUpdateBegin(APTVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APTVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APVVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APVVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APWVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APWVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
#endif
C
#if defined( USE_SUBVEC )
      CALL VecRestoreArray(FIL,FIARR,TTI,IERR)
      CALL VecRestoreSubVector(UVWPL,ISE,FIL,IERR)
      CALL VecRestoreArray(SSCL,SSCARR,SSCCI,IERR)
      CALL VecRestoreSubVector(SUVWPL,ISE,SSCL,IERR)
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
      CALL VecRestoreArray(SUVWPL,SUVWPARR,SUVWPPI,IERR)
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGhostRestoreLocalForm(SUVWPVEC,SUVWPL,IERR)
C
      CALL VecGhostUpdateBegin(SUVWPVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SUVWPVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....SOLVING EQUATION SYSTEM FOR SCALAR FI USING PETSC SOLVER
C
      CALL ASSEMBLESYSEN(AMAT)
C
      CALL MatZeroRowsColumns(
     *     AMAT,NZERO,ZEROS,PONE,UVWPVEC,SUVWPVEC,IERR)
      CALL PetscLogStagePush(STAGE1,IERR)
      CALL SOLVESYS(UVWPVEC,IU,AMAT,SUVWPVEC)
      CALL PetscLogStagePop(IERR)
C
C.....SYMMETRY AND OUTLET BOUNDARIES
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISE,FIL,IERR)
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#endif
C
      DO ISY=1,NSYMBKAL
        FI(IJS(ISY))=FI(IJPS(ISY))
      END DO
C
      DO IO=1,NOUTBKAL
        FI(IJO(IO))=FI(IJPO(IO))
      END DO
C
#if defined( USE_SUBVEC )
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
      CALL VecRestoreSubVector(UVWPL,ISE,FIL,IERR)
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
      CALL BCIN
C
C.....UPDATE DENSITY AND VISCOSITY
C
#if defined( USE_VARIABLEDENS )
      IF (IFI.EQ.IEN) THEN
      CALL VecGhostUpdateBegin(
     *     DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      ENDIF
#endif
C
      RETURN
      END 
C
C#ifndef( USE_SUBVEC ) 
C#undef FI
C#endif
#include "petsc.user.inc"
C
C
#endif
C#############################################################
      SUBROUTINE CALCSC(IFI,FIVEC,FIO,FIOO)
C#############################################################
C     This routine discretizes and solves the scalar transport
C     equations (temperature, turbulent kinetic energy, diss.).
C=============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C     
      INTEGER IFI,M,I,J,K,IJK,IJP,IJN,MIJ,II,IJB
      INTEGER IO,ISY
C
      REAL*8 GFI,URFFI,APT,CP,CB
      REAL*8 FIARR(1),FIO(NXYZA),FIOO(NXYZA)
      REAL*8  LPI
      PARAMETER (LPI=3.141592653589793238462643383279D0) 
C
      PetscScalar PONE,PZERO
      Vec SSCL,APL,
     *    FIVEC,FIL,
     *    DSCXL,DSCYL,DSCZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL
      PetscOffset FIII
      PetscErrorCode IERR
      COMMON /SCALARFI/ FIII
C
#include "petsc.user.inc"
C============================================================
      PZERO=0.0D0
      PONE=1.0D0
C
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
C.....FETCH SCALAR VALUES FROM OTHER PROCESSORS
C
      CALL VecGhostGetLocalform(FIVEC,FIL,IERR)
C
C.....CALCULATE GRADIENTS OF FI
C
#if defined( USE_SUBVEC )
      CALL GRADFI(FIL,DSCXVEC,DSCYVEC,DSCZVEC)
#else
      CALL GRADFI(FIL,DSCXVEC,DSCYVEC,DSCZVEC,1,1)
#endif
C
C.....GET ARRAY OF VECTOR VALUES: VELOCITY, GRADIENT COMPONENTS, DENS, VISC
C
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C
      CALL VecGhostGetLocalForm(DSCXVEC,DSCXL,IERR)
      CALL VecGhostGetLocalForm(DSCYVEC,DSCYL,IERR)
      CALL VecGhostGetLocalForm(DSCZVEC,DSCZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(DSCXL,DSCXARR,DSCXXI,IERR)
      CALL VecGetArray(DSCYL,DSCYARR,DSCYYI,IERR)
      CALL VecGetArray(DSCZL,DSCZARR,DSCZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)
C
C.....INITIALIZE ARRAYS, SET BLENDING AND UNDER_RELAXATION COEFF.
C
C.....Zero the local Representation (including the ghost elements)
C
      CALL VecGhostGetLocalForm(SSCVEC,SSCL,IERR)
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
C
      CALL VecSet(SSCL,PZERO,IERR)
      CALL VecSet(APL,PZERO,IERR)
C
      CALL VecGetArray(SSCL,SSCARR,SSCCI,IERR)
      CALL VecGetArray(APL,APARR,APPI,IERR)
C
C 
      AE(1:NIJKBKAL)=0.0D0;  AW(1:NIJKBKAL)=0.0D0
      AN(1:NIJKBKAL)=0.0D0;  AS(1:NIJKBKAL)=0.0D0
      AT(1:NIJKBKAL)=0.0D0;  AB(1:NIJKBKAL)=0.0D0 
      AL(1:NOCBKAL) =0.0D0;  AR(1:NOCBKAL )=0.0D0
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
      GFI=GDS(IFI)
      URFFI=1.0D0/URF(IFI)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
C
CC.....OpenMP : Start parallel loop section
CC$OMP PARALLEL DO DEFAULT(SHARED), PRIVATE(M,K,I,J,
CC$OMP*  IJK,NKMT,NIMT,NJMT,NJT,NIJT,KSTT,ISTT,SB,APT)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                             XER(IJK),YER(IJK),ZER(IJK),
     *               F1(IJK),AW(IJK+NJ),AE(IJK),FX(IJK),GFI,M,FIARR)
      END DO
      END DO
      END DO
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                            XNR(IJK),YNR(IJK),ZNR(IJK),
     *               F2(IJK),AS(IJK+1),AN(IJK),FY(IJK),GFI,M,FIARR)
      END DO
      END DO
      END DO
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                              XTR(IJK),YTR(IJK),ZTR(IJK),
     *               F3(IJK),AB(IJK+NIJ),AT(IJK),FZ(IJK),GFI,M,FIARR)
      END DO
      END DO
      END DO
C
C......ADDITIONAL CONTRIBUTION DUE TO MANUFACTURED SOLUTION
C
#if defined( USE_ANALYTICAL )
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        SSC(IJK)=SSC(IJK)+STMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
C
      END DO
      END DO
      END DO
#endif
C
      END DO
C
C.....CONTRIBUTION FROM O- AND C-GRID CUTS
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        MIJ=IBLKOCBK(I)
        CALL FLUXSC(IFI,IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                          XOCR(I),YOCR(I),ZOCR(I),
     *              FMOC(I),AL(I),AR(I),FOCBK(I),GFI,MIJ,FIARR)
        AP(IJP)=AP(IJP)-AR(I)
        AP(IJN)=AP(IJN)-AL(I)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE NO INTERNAL BOUNDARIES!)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXSC(IFI,IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                          XFR(I),YFR(I),ZFR(I),
     *              FMF(I),AFL(I),AFR(I),FFSGBK(I),GFI,MIJ,FIARR)
        AP(IJP) =AP(IJP)-AFR(I)
        AP(IJN) =AP(IJN)-AFL(I)
      END DO
C
C.....UNSTEADY TERM CONTRIBUTION
C
      IF(LTIME) THEN
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        APT=DEN(IJK)*VOL(IJK)*DTR
      SSC(IJK)=SSC(IJK)+APT*((1.0D0+GAMT)*FIO(IJK)-0.5D0*GAMT*FIOO(IJK))
        AP(IJK)=AP(IJK)+APT*(1.0D0+0.5D0*GAMT)
      END DO
      END DO
      END DO
      END DO
      ENDIF
C
C.....INLET BOUNDARIES
C
      DO II=1,NINLBKAL
        IJP=IJPI(II)
        IJB=IJI(II)
        DSCX(IJB)=DSCX(IJP)
        DSCY(IJB)=DSCY(IJP)
        DSCZ(IJB)=DSCZ(IJP)
        CALL FLUXSC(IFI,IJP,IJB,XIC(II),YIC(II),ZIC(II),
     *                          XIR(II),YIR(II),ZIR(II),
     *               FMI(II),CP,CB,ONE,ZERO,1,FIARR)
        AP(IJP)=AP(IJP)-CB
        SSC(IJP)=SSC(IJP)-CB*FI(IJB)
      END DO
C
C.....OUTLET BOUNDARIES
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DSCX(IJB)=DSCX(IJP)
        DSCY(IJB)=DSCY(IJP)
        DSCZ(IJB)=DSCZ(IJP)
        CALL FLUXSC(IFI,IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                          XUR(IO),YOR(IO),ZOR(IO),
     *              FMO(IO),CP,CB,ONE,ZERO,1,FIARR)
        AP(IJP)=AP(IJP)-CB
        SSC(IJP)=SSC(IJP)-CB*FI(IJB)
      END DO
C
C.....RESTORE GHOSTED VERSION
C
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
      CALL VecGhostRestoreLocalForm(FIVEC,FIL,IERR)
C
C.....WALL BOUNDARY CONDITIONS AND SOURCES FOR TEMPERATURE (NOT
C.....IMPLEMENTED YET
C
      IF(IFI.EQ.ISC) CALL TEMP
C
C.....WALL BOUNDARY CONDITIONS AND SOURCE FOR K-e
C
C     IF(IFI.EQ.ITE) CALL KINE
C     IF(IFI.EQ.IED) CALL DISE
C
C.....GENERIC SOURCE TERM MODIFICATIONS
C
C.....GET LOCAL REPRESENTATION OF VECTOR
C
      CALL VecGhostGetLocalForm(FIVEC,FIL,IERR)
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C END NEW
C     CALL SOURCESC(IFI,FI)
C
C.....RESTORE ARRAYS THAT ARE NOT NEEDED ANYMORE
C
      CALL VecRestoreArray(DSCXL,DSCXARR,DSCXXI,IERR)
      CALL VecRestoreArray(DSCYL,DSCYARR,DSCYYI,IERR)
      CALL VecRestoreArray(DSCZL,DSCZARR,DSCZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostRestoreLocalForm(DSCXVEC,DSCXL,IERR)
      CALL VecGhostRestoreLocalForm(DSCYVEC,DSCYL,IERR)
      CALL VecGhostRestoreLocalForm(DSCZVEC,DSCZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecRestoreArray(SSCL,SSCARR,SSCCI,IERR)
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
C
      CALL VecGhostRestoreLocalForm(SSCVEC,SSCL,IERR)
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
C
C.....UPDATE SOURCE AND DIAGONAL VECTOR ON ALL PROCESSORS
C
      CALL VecGhostUpdateBegin(SSCVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SSCVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....GET LOCAL PART OF AP,SU,APR
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(SSCVEC,SSCARR,SSCCI,IERR)
C
C.....FINAL COEFFICIENT AND SOURCE MATRIX FOR FI-EQUATION
C
C.....OpenMP : Here starts parallel loop section
CC$OMP PARALLEL DO DEFAULT(SHARED), PRIVATE(M,
CC$OMP*  K,I,J,IJK,NKMT,NIMT,NJMT)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
C
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        AP(IJK)=(AP(IJK)-AE(IJK)-AW(IJK)
     *                  -AN(IJK)-AS(IJK)
     *                  -AB(IJK)-AT(IJK))*URFFI
        SSC(IJK)=SSC(IJK)+(1.0D0-URF(IFI))*AP(IJK)*FI(IJK)
C
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(SSCVEC,SSCARR,SSCCI,IERR)
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
      CALL VecGhostRestoreLocalForm(FIVEC,FIL,IERR)
C
#if defined( USE_SIPSOL )
C
C.....SOLVING EQUATION SYSTEM FOR SCALAR FI USING SIP/CGSTAB SOLVER
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(FIVEC,FIARR,FIII,IERR)
      CALL VecGetArray(SSCVEC,SSCARR,SSCCI,IERR)
      CALL PetscLogStagePush(STAGE2,IERR)
      CALL SOLVER(FIARR,FIII,IEN)
      CALL PetscLogStagePop(IERR)
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(FIVEC,FIARR,FIII,IERR)
      CALL VecRestoreArray(SSCVEC,SSCARR,SSCCI,IERR)
#else
C
C.....SOLVING EQUATION SYSTEM FOR SCALAR FI USING PETSC SOLVER
C
      CALL ASSEMBLESYSSC(SCMAT)
      CALL MatZeroRowsColumns(
     *     SCMAT,NZEROSC,ZEROSSC,PONE,FIVEC,SSCVEC,IERR)
C
      CALL PetscLogStagePush(STAGE2,IERR)
      CALL SOLVESYSSC(FIVEC,ISC,SCMAT,SSCVEC)
      CALL PetscLogStagePop(IERR)
      CALL VecGhostUpdateBegin(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
#endif
C
C.....SYMMETRY AND OUTLET BOUNDARIES
C
      CALL VecGetArray(FIVEC,FIARR,FIII,IERR)
C
      DO ISY=1,NSYMBKAL
        FI(IJS(ISY))=FI(IJPS(ISY))
      END DO
C
      DO IO=1,NOUTBKAL
        FI(IJO(IO))=FI(IJPO(IO))
      END DO
C
      CALL VecRestoreArray(FIVEC,FIARR,FIII,IERR)
C
      CALL BCIN
C
C.....UPDATE DENSITY AND VISCOSITY
C
C     IF (IFI.EQ.IEN) THEN
C     CALL VecGhostUpdateBegin(
C    *     DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostUpdateEnd(DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostUpdateBegin(
C    *     VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostUpdateEnd(VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     ENDIF
C
C
      RETURN
      END 
C
#include "petsc.user.inc"
C
C
C################################################################
      SUBROUTINE FLUXSC(IFI,IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                  FM,CAP,CAN,FAC,G,MB,FIARR)
C################################################################
C     This routine calculates scalar fluxes (convective and
C     diffusive) through the cell face between nodes IJP and IJN.
C     It is analogous to the routine FLUXUV, see above. 
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C================================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP,MB,IFI
C
      REAL*8 FIARR( 1)
      REAL*8 FM,FACP,FAC,ZZC,XXC,YYC,XCR,YCR,ZCR,FMI
      REAL*8 FMX,FII,DENI,VISI,DFXI,DFYI,DFZI,XPN,YPN,ZPN
      REAL*8 VSOL,FCFIE,FCFII,FDFII,CAN,CAP,FFIC,G,FDFIE
C 
      PetscOffset FIII
      COMMON /SCALARFI/ FIII
C 
#include "petsc.user.inc"
#if !defined( USE_SUBVEC ) && defined( USE_ENERGYCOUPLING )
#undef FI
#undef SSC
#define FI(IJK)  UVWPARR(UVWPPI+(IJK-1)*NOEQ+5)
#define SSC(IJK) SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+5)
#endif
C 
C==============================================================
C
C.....INTERPOLATE ALONG LINE P-N
C
      FACP=1.0D0-FAC
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....COMPUTE FM
C
      FMI=MIN(FM,ZERO)
      FMX=MAX(FM,ZERO)
C
C.....INTERPOLATE ALONG LINE P-N
C
      FII=FI(IJN)*FAC+FI(IJP)*FACP
      VISI=VIS(IJN)*FAC+VIS(IJP)*FACP-VISC
      DFXI=DSCX(IJN)*FAC+DSCX(IJP)*FACP
      DFYI=DSCY(IJN)*FAC+DSCY(IJP)*FACP
      DFZI=DSCZ(IJN)*FAC+DSCZ(IJP)*FACP
C
C.....DIFFUSION COEFFICIENT
C
#if defined( USE_ANALYTICAL )
      IF(IFI.EQ.IEN.OR.IFI.EQ.ISC) DCOEF=1.0D0
#else
      IF(IFI.EQ.IEN.OR.IFI.EQ.ISC) DCOEF=VISC/PRANL
#endif
C
C     IF(IFI.EQ.ITE) DCOEF=VISC+VISI/SIGTE
C     IF(IFI.EQ.IED) DCOEF=VISC+VISI/SIGED
C
C.....DISTANCE VECTOR COMPONENTS, DIFFUSION COEFFICIENT
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      VSOL=DCOEF*SQRT((XCR**2+YCR**2+ZCR**2)/
     *                (XPN**2+YPN**2+ZPN**2))
C
C.....EXPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCFIE=FM*FII
      FDFIE=DCOEF*(DFXI*XCR+DFYI*YCR+DFZI*ZCR)
C
C.....IMPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCFII=FMI*FI(IJN)+FMX*FI(IJP)
      FDFII=VSOL*(DFXI*XPN+DFYI*YPN+DFZI*ZPN)
C
C.....COEFFICIENTS, DEFERRED CORRECTION, SOURCE TERMS
C
      CAN=-VSOL+FMI
      CAP=-VSOL-FMX
      FFIC=G*(FCFIE-FCFII)
      SSC(IJP)=SSC(IJP)-FFIC+FDFIE-FDFII
      SSC(IJN)=SSC(IJN)+FFIC-FDFIE+FDFII
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C############################################################### 
      SUBROUTINE TEMP
C###############################################################
C     This routine assembles the source terms (volume integrals)
C     and applies wall boundary conditions for the temperature
C     (energy) equation.
C===============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER M,IW,IJP,IJB
C
      REAL*8 COEF
C
      PetscErrorCode IERR
C
#include "petsc.user.inc"
#if !defined( USE_SUBVEC ) && defined( USE_ENERGYCOUPLING )
#undef FI
#undef SSC
#define FI(IJK)  UVWPARR(UVWPPI+(IJK-1)*NOEQ+5)
#define SSC(IJK) SUVWPARR(SUVWPPI+(IJK-1)*NOEQ+5)
#endif
C
C==============================================================      
C
C.....GET ARRAY OF TEMPERATURE
C
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
C.....NO VOLUMETRIC SOURCES OF THERMAL ENERGY 
C
C.....ISOTHERMAL WALL BOUNDARIES
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO IW=IWST+1,IWST+NWALI
        IJP=IJPW(IW)
        IJB=IJW(IW)
        COEF=DCOEF*SRDW(IW)
        AP(IJP)=AP(IJP)+COEF
        SSC(IJP)=SSC(IJP)+COEF*T(IJB)
      END DO
C
      END DO
C
C.....ADIABATIC WALL BOUNDARIES
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO IW=IWAT+1,IWAT+NWALA
        T(IJW(IW))=T(IJPW(IW))
      END DO
C
      END DO
C
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C########################################################
      SUBROUTINE SETIND(M)
C########################################################
C     This routine sets the indices for the current grid
C     block.
C========================================================     
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "indexc3d.inc"
C
      INTEGER M
C========================================================
C
      NI=NIBK(M)
      NJ=NJBK(M)
      NK=NKBK(M)
      IST=IBK(M)
      JST=JBK(M)
      KST=KBK(M)
      IJKST=IJKBK(M)
      NIJK=NIJKBK(M)
C
      NINL=NINLBK(M)
      NOUT=NOUTBK(M)
      NSYM=NSYMBK(M)
      NWAL=NWALBK(M)
      NWALA=NWALABK(M)
      NWALI=NWALIBK(M)
C
      NMTM=NMTMBK(M)
      NFSG=NFSGBK(M)
C
      IIST=IIBK(M)
      IOST=IOBK(M)
      ISST=ISBK(M)
      IWST=IWBK(M)
      IWAT=IWABK(M)
C
      IMST=IMBK(M)
      IFST=IFBK(M)
C
C
      NIM=NI-1
      NJM=NJ-1
      NKM=NK-1
      NIJ=NI*NJ
C
      RETURN
      END
C
C
C########################################################
      SUBROUTINE INIT
C########################################################
C     This routine reads input parameters, grid data etc.
C
C     Extended to 3D and block-structured grids 
C========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,M,K,J,IJK,NJBKAL,NIABK,NOABK,NSABK,NWABK
C
      PetscErrorCode IERR
      CHARACTER(LEN=100) STAGENAME
      Vec UVWPL
C
#include "petsc.user.inc"
C
C========================================================
C
C.....READ INPUT DATA IN THE FOLLOWING ORDER OF RECORDS:
C
C   1.  TITLE FOR THE PROBLEM SOLVED;
C   2.  LOGICAL CONTROL PARAMETERS;
C   3.  INDICES OF MONITORING LOCATION AND PRESSURE REFERENCE POINT,
C       NUMBER OF PRESSURE CORRECTIONS AND ITERATIONS ON GRADIENT;
C   4.  CONVERGENCE AND DIVERGENCE CRITERION, SIP-PARAMETER;
C   5.  DENSITY, DYNAMIC VISCOSITY AND PRANDTL NUMBER;
C   6.  GRAVITY COMP., EXPANSION COEF., HOT, COLD AND REFERENCE TEMP.;
C   7.  FIELD INITIALIZATION (UIN,VIN,WIN,PIN,TIN,TEIN,EDIN);
C   8.  LID VELOCITY AND PHYSICAL PARAMETERS;
C   9.  NO. OF TIME STEPS, OUTPUT CONTROL, TIME STEP, BLENDING FACTOR;
C  10.  LOGICAL CONTROL VARIABLES (EQ. TO BE SOLVED: U,V,W,PP,T,SMG,TE,ED,VIS);
C  11.  UNDER-RELAXATION FACTORS;
C  12.  CONVERGENCE CRITERION FOR INNER ITERATIONS;
C  13.  MAXIMUM ALLOWED NUMBER OF INNER ITERATIONS;
C  14.  BLENDING FACTOR FOR CONVECTIVE FLUXES;
C  15.  NUMBER OF OUTER ITERATIONS PER TIME STEP
C  16.  NUMBER OF GRID BLOCKS
C
#if defined( USE_INFO )
      WRITE(*,*) RANK, "READING CONTROL SETTINGS"
#endif
      READ(5,'(A50)') TITLE
      READ(5,*) LREAD,LWRITE,LPOST,LTEST,LOUTS,LOUTE,LTIME,LGRAD
      READ(5,*) IMON,JMON,KMON,MMON,RMON,IPR,JPR,KPR,MPR,NPCOR,NIGRAD 
      READ(5,*) SORMAX,SLARGE,ALFA
      READ(5,*) DENS,VISC,PRANL
      READ(5,*) GRAVX,GRAVY,GRAVZ,BETA,TH,TC,TREF
      READ(5,*) UIN,VIN,WIN,PIN,TIN,TEIN,EDIN
      READ(5,*) ULID,TPER,TGEN
      READ(5,*) ITSTEP,NOTT,DT,GAMT
      READ(5,*) (LCAL(I),I=1,NFI)
      READ(5,*) (URF(I),I=1,NFI)
      READ(5,*) (SOR(I),I=1,NFI)
      READ(5,*) (NSW(I),I=1,NFI)
      READ(5,*) (GDS(I),I=1,NFI)
      READ(5,*) LSG
C
      IF (RANK.EQ.0)THEN
        WRITE(*,*) '***************************************************'
        WRITE(*,*) 'CONTROL SETTINGS'
        WRITE(*,*) '***************************************************'
        WRITE(*,*) 'LREAD,LWRITE,LPOST,LTEST,LOUTS,LOUTE,LTIME,LGRAD'
        WRITE(*,*) LREAD,LWRITE,LPOST,LTEST,LOUTS,LOUTE,LTIME,LGRAD
        WRITE(*,*)
     *  " IMON, JMON, KMON, MMON, RMON,
     *  IPR,  JPR,  KPR,  MPR,NPCOR,NIGRAD "
       WRITE(*,'(11I6)') 
     *   IMON,JMON,KMON,MMON,RMON,IPR,JPR,KPR,MPR,NPCOR,NIGRAD 
        WRITE(*,*) " SORMAX,     SLARGE,     ALFA"
        WRITE(*,"(3E12.4)") SORMAX,SLARGE,ALFA
C       WRITE(*,*) "ITSTEP,NOTT,DT,GAMT"
C       WRITE(*,*) ITSTEP,NOTT,DT,GAMT
C       WRITE(*,*) "(LCAL(I),I=1,NFI)"
C       WRITE(*,*) (LCAL(I),I=1,NFI)
        WRITE(*,*) "(URF(I),I=1,6)"
        WRITE(*,"(6E12.4)") (URF(I),I=1,6)
        WRITE(*,*) "(SOR(I),I=1,6)"
        WRITE(*,"(6E12.4)") (SOR(I),I=1,6)
        WRITE(*,*) "(GDS(I),I=1,6) - BLENDING (CDS-UDS)"
        WRITE(*,"(6E12.4)") (GDS(I),I=1,6)
        WRITE(*,*) "LSG"
        WRITE(*,"(I6)") LSG
#if defined (USE_ENERGYCOUPLING)
        WRITE(*,*) "USING ENERGYCOUPLING"
#endif
#if defined (USE_NEWTONRAPHSON)
        WRITE(*,*) "USING NEWTONRAPHSON LINEARIZATION"
#endif
      END IF
C
      IF (LCAL(IEN).AND.LCAL(ISC)) THEN
        WRITE(*,*) "ERROR - NOT IMPLEMENTED YET"
        STOP
      END IF
C
C.....READ BLOCK AND GRID DATA, GEOMETRY 
C
#if defined( USE_INFO )
      WRITE(*,*) RANK,"READING GRID"
#endif
      CALL READGRIDS
C
C.....OFFSET B.C. INDEXES
C
      CALL OFFSETBC
C
C.....CHECK IF MONITORING POINT IS OK.
C
      IF(IMON.GT.NIBK(MMON)-1) IMON=NIBK(MMON)/2
      IF(JMON.GT.NJBK(MMON)-1) JMON=NJBK(MMON)/2
      IF(KMON.GT.NKBK(MMON)-1) KMON=NKBK(MMON)/2
C
C.....SET MONITORING LOCATION 
C
      IJKMON=LKBK(KBK(MMON)+KMON)+LIBK(IBK(MMON)+IMON)+JMON
C
C.....SET PRESSURE REFERENCE LOCATION
C
      IJKPR=LKBK(KBK(MPR)+KPR)+LIBK(IBK(MPR)+IPR)+JPR
C
C.....RECIPROCAL VALUES OF URF & TIME STEP
C
      URFU=1.0D0/(URF(IU)+SMALL)
      URFV=1.0D0/(URF(IV)+SMALL)
      URFW=1.0D0/(URF(IWW)+SMALL)
      URFP=1.0D0/(URF(IP)+SMALL)
      DTR=1.0D0/DT
C
C.....INITIALIZE VISCOSITY AND DENSITY AT ALL NODES
C
      CALL VecSet(VISVEC,VISC,IERR)
      CALL VecSet(DENVEC,DENS,IERR)
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecGetSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef  USE_ENERGYCOUPLING 
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
C
C.....INITIALIZE VARIABLES AT INNER NODES OF ALL GRID BLOCKS
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        U(IJK) =UIN
        V(IJK) =VIN
        W(IJK) =WIN
        P(IJK) =PIN
        T(IJK) =TIN
        UO(IJK)=UIN
        VO(IJK)=VIN
        WO(IJK)=WIN
        TO(IJK)=TIN
        TE(IJK)=TEIN
        ED(IJK)=EDIN
        TEO(IJK)=TEIN
        EDO(IJK)=EDIN
      END DO
      END DO
      END DO
      END DO
C
C.....RESTORE ARRAYS FROM VECTORS
C
#if defined(USE_SUBVEC)
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecRestoreSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef USE_ENERGYCOUPLING 
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
C.....Initialize Geometry. 
C
      CALL INITGEO
C
C.....Call to Special I.C. Routine           
C
      CALL INITCOND
C
C......Petsc Logging Stages
C
      WRITE (STAGENAME,"(A8)") "CPLD_SOLVE"
      CALL PetscLogStageRegister(STAGENAME,STAGE1,IERR)
      WRITE (STAGENAME,"(A8)") "PSSC_SOLVE"
      CALL PetscLogStageRegister(STAGENAME,STAGE2,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C###########################################################
      SUBROUTINE READGRIDS
C###########################################################
C     This routine reads the block and grid files where
C     information about topology, geometry and boundary
C     is stored.
C
C     First the 'block' file ('name'.bck) file is read
C     and then each grid block is read through a loop.
C
C
C===========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "charac3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,M,K,XECC(NXYZA)
      INTEGER NJBKAL,NIABK,NOABK,NSABK,NWABK,NOCABK
      INTEGER NMABK,NFABK
      INTEGER NWAISBK,NWAADBK,NINX,NOUX,NSYX,NWAX,NOCX
      INTEGER NMTX
      PetscErrorCode IERR
C
#include "petsc.user.inc"
C
C===========================================================
C
C.....READ BLOCK FILE (UNIT=4)
C
      READ(4)  NBLKS,NIBKAL,NJBKAL,NKBKAL,NIJKBKAL,
     *         NINLBKAL,NOUTBKAL,NSYMBKAL,NWALBKAL,
     *         NOCBKAL,NIABK,NOABK,NSABK,NWABK,NOCABK,
C
     *         NFSGBKAL,NFABK
C
      READ(4) (LIBK(I) ,I=1,NIBKAL),( LKBK(I),I=1,NKBKAL),
     *        (NIBK(I) ,I=1,NBLKS) ,( NJBK(I),I=1,NBLKS),
     *        (NKBK(I) ,I=1,NBLKS) ,(  IBK(I),I=1,NBLKS),
     *        ( JBK(I) ,I=1,NBLKS) ,(  KBK(I),I=1,NBLKS),
     *        (IJKBK(I),I=1,NBLKS),(NIJKBK(I),I=1,NBLKS),
     *        IJKPRC
C
      READ(4) (NINLBK(I),I=1,NBLKS),(IIBK(I),I=1,NBLKS),
     *        (NOUTBK(I),I=1,NBLKS),(IOBK(I),I=1,NBLKS),
     *        (NWALBK(I),I=1,NBLKS),(IWBK(I),I=1,NBLKS),
     *        (NSYMBK(I),I=1,NBLKS),(ISBK(I),I=1,NBLKS),
     *        (NWALABK(I),I=1,NBLKS),(IWABK(I),I=1,NBLKS),
     *        (NWALIBK(I),I=1,NBLKS),
     *        (NMTMBK(I),I=1,NBLKS),(IMBK(I),I=1,NBLKS),
     *        (NFSGBK(I),I=1,NBLKS),(IFBK(I),I=1,NBLKS)
C
      READ(4) (IJLPBK(I) ,I=1,NOCABK),(IJLBBK(I) ,I=1,NOCABK),
     *        (IJRPBK(I) ,I=1,NOCABK),(IJRBBK(I) ,I=1,NOCABK),
     *        (IJOC1BK(I),I=1,NOCABK),(IJOC2BK(I),I=1,NOCABK),
     *        (IJOC3BK(I),I=1,NOCABK),(IJOC4BK(I),I=1,NOCABK),
     *        (FOCBK(I)  ,I=1,NOCABK),(ITAGOCBK(I),I=1,NOCABK),
     *        (IBLKOCBK(I),I=1,NOCABK)
C
      READ(4) (IJFL(I),I=1,NFABK),(IJFR(I),I=1,NFABK),
     *        (FFSGBK(I),I=1,NFABK),
     *        (XFR(I),I=1,NFABK),(YFR(I),I=1,NFABK),
     *        (ZFR(I),I=1,NFABK),
     *        (XFC(I),I=1,NFABK),(YFC(I),I=1,NFABK),
     *        (ZFC(I),I=1,NFABK)
C
C.....CREATE VECTORS, GHOSTING ETC.
C
      CALL DISTRIBUTELOAD
C
C.....PREPARE VOLUME VECTOR
C
      CALL VecGetArray(VOLVEC,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecGetArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCVEC,ZCARR,ZCCI,IERR)
C
C.....LOOP THROUGH BLOCKS READING EACH GRID BLOCK FILE INTO THE ARRAYS
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....READ FILE NAME FOR THIS GRID BLOCK, OPEN FILE
C
      READ(9,'(A13)') FILGRD
C
      OPEN (UNIT=8,FILE=FILGRD,FORM='UNFORMATTED',POSITION='REWIND')
C
      READ(8) NI,NJ,NK,NIJK,NIABK,NOABK,NSABK,NWABK,NOCABK,
     *        NMABK,
     *        NWAISBK,NWAADBK,NINX,NOUX,NSYX,NWAX,NOCX,
     *        NMTX
C
C.....ARRAY XEC IS USED HERE FOR 'dummy' READING
C
      READ(8) (XECC(I),I=1,NI),(XECC(K),K=1,NK)
C
      READ(8) (IJI(I),I=IIST+1,IIST+NINX),(IJPI(I),I=IIST+1,IIST+NINX),
     *   (IJI1(I) ,I=IIST+1,IIST+NINX),(IJI2(I),I=IIST+1,IIST+NINX),
     *   (IJI3(I) ,I=IIST+1,IIST+NINX),(IJI4(I),I=IIST+1,IIST+NINX),
     *   (ITAGI(I),I=IIST+1,IIST+NINX)
C
      READ(8) (IJO(I),I=IOST+1,IOST+NOUX),(IJPO(I),I=IOST+1,IOST+NOUX),
     *   (IJO1(I) ,I=IOST+1,IOST+NOUX),(IJO2(I),I=IOST+1,IOST+NOUX),
     *   (IJO3(I) ,I=IOST+1,IOST+NOUX),(IJO4(I),I=IOST+1,IOST+NOUX),
     *   (ITAGO(I),I=IOST+1,IOST+NOUX)

      READ(8) (IJW(I),I=IWST+1,IWST+NWAX),(IJPW(I),I=IWST+1,IWST+NWAX),
     *   (IJW1(I) ,I=IWST+1,IWST+NWAX),(IJW2(I),I=IWST+1,IWST+NWAX),
     *   (IJW3(I) ,I=IWST+1,IWST+NWAX),(IJW4(I),I=IWST+1,IWST+NWAX),
     *   (ITAGW(I),I=IWST+1,IWST+NWAX)
C
      READ(8) (IJS(I),I=ISST+1,ISST+NSYX),(IJPS(I),I=ISST+1,ISST+NSYX),
     *   (IJS1(I) ,I=ISST+1,ISST+NSYX),(IJS2(I),I=ISST+1,ISST+NSYX),
     *   (IJS3(I) ,I=ISST+1,ISST+NSYX),(IJS4(I),I=ISST+1,ISST+NSYX),
     *   (ITAGS(I),I=ISST+1,ISST+NSYX)
C
C.....ARRAY XECC IS USED HERE FOR 'dummy' READING
C
      READ(8) (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX)
C   
C.....ARRAY XECC IS USED HERE FOR 'dummy' READING
C
      READ(8)
     *        (IJML(I),I=IMST+1,IMST+NMTX),(IJMR(I),I=IMST+1,IMST+NMTX),
     *        (XECC(I),I=1,NMTX),(XECC(I),I=1,NMTX),
     *        (XECC(I),I=1,NMTX),(XECC(I),I=1,NMTX),
     *        (XECC(I),I=1,NMTX)
C
      READ(8) (X(I),I=IJKST+1,IJKST+NIJK),(Y(I),I=IJKST+1,IJKST+NIJK),
     *   (Z(I) ,I=IJKST+1,IJKST+NIJK), (XC(I),I=IJKST+1,IJKST+NIJK),
     *   (YC(I),I=IJKST+1,IJKST+NIJK), (ZC(I),I=IJKST+1,IJKST+NIJK),
     *   (FX(I),I=IJKST+1,IJKST+NIJK), (FY(I),I=IJKST+1,IJKST+NIJK),
     *   (FZ(I),I=IJKST+1,IJKST+NIJK),(VOL(I),I=IJKST+1,IJKST+NIJK),
     *   (SRDW(I),I=IWST+1,IWST+NWAX),(XNW(I),I=IWST+1,IWST+NWAX),
     *   (YNW(I) ,I=IWST+1,IWST+NWAX),(ZNW(I),I=IWST+1,IWST+NWAX),
     *   (SRDS(I),I=ISST+1,ISST+NSYX),(XNS(I),I=ISST+1,ISST+NSYX),
     *   (YNS(I),I=ISST+1,ISST+NSYX),(ZNS(I),I=ISST+1,ISST+NSYX),
C
C.....ARRAY XEC IS USED HERE FOR 'dummy' READING
C
     *   (XEC(I),I=1,NOCX)
C
      CLOSE(UNIT=8)
C
      END DO
C
C.....RESTORE VOLUME ARRAY AND EXCHANGE VALUES
C
      CALL VecRestoreArray(VOLVEC,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCVEC,ZCARR,ZCCI,IERR)
C
      CALL VecGhostUpdateBegin(
     *     VOLVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VOLVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(XCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(XCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(YCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(YCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(ZCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(ZCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C########################################################
      SUBROUTINE SETDAT
C########################################################
C     In this routine some constants are assigned values.
C
C
C========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "indexc3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
C========================================================
C
      IU=1
      IV=2
      IWW=3
      IP=4
      IEN=5
      ISC=6
C
      SMALL=1.E-20
      GREAT=1.E+20
      ONE=1.0D0
      ZERO=0.0D0
      PII=3.14159265358979
C
      RETURN
      END
C
C
C###################################################################
       SUBROUTINE SRES(RNK)
C###################################################################
C     This routine writes out the results onto a file
C     so that re-start is possible at a later stage.
C===================================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "charac3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER IJK,I,RNK
C
      PetscErrorCode IERR
      
#include "petsc.user.inc"
C
C==================================================================
C
      WRITE(FILRES,'(A7,I4.4,4H.res)') NAME, RNK
      OPEN (UNIT=3,FILE=FILRES,FORM='UNFORMATTED',POSITION='REWIND')
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
      WRITE(3)ITIM,TIME,(F1(IJK),IJK=1,NIJKBKAL),
     *        (F2(IJK),IJK=1,NIJKBKAL),(F3(IJK),IJK=1,NIJKBKAL),
     *        (U(IJK), IJK=1,NIJKBKAL),(V(IJK), IJK=1,NIJKBKAL),
     *        (W(IJK), IJK=1,NIJKBKAL),(P(IJK), IJK=1,NIJKBKAL),
     *        (T(IJK), IJK=1,NIJKBKAL),(TE(IJK), IJK=1,NIJKBKAL),
     *        (ED(IJK), IJK=1,NIJKBKAL),(FMOC(I),I=1,NOCBKAL),
     *        (FMF(I),I=1,NFSGBKAL),(RESINI(I),I=1,4),
     *        (RESOR(I),I=1,4)
C
      IF(LTIME) WRITE(3) (UO(IJK),IJK=1,NIJKBKAL),
     *        (VO (IJK),IJK=1,NIJKBKAL),(WO (IJK),IJK=1,NIJKBKAL),
     *        (TO (IJK),IJK=1,NIJKBKAL),(TEO(IJK), IJK=1,NIJKBKAL),
     *        (EDO(IJK),IJK=1,NIJKBKAL)
C
      CLOSE(UNIT=3)
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C########################################################
      SUBROUTINE INITGEO
C########################################################
C     This routine initializes geometry arrays
C========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
C
      INTEGER M,K,I,J,IJK,II,IO,IW,IJB,IJP
      REAL*8 ARE
C
#include "petsc.user.inc"
C========================================================
C
C.....Loop through nodes. East faces
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=1,NIM
      DO J=2,NJM
C
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK,IJK-1,IJK-1-NIJ,IJK-NIJ,
     *                XEC(IJK),YEC(IJK),ZEC(IJK),
     *                XER(IJK),YER(IJK),ZER(IJK))
      END DO
      END DO
      END DO
C
      END DO
C
C.....Loop through nodes. North faces
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=1,NJM
C
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK-NJ,IJK,IJK-NIJ,IJK-NIJ-NJ,
     *                XNC(IJK),YNC(IJK),ZNC(IJK),
     *                XNR(IJK),YNR(IJK),ZNR(IJK))
C
      END DO
      END DO
      END DO
C
      END DO
C
C.....Loop through nodes. Top faces
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=1,NKM
      DO I=2,NIM
      DO J=2,NJM
C
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK,IJK-NJ,IJK-NJ-1,IJK-1,
     *                XTC(IJK),YTC(IJK),ZTC(IJK),
     *                XTR(IJK),YTR(IJK),ZTR(IJK))
C
      END DO
      END DO
      END DO
C
      END DO
C
C.....Loop through Inlet faces
C
      DO II=1,NINLBKAL
        CALL CALCFACE(IJI1(II),IJI2(II),IJI3(II),IJI4(II),
     *                XIC(II),YIC(II),ZIC(II),
     *                XIR(II),YIR(II),ZIR(II))
      END DO
C
C.....Loop through Outlet faces
C
      DO IO=1,NOUTBKAL
        CALL CALCFACE(IJO1(IO),IJO2(IO),IJO3(IO),IJO4(IO),
     *                XOC(IO),YOC(IO),ZOC(IO),
     *                XUR(IO),YOR(IO),ZOR(IO))
      END DO
C
C.....Loop through OC faces
C
      DO I=1,NOCBKAL
        CALL CALCFACE(IJOC1BK(I),IJOC2BK(I),IJOC3BK(I),IJOC4BK(I),
     *                XOCC(I),YOCC(I),ZOCC(I),
     *                XOCR(I),YOCR(I),ZOCR(I))
      END DO
C
C.....NORMAL DISTANBE FROM CELL FACE CENTER TO CELL CENTER
C
      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        IJP=IJPW(IW)
        ARE=SQRT(XNW(IW)**2+YNW(IW)**2+ZNW(IW)**2)
        DN(IW)=((XC(IJB)-XC(IJP))*XNW(IW)+
     *          (YC(IJB)-XC(IJP))*YNW(IW)+
     *          (ZC(IJB)-ZC(IJP))*ZNW(IW))/(ARE+SMALL)
C
      END DO
C
      RETURN
C
      END
C
#include "petsc.user.inc"
C
C
C########################################################
      SUBROUTINE CALCFACE(IJK1,IJK2,IJK3,IJK4,
     *                    XXXC,YYYC,ZZZC,XXCR,YYCR,ZZCR)
C########################################################
C     This routine calculates the surface vector and
C     centre of cell IJK.
C
C     IJK1,IJK2,IJK3,IJK4 define a face by its four corners
C     in clockwise order ( so that the normal vector points out)
C     XXXC,YYYC,ZZZC are the coord of baricenter of the face
C     XXCR,YYCR,ZZCR are the components of the (normal) surface
C     vector (pointing out)
C
C     See 8.6.4 for details
C
C========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "geo3d.inc"
#include "rcont3d.inc"
C
      INTEGER IJK1,IJK2,IJK3,IJK4
C
      REAL*8 DX12,DY12,DZ12,DX13,DY13,DZ13,DX14,DY14,DZ14
      REAL*8 XR23,YR23,ZR23,XR34,YR34,ZR34,XXCR,YYCR,ZZCR
      REAL*8 S23,S34,XXXC,YYYC,ZZZC
C========================================================
C
C.....Vectors to vertices ( from IJK1 to IJK2, IJK3 and IJK4 )
      DX12=X(IJK2)-X(IJK1)
      DY12=Y(IJK2)-Y(IJK1)
      DZ12=Z(IJK2)-Z(IJK1)
C
      DX13=X(IJK3)-X(IJK1)
      DY13=Y(IJK3)-Y(IJK1)
      DZ13=Z(IJK3)-Z(IJK1)
C
      DX14=X(IJK4)-X(IJK1)
      DY14=Y(IJK4)-Y(IJK1)
      DZ14=Z(IJK4)-Z(IJK1)
C
C.....Cross Products for triangle surface vectors 
C.....This is (IJK1,IJK2,IJK3)
      XR23=DY12*DZ13-DZ12*DY13
      YR23=DZ12*DX13-DX12*DZ13
      ZR23=DX12*DY13-DY12*DX13
C
C.....This is (IJK1,IJK3,IJK4)
      XR34=DY13*DZ14-DZ13*DY14
      YR34=DZ13*DX14-DX13*DZ14
      ZR34=DX13*DY14-DY13*DX14
C
C.....Face surface vectors (add both triangles)
      XXCR=0.5D0*(XR23+XR34)
      YYCR=0.5D0*(YR23+YR34)
      ZZCR=0.5D0*(ZR23+ZR34)
C
C.....Baricenters of each triangle
      DX12=(X(IJK1)+X(IJK2)+X(IJK3))/3.0D0
      DY12=(Y(IJK1)+Y(IJK2)+Y(IJK3))/3.0D0
      DZ12=(Z(IJK1)+Z(IJK2)+Z(IJK3))/3.0D0
C
      DX14=(X(IJK1)+X(IJK3)+X(IJK4))/3.0D0
      DY14=(Y(IJK1)+Y(IJK3)+Y(IJK4))/3.0D0
      DZ14=(Z(IJK1)+Z(IJK3)+Z(IJK4))/3.0D0
C
C.....Area of each triangle
      S23=SQRT(XR23**2+YR23**2+ZR23**2)
      S34=SQRT(XR34**2+YR34**2+ZR34**2)
C
C.....Baricenter of face (weighted average)
      XXXC=(DX12*S23+DX14*S34)/(S23+S34+SMALL)
      YYYC=(DY12*S23+DY14*S34)/(S23+S34+SMALL)
      ZZZC=(DZ12*S23+DZ14*S34)/(S23+S34+SMALL)
C
      RETURN
      END
C
C
C########################################################
      SUBROUTINE OFFSETBC
C########################################################
C     This routine modifies the index arrays of B.C.s
C     so that they include the inter-block offsets
C
C========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
C
      INTEGER M,II,IO,ISY,IW,
C
     *        IM
C
C========================================================
C
C.....INLET BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO II=IIST+1,IIST+NINL
        IJI(II) =IJI(II) +IJKST
        IJPI(II)=IJPI(II)+IJKST
        IJI1(II)=IJI1(II)+IJKST      
        IJI2(II)=IJI2(II)+IJKST      
        IJI3(II)=IJI3(II)+IJKST      
        IJI4(II)=IJI4(II)+IJKST      
      END DO
      END DO
C
C.....OUTLET BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IO=IOST+1,IOST+NOUT
        IJO(IO) =IJO(IO) +IJKST
        IJPO(IO)=IJPO(IO)+IJKST
        IJO1(IO)=IJO1(IO)+IJKST      
        IJO2(IO)=IJO2(IO)+IJKST      
        IJO3(IO)=IJO3(IO)+IJKST      
        IJO4(IO)=IJO4(IO)+IJKST      
      END DO
      END DO
C
C.....SYMETRY BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO ISY=ISST+1,ISST+NSYM
        IJS(ISY) =IJS(ISY) +IJKST
        IJPS(ISY)=IJPS(ISY)+IJKST
        IJS1(ISY)=IJS1(ISY)+IJKST      
        IJS2(ISY)=IJS2(ISY)+IJKST      
        IJS3(ISY)=IJS3(ISY)+IJKST      
        IJS4(ISY)=IJS4(ISY)+IJKST      
      END DO
      END DO
C
C.....WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWST+1,IWST+NWAL
        IJW(IW) =IJW(IW) +IJKST
        IJPW(IW)=IJPW(IW)+IJKST
        IJW1(IW)=IJW1(IW)+IJKST      
        IJW2(IW)=IJW2(IW)+IJKST      
        IJW3(IW)=IJW3(IW)+IJKST      
        IJW4(IW)=IJW4(IW)+IJKST      
      END DO
      END DO
C
C.....MTM BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IM=IMST+1,IMST+NMTM
        IJML(IM)=IJML(IM)+IJKST
        IJMR(IM)=IJMR(IM)+IJKST
      END DO
      END DO
C
      RETURN
C
      END
C
C
C---------------------------------------------------------------
C     Here come the routines with user-programmed input data and
C     user-programmed interpretation of results.
C     For each case, create separate user-files,
C     and copy them prior to compilation to the file 'user.f'
C     (routine BCIN provides boundary conditions)
C---------------------------------------------------------------
#include "user.f"
#include "vtkpost.f"
C---------------------------------------------------------------
C     Next lines incorporate the optional solvers. 
C     Only one file should contain the chosen solver in a 
C     subroutine of the form :
C
C     SUBROUTINE SOLVER(FI,IFI)
C
C     The other file should be left empty. It is convenient
C     to set up subdirectories with the desired combination
C     and use the -I<subdir> flag when compiling
C---------------------------------------------------------------
#if defined( USE_SIPSOL )
C#include "cgstab3d.f"
#include "sipsol3d.f"
#endif
C---------------------------------------------------------------
C
C     Here the PETSc specific soubroutines get included into the
C     CAFFA Code
# include "petsc.user.f"
C
C
C     Here the Functions for MMS get included
#if defined( USE_ANALYTICAL )
#include "mms.f"
#include "user.error.f"
#endif

C REMOVE WRITE
#include "outin.f"
