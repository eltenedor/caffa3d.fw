C#########################################################
      SUBROUTINE BCIN
C#########################################################
C     This routine sets inlet boundary conditions (which 
C     may also change in time in an unsteady problem; in
C     that case, set the logical variable INIBC to 'true'.)
C     It is called once at the begining of calculations on 
C     each grid level, unles INIBC=.TRUE. forces calls in
C     each time step.
C     This routine includes several options which are most
C     often required and serves the test cases provided with
C     the code; it may require adaptation to the problem
C     being solved, especially regarding velocity or
C     temperature distribution at boundaries.
C=========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER IW,IJB,L,IJP,II,IJK1,IJK2,IJK3,IJK4,IO,IJK,M
C
      REAL*8 FLOWO,FLOWEN,DX12,DY12,DZ12,DX13,DY13,DZ13
      REAL*8 DX14,DY14,DZ14,S23,S34,FLOMON,XR23,YR23,ZR23
      REAL*8 XR34,YR34,ZR34,XNV,YNV,ZNV
      REAL*8  LPI, FLOMASH,TCA,TCD,SUMAREA
      REAL*8 FLOMASG
      PARAMETER (LPI=3.141592653589793238462643383279D0)
c
      REAL*8 SUMAREAG
      Vec UVWPL
      PetscErrorCode IERR
#include "petsc.user.inc"
C
C=========================================================
C
C.....SET INDICES, INITIALIZE MOMENTUM AND MASS FLOW RATE
C
      INIBC=.TRUE.
      FLOMON=0.0D0
      FLOMAS=0.0D0
      FLOWO=0.0D0
      FLOWEN=0.0D0      
C
C     WALL BOUNDARY CONDITIONS
C=========================================================
C
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
#if defined( USE_SUBVEC )
      CALL VecGetSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecGetSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#else
      CALL VecGetArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef USE_ENERGYCOUPLING 
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
C
C.....WALL BOUNDARY CONDITIONS
C
      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        IJP=IJPW(IW)
        U(IJB)=0.D0 
        V(IJB)=0.0D0
        W(IJB)=0.0D0
C
#if defined(USE_2DCASE) && defined( USE_LIDDRIVENCAVITY )
        IF ( YC(IJB).GT.0.9D0 ) U(IJB)=1.0D0
#elif defined( USE_LIDDRIVENCAVITY )
        IF ( ZC(IJB).GT.0.9D0 ) U(IJB)=1.0D0
#endif
C
        P(IJB)=P(IJP)
      END DO
C
      IF (LCAL(IEN).OR.LCAL(ISC)) THEN
C
C.....ISOTHERMAL WALL BOUNDARIES
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO IW=IWST+1,IWST+NWALI
C       IJP=IJPW(IW)
        IJB=IJW(IW)
        IF (XNW(IW).GT.0) T(IJB)=TH
        IF (XNW(IW).LT.0) T(IJB)=TC
        IF (YNW(IW).GT.0) T(IJB)=TH/2.0D0
        IF (YNW(IW).LT.0) T(IJB)=TC/2.0D0
      END DO
C
      END DO
C
C.....ADIABATIC WALL BOUNDARIES
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO IW=IWAT+1,IWAT+NWALA
        T(IJW(IW))=TREF
      END DO
C
      END DO
C
      ENDIF
C
C     INLET  BOUNDARY CONDITIONS
C=========================================================
C
      SUMAREA=0.D0
      DO II=1,NINLBKAL
         IJB=IJI(II)
         IJP=IJPI(II)
C
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  SPECIFICATION OF INLET VELOCITY
C%%%  SIGN ACCORDING TO FACE IS AUTOMATICALLY SET! 
C%%%  U POSITIV ON WEST SIDE RESULTS IN NEGATIVE MASS FLUX
C%%%  ON THIS SIDE!
#ifdef USE_ANALYTICAL
         U(IJB)=UMMS(XC(IJB),YC(IJB),ZC(IJB))
         V(IJB)=VMMS(XC(IJB),YC(IJB),ZC(IJB))
         W(IJB)=WMMS(XC(IJB),YC(IJB),ZC(IJB))
         T(IJB)=TMMS(XC(IJB),YC(IJB),ZC(IJB))
#else
         U(IJB)=16.d0*0.45d0*YC(IJB)*ZC(IJB)*
     *           (0.41d0-YC(IJB))*(0.41d0-ZC(IJB))
     *          /(0.41d0)**4.d0
         V(IJB)=0.0D0
         W(IJB)=0.0D0
#endif
         P(IJB)=P(IJP)
C         
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  CALCULATION OF MASS FLUX
C             
         FMI(II)=DENS*(U(IJB)*XIR(II)+
     *        V(IJB)*YIR(II)+W(IJB)*ZIR(II))
C#ifdef USE_ANALYTICAL
C         SUMAREA=SUMAREA+FMI(II)
C          FLOMASH=MAX(0.d0,FMI(II))
C          FLOMAS=FLOMAS+FLOMASH
C#else
         FLOMASH=ABS(FMI(II))
         FLOMAS=FLOMAS-FMI(II)
C#endif
C        FLOMON=FLOMON+FLOMASH*DSQRT(U(IJB)**2+V(IJB)**2+W(IJB)**2)   
      ENDDO
      CALL MPI_ALLREDUCE(
     *    FLOMAS,FLOMASG,1,MPI_REAL8,MPI_SUM,PETSC_COMM_WORLD,IERR)
      FLOMAS=FLOMASG
C
C     OUTLET  BOUNDARY CONDITIONS
C=========================================================
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
#if defined( USE_ANALYTICAL )
        P(IJB)=PMMS(XC(IJB),YC(IJB),ZC(IJB))
#else
        P(IJB)=0.0D0
#endif
      END DO
C     IJK=NI*NI*KMON+NI*IMON+JMON
C     P(IJK)=PMMS(XC(IJK),YC(IJK),ZC(IJK))
C
C     SUMAREAG=0.0D0
C     CALL MPI_REDUCE(SUMAREA,SUMAREAG,1,MPI_REAL8,MPI_SUM,0,
C    *     PETSC_COMM_WORLD,IERR)
C     write(*,*)'SUMAREA',SUMAREA
C     write(*,*)'SUMAREA',SUMAREAG
C
#if defined(USE_SUBVEC)
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PVEC,IERR)
#if defined( USE_ENERGYCOUPLING )
      CALL VecRestoreSubVector(UVWPL,ISE,TVEC,IERR)
#endif
C
#else
      CALL VecRestoreArray(UVWPL,UVWPARR,UVWPPI,IERR)
#ifndef USE_ENERGYCOUPLING 
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
#endif
C
#endif
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
C.....SET RESIDUAL NORMALISATION FACTORS
C
#ifdef USE_SIPSOL
      DO L=1,NFI
        RNOR(L)=1.0D0
        RESOR(L)=0.0D0
      END DO
#endif
C
      IF(FLOMAS.LT.SMALL) FLOMAS=1.0D0
      IF(FLOMOM.LT.SMALL) FLOMOM=1.0D0
C
C
      FLOWEN=1.0D0
C
      RNOR(IU) =1.0D0/(FLOMOM+SMALL)
      RNOR(IV) =RNOR(IU)
      RNOR(IWW) =RNOR(IU)
      RNOR(IP) =1.0D0/(FLOMAS+SMALL)
      RNOR(IEN)=1.D0
      RNOR(ISC)=1.0D0
C
     
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C#########################################################
      SUBROUTINE INITCOND
C#########################################################
C     Special intial conditions.                      
C=========================================================
      implicit none
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C=========================================================
C
C
      IJRBBKO=IJRBBK
      IJRPBKO=IJRPBK
      XBCC=XOCC
      YBCC=YOCC
      ZBCC=ZOCC
      XBCR=XOCR
      YBCR=YOCR
      ZBCR=ZOCR
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE SOURCEUVW
C#########################################################
C     Special intial conditions.                      
C=========================================================
      implicit none
C
#include "finclude/petsc.h"
c
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C=========================================================
C
      RETURN
      END
C
C#########################################################
      SUBROUTINE SOURCEM
C#########################################################
C     Special intial conditions.                      
C=========================================================
      implicit none
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C=========================================================
C
      RETURN
      END
C
C#########################################################
      SUBROUTINE SOURCESC(IFI,FI)
C#########################################################
C     Special intial conditions.                      
C=========================================================
      implicit none
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER IFI
C
       REAL*8 FI(    1)
C
C=========================================================
C
      RETURN
      END
C
C
