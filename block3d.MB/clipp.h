bool checkorientation(double * XYZL, double * XYZR);
void baricenter(double * vert, int x1, int x2, double * cent);
void baricenter(double * vert, double * cent);
void getPolyOrientation(const Polygon & poly, int & istart, int & orientation);
void getCenter(const Polygon & poly, double & xs, double & ys, double & a);

extern "C"
{
  //FORTRAN BLOCKPREPROCESSOR INTERFACE
//  void matchfaces_(double XYZL[], double XYZR[],
//                   double XYZCOM[], double &AR, double XYZFC[]);
void matchfaces_(double XYZL[], double XYZR[], double &AR, double XYZFC[]);

  //LAPACK INTERFACE
  void dgesv_(int *n, int *nrhs,  double *a,  int  *lda,
              int *ipivot, double *b, int *ldb, int *info) ;
  void dgetrf_(int *m, int *n,  double *a,  int  *lda,
               int *ipivot, int *info) ;
  void test_(double XYZL[], double XYZR[],
                   double &AR, double XYZFC[]);
}

