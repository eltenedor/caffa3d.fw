!########################################################
program grgen
!########################################################

    use indexModule
    use charModule
    implicit none

    print *, ' TOTAL NUMBER OF BLOCKS: '
    read(*,*) NB
    print *, ' "NAME" OF INPUT FILE (NAME_BLOCK.inp, * - KEYBOARD)'
    read *, FILIN
    print *, ' REFINEMENT FACTOR 2**X (X=0 <=> NO REFINEMENT): '
    read *, REFF
    print *, 'RANDOMLY SELECT RESOLUTION? 0-N, 1-Y'
    read (*,*) RANRES
    if (RANRES.NE.0) then
      print *, 'ENTER SEED:'
      read (*,*) SEED
    endif

    ! Create one grid.out and grid.vtk for each block
    do B=1,NB
        call readData
        call cartesian
        call gridExport
        call vtkExport
    end do

end program grgen

!########################################################
subroutine readData
!########################################################

    use boundaryModule
    use charModule
    use geoModule
    use indexModule
    implicit none

    integer :: ITYP
    
    BLOCKUNIT=OFFSET+B

    write(BLOCK_CH,*) B-1
    IF(FILIN.NE.'*') THEN
        BLOCKFILE=trim(FILIN)//'_'//trim(adjustl(BLOCK_CH))//'.inp'
        OPEN (UNIT=BLOCKUNIT,FILE=BLOCKFILE)
        REWIND BLOCKUNIT
        ITYP=0
    ELSE
        BLOCKFILE='grid_'//trim(adjustl(BLOCK_CH))//'.inp'
        OPEN (UNIT=BLOCKUNIT,FILE=BLOCKFILE)
        REWIND BLOCKUNIT
        ITYP=1
    ENDIF

    PRINT *, ' OUTPUT FILE NAME:  '
    IF(ITYP.EQ.1) THEN
        READ(*,1) FILOUT
        WRITE(BLOCKUNIT,1) FILOUT
    ELSE
        READ(BLOCKUNIT,1) FILOUT
        print *, FILOUT
        FILOUT = trim(adjustl(FILOUT))
        print *, FILOUT
    ENDIF
    1 FORMAT(A20)

    PRINT *, ' ENTER> XSTART, XEND, NUMBER OF CVS:  '
    IF(ITYP.EQ.1) THEN
        READ(*,*) XXS,XXE,NICV
        NICV=NICV*2**REFF
        WRITE(BLOCKUNIT,*) XXS,XXE,NICV,'   XS,XE,NICV '
    ELSE
        READ(BLOCKUNIT,*) XXS,XXE,NICV
        NICV=NICV*2**REFF
        print *, XXS,XXE,NICV
    END IF

    PRINT *, ' ENTER> YSTART, YEND, NUMBER OF CVS:  '
    IF(ITYP.EQ.1) THEN
        READ(*,*) YYS,YYE,NJCV
        NJCV=NJCV*2**REFF
        WRITE(BLOCKUNIT,*) YYS,YYE,NJCV,'   YS,YE,NJCV '
    ELSE
        READ(BLOCKUNIT,*) YYS,YYE,NJCV
        NJCV=NJCV*2**REFF
        print *, YYS,YYE,NJCV
    END IF

    PRINT *, ' ENTER> ZSTART, ZEND, NUMBER OF CVS:  '
    IF(ITYP.EQ.1) THEN
        READ(*,*) ZZS,ZZE,NKCV
        NKCV=NKCV*2**REFF
        WRITE(BLOCKUNIT,*) ZZS,ZZE,NKCV,'   ZS,ZE,NKCV '
    ELSE
        READ(BLOCKUNIT,*) ZZS,ZZE,NKCV
        NKCV=NKCV*2**REFF
        print *, ZZS,ZZE,NKCV
    END IF

    PRINT *, ' ENTER> BOUNDARY TYPE W S B T N E:'
    print *, '(1 - DIRICHLET, 2 - NEUMANN ZERO GRADIENT, 11 BLOCK)'
    IF(ITYP.EQ.1) THEN
        READ(*,*) BTYP(1:6)
        WRITE(BLOCKUNIT,*) BTYP(1:6),  '   WBTYP, SBTYP, BBTYP, TBTYP,  NBTYP ,EBTYP '
    ELSE
        READ(BLOCKUNIT,*) BTYP(1:6)
        print *, BTYP(1:6)
    END IF
    
    close(unit=BLOCKUNIT)
    OPEN (UNIT=BLOCKUNIT,FILE=FILOUT,POSITION='REWIND')

       
end subroutine readData

!========================================================
!>   cartesian orthogonal 3d grid
!########################################################
subroutine cartesian
!########################################################

    use geoModule
    use indexModule
    implicit none
    
    ! Initialize all values with zero
    X=0.0d0
    Y=0.0d0
    Z=0.0d0

    DX=(XXE-XXS)/dble(NICV)
    DY=(YYE-YYS)/dble(NJCV)
    DZ=(ZZE-ZZS)/dble(NKCV)

    NI=NICV+2
    NJ=NJCV+2
    NK=NKCV+2
    NIJ=NI*NJ
    NIJK=NI*NJ*NK
    NIM=NI-1
    NJM=NJ-1
    NKM=NK-1

    do K=1,NKM
    do I=1,NIM
    do J=1,NJM
        IJK=(K-1)*NI*NJ+(I-1)*NJ+J
        X(IJK)=XXS+dble(I-1)*DX
        Y(IJK)=YYS+dble(J-1)*DY
        Z(IJK)=ZZS+dble(K-1)*DZ
    end do
    end do
    end do

    if (RANRES.NE.0) then
      call srand(SEED)
      do K=2,NKM-1
      do I=2,NIM-1
      do J=2,NJM-1
          IJK=(K-1)*NI*NJ+(I-1)*NJ+J
          X(IJK)=X(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DX
          Y(IJK)=Y(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DY
          Z(IJK)=Z(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DZ
      end do
      end do
      end do
!east west boundary
      do K=2,NKM-1
      do I=1,NIM,NIM-1
      do J=2,NJM-1
          IJK=(K-1)*NI*NJ+(I-1)*NJ+J
          Y(IJK)=Y(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DY
          Z(IJK)=Z(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DZ
      end do
      end do
      end do
!north south boundary
      do K=2,NKM-1
      do I=2,NIM-1
      do J=1,NJM,NJM-1
          IJK=(K-1)*NI*NJ+(I-1)*NJ+J
          X(IJK)=X(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DY
          Z(IJK)=Z(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DZ
      end do
      end do
      end do
!top bottom boundary
      do K=1,NKM,NKM-1
      do I=2,NIM-1
      do J=2,NJM-1
          IJK=(K-1)*NI*NJ+(I-1)*NJ+J
          X(IJK)=X(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DY
          Y(IJK)=Y(IJK)+(rand()-0.5D0)*2.0D0/3.0D0*DZ
      end do
      end do
      end do
    endif

end subroutine cartesian

!########################################################
subroutine gridExport
!########################################################

    use boundaryModule
    use charModule
    use geoModule
    use indexModule
    implicit none

    write(BLOCKUNIT,'(4I10)') NIM,NJM,NKM,NIM*NJM*NKM

    do K=1,NKM
    do I=1,NIM
    do J=1,NJM
      IJK=(K-1)*NI*NJ+(I-1)*NJ+J
      write(BLOCKUNIT,'(3F10.6)') X(IJK),Y(IJK),Z(IJK)
    end do
    end do
    end do

    write(BLOCKUNIT,'(6I5)') 1,BTYP(1),1,NJM,1,NKM
    write(BLOCKUNIT,'(6I5)') 2,BTYP(2),1,NIM,1,NKM
    write(BLOCKUNIT,'(6I5)') 3,BTYP(3),1,NIM,1,NJM
    write(BLOCKUNIT,'(6I5)') 4,BTYP(4),1,NIM,1,NJM
    write(BLOCKUNIT,'(6I5)') 5,BTYP(5),1,NIM,1,NKM
    write(BLOCKUNIT,'(6I5)') 6,BTYP(6),1,NJM,1,NKM

    close(unit=BLOCKUNIT)

end subroutine gridExport

!########################################################
subroutine vtkExport
!########################################################
!
!.....Create .vtk file
!
    use charModule
    use geoModule
    use indexModule
    implicit none

    write(BLOCK_CH,*) B
    VTKFILE='grid_'//trim(adjustl(BLOCK_CH))//'.vtk'
    open (unit=BLOCKUNIT,FILE=VTKFILE)
    rewind BLOCKUNIT

    print *, ' *** GENERATING .VTK *** '
    write(BLOCKUNIT,'(A)') '# vtk DataFile Version 3.0'
    write(BLOCKUNIT,'(A)') 'grid'
    write(BLOCKUNIT,'(A)') 'ASCII'
    write(BLOCKUNIT,'(A)') 'DATASET STRUCTURED_GRID'
    write(BLOCKUNIT,'(A,I6,I6,I6)') 'DIMENSIONS', NIM,NJM,NKM
    write(BLOCKUNIT,'(A6,I10,A6)') 'Points ', NIM*NJM*NKM, ' float'
    do K=1,NKM
    do J=1,NJM
    do I=1,NIM
        IJK=(K-1)*NI*NJ+(I-1)*NJ+J
        write(BLOCKUNIT,'(E20.10,1X,E20.10,1X,E20.10)'), X(IJK), Y(IJK),Z(IJK)
    end do
    end do
    end do

    close(unit=BLOCKUNIT)

end subroutine vtkExport
