program grid

    implicit none
    
    integer :: NB,NIB,NJB,NKB
    integer :: I,J,K,IJK,RES,RANRES,SEED,RESX,RESY,RESZ
    integer, dimension(:,:),allocatable :: A
    character(len=20) :: FILOUT,BLOCKNUMBER,FILEIN
    
    print *, 'ENTER RESOLUTION IN I-DIRECTION'
    read (*,*) RESX
    print *, 'ENTER RESOLUTION IN J-DIRECTION'
    read (*,*) RESY
    print *, 'ENTER RESOLUTION IN K-DIRECTION'
    read (*,*) RESZ
    print *, 'ENTER NBLOCKS IN I-DIRECTION'
    read (*,*) NIB
    print *, 'ENTER NBLOCKS IN J-DIRECTION'
    read (*,*) NJB
    print *, 'ENTER NBLOCKS IN K-DIRECTION'
    read (*,*) NKB
    print *, 'RANDOMLY SELECT RESOLUTION? 0-N, 1-Y'
    read (*,*) RANRES
    if (RANRES.NE.0) then
      print *, 'ENTER SEED:'
      read (*,*) SEED
      call srand(SEED)
    end if

    NB=NIB*NJB*NKB
    
    allocate(A(NB,6))
    
    A=11

    !East and west boundary blocks
    do k=1,NKB
      i=1
      do
        do j=1,NJB
            ijk=j+NJB*(i-1)+NJB*NIB*(k-1)
            if (i.eq.1) A(ijk,1) = 1
            if (i.eq.NIB) A(ijk,6) = 1
        enddo
      if (i.eq.NIB) EXIT
      i=NIB
      enddo
    enddo


    !North and south boundary blocks
    do k=1,NKB
      do i=1,NIB
        j=1
        do
            ijk=j+NJB*(i-1)+NJB*NIB*(k-1)
            if (j.eq.1) A(ijk,2) = 1
            if (j.eq.NJB) A(ijk,5) = 1
            if (j.eq.NJB) EXIT
            j=NJB
        enddo
      enddo
    enddo

    !Top and bottom boundary blocks
    k=1
    do 
      do i=1,NIB
        do j=1,NJB
            ijk=j+NJB*(i-1)+NJB*NIB*(k-1)
            if (k.eq.1) A(ijk,3)= 1
            if (k.eq.NKB) A(ijk,4)= 1
        enddo
      enddo
      if (K.eq.NKB) EXIT
      K=NKB
    enddo
    
    if (ranres.eq.0) then

    do K=1,NKB
    do I=1,NIB
    do J=1,NJB
        IJK=J+NJB*(I-1)+NIB*NJB*(K-1)
        write(BLOCKNUMBER,*) IJK-1
        FILOUT='grid_'//trim(adjustl(BLOCKNUMBER))//'.inp'
        open(UNIT=20,FILE=FILOUT)
        rewind 20
        FILEIN='block'//trim(adjustl(BLOCKNUMBER))//'.txt'
        write(20,*) FILEIN
        write(20,*) (-1.0d0+(I-1)*2.0d0/NIB), (-1.0d0+I*2.0d0/NIB), RESX/NIB
        write(20,*) (-1.0d0+(J-1)*2.0d0/NJB), (-1.0d0+J*2.0d0/NJB), RESY/NJB
        write(20,*) (-1.0d0+(K-1)*2.0d0/NKB), (-1.0d0+K*2.0d0/NKB), RESZ/NKB
        write(20,*) A(IJK,:)
        close (UNIT=20)
    enddo
    enddo
    enddo

    else
    do K=1,NKB
    do I=1,NIB
    do J=1,NJB
        IJK=J+NJB*(I-1)+NIB*NJB*(K-1)
        write(BLOCKNUMBER,*) IJK-1
        FILOUT='grid_'//trim(adjustl(BLOCKNUMBER))//'.inp'
        open(UNIT=20,FILE=FILOUT)
        rewind 20
        FILEIN='block'//trim(adjustl(BLOCKNUMBER))//'.txt'
        write(20,*) FILEIN
        write(20,*) (-1.0d0+(I-1)*2.0d0/NIB), (-1.0d0+I*2.0d0/NIB), INT(rand()*(RESX/NIB-1))+(RESX/NIB)/4
        write(20,*) (-1.0d0+(J-1)*2.0d0/NJB), (-1.0d0+J*2.0d0/NJB), INT(rand()*(RESY/NJB-1))+(RESY/NJB)/4
        write(20,*) (-1.0d0+(K-1)*2.0d0/NKB), (-1.0d0+K*2.0d0/NKB), INT(rand()*(RESZ/NKB-1))+(RESZ/NKB)/4
        write(20,*) A(IJK,:)
        close (UNIT=20)
    enddo
    enddo
    enddo

    end if

end program grid
