C##########################################################
      PROGRAM CAFFA
C##########################################################
C     This version of CAFFA has been extended to 3D.
C     Please see below
CC 
C     This code incorporates the Finite Volume Method using
C     SIMPLE algorithm on colocated body-fitted grids. For
C     a description of the solution method, see the book by
C     Ferziger and Peric (1996) or paper by Demirdzic,
C     Muzaferija and Peric (1996). Description of code 
C     features is provided in the acompanying README-file.
C     This version is based on the laminar code including 
C     the multiple pressure corrections and the effects of 
C     non-smoothness of grid lines when computing gradients
C     (see code caffac.f in directory 2dgl) and includes the 
C     k-eps turbulence model, which was implemented by 
C     Martin Schmid, PhD student at the Institute of
C     Shipbuilding in Hamburg.
C
C     This is Version 1.3 of the code, August 1997.
C
C     The user may modify the code and give it to third
C     parties, provided that an acknowledgement to the
C     source of the original version is retained.
C
C                M. Peric, Hamburg, 1996
C                peric@schiffbau.uni-hamburg.de
C                M. Schmid, Hamburg, 1997
C                schmid@schiffbau.uni-hamburg.de
C     
C     This version has been extended to 3D problems in
C     block-structured grids. The K-e model has been
C     retained and a simple Smagorinsky LES model
C     has been introduced as well.
C
C     However, the functionality of multiple grid
C     levels has been left out. A future
C     extension to multigrid is planed.
C
C     The 'K' index now refers to the third ('Z') direction,
C     and not to the grid level as before.
C
C     Also a new index 'M' has been introduced for the 
C     grid-block numbering. General interfaces, between
C     and within, grid blocks are treated now, in addition
C     to the original OC cuts. The domain is now block
C     structured then.
C
C     Otherwise, the original notation was kept wherever
C     possible, and the book by Ferziger & Peric has been
C     followed as closely as possible for the 3D extension,
C     as well as the multi-block treatment.
C
C     Also an option for the linear solver has been introduced.
C     The code can be compiled to run Stone's SIP Solver
C     or CGSTAB solver. Please see the read.me file
C
C     Optional compiler directives for OpenMP parallelization
C     at grid block level have been introduced. Please
C     see the readme file.
C  
C     The code has been extended to handle arbitrary block
C     boundaries.
C
C     The code has been parallelized using PETSc.
C
C     25.10.2014
C     Fabian Gabel gabel@mathematik.tu-darmstadt.de
C
C
C===========================================================
C NB: CAFFA stands for "Computer Aided Fluid Flow Analysis". 
C===========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "charac3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER NCASE,IJK,I,ICONT,LS,M
      INTEGER NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT,K,J
      INTEGER RCOUNTG,RCOUNT
C
      REAL*8 SOURCE,ERRORV,ERRORP,UA,VA,UVA,UVW,HPA,HPA2,
     *              ERRORVG,ERRORPG,
     *              ERRORU,ERRORUG,
     *              ERRORW,ERRORWG,
     *              ERRORA,ERRORAG,
     *              ERRORT,ERRORTG,TA
      REAL*8  LPI,TCA,TCD,WA
      REAL*8 PPOG,VMG,VM
      PARAMETER (LPI=3.141592653589793238462643383279D0)
      INTEGER PID,OMP_GET_THREAD_NUM,NTHREADS,OMP_GET_NUM_THREADS
      PetscErrorCode IERR
      PetscLogDouble CALTIME,CALTIMS
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C=========================================================
C
C.....SET SOME CONSTANTS
C
      CALL PetscInitialize(PETSC_NULL_CHARACTER,IERR)
      CALL MPI_COMM_RANK(PETSC_COMM_WORLD,RANK,IERR)
      CALL SETDAT
C      CALL MODDAT
C
      F1(1:NXYZA)=0.0D0; F2(1:NXYZA)=0.0D0; F3(1:NXYZA)=0.0D0;
C
C.....READ PROBLEM NAME AND OPEN FILES
C
      IF (RANK.EQ.0) THEN
        WRITE(*,*), ' ENTER PROBLEM NAME (SIX CHARACTERS):  '

C        READ(*,'(A6)') NAME
        WRITE(*,*) '***************************************************'
      END IF
      NAME='control'
C
      IF (RANK.EQ.0) THEN
        WRITE(*,*)'NAME OF PROBLEM SOLVED ',NAME
        WRITE(*,*) ''
        WRITE(*,*) '***************************************************'
      END IF
C     
      NCASE=INDEX(NAME,' ')
      IF(NCASE.NE.ZERO) STOP
      WRITE( FILIN,'(A7,4H.cin)') NAME
      WRITE(FILOUT,'(A7,4H.out)') NAME
C     WRITE(FILBCK,'(A7,4H.bck)') NAME
      WRITE(FILERR,'(A7,4H.err)') NAME
      WRITE(FILBCK,'(A7,I4.4,4H.bck)') NAME, RANK
      WRITE(FILPRC,'(A7,I4.4,4H.prc)') NAME, RANK
C
      OPEN (UNIT=5,FILE=FILIN,POSITION='REWIND')
      OPEN (UNIT=2,FILE=FILOUT,POSITION='REWIND')
      OPEN (UNIT=4,FILE=FILBCK,FORM='UNFORMATTED',POSITION='REWIND')
      IF (RANK.EQ.0) OPEN (UNIT=23,FILE=FILERR)
      OPEN (UNIT=9,FILE=FILPRC,POSITION='REWIND')
C
C.....INPUT DATA AND INITIALIZATION
C
      CALL INIT
C
C.....INITIAL OUTPUT
C
      CALL OUTIN
      ITIM=0
      TIME=0.0D0
C
C======================================================
C.....READ RESULTS OF PREVIOUS RUN IF REQUIRED
C======================================================
C
      IF(LREAD) THEN
#ifdef USE_INFO
        IF (RANK.EQ.0) WRITE(*,*) "READING DATA FROM PREVIOUS RUN"
#endif
C
C.....EXTRACT ARRAYS FROM VECTORS
C
        CALL VecGetArray(UVEC,UARR,UUI,IERR)
        CALL VecGetArray(VVEC,VARR,VVI,IERR)
        CALL VecGetArray(WVEC,WARR,WWI,IERR)
        CALL VecGetArray(PVEC,PARR,PPI,IERR)
        CALL VecGetArray(TVEC,TARR,TTI,IERR)

        WRITE(FILRES,'(A7,I4.4,4H.res)') NAME, RANK
        OPEN (UNIT=3,FILE=FILRES,FORM='UNFORMATTED',POSITION='REWIND')
C 
        READ(3)ITIM,TIME,(F1(IJK),IJK=1,NIJKBKAL),
     *        (F2(IJK),IJK=1,NIJKBKAL),(F3(IJK),IJK=1,NIJKBKAL),
     *        (U(IJK), IJK=1,NIJKBKAL),(V(IJK), IJK=1,NIJKBKAL),
     *        (W(IJK), IJK=1,NIJKBKAL),(P(IJK), IJK=1,NIJKBKAL),
     *        (T(IJK), IJK=1,NIJKBKAL),(TE(IJK),IJK=1,NIJKBKAL),
     *        (ED(IJK),IJK=1,NIJKBKAL),(FMOC(I),I=1,NOCBKAL),
C 
     *        (FMF(I),I=1,NFSGBKAL),(RESINI(I),I=1,4),
     *        (RESOR(I),I=1,4)
C 
C
        IF(LTIME) READ(3) (UO(IJK),IJK=1,NIJKBKAL),
     *        (VO(IJK),IJK=1,NIJKBKAL),(WO(IJK),IJK=1,NIJKBKAL),
     *        (TO(IJK),IJK=1,NIJKBKAL),(TEO(IJK),IJK=1,NIJKBKAL),
     *        (EDO(IJK),IJK=1,NIJKBKAL)
        CLOSE(UNIT=3)
C
        ITIM=ITIM-1
C
C.....RESTORE ARRAYS FROM VECTORS
C
         CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
         CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
         CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
         CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
         CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
      ELSE
C
C======================================================
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
C Initialize FIELD VALUES
C
      F1(1:NIJKBKAL)=0.0D0
      F2(1:NIJKBKAL)=0.0D0
      F3(1:NIJKBKAL)=0.0D0
      FMOC(1:NOCBKAL)=0.0D0
      FMF(1:NFSGBKAL)=0.0D0
C
C.....OpenMP : Here starts parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*            NKMT,NIMT,NJMT,KSTT,ISTT)
       DO M=1,NBLKS
         NKMT=NKBK(M)-1
         NIMT=NIBK(M)-1
         NJMT=NJBK(M)-1
         KSTT=KBK(M)
         ISTT=IBK(M)
         NJT=NJMT+1
         NIJT=(NIMT+1)*NJT
C     
         DO K=1,NKMT+1 
         DO I=1,NIMT+1
         DO J=1,NJMT+1
           IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
C#ifdef USE_ANALYTICAL
C        U(IJK)=UMMS(XC(IJK),YC(IJK),ZC(IJK))
C        V(IJK)=VMMS(XC(IJK),YC(IJK),ZC(IJK))
C        W(IJK)=WMMS(XC(IJK),YC(IJK),ZC(IJK))
C        T(IJK)=TMMS(XC(IJK),YC(IJK),ZC(IJK))
C        P(IJK)=PMMS(XC(IJK),YC(IJK),ZC(IJK))
C#else
           U(IJK)=0.0D0
           V(IJK)=0.0D0
           W(IJK)=0.0D0  
           P(IJK)=0.0D0
C#endif
         ENDDO
         ENDDO
         ENDDO
      ENDDO
      END IF
C.....OpenMP : Here ends this parallel loop section
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
C.....UPDATE BOUNDARY VALUES
C
      CALL VecGhostUpdateBegin(UVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(UVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(VVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(WVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(WVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
      CALL VecGhostUpdateBegin(PVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(PVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
      CALL VecGhostUpdateBegin(
     *                        DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *                        VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
C     IF (LCAL(IEN)) THEN
C     CALL VecGhostUpdateBegin(TVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostUpdateEnd(TVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     END IF
C
C======================================================
C.....START TIME LOOP
C======================================================
C
      INIBC=.TRUE.
      INISOL=.TRUE.
      ICONT=0
      ITIMS=ITIM+1
      ITIME=ITIM+ITSTEP
C
      CALL PetscBarrier(UVEC,IERR)
      CALL PetscTime(CALTIMS,IERR)
      DO 400 ITIM=ITIMS,ITIME
      TIME=TIME+DT
C
C.....SHIFT SOLUTIONS IN TIME (OOLD = OLD, OLD = CURRENT)
C
      IF(LTIME) THEN
        UOO=UO; VOO=VO; WOO=WO
C 
C.....EXTRACT ARRAYS FROM VECTORS
C
       CALL VecGetArray(UVEC,UARR,UUI,IERR)
       CALL VecGetArray(VVEC,VARR,VVI,IERR)
       CALL VecGetArray(WVEC,WARR,WWI,IERR)
       CALL VecGetArray(TVEC,TARR,TTI,IERR)
        DO IJK=1,NIJKBKAL
          UO(IJK)=U(IJK)
          VO(IJK)=V(IJK)
          WO(IJK)=W(IJK)
          TO(IJK)=T(IJK)
        END DO
       CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
       CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
       CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
       CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
        TOO=TO
        TEOO=TEO; TEO=TE;
        EDOO=EDO; EDO=ED;
C
        WRITE(2,*) '  '
        WRITE(2,*) '  TIME = ',TIME
        WRITE(2,*) '  *****************************'
      ENDIF
C
C.....SET INLET BOUNDARY CONDITIONS
C
      IF(INIBC) CALL BCIN
C
C.....PRINT INITAL FIELDS
C
C      IF(LOUTS.AND.(ITIM.EQ.ITIMS)) CALL OUTRES
C
C.....PRINT INDEX TO MONITORING LOCATION 
C
C     WRITE(2,600) MMON,IMON,JMON,KMON
C
C======================================================
C.....START SIMPLE RELAXATIONS (OUTER ITERATIONS)
C======================================================
C
      IF (RANK.EQ.0) THEN
        WRITE(*,*) '***************************************************'
        WRITE(*,*) "START SIMPLE RELAXATIONS"
        WRITE(*,*) '***************************************************'
      END IF
C
      CALL CALCP(0)
      DO LS=1,LSG
#if defined( USE_ANALYTICAL ) && defined( USE_CONTOUTPUT )
       CALL CALCERR
#endif
        IF(LCAL(IU))    CALL CALCUVW
        IF(LCAL(IP))    CALL CALCP(1)
        IF(LCAL(IEN))   CALL CALCSC(IEN,TVEC,TO,TOO)
C
C.....Call to LES Smagorinsky routine.
C
C        IF(LCAL(ISMG))  CALL SMAGOR
C
C.....Call to K-e routines
C
C        IF(LCAL(ITE))   CALL CALCSC(ITE,TE,TEO,TEOO)
C        IF(LCAL(IED))   CALL CALCSC(IED,ED,EDO,EDOO)
C        IF(LCAL(IVIS))  CALL MODVIS
C
C.....NORMALIZE RESIDUALS, PRINT RES. LEVELS AND MONITORING VALUES
C
#ifdef USE_SIPSOL
         RESOR=RESOR*RNOR
#endif
C
C         WRITE(2,606) LS,(RESOR(I),I=1,5),(RESOR(I),I=7,8),
C    *           U(IJKMON),V(IJKMON),W(IJKMON),P(IJKMON),
C    *           T(IJKMON),RXVISC
 
C         IF (.NOT.LTIME) WRITE(*,606) LS,(RESOR(I),I=1,5),
C    *            (RESOR(I),I=7,8),
C    *            U(IJKMON),V(IJKMON),W(IJKMON),P(IJKMON),
C    *            T(IJKMON),RXVISC
C 
C       SOURCE=MAX(RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP),
C    *             RESOR(IEN),RESOR(ITE),RESOR(IED))
        SOURCE=MAX(RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP),RESOR(IEN))
        IF (RANK.EQ.0) THEN
          WRITE(*,"(I8.7,5E12.4)"),
     *          LS,RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP),RESOR(IEN)
          WRITE(23,"(I8.7,5E12.4)"),
     *          LS,RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP),RESOR(IEN)
        END IF
C
        IF (.NOT.LTIME.AND.LWRITE
     *                .AND.MOD(LS,NOTT).EQ.0) 
     *    CALL SRES(RANK)
        IF(SOURCE.GT.SLARGE) GO TO 510
        IF(SOURCE.LT.SORMAX) GO TO 250
      END DO
C
  250 CONTINUE
C
C==========================================================
C.....SAVE SOLUTIONS FOR RE-START OR POSTPROCESSING; OUTPUT
C==========================================================
C
C.....UNSTEADY FLOW - INTERMEDIATE SOLUTIONS:
C
C NEW - OUTPUT STILL NOT IMPLEMENTED COMPLETELY
C END NEW
        IF(LTIME) THEN
C         WRITE(*,606) LS,(RESOR(I),I=1,5),(RESOR(I),I=7,8),
C    *            U(IJKMON),V(IJKMON),W(IJKMON),P(IJKMON),
C    *            T(IJKMON),RXVISC
C
C NEW
C         IF(MOD(ITIM,NOTT).EQ.0.AND.LWRITE) THEN
          IF(MOD(ITIM,NOTT).EQ.0.AND.LPOST) THEN
C END NEW
            ICONT=ICONT+1
            CALL POST(ICONT,RANK)
          ENDIF
        ENDIF
C
  400 CONTINUE
      IF (RANK.EQ.0) THEN
        CALL PetscTime(CALTIME,IERR)
        WRITE(*, "(A,E12.4)"), "TIME FOR CALCULATION:",CALTIME-CALTIMS
        WRITE(23,"(A,E12.4)") ,"TIME FOR CALCULATION:",CALTIME-CALTIMS
      END IF
C
C.....STEADY FLOW, OR UNSTEADY FLOW - LAST TIME STEP: PRINT AND
C ....SAVE RESULTS 
C
       ICONT=ICONT+1
       IF(LOUTE) CALL OUTRES
       IF (LWRITE) CALL SRES(RANK)
       IF (LPOST) CALL POST(ICONT,RANK)
       ITIM=0
       TIME=0.0D0
C
#ifdef USE_ANALYTICAL
       CALL CALCERR
#endif
C
C==========================================================
C.....CLOSE FILES, FORMATS
C==========================================================
C
      CLOSE(UNIT=8)
      CLOSE(UNIT=3)
      CLOSE(UNIT=4)
      CLOSE(UNIT=2)
      CLOSE(UNIT=5)
      IF (RANK.EQ.0) CLOSE(UNIT=23)
      IF(RANK.EQ.0)
     *  WRITE(*,*),'     *** CALCULATION FINISHED - SEE RESULTS ***'
C
      CALL DESTROYPETSC
      CALL PetscFinalize(IERR)
      STOP
C
C.....MESSAGE FOR DIVERGENCE 
C
  510 IF (RANK.EQ.0) 
     * WRITE(*,*),'     *** TERMINATED - OUTER ITERATIONS DIVERGING ***'
      CALL DESTROYPETSC
      CALL PetscFinalize(IERR)
#include "petsc.user.inc"
C
C.....FORMAT SPECIFICATIONS
C
  600 FORMAT('IT',1X,
     * 'I-------------ABSOLUTE RESIDUAL SOURCE SUMS------------I',
     * 2X,
     * 'I--VALUES AT MONITOR POINT(',1X,I2,',',I3,',',I3,',',I3,
     * ')--I',/,
     * 'Nº',1X,2X,'UMOM',4X,'VMOM',4X,'WMOM',4X,'MASS',4X,
     *            'ENER',4X,'TKEN',4X,'TDIS',2X,
     * 2X,3X,'U' ,7X,'V',7X,'W',7X,'P',7X,'T',5X,'RXVIS')
  606 FORMAT(I6.6,1X,1P,7E8.1,2X,1P,6E8.1)
C
      END
C
C
C#########################################################
      SUBROUTINE CALCUVW
C#########################################################
C     This routine discretizes and solves the linearized
C     equations for X, Y and Z momentum componentS (U, V and W
C     Cartesian velocity components).
C
C=========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER M,NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT
      INTEGER K,J,I,IJK,II,IJB,IJP,IO,IW,ISY,IJN,MIJ
      INTEGER LS
C
      REAL*8 GU,SB,APT,CP,CB,VISS,COEF,ARE,UPB,VPB,WPB
      REAL*8 VNP,XTP,YTP,ZTP,VISOL,YNP,ZNP,XNP,FDE,DPB
      REAL*8  LPI
      PARAMETER (LPI=3.141592653589793238462643383279D0) 
C
      Vec SUL,SVL,SWL,APL,
     *    UL,VL,WL,PL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL
      PetscScalar PZERO,PONE
      PetscInt psize
      PetscErrorCode IERR
C
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C=========================================================
C
      PZERO=0.0D0
      PONE=1.0D0
C
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
      IF(LCAL(IEN)) CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
C.....CALCULATE GRADIENT VECTOR COMPONENTS AT CV-CENTER FOR U, V W, & P
C
C.....Call to Inner Walls FIX modifications routine
C
C
      CALL GRADFI(UVEC,DUXVEC,DUYVEC,DUZVEC)
      CALL GRADFI(VVEC,DVXVEC,DVYVEC,DVZVEC)
      CALL GRADFI(WVEC,DWXVEC,DWYVEC,DWZVEC)
      CALL GRADFI(PVEC,DPXVEC,DPYVEC,DPZVEC)
C
C.....GET ARRAY OF VECTOR VALUES: VELOCITY, GRADIENT COMPONENTS, DENS, VISC
C
      CALL VecGhostGetLocalForm(UVEC,UL,IERR)
      CALL VecGhostGetLocalForm(VVEC,VL,IERR)
      CALL VecGhostGetLocalForm(WVEC,WL,IERR)
C
      CALL VecGhostGetLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostGetLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostGetLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostGetLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostGetLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostGetLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostGetLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostGetLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostGetLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(UL,UARR,UUI,IERR)
      CALL VecGetArray(VL,VARR,VVI,IERR)
      CALL VecGetArray(WL,WARR,WWI,IERR)
C
      CALL VecGetArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecGetArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecGetArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecGetArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecGetArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecGetArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecGetArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecGetArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecGetArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)
C
C.....INITIALIZE ARRAYS, SET BLENDING FACTOR
C
C.....ZERO THE LOCAL REPRESENTATION (INCLUDING THE GHOST ELEMENTS)
C
      CALL VecGhostGetLocalForm(SUVEC,SUL,IERR)
      CALL VecGhostGetLocalForm(SVVEC,SVL,IERR)
      CALL VecGhostGetLocalForm(SWVEC,SWL,IERR)
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
C
      CALL VecSet(SUL,PZERO,IERR)
      CALL VecSet(SVL,PZERO,IERR)
      CALL VecSet(SWL,PZERO,IERR)
      CALL VecSet(APL,PZERO,IERR)
C
      CALL VecGetArray(SUL,SUARR,SUUI,IERR)
      CALL VecGetArray(SVL,SVARR,SVVI,IERR)
      CALL VecGetArray(SWL,SWARR,SWWI,IERR)
      CALL VecGetArray(APL,APARR,APPI,IERR)
C 
      AE(1:NIJKBKAL)=0.0D0;  AW(1:NIJKBKAL)=0.0D0  
      AN(1:NIJKBKAL)=0.0D0;  AS(1:NIJKBKAL)=0.0D0
      AT(1:NIJKBKAL)=0.0D0;  AB(1:NIJKBKAL)=0.0D0  
      AL(1:NOCBKAL) =0.0D0;  AR(1:NOCBKAL)=0.0D0
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
      GU=GDS(1)
C
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT,
C$OMP*                    SB,APT)
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: EAST
C
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXUVW(IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                           XER(IJK),YER(IJK),ZER(IJK),
     *               F1(IJK),AW(IJK+NJ),AE(IJK),FX(IJK),GU,M)
      END DO
      END DO
      END DO
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: NORTH
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXUVW(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                         XNR(IJK),YNR(IJK),ZNR(IJK),
     *               F2(IJK),AS(IJK+1),AN(IJK),FY(IJK),GU,M)
      END DO
      END DO
      END DO
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: TOP
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXUVW(IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                            XTR(IJK),YTR(IJK),ZTR(IJK),
     *               F3(IJK),AB(IJK+NIJ),AT(IJK),FZ(IJK),GU,M)
      END DO
      END DO
      END DO
C
C.....BUOYANCY SOURCE CONTRIBUTION (BOUSSINESQ APPROXIMATION)
C
      IF(LCAL(IEN)) THEN
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        SB=-BETA*DEN(IJK)*VOL(IJK)*(T(IJK)-TREF)
        SU(IJK)=SU(IJK)+GRAVX*SB
        SV(IJK)=SV(IJK)+GRAVY*SB
        SW(IJK)=SW(IJK)+GRAVZ*SB
      END DO
      END DO
      END DO
      ENDIF
C
C.....ADDITIONAL SOURCE CONTRIBUTION DUE TO MANUFACTURED SOLUTION
C
#ifdef USE_ANALYTICAL
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        SU(IJK)=SU(IJK)+SUMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
        SV(IJK)=SV(IJK)+SVMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
        SW(IJK)=SW(IJK)+SWMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
C
      END DO
      END DO
      END DO
#endif
C
C.....PRESSURE SOURCE CONTRIBUTION
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        SU(IJK)=SU(IJK)-DPX(IJK)*VOL(IJK)
        SV(IJK)=SV(IJK)-DPY(IJK)*VOL(IJK)
        SW(IJK)=SW(IJK)-DPZ(IJK)*VOL(IJK)
      END DO
      END DO
      END DO
C
C.....UNSTEADY TERM CONTRIBUTION (GAMT = 0 -> IMPLICIT EULER;
C.....GAMT = 1 -> THREE TIME LEVELS; BLENDING POSSIBLE)
C
      IF(LTIME) THEN
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        APT=DEN(IJK)*VOL(IJK)*DTR
        SU(IJK)=SU(IJK)+APT*((1.0D0+GAMT)*UO(IJK)-0.5D0*GAMT*UOO(IJK))
        SV(IJK)=SV(IJK)+APT*((1.0D0+GAMT)*VO(IJK)-0.5D0*GAMT*VOO(IJK))
        SW(IJK)=SW(IJK)+APT*((1.0D0+GAMT)*WO(IJK)-0.5D0*GAMT*WOO(IJK))
        AP(IJK)=AP(IJK)+APT*(1.0D0+0.5D0*GAMT)
      END DO
      END DO
      END DO
      ENDIF
C
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
C.....INLET BOUNDARIES (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C
      DO II=1,NINLBKAL
        IJP=IJPI(II)
        IJB=IJI(II)
C
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C
        CALL FLUXUVW(IJP,IJB,XIC(II),YIC(II),ZIC(II),
     *                       XIR(II),YIR(II),ZIR(II),
     *               FMI(II),CP,CB,PONE,PZERO,1)
C
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-CB*U(IJB)
        SV(IJP)=SV(IJP)-CB*V(IJB)
        SW(IJP)=SW(IJP)-CB*W(IJB)
      END DO
C
C.....OUTLET BOUNDARIES (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
C
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C
        CALL FLUXUVW(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                       XUR(IO),YOR(IO),ZOR(IO),
     *               FMO(IO),CP,CB,PONE,PZERO,1)
C
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-CB*U(IJB)
        SV(IJP)=SV(IJP)-CB*V(IJB)
        SW(IJP)=SW(IJP)-CB*W(IJB)
      END DO
C
C.....Zu Wand- und Symmetrierandbedingungen siehe Peric S.305 (8.90).
C
C.....WALL BOUNDARIES
C
      DO IW=1,NWALBKAL
        IJP=IJPW(IW)
        IJB=IJW(IW)
C       VISS=MAX(VISC,VISW(IW))
        VISS=VISC
        COEF=VISS*SRDW(IW)
        ARE=SQRT(XNW(IW)**2+YNW(IW)**2+ZNW(IW)**2)
C
        UPB=U(IJP)-U(IJB)
        VPB=V(IJP)-V(IJB)
        WPB=W(IJP)-W(IJB)
C
        VNP=UPB*XNW(IW)+VPB*YNW(IW)+WPB*ZNW(IW)
C
        XTP=UPB-VNP*XNW(IW)/(ARE**2+SMALL)
        YTP=VPB-VNP*YNW(IW)/(ARE**2+SMALL)
        ZTP=WPB-VNP*ZNW(IW)/(ARE**2+SMALL)
C
        DPB=DSQRT((XC(IJP)-XC(IJB))**2
     *           +(YC(IJP)-YC(IJB))**2
     *           +(ZC(IJP)-ZC(IJB))**2)
        VISOL=VISS*ARE/DPB
C
        AP(IJP)=AP(IJP)+VISOL
C.......USE DEFERRED CORRECTION, DO NOT USE U(IJB)!
        SU(IJP)=SU(IJP)+VISOL*U(IJP)-COEF*XTP
        SV(IJP)=SV(IJP)+VISOL*V(IJP)-COEF*YTP
        SW(IJP)=SW(IJP)+VISOL*W(IJP)-COEF*ZTP
      END DO
#ifndef USE_2DCASE
C
C.....SYMMETRY BOUNDARIES
C
      DO ISY=1,NSYMBKAL
        IJP=IJPS(ISY)
        IJB=IJS(ISY)
        COEF=VIS(IJB)*SRDS(ISY)
C
        ARE=SQRT(XNS(ISY)**2+YNS(ISY)**2+ZNS(ISY)**2)
        XNP=XNS(ISY)/(ARE+SMALL)
        YNP=YNS(ISY)/(ARE+SMALL)
        ZNP=ZNS(ISY)/(ARE+SMALL)
        FDE=2.0D0*COEF*((U(IJP)-U(IJB))*XNP
     *                 +(V(IJP)-V(IJB))*YNP
     *                 +(W(IJP)-W(IJB))*ZNP)
C
        DPB=SQRT((XC(IJP)-XC(IJB))**2+(YC(IJP)-YC(IJB))**2
     *          +(ZC(IJP)-ZC(IJB))**2)
        VISOL=VIS(IJB)*ARE/DPB
C
        AP(IJP)=AP(IJP)+VISOL
        SU(IJP)=SU(IJP)+VISOL*U(IJP)-FDE*XNP
        SV(IJP)=SV(IJP)+VISOL*V(IJP)-FDE*YNP
        SW(IJP)=SW(IJP)+VISOL*W(IJP)-FDE*ZNP
      END DO
#endif
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES!)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        MIJ=IBLKOCBK(I)        
        CALL FLUXUVW(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                       XOCR(I),YOCR(I),ZOCR(I),
     *              FMOC(I),AL(I),AR(I),FOCBK(I),GU,MIJ)
        AP(IJP) =AP(IJP)-AR(I)
        AP(IJN) =AP(IJN)-AL(I)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE NO INTERNAL BOUNDARIES!)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXUVW(IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                       XFR(I),YFR(I),ZFR(I),
     *               FMF(I),AFL(I),AFR(I),FFSGBK(I),GU,MIJ)
        AP(IJP) =AP(IJP)-AFR(I)
        AP(IJN) =AP(IJN)-AFL(I)
      END DO
C
C.....Call to Inner Walls UVW modifications routine
C
C
C.....RESTORE ARRAYS THAT ARE NOT NEEDED ANYMORE
C
      CALL VecRestoreArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecRestoreArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecRestoreArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecRestoreArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecRestoreArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecRestoreArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecRestoreArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecRestoreArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecRestoreArray(DWZL,DWZARR,DWZZI,IERR)
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
      IF(LCAL(IEN)) CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecGhostRestoreLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostRestoreLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostRestoreLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostRestoreLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostRestoreLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostRestoreLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostRestoreLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostRestoreLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostRestoreLocalForm(DWZVEC,DWZL,IERR)
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
C.....UPDATE SOURCE AND DIAGONAL VECTOR ON ALL PROCESSORS, ASSEMBLE MATRIX
C
      CALL VecRestoreArray(SUL,SUARR,SUUI,IERR)
      CALL VecRestoreArray(SVL,SVARR,SVVI,IERR)
      CALL VecRestoreArray(SWL,SWARR,SWWI,IERR)
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
C
      CALL VecGhostRestoreLocalForm(SUVEC,SUL,IERR)
      CALL VecGhostRestoreLocalForm(SVVEC,SVL,IERR)
      CALL VecGhostRestoreLocalForm(SWVEC,SWL,IERR)
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
C
      CALL VecGhostUpdateBegin(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(SVVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SVVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(SWVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SWVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....GET LOCAL PART OF AP,SU,APR
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecGetArray(APRVEC,APRARR,APRRI,IERR)
C
C.....FINAL COEFFICIENT AND SOURCES MATRIX FOR U-EQUATION
C
C.....OpenMP : Here starts parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*            NKMT,NIMT,NJMT,KSTT,ISTT)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM 
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C...ZWISCHENSPEICHERUNG VON AP OHNE UNTERRELAXATIONSFAKTOREN (ES IST
C...MÖGLICH, DASS U,V,W UNTERSCHIEDLICH UNTERRELAXIERT WERDEN)
        APR(IJK)=AP(IJK)
        AP(IJK)=(APR(IJK)-AE(IJK)-AW(IJK)-AN(IJK)-AS(IJK)
     *                   -AT(IJK)-AB(IJK))*URFU
C...NOTE THAT AP ALREADY HAS BEEN DIVIDED BY URF!!
        SU(IJK)=SU(IJK)+(1.0D0-URF(IU))*AP(IJK)*U(IJK)
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecRestoreArray(UL,UARR,UUI,IERR)
      CALL VecGhostRestoreLocalForm(UVEC,UL,IERR)
C
#ifdef USE_SIPSOL
C
C.....SOLVING EQUATION SYSTEM FOR U-VELOCITY USING SIP/CGSTAB SOLVER
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL PetscLogStagePush(STAGE1,IERR)
      CALL SOLVER(UARR,UUI,IU)
      CALL PetscLogStagePop(IERR)
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
#else
C
C.....SOLVING EQUATION SYSTEM FOR U-VELOCITY USING PETSC SOLVER
C
      CALL ASSEMBLESYS(AMAT)
      CALL MatZeroRows(AMAT,NZERO,ZEROS,PONE,UVEC,SUVEC,IERR)
      CALL PetscLogStagePush(STAGE1,IERR)
      CALL SOLVESYS(IU,UVEC,AMAT,SUVEC)
      CALL PetscLogStagePop(IERR)
      CALL VecGhostUpdateBegin(UVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(UVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
#endif
C
C.....FINAL COEFFICIENT AND SOURCES MATRIX FOR V-EQUATION
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecGetArray(SVVEC,SVARR,SVVI,IERR)
C
C.....OpenMP : Here starts parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*            NKMT,NIMT,NJMT,KSTT,ISTT)
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM 
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        AP(IJK)=(APR(IJK)-AE(IJK)-AW(IJK)
     *                   -AN(IJK)-AS(IJK)
     *                   -AT(IJK)-AB(IJK))*URFV
#ifdef USE_SIPSOL
        SU(IJK)=SV(IJK)+(1.0D0-URF(IV))*AP(IJK)*V(IJK)
#else
        SV(IJK)=SV(IJK)+(1.0D0-URF(IV))*AP(IJK)*V(IJK)
#endif
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecRestoreArray(SVVEC,SVARR,SVVI,IERR)
C
      CALL VecRestoreArray(VL,VARR,VVI,IERR)
      CALL VecGhostRestoreLocalForm(VVEC,VL,IERR)
C
#ifndef USE_SIPSOL
      IF (URF(IV).NE.URF(IU))
     *      CALL MatDiagonalSet(AMAT,APVEC,INSERT_VALUES,IERR)
      CALL MatZeroRows(AMAT,NZERO,ZEROS,PONE,VVEC,SVVEC,IERR)
#endif
C
#ifdef USE_SIPSOL
C
C.....SOLVING EQUATION SYSTEM FOR V-VELOCITY USING SIP/CGSTAB SOLVER
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL PetscLogStagePush(STAGE1,IERR)
      CALL SOLVER(VARR,VVI,IV)
      CALL PetscLogStagePop(IERR)
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
#else
C
C.....SOLVING EQUATION SYSTEM FOR V-VELOCITY USING PETSC SOLVER
C
      CALL PetscLogStagePush(STAGE1,IERR)
      CALL SOLVESYS(IV,VVEC,AMAT,SVVEC)
      CALL PetscLogStagePop(IERR)
      CALL VecGhostUpdateBegin(VVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
#endif
C
C.....FINAL COEFFICIENT AND SOURCES MATRIX FOR W-EQUATION
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecGetArray(SWVEC,SWARR,SWWI,IERR)
C
C.....OpenMP : Here starts parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*            NKMT,NIMT,NJMT,KSTT,ISTT)
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM 
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        AP(IJK)=APR(IJK)-AE(IJK)-AW(IJK)
     *                  -AN(IJK)-AS(IJK)
     *                  -AT(IJK)-AB(IJK)

#if defined(USE_SIMPLEC) && defined(USE_PWIM)
        AP(IJK)=AP(IJK)*URFU
        APR(IJK)=1.0D0/(AP(IJK)+SMALL)
#elif defined(USE_SIMPLEC)
        AP(IJK)=AP(IJK)*URFU
        APR(IJK)=1.0D0/(AP(IJK)
     *                 +AE(IJK)+AW(IJK)
     *                 +AN(IJK)+AS(IJK)
     *                 +AT(IJK)+AB(IJK)
     *                 +SMALL)
#else
        AP(IJK)=AP(IJK)*URFU
        APR(IJK)=1.0D0/(AP(IJK)+SMALL)
#endif
C
#ifdef USE_SIPSOL
        SU(IJK)=SW(IJK)+(1.0D0-URF(IWW))*AP(IJK)*W(IJK)
#else
        SW(IJK)=SW(IJK)+(1.0D0-URF(IWW))*AP(IJK)*W(IJK)
#endif
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
      CALL VecRestoreArray(WL,WARR,WWI,IERR)
      CALL VecGhostRestoreLocalForm(WVEC,WL,IERR)
C
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(APRVEC,APRARR,APRRI,IERR)
      CALL VecGhostUpdateBegin(
     *     APRVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(APRVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecRestoreArray(SWVEC,SWARR,SWWI,IERR)
C
#ifndef USE_SIPSOL
      IF (URF(IU).NE.URF(IV).OR.URF(IV).NE.URF(IWW))
     *      CALL MatDiagonalSet(AMAT,APVEC,INSERT_VALUES,IERR)
C
C     CALL MatZeroRows(AMAT,NZERO,ZEROS,PONE,WVEC,SUVEC,IERR)
      CALL MatZeroRows(AMAT,NZERO,ZEROS,PONE,WVEC,SWVEC,IERR)
#endif
#ifdef USE_SIPSOL
C
C.....SOLVING EQUATION SYSTEM FOR W-VELOCITY USING SIP/CGSTAB SOLVER
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL PetscLogStagePush(STAGE1,IERR)
      CALL SOLVER(WARR,WWI,IWW)
      CALL PetscLogStagePop(IERR)
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
#else
C
C.....SOLVING EQUATION SYSTEM FOR W-VELOCITY USING PETSC SOLVER
C
      CALL PetscLogStagePush(STAGE1,IERR)
      IF (LCAL(IWW)) CALL SOLVESYS(IWW,WVEC,AMAT,SWVEC)
      CALL PetscLogStagePop(IERR)
      CALL VecGhostUpdateBegin(WVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(WVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
#endif
C
      RETURN
      END 
C
#include "petsc.user.inc"
C
C
C################################################################
      SUBROUTINE FLUXUVW(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                   FM,CAP,CAN,FAC,G,MB)
C################################################################
C     This routine calculates momentum fluxes (convective and
C     diffusive) through the cell face between nodes IJP and IJN. 
C     IJ1 and IJ2 are the indices of CV corners defining the cell 
C     face. FM is the mass flux through the face, and FAC is the 
C     interpolation factor (distance from node IJP to cell face 
C     center over the sum of this distance and the distance from 
C     cell face center to node IJN).
C 
C     One can then use a convex combination of the values at IJP and
C     IJN to interpolate linearily to the boundary face by
C     FI_f = (1-FAC) * FI_P + FAC * FI_N
C
C     CAP and CAN are the 
C     contributions to matrix coefficients in the momentum
C     equations at nodes IJP and IJN. Diffusive fluxes are
C     discretized using central differences; for convective
C     fluxes, linear interpolation can be blended with upwind
C     approximation; see Sect. 8.6 for details. Note: cell
C     face surface vector is directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP,MB,M
      INTEGER LS
C
      REAL*8 FM,FACP,FAC,VISI,DENI,ZZC,YYC,XXC
      REAL*8 YCR,ZCR,XCR,FMI,FMX,DUXI,DVXI,DWXI
      REAL*8 DUYI,DVYI,DWYI,DUZI,DVZI,DWZI,XI,YI,ZI
      REAL*8 UI,VI,WI,XPN,ZPN,YPN,VSOL,FCUE
      REAL*8 FCWE,FDUE,FDVE,FDWE,FCUI,FCVI,FCWI,FDWI
      REAL*8 CAN,CAP,FUC,G,FVC,SUM,FCVE,FDUI,FDVI,FWC 
C
#include "petsc.user.inc"
C=========================================================
C
C.....INTERPOLATE ALONG LINE P-N
C
      FACP=1.0D0-FAC
      VISI=VIS(IJN)*FAC+VIS(IJP)*FACP
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....COMPUTE FM 
C
      FMI=MIN(FM,ZERO)
      FMX=MAX(FM,ZERO)
C
C.....INTERPOLATE ALONG LINE P-N
C
      DUXI=DUX(IJN)*FAC+DUX(IJP)*FACP
      DVXI=DVX(IJN)*FAC+DVX(IJP)*FACP
      DWXI=DWX(IJN)*FAC+DWX(IJP)*FACP
      DUYI=DUY(IJN)*FAC+DUY(IJP)*FACP
      DVYI=DVY(IJN)*FAC+DVY(IJP)*FACP
      DWYI=DWY(IJN)*FAC+DWY(IJP)*FACP
      DUZI=DUZ(IJN)*FAC+DUZ(IJP)*FACP
      DVZI=DVZ(IJN)*FAC+DVZ(IJP)*FACP
      DWZI=DWZ(IJN)*FAC+DWZ(IJP)*FACP

      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
C
C.....CALCULATE CELL-FACE VELOCITIES AND VISCOSITY
C
      UI=U(IJN)*FAC+U(IJP)*FACP
     *                         +DUXI*(XXC-XI)
     *                         +DUYI*(YYC-YI)
     *                         +DUZI*(ZZC-ZI)
      VI=V(IJN)*FAC+V(IJP)*FACP
     *                         +DVXI*(XXC-XI)
     *                         +DVYI*(YYC-YI)
     *                         +DVZI*(ZZC-ZI)
      WI=W(IJN)*FAC+W(IJP)*FACP
     *                         +DWXI*(XXC-XI)
     *                         +DWYI*(YYC-YI)
     *                         +DWZI*(ZZC-ZI)
C
C.....DISTANCE VECTOR COMPONENTS, DIFFUSION COEFFICIENT
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      VSOL=VISI*DSQRT((XCR**2+YCR**2+ZCR**2)/
     *                (XPN**2+YPN**2+ZPN**2))
C
C.....EXPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCUE=FM*UI
      FCVE=FM*VI
      FCWE=FM*WI
#ifdef USE_FULLNAVIERSTOKES
C.....projektion auf den normalenvektor n (FULL NAVIER STOKES)
      FDUE=VISI*((DUXI+DUXI)*XCR+(DUYI+DVXI)*YCR+(DUZI+DWXI)*ZCR)
      FDVE=VISI*((DVXI+DUYI)*XCR+(DVYI+DVYI)*YCR+(DVZI+DWYI)*ZCR)
      FDWE=VISI*((DWXI+DUZI)*XCR+(DWYI+DVZI)*YCR+(DWZI+DWZI)*ZCR)
#else
C......NO VARIABLE VISCOSITY VERSION NAVIER-STOKES-EQUATION (CONSISTENT
C......TREATMENT - VANISHES ON ORTHOGONAL GRID, VALID IN CASE OF
C......CONSTANT DENSITY AND VISCOSITY)
      FDUE=VISI*((DUXI)*XCR+(DUYI)*YCR+(DUZI)*ZCR)
      FDVE=VISI*((DVXI)*XCR+(DVYI)*YCR+(DVZI)*ZCR)
      FDWE=VISI*((DWXI)*XCR+(DWYI)*YCR+(DWZI)*ZCR)
#endif
C
C.....IMPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCUI=FMI*U(IJN)+FMX*U(IJP)
      FCVI=FMI*V(IJN)+FMX*V(IJP)
      FCWI=FMI*W(IJN)+FMX*W(IJP)
C.....projektion auf den verbindungsvektor xi
      FDUI=VSOL*(DUXI*XPN+DUYI*YPN+DUZI*ZPN)
      FDVI=VSOL*(DVXI*XPN+DVYI*YPN+DVZI*ZPN)
      FDWI=VSOL*(DWXI*XPN+DWYI*YPN+DWZI*ZPN)
C
C.....COEFFICIENTS, DEFERRED CORRECTION, SOURCE TERMS
C
      CAN=-VSOL+FMI
      CAP=-VSOL-FMX
C
      FUC=G*(FCUE-FCUI)
      FVC=G*(FCVE-FCVI)
      FWC=G*(FCWE-FCWI)
C
      SU(IJP)=SU(IJP)-FUC+FDUE-FDUI
      SU(IJN)=SU(IJN)+FUC-FDUE+FDUI
C
      SV(IJP)=SV(IJP)-FVC+FDVE-FDVI
      SV(IJN)=SV(IJN)+FVC-FDVE+FDVI
C
      SW(IJP)=SW(IJP)-FWC+FDWE-FDWI
      SW(IJN)=SW(IJN)+FWC-FDWE+FDWI
C
      RETURN
C
C
      END
C 
#include "petsc.user.inc"
C
C
C############################################################## 
      SUBROUTINE CALCP(INTMF)
C############################################################## 
C     This routine assembles and solves the pressure-correction
C     equation using colocated grid. SIMPLE algorithm with one
C     corrector step (non-orthogonality effects neglected) is
C     applied.
C
C==============================================================
      implicit none
C
#include "finclude/petsc.h"
c
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER M
      INTEGER I,J,K,IJK,IJP,IJN,II,INTMF,IO,LC,ISY
      INTEGER LS
      INTEGER IJB,IJK1,IJK2,IJK3,IJK4
C
      REAL*8 SUM,FLOWO,PPO,UN,FMCOR,DX12,DY12,DZ12,DX13,DY13,DZ13
      REAL*8 DX14,DY14,DZ14,S23,S34,FLOMON,XR23,YR23,ZR23
      REAL*8 XR34,YR34,ZR34,XNV,YNV,ZNV,FAC
      REAL*8 VM,PPOG,VMG,FLOWOG,CB,CP
C 
      Vec SUL,APL,APRL,PL,PPL,
     *    UL,VL,WL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL
      PetscScalar PZERO,PONE,ZEROSC
      PetscErrorCode IERR
      PetscInt ZEROPP
      PetscLogStage stage
      REAL*8 TEMP
C
      COMMON /CORRECTOR/ LC
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C==============================================================
C
C.....INITIALIZE SUM AND COEFs
C
      SUM=0.0D0
      PZERO=0.0D0
      PONE=1.0D0
C
      CALL VecGhostGetLocalForm(SUVEC,SUL,IERR)
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
C
      CALL VecSet(SUL,PZERO,IERR)
      CALL VecSet(APL,PZERO,IERR)
C
      CALL VecGetArray(SUL,SUARR,SUUI,IERR)
      CALL VecGetArray(APL,APARR,APPI,IERR)
C
      AE(1:NIJKBKAL)=0.0D0;  AW(1:NIJKBKAL)=0.0D0 
      AN(1:NIJKBKAL)=0.0D0;  AS(1:NIJKBKAL)=0.0D0 
      AT(1:NIJKBKAL)=0.0D0;  AB(1:NIJKBKAL)=0.0D0 
      AL(1:NOCBKAL)=0.0D0;   AR(1:NOCBKAL)=0.0D0 
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
C
C.....GET ARRAY OF VECTOR VALUES (without update)
C
      CALL VecGhostGetLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostGetLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostGetLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostGetLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostGetLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostGetLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostGetLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostGetLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostGetLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
C
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecGetArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecGetArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecGetArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecGetArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecGetArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecGetArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecGetArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecGetArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
C
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
C.....GET ARRAY OF VECTOR VALUES
C
      CALL VecGhostGetLocalForm(UVEC,UL,IERR)
      CALL VecGhostGetLocalForm(VVEC,VL,IERR)
      CALL VecGhostGetLocalForm(WVEC,WL,IERR)
      CALL VecGhostGetLocalForm(PVEC,PL,IERR)
      CALL VecGhostGetLocalForm(APRVEC,APRL,IERR)
C
      CALL VecGetArray(UL,UARR,UUI,IERR)
      CALL VecGetArray(VL,VARR,VVI,IERR)
      CALL VecGetArray(WL,WARR,WWI,IERR)
      CALL VecGetArray(PL,PARR,PPI,IERR)
      CALL VecGetArray(APRL,APRARR,APRRI,IERR)
C
C.....Call to Inner Walls FIX modifications routine
C
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT)
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
      DO K=2,NKM 
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXM(IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                        XER(IJK),YER(IJK),ZER(IJK),
     *             F1(IJK),AW(IJK+NJ),AE(IJK),FX(IJK))
      END DO
      END DO
      END DO
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXM(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                       XNR(IJK),YNR(IJK),ZNR(IJK),
     *             F2(IJK),AS(IJK+1),AN(IJK),FY(IJK))
      END DO
      END DO
      END DO
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXM(IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                         XTR(IJK),YTR(IJK),ZTR(IJK),
     *             F3(IJK),AB(IJK+NIJ),AT(IJK),FZ(IJK))
      END DO
      END DO
      END DO
C
      END DO
c.....OpenMP : Here ends this parallel loop section
C
C.....O- AND C-GRID CUTS
C    
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        CALL FLUXM(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                     XOCR(I),YOCR(I),ZOCR(I),
     *             FMOC(I),AL(I),AR(I),FOCBK(I))
        AP(IJP)=AP(IJP)-AR(I)
        AP(IJN)=AP(IJN)-AL(I)
        SU(IJP)=SU(IJP)-FMOC(I)
        SU(IJN)=SU(IJN)+FMOC(I)
      END DO
C
C.....FACE SEGMENTS
C    
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXM(IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                     XFR(I),YFR(I),ZFR(I),
     *             FMF(I),AFL(I),AFR(I),FFSGBK(I))
        AP(IJP)=AP(IJP)-AFR(I)
        AP(IJN)=AP(IJN)-AFL(I)
        SU(IJP)=SU(IJP)-FMF(I)
        SU(IJN)=SU(IJN)+FMF(I)
      END DO
C
C.....UPDATE ARRAYS AND GET ONLY LOCAL PART (because of possible return
C.....to main)
C
      CALL VecRestoreArray(SUL,SUARR,SUUI,IERR)
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
C
      CALL VecGhostRestoreLocalForm(SUVEC,SUL,IERR)
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
C
C.....RESTORE UNNEEDED ARRAYS
C
      CALL VecRestoreArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecRestoreArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecRestoreArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecRestoreArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecRestoreArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecRestoreArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecRestoreArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecRestoreArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecRestoreArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGhostRestoreLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostRestoreLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostRestoreLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostRestoreLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostRestoreLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostRestoreLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostRestoreLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostRestoreLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostRestoreLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
C.....RETURN IF ONLY MASS FLUXES UPDATED !!!
C
      IF(INTMF.EQ.0) RETURN
C
      CALL VecGhostUpdateBegin(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C 
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
C
C.....INLET BOUNDARIES (MASS FLUXES PRESCRIBED IN ROUTINE 'BCIN')
C
      DO II=1,NINLBKAL
C       FLOMAS=FLOMAS-FMI(II) ....NOT NECESSARY SEE BCIN
        SU(IJPI(II))=SU(IJPI(II))-FMI(II)
      END DO
C
      outletbc: IF (NOUTBKAL.GT.0) THEN
#if !defined( USE_DIRICHLETPRESSURE )
C
C.....Prescribed Outlet Velocity Profile
C
C     FLOWO=0.0D0
C     DO IO=1,NOUTBKAL
C       FLOWO=FLOWO+FMO(IO)
C       SU(IJPO(IO))=SU(IJPO(IO))-FMO(IO)
C     END DO
      FLOWO=0.0D0
      DO IO=1,NOUTBKAL
         IJP=IJPO(IO)
         IJB=IJO(IO)
C.....Set velocity gradient outlet to zero
         U(IJB)=U(IJP) 
         V(IJB)=V(IJP) 
         W(IJB)=W(IJP) 
C%%%  CALCULATION OF MASS FLUX        
         FMO(IO)=DENS*(U(IJB)*XUR(IO)
     *                +V(IJB)*YOR(IO)
     *                +W(IJB)*ZOR(IO))
         FLOWO=FLOWO+FMO(IO)
      ENDDO
      CALL MPI_ALLREDUCE(
     *     FLOWO,FLOWOG,1,MPI_REAL8,MPI_SUM,PETSC_COMM_WORLD,IERR)
      FLOWO=FLOWOG
      FAC=FLOMAS/(FLOWO+SMALL)
#if defined( USE_INFO )
      IF (RANK.EQ.0) WRITE(*,*) "RELATION OF INFLOW TO OUTFLOW", FAC
#endif
C
      DO IO=1,NOUTBKAL
         FMO(IO)=FMO(IO)*FAC
         IJB=IJO(IO)
C        
C.......NOT SURE IF THIS IS CORRECT
C        TEMP=DSQRT(XUR(IO)**2+YOR(IO)**2+ZOR(IO)**2)
C        U(IJB)=U(IJB)*FAC*XUR(IO)/TEMP 
C        V(IJB)=V(IJB)*FAC*YOR(IO)/TEMP
C        W(IJB)=W(IJB)*FAC*ZOR(IO)/TEMP
         U(IJB)=U(IJB)*FAC 
         V(IJB)=V(IJB)*FAC
         W(IJB)=W(IJB)*FAC
         SU(IJPO(IO))=SU(IJPO(IO))-FMO(IO)
      END DO
#else
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        APR(IJB)=APR(IJP)
C
        DPX(IJB)=DPX(IJP)
        DPY(IJB)=DPY(IJP)
        DPZ(IJB)=DPZ(IJP)
C
        CALL FLUXM(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                     XUR(IO),YOR(IO),ZOR(IO),
     *             FMO(IO),CP,CB,0.0D0)
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-FMO(IO)-CB*PP(IJB)
      END DO
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
#endif
#if defined( USE_INFO )
C.......CHECK CORRECTION
      TEMP=0.0D0
      DO IO=1,NOUTBKAL
        TEMP=TEMP+FMO(IO)
      END DO
      WRITE(*,*) "COMPARE",FLOMAS,TEMP
#endif
      ENDIF outletbc
C
C.....Call to Inner Walls modification routines
C
C     CALL INNWALLFIX
C     CALL INNWALLPP
C     CALL SOURCEM
C
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT),
C$OMP*            REDUCTION(+:SUM)
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....SOURCE TERM AND CENTRAL COEFFICIENT 
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        SU(IJK)=SU(IJK)+F1(IJK-NJ) -F1(IJK)
     *                 +F2(IJK-1)  -F2(IJK)
     *                 +F3(IJK-NIJ)-F3(IJK)
C
#if defined( USE_SIMPLEC ) && defined( USE_PWIM )
        SU(IJK)=SU(IJK)*(1.0D0-URF(IU))
#endif
C
        AP(IJK)=AP(IJK)-AE(IJK)-AW(IJK)
     *                 -AN(IJK)-AS(IJK)
     *                 -AT(IJK)-AB(IJK)
        SUM=SUM+SU(IJK)
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C      SUMG=SUM
C      CALL MPI_REDUCE(
C    *      SUM,SUMG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
C STILL NECESSARY TO BROADCAST THE VALUE OF SUM (GLOBAL REDUCTION ON 
C PROCESSOR 0!)
C
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
C
      CALL VecSet(PPVEC,PZERO,IERR)
C
#if !defined( USE_SIPSOL )
      CALL ASSEMBLESYS(AMAT)
#endif
C
C
C.....TEST GLOBAL MASS CONSERVATION & SOLVE EQUATIONS SYSTEM FOR P'
C
      presscorr: DO LC=1,NPCOR
C
        IF(LTEST) WRITE(2,*) '         SUM = ',SUM
C
C.....SOLVING EQUATION SYSTEM FOR PRESSURE CORRECTION
C
C       CALL MatZeroRowsColumns(AMAT,NZERO,ZEROS,PONE,PPVEC,SUVEC,IERR)
C
#if !defined( USE_SIPSOL )
C#ifdef USE_ZEROS
C        IJK=NI*NI*KMON+NI*IMON+JMON-1
C        CALL VecGetArray(PPVEC,PPARR,PPPI,IERR)
C        PP(IJK)=0.0D0
C        CALL VecRestoreArray(PPVEC,PPARR,PPPI,IERR)
CC       CALL MatZeroRowsColumns(AMAT,1,IJK,1.0D0,PPVEC,SUVEC,IERR)
C#endif
#endif
C
#if defined( USE_SIPSOL )
        CALL VecGetArray(APVEC,APARR,APPI,IERR)
        CALL VecGetArray(PPVEC,PPARR,PPPI,IERR)
        CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
C     
        CALL PetscLogStagePush(STAGE2,IERR)
        IF(LC.EQ.1)THEN
          CALL SOLVER(PPARR,PPPI,IP)
        ELSE
          CALL SOLVER(PPARR,PPPI,6)
        END IF
        CALL PetscLogStagePop(IERR)
C
        CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
        CALL VecRestoreArray(PPVEC,PPARR,PPPI,IERR)
        CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
#else
        CALL PetscLogStagePush(STAGE2,IERR)
        IF(LC.EQ.1)THEN
          CALL SOLVESYS(IP,PPVEC,AMAT,SUVEC)
        ELSE
          CALL SOLVESYS(6,PPVEC,AMAT,SUVEC)
        END IF
        CALL PetscLogStagePop(IERR)
      CALL VecGhostUpdateBegin(PPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
        CALL VecGhostUpdateEnd(PPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
#endif
C
C.....UPDATE PRESSURE CORECTION AT BOUNDARIES
C
        CALL PRESB(PPVEC)
#if defined( USE_DIRICHLETPRESSURE )
        CALL VecGetArray(PPVEC,PPARR,PPPI,IERR)
        DO IO=1,NOUTBKAL
          IJB=IJO(IO)
          PP(IJB)=0.0D0
        END DO
        CALL VecRestoreArray(PPVEC,PPARR,PPPI,IERR)
#endif
C
C.....Call to Inner Walls FIX modifications routine
C
C
C.....CALCULATE PRESSURE-CORRECTION GRADIENTS, REFERENCE P'
C
        CALL GRADFI(PPVEC,DPXVEC,DPYVEC,DPZVEC)
        CALL VecGetArray(PPVEC,PPARR,PPPI,IERR)
C
        CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
        CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
        CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
        CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
        CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
        CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
C.......REFERENZDRUCKKORREKTUR
        PPO=0.0D0
#if !defined( USE_DIRICHLETPRESSURE )
#if defined( USE_INTERPOLATION )
C.......CALCULATE REFERENCE LOCATION AND REFERENCE PRESSURE
        IF (RANK.EQ.RMON) THEN
          CALL SETIND(MMON)
C
          IJK=NI*NI*KMON+NI*IMON+JMON
          PPO=PPO+PP(IJK)
C  
          IJK=NI*NI*KMON+NI*IMON+JMON+1
          PPO=PPO+PP(IJK)
C  
          IJK=NI*NI*KMON+NI*(IMON+1)+JMON
          PPO=PPO+PP(IJK)
C  
          IJK=NI*NI*KMON+NI*(IMON+1)+JMON+1
          PPO=PPO+PP(IJK)
C  
#ifndef USE_2DCASE
          IJK=NI*NI*(KMON+1)+NI*IMON+JMON
          PPO=PPO+PP(IJK)
C  
          IJK=NI*NI*(KMON+1)+NI*IMON+JMON+1
          PPO=PPO+PP(IJK)
C
          IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON
          PPO=PPO+PP(IJK)
C
          IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON+1
          PPO=PPO+PP(IJK)
C
C......LINEAR INTERPOLATION (EQUIDISTANT GRID ASSUMED)
C
          PPO=PPO*0.125D0
#else
          PPO=PPO*0.25D0
#endif
        END IF
#elif defined( USE_MEANPRESSURE )
        PPOG=0.0D0
        VM=0.0D0
        VMG=0.0D0
        DO M=1,NBLKS
C
        CALL SETIND(M)
C
        DO K=2,NKM 
        DO I=2,NIM 
        DO J=2,NJM 
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          PPO=PPO+PP(IJK)*VOL(IJK)
          VM=VM+VOL(IJK)
        END DO
        END DO
        END DO
C
        ENDDO
        CALL MPI_REDUCE(
     *      PPO,PPOG,1,MPI_REAL8,MPI_SUM,RMON,PETSC_COMM_WORLD,IERR)
        CALL MPI_REDUCE(
     *      VM,VMG,1,MPI_REAL8,MPI_SUM,RMON,PETSC_COMM_WORLD,IERR)
        IF (RANK.EQ.RMON) PPO=PPOG/VMG
#else
        IF (RANK.EQ.RMON) THEN
          CALL SETIND(MMON)
          IJKMON=NI*NI*KMON+NI*IMON+JMON
          PPO=PP(IJKMON)
        END IF
#endif
C
#endif
        CALL MPI_BCAST(PPO,1,MPI_REAL8,RMON,PETSC_COMM_WORLD,IERR)
C
C.....CORRECT MASS FLUXES AT INNER CV-FACES (O-C CUTS)
C
        DO I=1,NOCBKAL
          FMOC(I)=FMOC(I)+
#if defined(USE_SIMPLEC) && defined(USE_PWIM)
     *           1.0D0/(1.0D0-URF(IU))*
#endif
     *           AR(I)*(PP(IJRPBK(I))-PP(IJLPBK(I)))
        END DO
C
C.....CORRECT MASS FLUXES AT INNER CV-FACES (FACE SEGMENTS)
C
        DO I=1,NFSGBKAL
          FMF(I)=FMF(I)+
#if defined(USE_SIMPLEC) && defined(USE_PWIM)
     *           1.0D0/(1.0D0-URF(IU))*
#endif
     *          AFR(I)*(PP(IJFR(I))-PP(IJFL(I)))
        END DO
C
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT)
C
C.....CORRECT MASS FLUXES AT INNER CV-FACES (COMMON INNER FACES)
C
        correct: DO M=1,NBLKS
C
        CALL SETIND(M)
C
        DO K=2,NKM
        DO I=2,NIM-1
        DO J=2,NJM
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          F1(IJK)=F1(IJK)+
#if defined(USE_SIMPLEC) && defined(USE_PWIM)
     *           1.0D0/(1.0D0-URF(IU))*
#endif
C.....DAS BENÖTIGTE MINUS STECKT BEREITS IM KOEFFIZIENTEN
     *           AE(IJK)*(PP(IJK+NJ)-PP(IJK))
        END DO
        END DO
        END DO
C
        DO K=2,NKM
        DO I=2,NIM
        DO J=2,NJM-1
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          F2(IJK)=F2(IJK)+
#if defined(USE_SIMPLEC) && defined(USE_PWIM)
     *           1.0D0/(1.0D0-URF(IU))*
#endif
     *           AN(IJK)*(PP(IJK+1)-PP(IJK))
        END DO
        END DO
        END DO
C
        DO K=2,NKM-1
        DO I=2,NIM
        DO J=2,NJM
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          F3(IJK)=F3(IJK)+
#if defined(USE_SIMPLEC) && defined(USE_PWIM)
     *           1.0D0/(1.0D0-URF(IU))*
#endif
     *           AT(IJK)*(PP(IJK+NIJ)-PP(IJK))
        END DO
        END DO
        END DO
C
C.....CORRECT PRESSURE AND VELOCITIES (UPDATE WILL BE AFTER LOOP)
C
        DO K=2,NKM
        DO I=2,NIM
        DO J=2,NJM
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
#if defined(USE_SIMPLEC) && defined(USE_PWIM)
          U(IJK)=U(IJK)
     *          -1.0D0/(1.0D0-URF(IU))*DPX(IJK)*VOL(IJK)*APR(IJK)
          V(IJK)=V(IJK)
     *          -1.0D0/(1.0D0-URF(IU))*DPY(IJK)*VOL(IJK)*APR(IJK)
          W(IJK)=W(IJK)
     *          -1.0D0/(1.0D0-URF(IU))*DPZ(IJK)*VOL(IJK)*APR(IJK)
#else
          U(IJK)=U(IJK)-DPX(IJK)*VOL(IJK)*APR(IJK)
          V(IJK)=V(IJK)-DPY(IJK)*VOL(IJK)*APR(IJK)
          W(IJK)=W(IJK)-DPZ(IJK)*VOL(IJK)*APR(IJK)
#endif
#if defined(USE_SIMPLEC)
          P(IJK)=P(IJK)+(PP(IJK)-PPO)
#else
          P(IJK)=P(IJK)+URF(IP)*(PP(IJK)-PPO)
#endif
        END DO
        END DO
        END DO
C
        END DO correct
#if defined( USE_DIRICHLETPRESSURE )
C
C.....CORRECT MASS FLUX AT PRESSURE OUTLET
C
        DO IO=1,NOUTBKAL
          IJP=IJPO(IO)
          IJB=IJO(IO)
          U(IJB)=U(IJP)-APR(IJP)*2.0D0*XUR(IO)*(PP(IJP)-PP(IJB))
          V(IJB)=V(IJP)-APR(IJP)*2.0D0*YOR(IO)*(PP(IJP)-PP(IJB))
          W(IJB)=W(IJP)-APR(IJP)*2.0D0*ZOR(IO)*(PP(IJP)-PP(IJB))
          FMO(IO)=DENS*(U(IJB)*XUR(IO)
     *                 +V(IJB)*YOR(IO)
     *                 +W(IJB)*ZOR(IO))
        END DO
#endif
C       IJK=NI*NI*KMON+NI*IMON+JMON
C.....OpenMP : Here ends this parallel loop section
        CALL VecRestoreArray(PPVEC,PPARR,PPPI,IERR)
C
C.....SOURCE TERM MODIFICATION FOR THE SECOND CORRECTOR
C
        IF(LC.NE.NPCOR)THEN
C
          CALL VecSet(PPVEC,PZERO,IERR)
C
          CALL VecSet(SUVEC,PZERO,IERR)
          CALL VecGhostGetLocalForm(SUVEC,SUL,IERR)
          CALL VecSet(SUL,PZERO,IERR)
          CALL VecGetArray(SUL,SUARR,SUUI,IERR)
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT,
C$OMP*                    FMCOR)
          modblock: DO M=1,NBLKS
C          
          CALL SETIND(M)
C
          DO K=2,NKM
          DO I=2,NIM-1
          DO J=2,NJM
            IJK=LKBK(K+KST)+LIBK(I+IST)+J
            CALL FLUXMC(IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *           XER(IJK),YER(IJK),ZER(IJK),
     *           FMCOR,FX(IJK))
            F1(IJK)=F1(IJK)+FMCOR
            SU(IJK)=SU(IJK)-FMCOR
            SU(IJK+NJ)=SU(IJK+NJ)+FMCOR
          END DO
          END DO
          END DO
          
          DO K=2,NKM
          DO I=2,NIM
          DO J=2,NJM-1
             IJK=LKBK(K+KST)+LIBK(I+IST)+J
             CALL FLUXMC(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *            XNR(IJK),YNR(IJK),ZNR(IJK),
     *            FMCOR,FY(IJK))
             F2(IJK)=F2(IJK)+FMCOR
             SU(IJK)=SU(IJK)-FMCOR
             SU(IJK+1)=SU(IJK+1)+FMCOR
          END DO
          END DO
          END DO
          
          DO K=2,NKM-1
          DO I=2,NIM
          DO J=2,NJM
            IJK=LKBK(K+KST)+LIBK(I+IST)+J
            CALL FLUXMC(IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *           XTR(IJK),YTR(IJK),ZTR(IJK),
     *           FMCOR,FZ(IJK))
            F3(IJK)=F3(IJK)+FMCOR
            SU(IJK)=SU(IJK)-FMCOR
            SU(IJK+NIJ)=SU(IJK+NIJ)+FMCOR
          END DO
          END DO
          END DO
C
C          DO K=2,NKM
C          DO I=2,NIM
C          DO J=2,NJM
C            IJK=LKBK(K+KST)+LIBK(I+IST)+J
CC           PP(IJK)=0.0D0
C            SUM=SUM+SU(IJK)
C          ENDDO
C          ENDDO
C          ENDDO
C
          ENDDO modblock
C.....OpenMP : Here ends this parallel loop section
C
          DO I=1,NOCBKAL
            IJP=IJLPBK(I)
            IJN=IJRPBK(I)
            CALL FLUXMC(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                          XOCR(I),YOCR(I),ZOCR(I),
     *           FMCOR,FOCBK(I))
            FMOC(I)=FMOC(I)+FMCOR
            SU(IJP)=SU(IJP)-FMCOR
            SU(IJN)=SU(IJN)+FMCOR
          END DO    
C 
          DO I=1,NFSGBKAL
            IJP=IJFL(I)
            IJN=IJFR(I)
            CALL FLUXMC(IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                          XFR(I),YFR(I),ZFR(I),
     *           FMCOR,FFSGBK(I))
            FMF(I)=FMF(I)+FMCOR
            SU(IJP)=SU(IJP)-FMCOR
            SU(IJN)=SU(IJN)+FMCOR
          END DO    
C
C.....UPDATE SU VECTOR COMPONENTS
C
          CALL VecRestoreArray(SUL,SUARR,SUUI,IERR)
          CALL VecGhostRestoreLocalForm(SUVEC,SUL,IERR)
          CALL VecGhostUpdateBegin(
     *         SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
          CALL VecGhostUpdateEND(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C 
        ENDIF
C 
        CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
        CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
        CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
        CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
        CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
        CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C 
      END DO presscorr
C
C.....UPDATE PRESSURE AT BOUNDARIES
C
      CALL PRESB(PVEC)
C
C.....Call to Inner Walls FIX modifications routine
C
#ifndef USE_2DCASE
C
C.....UPDATE VELOCITY COMPONENTS ALONG SYMMETRY BOUNDARIES
C
      DO ISY=1,NSYMBKAL
        IJP=IJPS(ISY)
        IJB=IJS(ISY)
        UN=(U(IJP)*XNS(ISY)+V(IJP)*YNS(ISY)+W(IJP)*ZNS(ISY))
     *    /(XNS(ISY)**2+YNS(ISY)**2+ZNS(ISY)**2)
        U(IJB)=U(IJP)-UN*XNS(ISY)
        V(IJB)=V(IJP)-UN*YNS(ISY)
        W(IJB)=W(IJP)-UN*ZNS(ISY)
      END DO
#endif
C
C.....RESTORE REMAINING ARRAYS
C
      CALL VecRestoreArray(UL,UARR,UUI,IERR)
      CALL VecRestoreArray(VL,VARR,VVI,IERR)
      CALL VecRestoreArray(WL,WARR,WWI,IERR)
      CALL VecRestoreArray(PL,PARR,PPI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(APRL,APRARR,APRRI,IERR)
C
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostRestoreLocalForm(UVEC,UL,IERR)
      CALL VecGhostRestoreLocalForm(VVEC,VL,IERR)
      CALL VecGhostRestoreLocalForm(WVEC,WL,IERR)
      CALL VecGhostRestoreLocalForm(PVEC,PL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(APRVEC,APRL,IERR)
C
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
C.....UPDATE VECTORS (U,V,W,P)
C
      CALL VecGhostUpdateBegin(UVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(UVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(VVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(WVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(WVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(PVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(PVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
C
#if !defined( USE_SIPSOL )
C NECESSARY IF NOT USING PC REDISTRIBUTE; KSP CHANGES BOUNDARY VALUES
      CALL BCIN
      CALL GRADFI(PVEC,DPXVEC,DPYVEC,DPZVEC)
#endif
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C##############################################################
      SUBROUTINE FLUXM(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                 FM,CAP,CAN,FAC)
C##############################################################
C     This routine calculates mass flux through the cell face 
C     between nodes IJP and IJN. IJ1 and IJ2 are the indices of 
C     CV corners defining the cell face. FM is the mass flux 
C     through the face, and FAC is the interpolation
C     factor (distance from node IJP to cell face center over
C     the sum of this distance and the distance from cell face 
C     center to node IJN). CAP and CAN are the contributions to
C     matrix coefficients in the pressure-correction equation
C     at nodes IJP and IJN. Surface vector directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI,DUXI,DVXI,DWXI
      REAL*8 DUYI,DVYI,DWYI,DUZI,DVZI,DWZI
      REAL*8 YYC,ZZC,UI,XXC,VI,WI,DENI,XPN,YPN
      REAL*8 ZPN,SMDPN,XCR,YCR,ZCR,CAP,CAN
      REAL*8 DPXI,DPYI,DPZI,FM,SAP
C
#include "petsc.user.inc"
C
C==============================================================
C
C.....INTERPOLATE ALONG LINE P-N (COORDINATES, VELOCITY GRADIENTS)
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
C
      DUXI=DUX(IJN)*FAC+DUX(IJP)*FACP
      DVXI=DVX(IJN)*FAC+DVX(IJP)*FACP
      DWXI=DWX(IJN)*FAC+DWX(IJP)*FACP
      DUYI=DUY(IJN)*FAC+DUY(IJP)*FACP
      DVYI=DVY(IJN)*FAC+DVY(IJP)*FACP
      DWYI=DWY(IJN)*FAC+DWY(IJP)*FACP
      DUZI=DUZ(IJN)*FAC+DUZ(IJP)*FACP
      DVZI=DVZ(IJN)*FAC+DVZ(IJP)*FACP
      DWZI=DWZ(IJN)*FAC+DWZ(IJP)*FACP
C
C.....CALCULATE CELL-FACE VALUES (VELOCITIES AND DENSITY)
C
      UI=U(IJN)*FAC+U(IJP)*FACP
     *                         +DUXI*(XXC-XI)
     *                         +DUYI*(YYC-YI)
     *                         +DUZI*(ZZC-ZI)
      VI=V(IJN)*FAC+V(IJP)*FACP
     *                         +DVXI*(XXC-XI)
     *                         +DVYI*(YYC-YI) 
     *                         +DVZI*(ZZC-ZI)
      WI=W(IJN)*FAC+W(IJP)*FACP
     *                         +DWXI*(XXC-XI)
     *                         +DWYI*(YYC-YI)
     *                         +DWZI*(ZZC-ZI)
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....SURFACE AND DISTANCE VECTOR COMPONENTS
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      SMDPN=(XCR**2+YCR**2+ZCR**2)/
     *      (XCR*XPN+YCR*YPN+ZCR*ZPN+SMALL)
C
      SAP=-((FAC *APR(IJN)*(DPX(IJN)*XCR
     *                     +DPY(IJN)*YCR
     *                     +DPZ(IJN)*ZCR)*VOL(IJN))
     *     +(FACP*APR(IJP)*(DPX(IJP)*XCR
     *                     +DPY(IJP)*YCR
     *                     +DPZ(IJP)*ZCR)*VOL(IJP)))
C
C.....MASS FLUX, COEFFICIENTS FOR THE P'-EQUATION
C
C     CAP=-0.5D0*(VOL(IJP)*APR(IJP)+VOL(IJN)*APR(IJN))*DENI*SMDPN
      CAP=-(FACP*VOL(IJP)*APR(IJP)+FAC*VOL(IJN)*APR(IJN))*DENI*SMDPN
      CAN=CAP
      DPXI=0.5D0*(DPX(IJN)+DPX(IJP))*XPN
      DPYI=0.5D0*(DPY(IJN)+DPY(IJP))*YPN
      DPZI=0.5D0*(DPZ(IJN)+DPZ(IJP))*ZPN
      FM=DENI*(UI*XCR+VI*YCR+WI*ZCR)
     *   +CAP*(P(IJN)-P(IJP)-DPXI-DPYI-DPZI)
C    *   +CAP*(P(IJN)-P(IJP))
C    *   -SAP
#if defined(USE_PWIM)
     *   +DENI*(1.0D0-URF(IU))
     *        *(FM
     *         -FAC* (U(IJN)*XCR+V(IJN)*YCR+W(IJN)*ZCR)
     *         -FACP*(U(IJP)*XCR+V(IJP)*YCR+W(IJP)*ZCR))
#endif
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C##############################################################
      SUBROUTINE FLUXMC(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                 FM,FAC)
C##############################################################
C     This routine calculates mass flux through the cell face 
C     between nodes IJP and IJN. IJ1 and IJ2 are the indices of 
C     CV corners defining the cell face. FM is the mass flux 
C     through the face, and FAC is the interpolation
C     factor (distance from node IJP to cell face center over
C     the sum of this distance and the distance from cell face 
C     center to node IJN). CAP and CAN are the contributions to
C     matrix coefficients in the pressure-correction equation
C     at nodes IJP and IJN. Surface vector directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      implicit none
C
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI
      REAL*8 YYC,ZZC,XXC,DENI,XPN,YPN
      REAL*8 ZPN,SMDPN,XCR,YCR,ZCR
      REAL*8 DPXI,DPYI,DPZI,FM,RAPR,DN
C
#include "petsc.user.inc"
C==============================================================
C
C.....INTERPOLATE ALONG LINE P-N COORDINATES
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP    
C
C.....CALCULATE CELL-FACE VALUES (DENSITY)
C
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....SURFACE AND DISTANCE VECTOR COMPONENTS
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      SMDPN=XCR**2+YCR**2+ZCR**2
      DN=XPN*XCR+YPN*YCR+ZPN*ZCR
C
C.....MASS FLUX CORRECTION
C
      RAPR=-0.5D0*(VOL(IJP)*APR(IJP)+VOL(IJN)*APR(IJN))*DENI
      DPXI=0.5D0*(DPX(IJN)+DPX(IJP))
      DPYI=0.5D0*(DPY(IJN)+DPY(IJP))
      DPZI=0.5D0*(DPZ(IJN)+DPZ(IJP))
      FM=RAPR*((DN*XCR-XPN*SMDPN)*DPXI+(DN*YCR-YPN*SMDPN)*DPYI+
     *         (DN*ZCR-ZPN*SMDPN)*DPZI)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C    
C###############################################################
      SUBROUTINE PRESB(FIVEC)
C###############################################################
C     This routine extrapolates the pressure or pressure
C     correction from interior to the boundary. Linear 
C     extrapolation is used, but one can also linearly
C     extrapolate the gradient...
C
C
C==============================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
C
      INTEGER M,K,I,J,IJK
C
      REAL*8 FIARR( 1)
C
      PetscOffset FIII
      Vec FIVEC
      PetscErrorCode IERR
C 
#include "petsc.user.inc"
C==============================================================
C
C.....SET INDICES
C
      CALL VecGetArray(FIVEC,FIARR,FIII,IERR)
      DO M=1,NBLKS 
      CALL SETIND(M)
#if defined( USE_LINEAREXTRAPOLATION )
C
C.....EXTRAPOLATE TO SOUTH AN NORTH BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM
        IJK=LKBK(K+KST)+LIBK(I+IST)+1
        FI(IJK)=FI(IJK+1)+(FI(IJK+1)-FI(IJK+2))*FY(IJK+1)
        IJK=LKBK(K+KST)+LIBK(I+IST)+NJ
        FI(IJK)=FI(IJK-1)+(FI(IJK-1)-FI(IJK-2))*(1.0D0-FY(IJK-2))
      END DO
      END DO
C
C.....EXTRAPOLATE TO WEST AND EAST BOUNDARIES
C
      DO K=2,NKM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(1+IST)+J
        FI(IJK)=FI(IJK+NJ)+(FI(IJK+NJ)-FI(IJK+NJ+NJ))*FX(IJK+NJ)
        IJK=LKBK(K+KST)+LIBK(NI+IST)+J
        FI(IJK)=FI(IJK-NJ)+(FI(IJK-NJ)-FI(IJK-NJ-NJ))*
     *          (1.0D0-FX(IJK-NJ-NJ))
      END DO
      END DO
#if !defined( USE_2DCASE )
C
C.....EXTRAPOLATE TO BOTTOM AND TOP BOUNDARIES
C
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(1+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK+NIJ)+(FI(IJK+NIJ)-FI(IJK+NIJ+NIJ))*FZ(IJK+NIJ)
        IJK=LKBK(NK+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK-NIJ)+(FI(IJK-NIJ)-FI(IJK-NIJ-NIJ))*
     *         (1.0D0-FZ(IJK-NIJ-NIJ))
      END DO
      END DO
#endif
C
      END DO
#else
C
C.....EXTRAPOLATE TO SOUTH AN NORTH BOUNDARIES
C
#if defined( USE_LINEAREXTRAPOLATION )
      DO K=2,NKM
      DO I=2,NIM
        IJK=LKBK(K+KST)+LIBK(I+IST)+1
        FI(IJK)=FI(IJK+1)+(FI(IJK+1)-FI(IJK+2))*FY(IJK+1)
        IJK=LKBK(K+KST)+LIBK(I+IST)+NJ
        FI(IJK)=FI(IJK-1)+(FI(IJK-1)-FI(IJK-2))*(1.0D0-FY(IJK-2))
      END DO
      END DO
C
C.....EXTRAPOLATE TO WEST AND EAST BOUNDARIES
C
      DO K=2,NKM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(1+IST)+J
        FI(IJK)=FI(IJK+NJ)+(FI(IJK+NJ)-FI(IJK+NJ+NJ))*FX(IJK+NJ)
        IJK=LKBK(K+KST)+LIBK(NI+IST)+J
        FI(IJK)=FI(IJK-NJ)+(FI(IJK-NJ)-FI(IJK-NJ-NJ))*
     *          (1.0D0-FX(IJK-NJ-NJ))
      END DO
      END DO
#ifndef USE_2DCASE
C
C.....EXTRAPOLATE TO BOTTOM AND TOP BOUNDARIES
C
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(1+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK+NIJ)+(FI(IJK+NIJ)-FI(IJK+NIJ+NIJ))*FZ(IJK+NIJ)
        IJK=LKBK(NK+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK-NIJ)+(FI(IJK-NIJ)-FI(IJK-NIJ-NIJ))*
     *         (1.0D0-FZ(IJK-NIJ-NIJ))
      END DO
      END DO
#endif
#else
      DO K=2,NKM
      DO I=2,NIM
        IJK=LKBK(K+KST)+LIBK(I+IST)+1
        FI(IJK)=FI(IJK+1)
        IJK=LKBK(K+KST)+LIBK(I+IST)+NJ
        FI(IJK)=FI(IJK-1)
      END DO
      END DO
C
C.....EXTRAPOLATE TO WEST AND EAST BOUNDARIES
C
      DO K=2,NKM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(1+IST)+J
        FI(IJK)=FI(IJK+NJ)
        IJK=LKBK(K+KST)+LIBK(NI+IST)+J
        FI(IJK)=FI(IJK-NJ)
      END DO
      END DO
#ifndef USE_2DCASE
C
C.....EXTRAPOLATE TO BOTTOM AND TOP BOUNDARIES
C
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(1+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK+NIJ)
        IJK=LKBK(NK+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK-NIJ)
      END DO
      END DO
#endif

#endif
C
      END DO
#endif
C
      CALL VecRestoreArray(FIVEC,FIARR,FIII,IERR)
C
      RETURN
      END
C 
#include "petsc.user.inc"
C
C
C###############################################################
      SUBROUTINE GRADFI(FIVEC,DFXVEC,DFYVEC,DFZVEC)
C###############################################################
C     This routine calculates the components of the gradient
C     vector of a scalar FI at the CV center, using conservative
C     scheme based on the Gauss theorem; see Sect. 8.6 for 
C     details. FIE are values at east side, FIN at north side.
C     Contributions from boundary faces are calculated in a
C     separate loops...
C
C
C===============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "gradold3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER LC,M
      INTEGER I,II,IO,ISY,IW,K,LKK,LKI,JP,JPL,J,IJK
      INTEGER LS
C
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C
      Vec    DFXVEC,DFYVEC,DFZVEC,
     *       DFXL  ,DFYL  ,DFZL,
     *       DFXOL ,DFYOL ,DFZOL,
     *       FIVEC,FIL
      PetscOffset FIII,FIIIN,DFXXI ,DFYYI ,DFZZI,
     *                       DFXOOI,DFYOOI,DFZOOI
      PetscScalar PZERO
      PetscErrorCode IERR
C
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
     *                      DFXOOI,DFYOOI,DFZOOI
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C==============================================================
C
      PZERO=0.0D0
C     CALL VecGhostUpdateBegin(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostUpdateEnd(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostGetLocalForm(FIVEC,FIL,IERR)
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C
C.....INITIALIZE OLD GRADIENT (NOTE THAT DFXO MIGHT CONTAIN VALUES FROM
C.....PREVIOUS CALL TO GRADFI
C
      CALL VecGhostGetLocalForm(DFXOVEC,DFXOL,IERR)
      CALL VecGhostGetLocalForm(DFYOVEC,DFYOL,IERR)
      CALL VecGhostGetLocalForm(DFZOVEC,DFZOL,IERR)
      CALL VecSet(DFXOL,PZERO,IERR)
      CALL VecSet(DFYOL,PZERO,IERR)
      CALL VecSet(DFZOL,PZERO,IERR)
      CALL VecGhostRestoreLocalForm(DFXOVEC,DFXOL,IERR)
      CALL VecGhostRestoreLocalForm(DFYOVEC,DFYOL,IERR)
      CALL VecGhostRestoreLocalForm(DFZOVEC,DFZOL,IERR)
C
C.....START ITERATIVE CALCULATION OF GRADIENTS
C
      DO LC=1,NIGRAD
C
C.....GET ARRAY OF VECTOR VALUES
C
        CALL VecGhostGetLocalForm(DFXVEC,DFXL,IERR)
        CALL VecGhostGetLocalForm(DFYVEC,DFYL,IERR)
        CALL VecGhostGetLocalForm(DFZVEC,DFZL,IERR)
C
        CALL VecGhostGetLocalForm(DFXOVEC,DFXOL,IERR)
        CALL VecGhostGetLocalForm(DFYOVEC,DFYOL,IERR)
        CALL VecGhostGetLocalForm(DFZOVEC,DFZOL,IERR)
C
C.......INITIALIZE NEW GRADIENT
C
        CALL VecSet(DFXL,PZERO,IERR)
        CALL VecSet(DFYL,PZERO,IERR)
        CALL VecSet(DFZL,PZERO,IERR)
C
        CALL VecGetArray(DFXL,DFXARR,DFXXI,IERR)
        CALL VecGetArray(DFYL,DFYARR,DFYYI,IERR)
        CALL VecGetArray(DFZL,DFZARR,DFZZI,IERR)
C
        CALL VecGetArray(DFXOL,DFXOARR,DFXOOI,IERR)
        CALL VecGetArray(DFYOL,DFYOARR,DFYOOI,IERR)
        CALL VecGetArray(DFZOL,DFZOARR,DFZOOI,IERR)
C
C.....OpenMP : Start parallel loop
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT)
        DO M=1,NBLKS
C
        CALL SETIND(M)
C        
C.......CONTRIBUTION FROM INNER EAST SIDES
C
        CALL GRADCOLOOP(KST,IST,NKM,NIM-1,NJM,NJ,
     *       FIARR,DFXARR,DFYARR,DFZARR,FX,XEC,YEC,ZEC,XER,YER,ZER)
C
C.......CONTRIBUTION FROM INNER NORTH SIDES
C
        CALL GRADCOLOOP(KST,IST,NKM,NIM,NJM-1,1,
     *       FIARR,DFXARR,DFYARR,DFZARR,FY,XNC,YNC,ZNC,XNR,YNR,ZNR)
C
C.......CONTRIBUTION FROM INNER TOP SIDES
C
        CALL GRADCOLOOP(KST,IST,NKM-1,NIM,NJM,NIJ,
     *       FIARR,DFXARR,DFYARR,DFZARR,FZ,XTC,YTC,ZTC,XTR,YTR,ZTR)
C
        END DO
C.....OpenMP : Here ends this parrallel do
C
C.......CONTRIBUTION FROM O- AND C-GRID CUTS
C
        DO I=1,NOCBKAL
          CALL GRADCO(FIARR,DFXARR,DFYARR,DFZARR,
     *                FOCBK(I),IJLPBK(I),IJRPBK(I),
     *                XOCC(I),YOCC(I),ZOCC(I),
     *                XOCR(I),YOCR(I),ZOCR(I))
        END DO
C
C.......CONTRIBUTION FROM FACE SEGMENT BOUNDARIES
C
        DO I=1,NFSGBKAL
          CALL GRADCO(FIARR,DFXARR,DFYARR,DFZARR,
     *                FFSGBK(I),IJFL(I),IJFR(I),
     *                XFC(I),YFC(I),ZFC(I),
     *                XFR(I),YFR(I),ZFR(I))
        END DO
C
C.......CONTRIBUTION FROM INLET BOUNDARIES
C
        DO II=1,NINLBKAL
          CALL GRADBC(IJPI(II),IJI(II),XIR(II),YIR(II),ZIR(II),
     *                DFXARR,DFYARR,DFZARR,FIARR)
        END DO
C
C.......CONTRIBUTION FROM OUTLET BOUNDARIES
C
        DO IO=1,NOUTBKAL
          CALL GRADBC(IJPO(IO),IJO(IO),XUR(IO),YOR(IO),ZOR(IO),
     *                DFXARR,DFYARR,DFZARR,FIARR)
        END DO
C
C.......CONTRIBUTION FROM SYMMETRY BOUNDARIES
C
#ifndef USE_2DCASE
        DO ISY=1,NSYMBKAL
          CALL GRADBC(IJPS(ISY),IJS(ISY),XNS(ISY),YNS(ISY),ZNS(ISY),
     *                DFXARR,DFYARR,DFZARR,FIARR)
        END DO
#endif
C
C.......CONTRIBUTION FROM WALL BOUNDARIES
C
        DO IW=1,NWALBKAL
          CALL GRADBC(IJPW(IW),IJW(IW),XNW(IW),YNW(IW),ZNW(IW),
     *                DFXARR,DFYARR,DFZARR,FIARR)
        END DO
C
C.....UPDATE GRADIENT COMPONENTS ON ALL PROCESSORS
C
        CALL VecRestoreArray(DFXL,DFXARR,DFXXI,IERR)
        CALL VecRestoreArray(DFYL,DFYARR,DFYYI,IERR)
        CALL VecRestoreArray(DFZL,DFZARR,DFZZI,IERR)
C
        CALL VecRestoreArray(DFXOL,DFXOARR,DFXOOI,IERR)
        CALL VecRestoreArray(DFYOL,DFYOARR,DFYOOI,IERR)
        CALL VecRestoreArray(DFZOL,DFZOARR,DFZOOI,IERR)
C
        CALL VecGhostRestoreLocalForm(DFXVEC,DFXL,IERR)
        CALL VecGhostRestoreLocalForm(DFYVEC,DFYL,IERR)
        CALL VecGhostRestoreLocalForm(DFZVEC,DFZL,IERR)
C
        CALL VecGhostRestoreLocalForm(DFXOVEC,DFXOL,IERR)
        CALL VecGhostRestoreLocalForm(DFYOVEC,DFYOL,IERR)
        CALL VecGhostRestoreLocalForm(DFZOVEC,DFZOL,IERR)
C
        CALL VecGhostUpdateBegin(DFXVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFXVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateBegin(DFYVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFYVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateBegin(DFZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
        CALL VecGetArray(DFXVEC,DFXARR,DFXXI,IERR)
        CALL VecGetArray(DFYVEC,DFYARR,DFYYI,IERR)
        CALL VecGetArray(DFZVEC,DFZARR,DFZZI,IERR)
C
C.......CALCULATE GRADIENT COMPONENTS AT CV-CENTERS
C
C.....OpenMP : Here starts a parallel do
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,K,I,J,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,
C$OMP*                    LKK,LKI,JP,JPL)
        DO M=1,NBLKS
C
        CALL SETIND(M)
C        
        DO K=2,NKM
        DO I=2,NIM
        DO J=2,NJM
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          DFX(IJK)=DFX(IJK)/VOL(IJK)
          DFY(IJK)=DFY(IJK)/VOL(IJK)
          DFZ(IJK)=DFZ(IJK)/VOL(IJK)
        END DO
        END DO
        END DO
C
        END DO
C.....OpenMP : Here ends this parallel loop
C
        CALL VecRestoreArray(DFXVEC,DFXARR,DFXXI,IERR)
        CALL VecRestoreArray(DFYVEC,DFYARR,DFYYI,IERR)
        CALL VecRestoreArray(DFZVEC,DFZARR,DFZZI,IERR)
C
C.......SET OLD GRADIENT = NEW GRADIENT FOR THE NEXT ITERATION
C
        IF(LC.NE.NIGRAD) THEN
          CALL VecCopy(DFXVEC,DFXOVEC,IERR)
          CALL VecCopy(DFYVEC,DFYOVEC,IERR)
          CALL VecCopy(DFZVEC,DFZOVEC,IERR)
          CALL VecGhostUpdateBegin(
     *         DFXOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFXOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateBegin(
     *         DFYOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFYOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateBegin(
     *         DFZOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFZOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
        ENDIF
C
      END DO
C
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
      CALL VecGhostRestoreLocalForm(FIVEC,FIL,IERR)
C
      CALL PetscBarrier(FIVEC,IERR)
      CALL VecGhostUpdateBegin(
     *     DFXVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFXVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DFYVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFYVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DFZVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFZVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C###############################################################
      SUBROUTINE GRADCO(FIARR,DFXARR,DFYARR,DFZARR,FAC,IJP,IJN,
     *                  XXC,YYC,ZZC,XCR,YCR,ZCR)
C###############################################################
C     This routine calculates contribution to the gradient
C     vector of a scalar FI at the CV center, arising from
C     an inner cell face (cell-face value of FI times the 
C     corresponding component of the surface vector).
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C     For inner cells we use now gradcoloop. Gradco is used
C     only for interfaces now.
C
C===============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "gradold3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI,DFXI,DFYI,DFZI,XCC,YCC,ZCC
      REAL*8 DFXE,XCR,YCR,ZCR,DFYE,DFZE,FIE,XXC,YYC,ZZC
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
C
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
     *                      DFXOOI,DFYOOI,DFZOOI
C
#include "petsc.user.inc"
C==============================================================
C
C.....COORDINATES OF POINT ON THE LINE CONNECTING CENTER AND NEIGHBOR,
C     OLD GRADIENT VECTOR COMPONENTS INTERPOLATED FOR THIS LOCATION
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
      DFXI=DFXO(IJN)*FAC+DFXO(IJP)*FACP
      DFYI=DFYO(IJN)*FAC+DFYO(IJP)*FACP
      DFZI=DFZO(IJN)*FAC+DFZO(IJP)*FACP
C
C.....VARIABLE VALUE AT THE CELL-FACE CENTER
C
      FIE=FI(IJN)*FAC+FI(IJP)*FACP+DFXI*(XXC-XI)
     *                            +DFYI*(YYC-YI)
     *                            +DFZI*(ZZC-ZI)
C
C.....GRADIENT CONTRIBUTION FROM CELL FACE
C
      DFXE=FIE*XCR
      DFYE=FIE*YCR
      DFZE=FIE*ZCR  
C
C.....ACCUMULATE CONTRIBUTION AT CELL CENTER AND NEIGHBOR
C
      DFX(IJP)=DFX(IJP)+DFXE
      DFY(IJP)=DFY(IJP)+DFYE
      DFZ(IJP)=DFZ(IJP)+DFZE
      DFX(IJN)=DFX(IJN)-DFXE
      DFY(IJN)=DFY(IJN)-DFYE
      DFZ(IJN)=DFZ(IJN)-DFZE
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C###############################################################
      SUBROUTINE GRADCOLOOP(KSTTT,ISTTT,MKM,MIM,MJM,NPN,
     *           FIARR,DFXARR,DFYARR,DFZARR,
     *                          FACV,XXC,YYC,ZZC,XCR,YCR,ZCR)
C###############################################################
C     This routine calculates contribution to the gradient
C     vector of a scalar FI at the CV center, arising from
C     inner cell faces (cell-face value of FI times the 
C     corresponding component of the surface vector).
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C     This 'gradcoloop' routine substitutes gradco for the inner
C     cells. It incorporates de I,J,K loops, rather than being
C     called once for each node, so that it reduces the overhead
C     with significant savings ( gradient computations are 40%
C     faster this way ).      
C
C===============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "gradold3d.inc"
#include "indexc3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER K,I,J,MKM,MIM,MJM,IJP,KSTTT,ISTTT,IJN,NPN
C
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
      REAL*8 XXC(NXYZA),YYC(NXYZA),ZZC(NXYZA)
      REAL*8 XCR(NXYZA),YCR(NXYZA),ZCR(NXYZA)
      REAL*8 FACV(NXYZA)
      REAL*8 FAC,FACP,XI,YI,ZI,DFXI,DFYI,DFZI,FIE
      REAL*8 DFXE,DFYE,DFZE
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
C
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
     *                      DFXOOI,DFYOOI,DFZOOI
C
#include "petsc.user.inc"
C==============================================================
C
C.....LOOP THROUGH NODES
C
      DO K=2,MKM
      DO I=2,MIM
      DO J=2,MJM
        IJP=LKBK(K+KSTTT)+LIBK(I+ISTTT)+J
        IJN=IJP+NPN
C
C.....COORDINATES OF POINT ON THE LINE CONNECTING CENTER AND NEIGHBOR,
C     OLD GRADIENT VECTOR COMPONENTS INTERPOLATED FOR THIS LOCATION
C
        FAC=FACV(IJP)
        FACP=1.0D0-FAC
        XI=XC(IJN)*FAC+XC(IJP)*FACP
        YI=YC(IJN)*FAC+YC(IJP)*FACP
        ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
        DFXI=DFXO(IJN)*FAC+DFXO(IJP)*FACP
        DFYI=DFYO(IJN)*FAC+DFYO(IJP)*FACP
        DFZI=DFZO(IJN)*FAC+DFZO(IJP)*FACP
C
C.....VARIABLE VALUE AT THE CELL-FACE CENTER
C
        FIE=FI(IJN)*FAC+FI(IJP)*FACP
     *     +DFXI*(XXC(IJP)-XI)
     *     +DFYI*(YYC(IJP)-YI)
     *     +DFZI*(ZZC(IJP)-ZI)
C
C.....GRADIENT CONTRIBUTION FROM CELL FACE
C
        DFXE=FIE*XCR(IJP)
        DFYE=FIE*YCR(IJP)
        DFZE=FIE*ZCR(IJP)
C
C.....ACCUMULATE CONTRIBUTION AT CELL CENTER AND NEIGHBOR
C
        DFX(IJP)=DFX(IJP)+DFXE
        DFY(IJP)=DFY(IJP)+DFYE
        DFZ(IJP)=DFZ(IJP)+DFZE
        DFX(IJN)=DFX(IJN)-DFXE
        DFY(IJN)=DFY(IJN)-DFYE
        DFZ(IJN)=DFZ(IJN)-DFZE
C
      END DO
      END DO
      END DO
C
      RETURN
      END
C 
#include "petsc.user.inc"
C
C
C########################################################
      SUBROUTINE GRADBC(IJP,IJB,XCR,YCR,ZCR,
     *                  DFXARR,DFYARR,DFZARR,FIARR)
C########################################################
C     This routine calculates the contribution of a 
C     boundary cell face to the gradient at CV-center.
C
C=======================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "geo3d.inc"
#include "rcont3d.inc"
C
      INTEGER IJP,IJB
C
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
      REAL*8 XCR,YCR,ZCR
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
     *                      DFXOOI,DFYOOI,DFZOOI
C
#include "petsc.user.inc"
C=======================================================
C
      DFX(IJP)=DFX(IJP)+FI(IJB)*XCR
      DFY(IJP)=DFY(IJP)+FI(IJB)*YCR
      DFZ(IJP)=DFZ(IJP)+FI(IJB)*ZCR
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C#############################################################
      SUBROUTINE CALCSC(IFI,FIVEC,FIO,FIOO)
C#############################################################
C     This routine discretizes and solves the scalar transport
C     equations (temperature, turbulent kinetic energy, diss.).
C=============================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C     
      INTEGER IFI,M,I,J,K,IJK,IJP,IJN,MIJ,II,IJB
      INTEGER IO,ISY
C
      REAL*8 GFI,URFFI,APT,CP,CB
      REAL*8 FIARR(1),FIO(NXYZA),FIOO(NXYZA)
      REAL*8  LPI
      PARAMETER (LPI=3.141592653589793238462643383279D0) 
C
      PetscScalar PONE,PZERO
      Vec SUL,APL,
     *    FIVEC,FIL,
     *    DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL
      PetscOffset FIII
      PetscErrorCode IERR
      COMMON /SCALARFI/ FIII
C
#include "petsc.user.inc"
C============================================================
      PZERO=0.0D0
      PONE=1.0D0
C
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
C.....FETCH SCALAR VALUES FROM OTHER PROCESSORS
C
      CALL VecGhostGetLocalform(FIVEC,FIL,IERR)
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C
C.....CALCULATE GRADIENTS OF FI
C
      CALL GRADFI(FIVEC,DPXVEC,DPYVEC,DPZVEC)
C
C.....GET ARRAY OF VECTOR VALUES: VELOCITY, GRADIENT COMPONENTS, DENS, VISC
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)
C
C.....INITIALIZE ARRAYS, SET BLENDING AND UNDER_RELAXATION COEFF.
C
C.....Zero the local Representation (including the ghost elements)
C
      CALL VecGhostGetLocalForm(SUVEC,SUL,IERR)
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
C
      CALL VecSet(SUL,PZERO,IERR)
      CALL VecSet(APL,PZERO,IERR)
C
      CALL VecGetArray(SUL,SUARR,SUUI,IERR)
      CALL VecGetArray(APL,APARR,APPI,IERR)
C
C 
      AE(1:NIJKBKAL)=0.0D0;  AW(1:NIJKBKAL)=0.0D0
      AN(1:NIJKBKAL)=0.0D0;  AS(1:NIJKBKAL)=0.0D0
      AT(1:NIJKBKAL)=0.0D0;  AB(1:NIJKBKAL)=0.0D0 
      AL(1:NOCBKAL) =0.0D0;  AR(1:NOCBKAL )=0.0D0
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
      GFI=GDS(IFI)
      URFFI=1.0D0/URF(IFI)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
C
CC.....OpenMP : Start parallel loop section
CC$OMP PARALLEL DO DEFAULT(SHARED), PRIVATE(M,K,I,J,
CC$OMP*  IJK,NKMT,NIMT,NJMT,NJT,NIJT,KSTT,ISTT,SB,APT)
C
      DO M=1,NBLKS
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                             XER(IJK),YER(IJK),ZER(IJK),
     *               F1(IJK),AW(IJK+NJ),AE(IJK),FX(IJK),GFI,M,FIARR)
      END DO
      END DO
      END DO
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                            XNR(IJK),YNR(IJK),ZNR(IJK),
     *               F2(IJK),AS(IJK+1),AN(IJK),FY(IJK),GFI,M,FIARR)
      END DO
      END DO
      END DO
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                              XTR(IJK),YTR(IJK),ZTR(IJK),
     *               F3(IJK),AB(IJK+NIJ),AT(IJK),FZ(IJK),GFI,M,FIARR)
      END DO
      END DO
      END DO
C
C......TODO IMPLEMENT
C......ADDITIONAL CONTRIBUTION DUE TO MANUFACTURED SOLUTION
C
#ifdef USE_ANALYTICAL
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C
        SU(IJK)=SU(IJK)+STMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
C
      END DO
      END DO
      END DO
#endif
C
      END DO
C
C.....CONTRIBUTION FROM O- AND C-GRID CUTS
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        MIJ=IBLKOCBK(I)
        CALL FLUXSC(IFI,IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                          XOCR(I),YOCR(I),ZOCR(I),
     *              FMOC(I),AL(I),AR(I),FOCBK(I),GFI,MIJ,FIARR)
        AP(IJP)=AP(IJP)-AR(I)
        AP(IJN)=AP(IJN)-AL(I)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE NO INTERNAL BOUNDARIES!)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXSC(IFI,IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                          XFR(I),YFR(I),ZFR(I),
     *              FMF(I),AFL(I),AFR(I),FFSGBK(I),GFI,MIJ,FIARR)
        AP(IJP) =AP(IJP)-AFR(I)
        AP(IJN) =AP(IJN)-AFL(I)
      END DO
C
C.....UNSTEADY TERM CONTRIBUTION
C
      IF(LTIME) THEN
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        APT=DEN(IJK)*VOL(IJK)*DTR
        SU(IJK)=SU(IJK)+APT*((1.0D0+GAMT)*FIO(IJK)-0.5D0*GAMT*FIOO(IJK))
        AP(IJK)=AP(IJK)+APT*(1.0D0+0.5D0*GAMT)
      END DO
      END DO
      END DO
      END DO
      ENDIF
C
C.....INLET BOUNDARIES
C
      DO II=1,NINLBKAL
        IJP=IJPI(II)
        IJB=IJI(II)
        DPX(IJB)=DPX(IJP)
        DPY(IJB)=DPY(IJP)
        DPZ(IJB)=DPZ(IJP)
        CALL FLUXSC(IFI,IJP,IJB,XIC(II),YIC(II),ZIC(II),
     *                          XIR(II),YIR(II),ZIR(II),
     *               FMI(II),CP,CB,ONE,ZERO,1,FIARR)
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-CB*FI(IJB)
      END DO
C
C.....OUTLET BOUNDARIES
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DPX(IJB)=DPX(IJP)
        DPY(IJB)=DPY(IJP)
        DPZ(IJB)=DPZ(IJP)
        CALL FLUXSC(IFI,IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                          XUR(IO),YOR(IO),ZOR(IO),
     *              FMO(IO),CP,CB,ONE,ZERO,1,FIARR)
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-CB*FI(IJB)
      END DO
C
C.....RESTORE GHOSTED VERSION
C
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
      CALL VecGhostRestoreLocalForm(FIVEC,FIL,IERR)
C
C.....WALL BOUNDARY CONDITIONS AND SOURCES FOR TEMPERATURE (NOT
C.....IMPLEMENTED YET
C
      IF(IFI.EQ.IEN) CALL TEMP
C
C.....WALL BOUNDARY CONDITIONS AND SOURCE FOR K-e
C
C     IF(IFI.EQ.ITE) CALL KINE
C     IF(IFI.EQ.IED) CALL DISE
C
C.....GENERIC SOURCE TERM MODIFICATIONS
C
C
C.....GET LOCAL REPRESENTATION OF VECTOR
C
      CALL VecGhostGetLocalForm(FIVEC,FIL,IERR)
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C     CALL SOURCESC(IFI,FI)
C
C.....RESTORE ARRAYS THAT ARE NOT NEEDED ANYMORE
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecRestoreArray(SUL,SUARR,SUUI,IERR)
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
C
      CALL VecGhostRestoreLocalForm(SUVEC,SUL,IERR)
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
C
C.....UPDATE SOURCE AND DIAGONAL VECTOR ON ALL PROCESSORS
C
      CALL VecGhostUpdateBegin(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....GET LOCAL PART OF AP,SU,APR
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
C
C.....FINAL COEFFICIENT AND SOURCE MATRIX FOR FI-EQUATION
C
C.....OpenMP : Here starts parallel loop section
CC$OMP PARALLEL DO DEFAULT(SHARED), PRIVATE(M,
CC$OMP*  K,I,J,IJK,NKMT,NIMT,NJMT)
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
C
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        AP(IJK)=(AP(IJK)-AE(IJK)-AW(IJK)
     *                  -AN(IJK)-AS(IJK)
     *                  -AB(IJK)-AT(IJK))*URFFI
        SU(IJK)=SU(IJK)+(1.0D0-URF(IFI))*AP(IJK)*FI(IJK)
C
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
      CALL VecGhostRestoreLocalForm(FIVEC,FIL,IERR)
C
#ifdef USE_SIPSOL
C
C.....SOLVING EQUATION SYSTEM FOR SCALAR FI USING SIP/CGSTAB SOLVER
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
      CALL VecGetArray(FIVEC,FIARR,FIII,IERR)
      CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL PetscLogStagePush(STAGE3,IERR)
      CALL SOLVER(FIARR,FIII,IEN)
      CALL PetscLogStagePop(IERR)
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(FIVEC,FIARR,FIII,IERR)
      CALL VecRestoreArray(SUVEC,SUARR,SUUI,IERR)
#else
C
C.....SOLVING EQUATION SYSTEM FOR SCALAR FI USING PETSC SOLVER
C
      CALL ASSEMBLESYS(AMAT)
      CALL MatZeroRowsColumns(AMAT,NZERO,ZEROS,PONE,FIVEC,SUVEC,IERR)
C
      CALL PetscLogStagePush(STAGE3,IERR)
      CALL SOLVESYS(IEN,FIVEC,AMAT,SUVEC)
      CALL PetscLogStagePop(IERR)
      CALL VecGhostUpdateBegin(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
#endif
C
C.....SYMMETRY AND OUTLET BOUNDARIES
C
      CALL VecGetArray(FIVEC,FIARR,FIII,IERR)
C
#ifndef USE_2DCASE
      DO ISY=1,NSYMBKAL
        FI(IJS(ISY))=FI(IJPS(ISY))
      END DO
#endif
C
      DO IO=1,NOUTBKAL
        FI(IJO(IO))=FI(IJPO(IO))
      END DO
C
      CALL VecRestoreArray(FIVEC,FIARR,FIII,IERR)
C
C     CALL BCIN
C
C.....UPDATE DENSITY AND VISCOSITY
C
      IF (IFI.EQ.IEN) THEN
      CALL VecGhostUpdateBegin(
     *     DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      ENDIF
C
C
      RETURN
      END 
C
#include "petsc.user.inc"
C
C
C################################################################
      SUBROUTINE FLUXSC(IFI,IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                  FM,CAP,CAN,FAC,G,MB,FIARR)
C################################################################
C     This routine calculates scalar fluxes (convective and
C     diffusive) through the cell face between nodes IJP and IJN.
C     It is analogous to the routine FLUXUV, see above. 
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C================================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER IJN,IJP,MB,IFI
C
      REAL*8 FIARR( 1)
      REAL*8 FM,FACP,FAC,ZZC,XXC,YYC,XCR,YCR,ZCR,FMI
      REAL*8 FMX,FII,DENI,VISI,DFXI,DFYI,DFZI,XPN,YPN,ZPN
      REAL*8 VSOL,FCFIE,FCFII,FDFII,CAN,CAP,FFIC,G,FDFIE
C
      PetscOffset FIII
      COMMON /SCALARFI/ FIII
C
#include "petsc.user.inc"
C
C==============================================================
C
C.....INTERPOLATE ALONG LINE P-N
C
      FACP=1.0D0-FAC
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....COMPUTE FM 
C
      FMI=MIN(FM,0.0D0)
      FMX=MAX(FM,0.0D0)
C
C.....INTERPOLATE ALONG LINE P-N
C
      FII=FI(IJN)*FAC+FI(IJP)*FACP
      VISI=VIS(IJN)*FAC+VIS(IJP)*FACP-VISC
      DFXI=DPX(IJN)*FAC+DPX(IJP)*FACP
      DFYI=DPY(IJN)*FAC+DPY(IJP)*FACP
      DFZI=DPZ(IJN)*FAC+DPZ(IJP)*FACP
C
C.....DIFFUSION COEFFICIENT
C
C#ifdef USE_ANALYTICAL
C      IF(IFI.EQ.IEN) DCOEF=1.0D0
C#else
      IF(IFI.EQ.IEN) DCOEF=VISC/PRANL
C#endif
C
C     IF(IFI.EQ.ITE) DCOEF=VISC+VISI/SIGTE
C     IF(IFI.EQ.IED) DCOEF=VISC+VISI/SIGED
C
C.....DISTANCE VECTOR COMPONENTS, DIFFUSION COEFFICIENT
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      VSOL=DCOEF*DSQRT((XCR**2+YCR**2+ZCR**2)/
     *                 (XPN**2+YPN**2+ZPN**2))
C
C.....EXPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCFIE=FM*FII
      FDFIE=DCOEF*(DFXI*XCR+DFYI*YCR+DFZI*ZCR)
C
C.....IMPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCFII=FMI*FI(IJN)+FMX*FI(IJP)
      FDFII=VSOL*(DFXI*XPN+DFYI*YPN+DFZI*ZPN)
C
C.....COEFFICIENTS, DEFERRED CORRECTION, SOURCE TERMS
C
      CAN=-VSOL+FMI
      CAP=-VSOL-FMX
      FFIC=G*(FCFIE-FCFII)
      SU(IJP)=SU(IJP)-FFIC+FDFIE-FDFII
      SU(IJN)=SU(IJN)+FFIC-FDFIE+FDFII
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C############################################################### 
      SUBROUTINE TEMP
C###############################################################
C     This routine assembles the source terms (volume integrals)
C     and applies wall boundary conditions for the temperature
C     (energy) equation.
C===============================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      INTEGER M,IW,IJP,IJB
C
      REAL*8 COEF
C
      PetscErrorCode IERR
C
#include "petsc.user.inc"
C==============================================================      
C
C
C.....GET ARRAY OF TEMPERATURE
C
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C.....NO VOLUMETRIC SOURCES OF THERMAL ENERGY 
C
C.....ISOTHERMAL WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWST+1,IWST+NWALI
        IJP=IJPW(IW)
        IJB=IJW(IW)
        COEF=DCOEF*SRDW(IW)
        AP(IJP)=AP(IJP)+COEF
        SU(IJP)=SU(IJP)+COEF*T(IJB)
      END DO
      END DO
C
C.....ADIABATIC WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWAT+1,IWAT+NWALA
        T(IJW(IW))=T(IJPW(IW))
      END DO
      END DO
C
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
      RETURN
      END
C
#include "petsc.user.inc"
C
C########################################################
      SUBROUTINE SETIND(M)
C########################################################
C     This routine sets the indices for the current grid
C     block.
C========================================================     
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "indexc3d.inc"
C
      INTEGER M
C========================================================
C
      NI=NIBK(M)
      NJ=NJBK(M)
      NK=NKBK(M)
      IST=IBK(M)
      JST=JBK(M)
      KST=KBK(M)
      IJKST=IJKBK(M)
      NIJK=NIJKBK(M)
C
      NINL=NINLBK(M)
      NOUT=NOUTBK(M)
      NSYM=NSYMBK(M)
      NWAL=NWALBK(M)
      NWALA=NWALABK(M)
      NWALI=NWALIBK(M)
C
      NMTM=NMTMBK(M)
      NFSG=NFSGBK(M)
C
      IIST=IIBK(M)
      IOST=IOBK(M)
      ISST=ISBK(M)
      IWST=IWBK(M)
      IWAT=IWABK(M)
C
      IMST=IMBK(M)
      IFST=IFBK(M)
C
      NIM=NI-1
      NJM=NJ-1
      NKM=NK-1
      NIJ=NI*NJ
C
      RETURN
      END
C
C
C########################################################
      SUBROUTINE INIT
C########################################################
C     This routine reads input parameters, grid data etc.
C
C     Extended to 3D and block-structured grids 
C========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,M,K,J,IJK,NJBKAL,NIABK,NOABK,NSABK,NWABK
      PetscErrorCode IERR
      CHARACTER(LEN=100) STAGENAME
C
#include "petsc.user.inc"
C========================================================
C
C.....READ INPUT DATA IN THE FOLLOWING ORDER OF RECORDS:
C
C   1.  TITLE FOR THE PROBLEM SOLVED;
C   2.  LOGICAL CONTROL PARAMETERS;
C   3.  INDICES OF MONITORING LOCATION AND PRESSURE REFERENCE POINT,
C       NUMBER OF PRESSURE CORRECTIONS AND ITERATIONS ON GRADIENT;
C   4.  CONVERGENCE AND DIVERGENCE CRITERION, SIP-PARAMETER;
C   5.  DENSITY, DYNAMIC VISCOSITY AND PRANDTL NUMBER;
C   6.  GRAVITY COMP., EXPANSION COEF., HOT, COLD AND REFERENCE TEMP.;
C   7.  FIELD INITIALIZATION (UIN,VIN,WIN,PIN,TIN,TEIN,EDIN);
C   8.  LID VELOCITY AND PHYSICAL PARAMETERS;
C   9.  NO. OF TIME STEPS, OUTPUT CONTROL, TIME STEP, BLENDING FACTOR;
C  10.  LOGICAL CONTROL VARIABLES (EQ. TO BE SOLVED: U,V,W,PP,T,SMG,TE,ED,VIS);
C  11.  UNDER-RELAXATION FACTORS;
C  12.  CONVERGENCE CRITERION FOR INNER ITERATIONS;
C  13.  MAXIMUM ALLOWED NUMBER OF INNER ITERATIONS;
C  14.  BLENDING FACTOR FOR CONVECTIVE FLUXES;
C  15.  NUMBER OF OUTER ITERATIONS PER TIME STEP
C  16.  NUMBER OF GRID BLOCKS
C
#ifdef USE_INFO
      WRITE(*,*) RANK, "READING CONTROL SETTINGS"
#endif
      READ(5,'(A50)') TITLE
      READ(5,*) LREAD,LWRITE,LPOST,LTEST,LOUTS,LOUTE,LTIME,LGRAD
      READ(5,*) IMON,JMON,KMON,MMON,RMON,IPR,JPR,KPR,MPR,NPCOR,NIGRAD 
      READ(5,*) SORMAX,SLARGE,ALFA
      READ(5,*) DENS,VISC,PRANL
      READ(5,*) GRAVX,GRAVY,GRAVZ,BETA,TH,TC,TREF
      READ(5,*) UIN,VIN,WIN,PIN,TIN,TEIN,EDIN
      READ(5,*) ULID,TPER,TGEN
      READ(5,*) ITSTEP,NOTT,DT,GAMT
      READ(5,*) (LCAL(I),I=1,NFI)
      READ(5,*) (URF(I),I=1,NFI)
      READ(5,*) (SOR(I),I=1,NFI)
      READ(5,*) (NSW(I),I=1,NFI)
      READ(5,*) (GDS(I),I=1,NFI)
      READ(5,*) LSG
C
C.....WRITE INFORMATION TO STDOUT
C
      IF (RANK.EQ.0)THEN
        WRITE(*,*) '***************************************************'
        WRITE(*,*) 'CONTROL SETTINGS'
        WRITE(*,*) '***************************************************'
        WRITE(*,*) 'LREAD,LWRITE,LPOST,LTEST,LOUTS,LOUTE,LTIME,LGRAD'
        WRITE(*,*) LREAD,LWRITE,LPOST,LTEST,LOUTS,LOUTE,LTIME,LGRAD
        WRITE(*,*)
     *  " IMON, JMON, KMON, MMON, RMON,
     *  IPR,  JPR,  KPR,  MPR,NPCOR,NIGRAD "
       WRITE(*,'(11I6)') 
     *   IMON,JMON,KMON,MMON,RMON,IPR,JPR,KPR,MPR,NPCOR,NIGRAD 
        WRITE(*,*) " SORMAX,     SLARGE,     ALFA"
        WRITE(*,"(3E12.4)") SORMAX,SLARGE,ALFA
C       WRITE(*,*) "ITSTEP,NOTT,DT,GAMT"
C       WRITE(*,*) ITSTEP,NOTT,DT,GAMT
C       WRITE(*,*) "(LCAL(I),I=1,NFI)"
C       WRITE(*,*) (LCAL(I),I=1,NFI)
        WRITE(*,*) "(URF(I),I=1,5)"
        WRITE(*,"(5E12.4)") (URF(I),I=1,5)
        WRITE(*,*) "(SOR(I),I=1,5)"
        WRITE(*,"(5E12.4)") (SOR(I),I=1,5)
#ifdef USE_SIPSOL
        WRITE(*,*) "(NSW(I),I=1,5) - MAX INNER ITERATIONS SIP"
        WRITE(*,"(5I6)") (NSW(I),I=1,5)
#endif
        WRITE(*,*) "(GDS(I),I=1,5) - BLENDING (CDS-UDS)"
        WRITE(*,"(5E12.4)") (GDS(I),I=1,5)
        WRITE(*,*) "LSG"
        WRITE(*,"(I6)") LSG
#if defined( USE_PWIM )
        WRITE(*,*) "USING PWIM INSTEAD OF RHIE-CHOW INTERPOLATION"
#endif
#if defined( USE_REUSEPC )
        WRITE(*,*) "REUSING PRECONDITIONERS FOR MOMENTUM BALANCES"
#endif
#if defined( USE_FULLNAVIERSTOKES )
        WRITE(*,*) "USING FULL DIFFUSIVE PART OF NAVIER-STOKES"
#endif
      END IF
C
C.....READ BLOCK AND GRID DATA, GEOMETRY 
C
#ifdef USE_INFO
      WRITE(*,*) RANK,"READING GRID"
#endif
      CALL READGRIDS
C
C.....OFFSET B.C. INDEXES
C
      CALL OFFSETBC
C
C.....CHECK IF MONITORING POINT IS OK.
C
      IF(IMON.GT.NIBK(MMON)-1) IMON=NIBK(MMON)/2
      IF(JMON.GT.NJBK(MMON)-1) JMON=NJBK(MMON)/2
      IF(KMON.GT.NKBK(MMON)-1) KMON=NKBK(MMON)/2
C
C.....SET MONITORING LOCATION 
C
      IJKMON=LKBK(KBK(MMON)+KMON)+LIBK(IBK(MMON)+IMON)+JMON
C
C.....SET PRESSURE REFERENCE LOCATION
C
      IJKPR=LKBK(KBK(MPR)+KPR)+LIBK(IBK(MPR)+IPR)+JPR
C
C.....RECIPROCAL VALUES OF URF & TIME STEP
C
      URFU=1.0D0/(URF(IU)+SMALL)
      URFV=1.0D0/(URF(IV)+SMALL)
      URFW=1.0D0/(URF(IWW)+SMALL)
      DTR=1.0D0/DT
C
C.....INITIALIZE VISCOSITY AND DENSITY AT ALL NODES
C
      CALL VecSet(VISVEC,VISC,IERR)
      CALL VecSet(DENVEC,DENS,IERR)
#ifndef USE_ANALYTICAL
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
C.....INITIALIZE VARIABLES AT INNER NODES OF ALL GRID BLOCKS
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        U(IJK) =UIN
        V(IJK) =VIN
        W(IJK) =WIN
        P(IJK) =PIN
        T(IJK) =TIN
        UO(IJK)=UIN
        VO(IJK)=VIN
        WO(IJK)=WIN
        TO(IJK)=TIN
        TE(IJK)=TEIN
        ED(IJK)=EDIN
        TEO(IJK)=TEIN
        EDO(IJK)=EDIN
      END DO
      END DO
      END DO
      END DO
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
#endif
C
C.....Initialize Geometry. 
C
      CALL INITGEO
C
C.....Call to Special I.C. Routine           
C
      CALL INITCOND
C
C......Petsc Logging Stages
C
      WRITE (STAGENAME,"(A8)") "MOMENTUM"
      CALL PetscLogStageRegister(STAGENAME,STAGE1,IERR)
      WRITE (STAGENAME,"(A8)") "PRESCORR"
      CALL PetscLogStageRegister(STAGENAME,STAGE2,IERR)
      WRITE (STAGENAME,"(A6)") "ENERGY"
      CALL PetscLogStageRegister(STAGENAME,STAGE3,IERR)
      RETURN
      END
C 
#include "petsc.user.inc"
C
C
C###########################################################
      SUBROUTINE READGRIDS
C###########################################################
C     This routine reads the block and grid files where
C     information about topology, geometry and boundary
C     is stored.
C
C     First the 'block' file ('name'.bck) file is read
C     and then each grid block is read through a loop.
C
C
C===========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "charac3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER I,M,K,XECC(NXYZA)
      INTEGER NJBKAL,NIABK,NOABK,NSABK,NWABK,NOCABK
      INTEGER NMABK,NFABK
      INTEGER NWAISBK,NWAADBK,NINX,NOUX,NSYX,NWAX,NOCX
      INTEGER NMTX
      PetscErrorCode IERR
C 
#include "petsc.user.inc"
C===========================================================
C
C.....READ BLOCK FILE (UNIT=4)
C
      READ(4)  NBLKS,NIBKAL,NJBKAL,NKBKAL,NIJKBKAL,
     *         NINLBKAL,NOUTBKAL,NSYMBKAL,NWALBKAL,
     *         NOCBKAL,NIABK,NOABK,NSABK,NWABK,NOCABK,
     *         NFSGBKAL,NFABK
C
      READ(4) (LIBK(I) ,I=1,NIBKAL),( LKBK(I),I=1,NKBKAL),
     *        (NIBK(I) ,I=1,NBLKS) ,( NJBK(I),I=1,NBLKS),
     *        (NKBK(I) ,I=1,NBLKS) ,(  IBK(I),I=1,NBLKS),
     *        ( JBK(I) ,I=1,NBLKS) ,(  KBK(I),I=1,NBLKS),
     *        (IJKBK(I),I=1,NBLKS),(NIJKBK(I),I=1,NBLKS),
     *        IJKPRC
C
      READ(4) (NINLBK(I),I=1,NBLKS),(IIBK(I),I=1,NBLKS),
     *        (NOUTBK(I),I=1,NBLKS),(IOBK(I),I=1,NBLKS),
     *        (NWALBK(I),I=1,NBLKS),(IWBK(I),I=1,NBLKS),
     *        (NSYMBK(I),I=1,NBLKS),(ISBK(I),I=1,NBLKS),
     *        (NWALABK(I),I=1,NBLKS),(IWABK(I),I=1,NBLKS),
     *        (NWALIBK(I),I=1,NBLKS),
     *        (NMTMBK(I),I=1,NBLKS),(IMBK(I),I=1,NBLKS),
     *        (NFSGBK(I),I=1,NBLKS),(IFBK(I),I=1,NBLKS)
C
      READ(4) (IJLPBK(I) ,I=1,NOCABK),(IJLBBK(I) ,I=1,NOCABK),
     *        (IJRPBK(I) ,I=1,NOCABK),(IJRBBK(I) ,I=1,NOCABK),
     *        (IJOC1BK(I),I=1,NOCABK),(IJOC2BK(I),I=1,NOCABK),
     *        (IJOC3BK(I),I=1,NOCABK),(IJOC4BK(I),I=1,NOCABK),
     *        (FOCBK(I)  ,I=1,NOCABK),(ITAGOCBK(I),I=1,NOCABK),
     *        (IBLKOCBK(I),I=1,NOCABK)
      READ(4) (IJFL(I),I=1,NFABK),(IJFR(I),I=1,NFABK),
     *        (FFSGBK(I),I=1,NFABK),
     *        (XFR(I),I=1,NFABK),(YFR(I),I=1,NFABK),
     *        (ZFR(I),I=1,NFABK),
     *        (XFC(I),I=1,NFABK),(YFC(I),I=1,NFABK),
     *        (ZFC(I),I=1,NFABK)
C
C.....CREATE VECTORS, GHOSTING ETC.
C
      CALL DISTRIBUTELOAD
C
C.....PREPARE VOLUME VECTOR
C
      CALL VecGetArray(VOLVEC,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecGetArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCVEC,ZCARR,ZCCI,IERR)
C
C.....LOOP THROUGH BLOCKS READING EACH GRID BLOCK FILE INTO THE ARRAYS
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....READ FILE NAME FOR THIS GRID BLOCK, OPEN FILE
C
      READ(9,'(A13)') FILGRD
      OPEN (UNIT=8,FILE=FILGRD,FORM='UNFORMATTED',POSITION='REWIND')
      READ(8) NI,NJ,NK,NIJK,NIABK,NOABK,NSABK,NWABK,NOCABK,
     *        NMABK,
     *        NWAISBK,NWAADBK,NINX,NOUX,NSYX,NWAX,NOCX,
     *        NMTX
C
C.....ARRAY XEC IS USED HERE FOR 'dummy' READING
C
      READ(8) (XECC(I),I=1,NI),(XECC(K),K=1,NK)
C
      READ(8) (IJI(I),I=IIST+1,IIST+NINX),(IJPI(I),I=IIST+1,IIST+NINX),
     *   (IJI1(I) ,I=IIST+1,IIST+NINX),(IJI2(I),I=IIST+1,IIST+NINX),
     *   (IJI3(I) ,I=IIST+1,IIST+NINX),(IJI4(I),I=IIST+1,IIST+NINX),
     *   (ITAGI(I),I=IIST+1,IIST+NINX)
C
      READ(8) (IJO(I),I=IOST+1,IOST+NOUX),(IJPO(I),I=IOST+1,IOST+NOUX),
     *   (IJO1(I) ,I=IOST+1,IOST+NOUX),(IJO2(I),I=IOST+1,IOST+NOUX),
     *   (IJO3(I) ,I=IOST+1,IOST+NOUX),(IJO4(I),I=IOST+1,IOST+NOUX),
     *   (ITAGO(I),I=IOST+1,IOST+NOUX)

      READ(8) (IJW(I),I=IWST+1,IWST+NWAX),(IJPW(I),I=IWST+1,IWST+NWAX),
     *   (IJW1(I) ,I=IWST+1,IWST+NWAX),(IJW2(I),I=IWST+1,IWST+NWAX),
     *   (IJW3(I) ,I=IWST+1,IWST+NWAX),(IJW4(I),I=IWST+1,IWST+NWAX),
     *   (ITAGW(I),I=IWST+1,IWST+NWAX)
C
      READ(8) (IJS(I),I=ISST+1,ISST+NSYX),(IJPS(I),I=ISST+1,ISST+NSYX),
     *   (IJS1(I) ,I=ISST+1,ISST+NSYX),(IJS2(I),I=ISST+1,ISST+NSYX),
     *   (IJS3(I) ,I=ISST+1,ISST+NSYX),(IJS4(I),I=ISST+1,ISST+NSYX),
     *   (ITAGS(I),I=ISST+1,ISST+NSYX)
C
C.....ARRAY XECC IS USED HERE FOR 'dummy' READING
C
      READ(8) (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX)
C   
C.....ARRAY XECC IS USED HERE FOR 'dummy' READING
C
      READ(8)
     *        (IJML(I),I=IMST+1,IMST+NMTX),(IJMR(I),I=IMST+1,IMST+NMTX),
     *        (XECC(I),I=1,NMTX),(XECC(I),I=1,NMTX),
     *        (XECC(I),I=1,NMTX),(XECC(I),I=1,NMTX),
     *        (XECC(I),I=1,NMTX)
C
      READ(8) (X(I),I=IJKST+1,IJKST+NIJK),(Y(I),I=IJKST+1,IJKST+NIJK),
     *   (Z(I) ,I=IJKST+1,IJKST+NIJK), (XC(I),I=IJKST+1,IJKST+NIJK),
     *   (YC(I),I=IJKST+1,IJKST+NIJK), (ZC(I),I=IJKST+1,IJKST+NIJK),
     *   (FX(I),I=IJKST+1,IJKST+NIJK), (FY(I),I=IJKST+1,IJKST+NIJK),
     *   (FZ(I),I=IJKST+1,IJKST+NIJK),(VOL(I),I=IJKST+1,IJKST+NIJK),
     *   (SRDW(I),I=IWST+1,IWST+NWAX),(XNW(I),I=IWST+1,IWST+NWAX),
     *   (YNW(I) ,I=IWST+1,IWST+NWAX),(ZNW(I),I=IWST+1,IWST+NWAX),
     *   (SRDS(I),I=ISST+1,ISST+NSYX),(XNS(I),I=ISST+1,ISST+NSYX),
     *   (YNS(I),I=ISST+1,ISST+NSYX),(ZNS(I),I=ISST+1,ISST+NSYX),
C
C.....ARRAY XEC IS USED HERE FOR 'dummy' READING
C
     *   (XEC(I),I=1,NOCX)
C
      CLOSE(UNIT=8)
C
      END DO
C
C.....RESTORE VOLUME ARRAY AND EXCHANGE VALUES
C
      CALL VecRestoreArray(VOLVEC,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCVEC,ZCARR,ZCCI,IERR)
C
      CALL VecGhostUpdateBegin(
     *     VOLVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VOLVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(XCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(XCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(YCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(YCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(ZCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(ZCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C########################################################
      SUBROUTINE SETDAT
C########################################################
C     In this routine some constants are assigned values.
C
C
C========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "indexc3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
C========================================================
C
      IU=1
      IV=2
      IWW=3
      IP=4
      IEN=5
C
      SMALL=1.E-20
      GREAT=1.E+20
      ONE=1.0
      ZERO=0.
      PII=3.14159265358979
C
C
      RETURN
      END
C
C
C###################################################################
       SUBROUTINE SRES(RNK)
C###################################################################
C     This routine writes out the results onto a file
C     so that re-start is possible at a later stage.
C===================================================================
      implicit none
C
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "charac3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
C
      INTEGER IJK,I
      INTEGER RNK
      PetscErrorCode IERR
C
#include "petsc.user.inc"
C
C==================================================================
C
C......SYNCHRONIZE PROCESSES
      CALL PetscBarrier(UVEC,IERR)
#ifdef USE_INFO
      WRITE(*,"(I0,A)") RNK, " WRITING INTERMEDIATE RESULTS INTO FILE"
#endif
      WRITE(FILRES,'(A7,I4.4,4H.res)') NAME, RNK
      OPEN (UNIT=3,FILE=FILRES,FORM='UNFORMATTED',POSITION='REWIND')
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
      WRITE(3)ITIM,TIME,(F1(IJK),IJK=1,NIJKBKAL),
     *        (F2(IJK),IJK=1,NIJKBKAL),(F3(IJK),IJK=1,NIJKBKAL),
     *        (U(IJK), IJK=1,NIJKBKAL),(V(IJK), IJK=1,NIJKBKAL),
     *        (W(IJK), IJK=1,NIJKBKAL),(P(IJK), IJK=1,NIJKBKAL),
     *        (T(IJK), IJK=1,NIJKBKAL),(TE(IJK), IJK=1,NIJKBKAL),
     *        (ED(IJK), IJK=1,NIJKBKAL),(FMOC(I),I=1,NOCBKAL),
     *        (FMF(I),I=1,NFSGBKAL),(RESINI(I),I=1,4),
     *        (RESOR(I),I=1,4)
C
      IF(LTIME) WRITE(3) (UO(IJK),IJK=1,NIJKBKAL),
     *        (VO (IJK),IJK=1,NIJKBKAL),(WO (IJK),IJK=1,NIJKBKAL),
     *        (TO (IJK),IJK=1,NIJKBKAL),(TEO(IJK), IJK=1,NIJKBKAL),
     *        (EDO(IJK),IJK=1,NIJKBKAL)
C
      CLOSE(UNIT=3)
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C########################################################
      SUBROUTINE INITGEO
C########################################################
C     This routine initializes geometry arrays
C========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
C
      INTEGER M,K,I,J,IJK,II,IO,IW,IJB,IJP
      REAL*8 ARE
C
#include "petsc.user.inc"
C========================================================
C
C.....Loop through nodes. East faces
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=1,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK,IJK-1,IJK-1-NIJ,IJK-NIJ,
     *                XEC(IJK),YEC(IJK),ZEC(IJK),
     *                XER(IJK),YER(IJK),ZER(IJK))
      END DO
      END DO
      END DO
      END DO
C
C.....Loop through nodes. North faces
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=1,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK-NJ,IJK,IJK-NIJ,IJK-NIJ-NJ,
     *                XNC(IJK),YNC(IJK),ZNC(IJK),
     *                XNR(IJK),YNR(IJK),ZNR(IJK))
C
      END DO
      END DO
      END DO
      END DO
C
C.....Loop through nodes. Top faces
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=1,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK,IJK-NJ,IJK-NJ-1,IJK-1,
     *                XTC(IJK),YTC(IJK),ZTC(IJK),
     *                XTR(IJK),YTR(IJK),ZTR(IJK))
C
      END DO
      END DO
      END DO
      END DO
C
C.....Loop through Inlet faces
C
      DO II=1,NINLBKAL
        CALL CALCFACE(IJI1(II),IJI2(II),IJI3(II),IJI4(II),
     *                XIC(II),YIC(II),ZIC(II),
     *                XIR(II),YIR(II),ZIR(II))
      END DO
C
C.....Loop through Outlet faces
C
      DO IO=1,NOUTBKAL
        CALL CALCFACE(IJO1(IO),IJO2(IO),IJO3(IO),IJO4(IO),
     *                XOC(IO),YOC(IO),ZOC(IO),
     *                XUR(IO),YOR(IO),ZOR(IO))
      END DO
C
C.....Loop through OC faces
C
      DO I=1,NOCBKAL
        CALL CALCFACE(IJOC1BK(I),IJOC2BK(I),IJOC3BK(I),IJOC4BK(I),
     *                XOCC(I),YOCC(I),ZOCC(I),
     *                XOCR(I),YOCR(I),ZOCR(I))
      END DO
C
C.....NORMAL DISTANBE FROM CELL FACE CENTER TO CELL CENTER
C
      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        IJP=IJPW(IW)
        ARE=DSQRT(XNW(IW)**2+YNW(IW)**2+ZNW(IW)**2)
        DN(IW)=((XC(IJB)-XC(IJP))*XNW(IW)+
     *          (YC(IJB)-XC(IJP))*YNW(IW)+
     *          (ZC(IJB)-ZC(IJP))*ZNW(IW))/(ARE+SMALL)
C
      END DO
C
      RETURN
C
      END
C
#include "petsc.user.inc"
C
C
C########################################################
      SUBROUTINE CALCFACE(IJK1,IJK2,IJK3,IJK4,
     *                    XXXC,YYYC,ZZZC,XXCR,YYCR,ZZCR)
C########################################################
C     This routine calculates the surface vector and
C     centre of cell IJK.
C
C     IJK1,IJK2,IJK3,IJK4 define a face by its four corners
C     in clockwise order ( so that the normal vector points out)
C     XXXC,YYYC,ZZZC are the coord of baricenter of the face
C     XXCR,YYCR,ZZCR are the components of the (normal) surface
C     vector (pointing out)
C
C     See 8.6.4 for details
C
C========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "geo3d.inc"
#include "rcont3d.inc"
C
      INTEGER IJK1,IJK2,IJK3,IJK4
C
      REAL*8 DX12,DY12,DZ12,DX13,DY13,DZ13,DX14,DY14,DZ14
      REAL*8 XR23,YR23,ZR23,XR34,YR34,ZR34,XXCR,YYCR,ZZCR
      REAL*8 S23,S34,XXXC,YYYC,ZZZC
C========================================================
C
C.....Vectors to vertices ( from IJK1 to IJK2, IJK3 and IJK4 )
      DX12=X(IJK2)-X(IJK1)
      DY12=Y(IJK2)-Y(IJK1)
      DZ12=Z(IJK2)-Z(IJK1)
C
      DX13=X(IJK3)-X(IJK1)
      DY13=Y(IJK3)-Y(IJK1)
      DZ13=Z(IJK3)-Z(IJK1)
C
      DX14=X(IJK4)-X(IJK1)
      DY14=Y(IJK4)-Y(IJK1)
      DZ14=Z(IJK4)-Z(IJK1)
C
C.....Cross Products for triangle surface vectors 
C.....This is (IJK1,IJK2,IJK3)
      XR23=DY12*DZ13-DZ12*DY13
      YR23=DZ12*DX13-DX12*DZ13
      ZR23=DX12*DY13-DY12*DX13
C
C.....This is (IJK1,IJK3,IJK4)
      XR34=DY13*DZ14-DZ13*DY14
      YR34=DZ13*DX14-DX13*DZ14
      ZR34=DX13*DY14-DY13*DX14
C
C.....Face surface vectors (add both triangles)
      XXCR=0.5D0*(XR23+XR34)
      YYCR=0.5D0*(YR23+YR34)
      ZZCR=0.5D0*(ZR23+ZR34)
C
C.....Baricenters of each triangle
      DX12=(X(IJK1)+X(IJK2)+X(IJK3))/3.0D0
      DY12=(Y(IJK1)+Y(IJK2)+Y(IJK3))/3.0D0
      DZ12=(Z(IJK1)+Z(IJK2)+Z(IJK3))/3.0D0
C
      DX14=(X(IJK1)+X(IJK3)+X(IJK4))/3.0D0
      DY14=(Y(IJK1)+Y(IJK3)+Y(IJK4))/3.0D0
      DZ14=(Z(IJK1)+Z(IJK3)+Z(IJK4))/3.0D0
C
C.....Area of each triangle
      S23=SQRT(XR23**2+YR23**2+ZR23**2)
      S34=SQRT(XR34**2+YR34**2+ZR34**2)
C
C.....Baricenter of face (weighted average)
      XXXC=(DX12*S23+DX14*S34)/(S23+S34+SMALL)
      YYYC=(DY12*S23+DY14*S34)/(S23+S34+SMALL)
      ZZZC=(DZ12*S23+DZ14*S34)/(S23+S34+SMALL)
C
      RETURN
      END
C
C
C########################################################
      SUBROUTINE OFFSETBC
C########################################################
C     This routine modifies the index arrays of B.C.s
C     so that they include the inter-block offsets
C
C========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "bound3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
C
      INTEGER M,II,IO,ISY,IW,
     *        IM
C========================================================
C
C.....INLET BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO II=IIST+1,IIST+NINL
        IJI(II) =IJI(II) +IJKST
        IJPI(II)=IJPI(II)+IJKST
        IJI1(II)=IJI1(II)+IJKST      
        IJI2(II)=IJI2(II)+IJKST      
        IJI3(II)=IJI3(II)+IJKST      
        IJI4(II)=IJI4(II)+IJKST      
      END DO
      END DO
C
C.....OUTLET BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IO=IOST+1,IOST+NOUT
        IJO(IO) =IJO(IO) +IJKST
        IJPO(IO)=IJPO(IO)+IJKST
        IJO1(IO)=IJO1(IO)+IJKST      
        IJO2(IO)=IJO2(IO)+IJKST      
        IJO3(IO)=IJO3(IO)+IJKST      
        IJO4(IO)=IJO4(IO)+IJKST      
      END DO
      END DO
C
C.....SYMETRY BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO ISY=ISST+1,ISST+NSYM
        IJS(ISY) =IJS(ISY) +IJKST
        IJPS(ISY)=IJPS(ISY)+IJKST
        IJS1(ISY)=IJS1(ISY)+IJKST      
        IJS2(ISY)=IJS2(ISY)+IJKST      
        IJS3(ISY)=IJS3(ISY)+IJKST      
        IJS4(ISY)=IJS4(ISY)+IJKST      
      END DO
      END DO
C
C.....WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWST+1,IWST+NWAL
        IJW(IW) =IJW(IW) +IJKST
        IJPW(IW)=IJPW(IW)+IJKST
        IJW1(IW)=IJW1(IW)+IJKST      
        IJW2(IW)=IJW2(IW)+IJKST      
        IJW3(IW)=IJW3(IW)+IJKST      
        IJW4(IW)=IJW4(IW)+IJKST      
      END DO
      END DO
C
C.....MTM BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IM=IMST+1,IMST+NMTM
        IJML(IM)=IJML(IM)+IJKST
        IJMR(IM)=IJMR(IM)+IJKST
      END DO
      END DO
C
C
      RETURN
C
      END
C
C
C---------------------------------------------------------------
C     Here come the routines with user-programmed input data and
C     user-programmed interpretation of results.
C     For each case, create separate user-files,
C     and copy them prior to compilation to the file 'user.f'
C     (routine BCIN provides boundary conditions)
C---------------------------------------------------------------
#include "user.f"
C#include "turbulence.models.f"
#ifdef USE_TECPLOT
#include "tecpost.f"
#else
#include "vtkpost.f"
#endif
C---------------------------------------------------------------
C     Next lines incorporate the optional solvers. 
C     Only one file should contain the chosen solver in a 
C     subroutine of the form :
C
C     SUBROUTINE SOLVER(FI,IFI)
C
C     The other file should be left empty. It is convenient
C     to set up subdirectories with the desired combination
C     and use the -I<subdir> flag when compiling
C---------------------------------------------------------------
#ifdef USE_SIPSOL
C#include "cgstab3d.f"
#include "sipsol3d.f"
#endif
C---------------------------------------------------------------
C
C     Here the PETSc specific soubroutines get included into the
C     CAFFA Code
# include "petsc.user.f"
C
C
C     Here the Functions for MMS get included
#ifdef USE_ANALYTICAL
#include "mms.f"
#include "user.error.f"
#endif
C
C
#include "outin.f"
