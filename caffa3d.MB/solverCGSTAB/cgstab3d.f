C#######################################################################
      SUBROUTINE SOLVER(FI,IFI)
C#######################################################################
C    This routine incorporates the CGSTAB solver for seven-diagonal,
C    non-symmetric coefficient matrices (suitable for convection/
C    diffusion problems). See Sect. 5.3.7 for details. Array index
C    IJK calculated from indices I, J, and K according to Table 3.1.
C
C    Writen by Samir Muzaferija, Institut fuer Schiffbau, Hamburg, 1995.
C
C    This version was adapted for use with caffa3D. Please note that
C    this solver does not supports multiblock domains. Attempting
C    to solve such grids with this solver will produce unexpected 
C    results.
C
C=======================================================================
#INCLUDE "param3d.inc"
#INCLUDE "partype3d.inc"
#INCLUDE "parinw3d.inc"
#INCLUDE "indexc3d.inc"
#INCLUDE "logic3d.inc"
#INCLUDE "rcont3d.inc"
#INCLUDE "coef3d.inc"
#INCLUDE "bound3d.inc"
#INCLUDE "bcinw3d.inc"
      DIMENSION FI(NXYZA),RES(NXYZA),RESO(NXYZA),PK(NXYZA),UK(NXYZA),
     *          ZK(NXYZA),VK(NXYZA),D(NXYZA),ALF(NBK),BETO(NBK),
     *          GAM(NBK),BET(NBK)
      DATA D /NXYZA*0./
C
C.....CALCULATE INITIAL RESIDUAL VECTOR
C
      RES0=0.
C
C.....HERE STARTS A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�1 STARTS <<---
C
      DO M=1,NBLKS
      NKM=NKBK(M)-1
      NI=NIBK(M)
      NJ=NJBK(M)
      KST=KBK(M)
      IST=IBK(M)
      NIM=NI-1
      NJM=NJ-1
      NIJ=NI*NJ
C
      DO K=2,NKM
      DO I=2,NIM 
      DO J=2,NJM 
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        RES(IJK)=SU(IJK)-AP(IJK)*FI(IJK)
     *    -AE(IJK)*FI(IJK+NJ)  -AW(IJK)*FI(IJK-NJ) 
     *    -AN(IJK)*FI(IJK+1)   -AS(IJK)*FI(IJK-1)  
     *    -AT(IJK)*FI(IJK+NIJ) -AB(IJK)*FI(IJK-NIJ)
        RES0=RES0+ABS(RES(IJK))
      END DO
      END DO
      END DO
C
C.....HERE FINISHES A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�1 FINISH <<---
C
      END DO
C
C.....CONTRIBUTIONS FROM OC CUTS TO RESIDUAL VECTOR
C
      DO I=1,NOCBKAL
        RES0=RES0-ABS(RES(IJLPBK(I)))-ABS(RES(IJRPBK(I)))
        RES(IJLPBK(I))=RES(IJLPBK(I))-AR(I)*FI(IJRPBK(I))
        RES(IJRPBK(I))=RES(IJRPBK(I))-AL(I)*FI(IJLPBK(I))
        RES0=RES0+ABS(RES(IJLPBK(I)))+ABS(RES(IJRPBK(I)))
      END DO
C
      IF(LTEST) WRITE(6,*) 0,' SWEEP, RES0 = ',RES0
      RESOR(IFI)=RES0
C
C.....HERE STARTS A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�2 STARTS <<---
C
      DO M=1,NBLKS
      NKM=NKBK(M)-1
      NI=NIBK(M)
      NJ=NJBK(M)
      KST=KBK(M)
      IST=IBK(M)
      NIM=NI-1
      NJM=NJ-1
      NIJ=NI*NJ
C
C.....CALCULATE ELEMENTS OF PRECONDITIONING MATRIX DIAGONAL
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        D(IJK)=1./(AP(IJK) -AW(IJK)*D(IJK-NJ)*AE(IJK-NJ)
     *             -AS(IJK)*D(IJK-1)*AN(IJK-1) 
     *             -AB(IJK)*D(IJK-NIJ)*AT(IJK-NIJ)) 
      END DO
      END DO
      END DO
C
C.....INITIALIZE WORKING ARRAYS AND CONSTANTS
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        RESO(IJK)=RES(IJK)
        PK(IJK)=0.
        UK(IJK)=0.
        ZK(IJK)=0.
        VK(IJK)=0.
      END DO
      END DO
      END DO
      ALF(M)=1.
      BETO(M)=1.
      GAM(M)=1.
C
C.....HERE FINISHES A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�2 FINISH <<---
C
      END DO
C
C.....START INNER ITERATIONS
C
      DO L=1,NSW(IFI)
C
C.....HERE STARTS A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�3 STARTS <<---
C
      DO M=1,NBLKS
      NKM=NKBK(M)-1
      NI=NIBK(M)
      NJ=NJBK(M)
      KST=KBK(M)
      IST=IBK(M)
      NIM=NI-1
      NJM=NJ-1
      NIJ=NI*NJ
C
C..... CALCULATE BETA AND OMEGA
C
      BET(M)=0.
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        BET(M)=BET(M)+RES(IJK)*RESO(IJK)
      END DO
      END DO
      END DO
      OM=BET(M)*GAM(M)/(ALF(M)*BETO(M)+1.E-30)
      BETO(M)=BET(M)
C
C..... CALCULATE PK
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        PK(IJK)=RES(IJK)+OM*(PK(IJK)-ALF(M)*UK(IJK))
      END DO
      END DO
      END DO
C
C.....SOLVE (M ZK = PK) - FORWARD SUBSTITUTION
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        ZK(IJK)=(PK(IJK)-AW(IJK)*ZK(IJK-NJ)
     *                  -AS(IJK)*ZK(IJK-1)
     *                  -AB(IJK)*ZK(IJK-NIJ))*D(IJK)
      END DO
      END DO
      END DO
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        ZK(IJK)=ZK(IJK)/(D(IJK)+1.E-30)
      END DO
      END DO
      END DO
C
C..... BACKWARD SUBSTITUTION
C
      DO K=NKM,2,-1
      DO I=NIM,2,-1
      DO J=NJM,2,-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        ZK(IJK)=(ZK(IJK)-AE(IJK)*ZK(IJK+NJ)
     *                  -AN(IJK)*ZK(IJK+1)
     *                  -AT(IJK)*ZK(IJK+NIJ))*D(IJK)
      END DO
      END DO
      END DO
C
C.....CALCULATE UK = A.PK
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        UK(IJK)=AP(IJK)*ZK(IJK)
     *      +AE(IJK)*ZK(IJK+NJ) +AW(IJK)*ZK(IJK-NJ)
     *      +AN(IJK)*ZK(IJK+1)  +AS(IJK)*ZK(IJK-1) 
     *      +AT(IJK)*ZK(IJK+NIJ)+AB(IJK)*ZK(IJK-NIJ) 
      END DO
      END DO
      END DO
C
C.....HERE FINISHES A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�3 FINISH <<---
C
      END DO
C
C.....CONTRIBUTIONS FROM OC CUTS TO RESIDUAL VECTOR
C
c     DO I=1,NOCBKAL
c       UK(IJLPBK(I))=UK(IJLPBK(I))+AR(I)*ZK(IJRPBK(I))
c       UK(IJRPBK(I))=UK(IJRPBK(I))+AL(I)*ZK(IJLPBK(I))
c     END DO
C
C.....HERE STARTS A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�4 STARTS <<---
C
      DO M=1,NBLKS
      NKM=NKBK(M)-1
      NI=NIBK(M)
      NJ=NJBK(M)
      KST=KBK(M)
      IST=IBK(M)
      NIM=NI-1
      NJM=NJ-1
      NIJ=NI*NJ
C
C..... CALCULATE SCALAR PRODUCT UK.RESO AND GAMMA
C
      UKRESO=0.
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        UKRESO=UKRESO+UK(IJK)*RESO(IJK)
      END DO
      END DO
      END DO
      GAM(M)=BET(M)/(UKRESO+1.E-30)
C
C.....UPDATE (FI) AND CALCULATE W (OVERWRITE RES - IT IS RES-UPDATE)
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK)+GAM(M)*ZK(IJK)
        RES(IJK)=RES(IJK)-GAM(M)*UK(IJK)
      END DO
      END DO
      END DO
C
C.....SOLVE (M Y = W); Y OVERWRITES ZK; FORWARD SUBSTITUTION
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        ZK(IJK)=(RES(IJK)-AW(IJK)*ZK(IJK-NJ)-
     *           AS(IJK)*ZK(IJK-1)-AB(IJK)*ZK(IJK-NIJ))*D(IJK)
      END DO
      END DO
      END DO
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        ZK(IJK)=ZK(IJK)/(D(IJK)+1.E-30)
      END DO
      END DO
      END DO
C
C.....BACKWARD SUBSTITUTION
C
      DO K=NKM,2,-1
      DO I=NIM,2,-1
      DO J=NJM,2,-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        ZK(IJK)=(ZK(IJK)-AE(IJK)*ZK(IJK+NJ)
     *                  -AN(IJK)*ZK(IJK+1)
     *                  -AT(IJK)*ZK(IJK+NIJ))*D(IJK)
      END DO
      END DO
      END DO
C
C.....CALCULATE V = A.Y (VK = A.ZK)
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
         VK(IJK)=AP(IJK)*ZK(IJK)
     *          +AE(IJK)*ZK(IJK+NJ) +AW(IJK)*ZK(IJK-NJ)
     *          +AN(IJK)*ZK(IJK+1)  +AS(IJK)*ZK(IJK-1)
     *          +AT(IJK)*ZK(IJK+NIJ)+AB(IJK)*ZK(IJK-NIJ) 
      END DO
      END DO
      END DO
C
C.....HERE FINISHES A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�4 FINISH <<---
C
      END DO
C
C.....CONTRIBUTIONS FROM OC CUTS TO RESIDUAL VECTOR
C
c     DO I=1,NOCBKAL
c       VK(IJLPBK(I))=VK(IJLPBK(I))+AR(I)*ZK(IJRPBK(I))
c       VK(IJRPBK(I))=VK(IJRPBK(I))+AL(I)*ZK(IJLPBK(I))
c     END DO
C
C.....INITIALIZE RESIDUAL SUM
C
      RESL=0.
C
C.....HERE STARTS A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�5 STARTS <<---
C
      DO M=1,NBLKS
      NKM=NKBK(M)-1
      NI=NIBK(M)
      NJ=NJBK(M)
      KST=KBK(M)
      IST=IBK(M)
      NIM=NI-1
      NJM=NJ-1
      NIJ=NI*NJ
C
C..... CALCULATE ALPHA (ALF)
C
      VRES=0.
      VV=0.
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        VRES=VRES+VK(IJK)*RES(IJK)
        VV=VV+VK(IJK)*VK(IJK)
      END DO
      END DO
      END DO
C
      ALF(M)=VRES/(VV+1.E-30)
C
C.....UPDATE VARIABLE (FI) AND RESIDUAL (RES) VECTORS
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK)+ALF(M)*ZK(IJK)
        RES(IJK)=RES(IJK)-ALF(M)*VK(IJK)
        RESL=RESL+ABS(RES(IJK))
      END DO
      END DO
      END DO
C
C.....HERE FINISHES A LOOP THROUGH BLOCKS  <<--- BLOCK LOOP N�4 FINISH <<---
C
      END DO
C
C.....CHECK CONVERGENCE
C
      RSM=RESL/(RES0+1.E-30)
      IF(LTEST) WRITE(6,600) IFI,L,RESL
  600 FORMAT(20X,'FI=',I2,'  SWEEP=',I3,'  RES=',1PE10.3)
      IF(RSM.LT.SOR(IFI)) RETURN
C
C.....END OF ITERATION LOOP
C
      END DO
C
      RETURN
      END
C
