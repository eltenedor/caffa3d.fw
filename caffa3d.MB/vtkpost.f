C###################################################################
      SUBROUTINE POST(ICOUNT,RNK)
C###################################################################
C    This subroutine generates .vtk files containing the output
C    for each block
C
C==============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
#include "param3d.inc"
#include "coef3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
C
      CHARACTER(LEN=28) VTKFILE
      INTEGER M,I,J,K,NKMT,NIMT,NJMT,KSTT,ISTT,IJK
      INTEGER ICOUNT,RNK
      REAL*8  LPI,TCA,HPA,HPA2,TA
      REAL*8  VM,VMG,PPOG
      PARAMETER (LPI=3.141592653589793238462643383279d0)
      PetscErrorCode IERR
#include "petsc.user.inc"
C=========================================================
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
      CALL VecGetArray(DPXVEC,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYVEC,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZVEC,DPZARR,DPZZI,IERR)
C
#ifdef USE_ANALYTICAL
C
      CALL VecGetArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecGetArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCVEC,ZCARR,ZCCI,IERR)
      CALL VecGetArray(VOLVEC,VOLARR,VOLLI,IERR)
C
#ifdef USE_INTERPOLATION
C.....CALCULATE REFERENCE LOCATION AND REFERENCE PRESSURE
      IF (RANK.EQ.RMON) THEN
        CALL SETIND(MMON)
        XMON=0.D0
        YMON=0.D0
        ZMON=0.D0
C
        IJK=NI*NI*KMON+NI*IMON+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*IMON+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*(IMON+1)+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*KMON+NI*(IMON+1)+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*IMON+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*IMON+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
        IJK=NI*NI*(KMON+1)+NI*(IMON+1)+JMON+1
        XMON=XMON+XC(IJK)
        YMON=YMON+YC(IJK)
        ZMON=ZMON+ZC(IJK)
C
C......LINEAR INTERPOLATION (EQUIDISTANT GRID ASSUMED)
C
        XMON=0.125D0*XMON
        YMON=0.125D0*YMON
        ZMON=0.125D0*ZMON
        HPA2=PMMS(XMON,YMON,ZMON)
      END IF
#elif defined( USE_MEANPRESSURE )
      HPA2=0.0D0
      VM=0.0D0
      VMG=0.0D0
      PPOG=0.0D0
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        HPA2=HPA2+PMMS(XC(IJK),YC(IJK),ZC(IJK))*VOL(IJK)
        VM=VM+VOL(IJK)
      END DO
      END DO
      END DO
C
      END DO
C
      CALL MPI_REDUCE(
     *    HPA2,PPOG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
      CALL MPI_REDUCE(
     *    VM,VMG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
      IF (RANK.EQ.RMON) HPA2=PPOG/VMG
#else
C
C......USE MONITORING LOCATION FROM control.cin
C
      IF (RANK.EQ.RMON) THEN
        CALL SETIND(MMON)
        XMON=XC(IJKMON)
        YMON=YC(IJKMON)
        ZMON=ZC(IJKMON)
        HPA2=PMMS(XMON,YMON,ZMON)
      END IF
#endif
      CALL MPI_BCAST(HPA2,1,MPI_REAL8,RMON,PETSC_COMM_WORLD,IERR)
C
      CALL VecRestoreArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCVEC,ZCARR,ZCCI,IERR)
      CALL VecRestoreArray(VOLVEC,VOLARR,VOLLI,IERR)
C
#endif
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      DO M=1,NBLKS
C
        NKMT=NKBK(M)-1
        NIMT=NIBK(M)-1
        NJMT=NJBK(M)-1
        KSTT=KBK(M)
        ISTT=IBK(M)
C
        WRITE(VTKFILE,'(A8,I4.4,A1,I4.4,A1,I6.6,A4)') 
     *               'res_out_',RNK,'_',M,'_',ICOUNT,'.vtk'
#ifdef USE_INFO
        WRITE(*,*), ' *** GENERATING .VTK *** '
#endif
C
        OPEN (UNIT=22,FILE=VTKFILE,POSITION='REWIND')
        WRITE(22,'(A)') '# vtk DataFile Version 3.0'
        WRITE(22,'(A)') 'grid'
        WRITE(22,'(A)') 'ASCII'
        WRITE(22,'(A)') 'DATASET STRUCTURED_GRID'
        WRITE(22,'(A,I6,I6,I6)') 'DIMENSIONS', NIMT,NJMT,NKMT
        WRITE(22,'(A6,I9,A6)') 'Points', NIMT*NJMT*NKMT, ' float'
C
        DO K=1,NKMT
        DO J=1,NJMT
        DO I=1,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            WRITE(22,'(E20.10,1X,E20.10,1X,E20.10)'), 
     *                     X(IJK),Y(IJK),Z(IJK)
        END DO
        END DO
        END DO
C
        WRITE(22,'(A10,1X,I9)') 'CELL_DATA ',(NIMT-1)*(NJMT-1)*(NKMT-1)
        WRITE(22,'(A17)') 'VECTORS UVW float'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            WRITE(22,'(3E20.10)') U(IJK),V(IJK),W(IJK)
        END DO
        END DO
        END DO
C
        WRITE(22,'(A19)') 'VECTORS GRADP float'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            WRITE(22,'(3E20.10)') DPX(IJK),DPY(IJK),DPZ(IJK)
        END DO
        END DO
        END DO
C
        WRITE(22,'(A15)') 'SCALARS P float'
        WRITE(22,'(A20)') 'LOOKUP_TABLE DEFAULT'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            WRITE(22,'(E20.10)') P(IJK)
        END DO
        END DO
        END DO
C
        IF (LCAL(IEN)) THEN
        WRITE(22,'(A15)') 'SCALARS T float'
        WRITE(22,'(A20)') 'LOOKUP_TABLE default'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            WRITE(22,'(E20.10)') T(IJK)
        END DO
        END DO
        END DO
        ENDIF
C
#ifdef USE_ANALYTICAL
        WRITE(22,'(A17)') 'SCALARS HPA float'
        WRITE(22,'(A20)') 'LOOKUP_TABLE default'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            HPA=PMMS(XC(IJK),YC(IJK),ZC(IJK))
            WRITE(22,'(E20.10)') HPA-HPA2
        END DO
        END DO
        END DO
C
        WRITE(22,'(A18)') 'SCALARS PERR float'
        WRITE(22,'(A20)') 'LOOKUP_TABLE default'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            HPA=PMMS(XC(IJK),YC(IJK),ZC(IJK))
            WRITE(22,'(E20.10)') HPA-HPA2-P(IJK)
        END DO
        END DO
        END DO
C
        IF (LCAL(IEN)) THEN
        WRITE(22,'(A17)') 'SCALARS TA float'
        WRITE(22,'(A20)') 'LOOKUP_TABLE default'
C
        DO K=2,NKMT
        DO J=2,NJMT
        DO I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            TA =TMMS(XC(IJK),YC(IJK),ZC(IJK))
            WRITE(22,'(E20.10)') TA
        END DO
        END DO
        END DO
        ENDIF
#endif
        CLOSE(UNIT=22)
C
      END DO
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreArray(DPXVEC,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYVEC,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZVEC,DPZARR,DPZZI,IERR)
C
      RETURN
      END 
C
#include "petsc.user.inc"
